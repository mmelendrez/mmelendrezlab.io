---
title: Having an out of money experience
author: Mel
type: post
date: 2011-11-09T02:30:13+00:00
url: /2011/11/09/having-an-out-of-money-experience/
categories:
  - Devotion
tags:
  - Bible
  - blessing
  - famiy
  - God
  - money

---
Devotional Blog: &#8220;Family and Finance&#8221;, 11/4/2011; 1 Timothy 5:3-4, 8, 16

I took the title above from a quote by Author Unknown: &#8220;I am having an out of money experience&#8221;. It amused me.

So surprise, surprise this is yet another devotional entry in the book that I see differently than the author perhaps. Family, finances and lending money are huge topics. We all know that one of the biggest problems that can arise in a marriage can be over money or lack of it rather. I&#8217;ve seen money tear people and apart sometimes because of greed sometimes because of the emotions attached to the money that may have nothing to do with the money itself.

In the book, Pam talks about the duality of lending money to family that is discussed in the Bible. In 2 Corinthians 12:14 it states &#8220;&#8230;After all, children should not have to save up for their parents, but parents for their children.&#8221; and then in 1 Timothy 5:8 it states: &#8220;But those who won&#8217;t care for their own relatives, especially those living in the same household have denied what we believe. Such people are worse than unbelievers.&#8221; She then goes on to ask what should our responsibility be in terms of using our money to care for relatives/family? Her answer: &#8220;When in doubt, do like God recommends: &#8216;speak up for the poor and helpless, and see that they get justice (Proverbs 31:9). He doesn&#8217;t allow people to continue in unhealthy patterns, but if they have tried their best and fall short, his long arm of love reaches out.&#8221;

I agree with her statements and the verses she used in some respects. <!--more-->I agree its not healthy to &#8216;enable&#8217; someone with their unhealthy habits by giving them money. But I also know God desires us to honor our parents (Ephesians 6:2); there&#8217;s a whole section on &#8216;faith and deeds&#8217; in James (2:14-17) talking about taking care of our brothers and sisters; take care of our church (1 Corinthians 16:2, Malachi 3:10 ) and for all the verses you can cite about saving and not lending money there are many about giving and being a blessing to those in need (Deuteronomy 15:10, Deuteronomy 16:17, 1 Chronicles 29:9, Proverbs 3:9-10, Proverbs 11:24-25, Proverbs 21:26, Proverbs 22:9, Proverbs 28:27, Mark 12:41-44, Luke 3:11, Acts 20:35, Romans 12:8, 2 Corinthians 9:6-8, 2 Corinthians 9:10).

In my life I have gone without, I understand the frustration and helplessness that comes from not having what you need to take care of those you love&#8211;I&#8217;ve seen this happen to those closest to me. Everything I have, I have earned and been blessed by the good graces of God. I could complain about working my entire life from when I was in high school on, but instead I choose to say I had jobs and they paid for my ability to go to prom or pay my way through school. When I worked for my folks business early on in high school they paid me before they paid themselves at the beginning of the month on principle, they honored my commitment to work. If I needed leads for a job, my father would contact EVERYONE he&#8217;d ever known to assist me and give me the opportunity to pick what I wanted. He&#8217;d drive me to interviews no matter when or where they were because we had only one car to share. When I got jobs our family became a &#8216;team&#8217; driving each other around making sure we made our jobs, our meetings, got to the store to buy food. This is what I believe should happen in a family and finances situation. Your family is a team working for the betterment of ALL in the mix.

It should never be about the selfishness of &#8216;what you can do for me&#8217;&#8230;It should never be &#8216;well I&#8217;m the poor child help me, give me an allowance, succor my inability to get work for whatever reason, I want money and I&#8217;m younger and just starting out in life so just give it to me&#8217;. I was not raised to be a &#8216;poor helpless child&#8217;. Nor should it be &#8216;well I birthed you and supported you for 18 years so now its about you giving me money, take care of me, help me&#8217;. The word &#8220;me&#8221; does not appear in the word family. God has blessed us with our relationships, our family and we simply have to honor that. What makes you happier? Seeing your family prosper and do amazing things? Or seeing them go on food stamps and debate whether to pay the electric, rent or buy food.

Money is important, I acknowledge and respect the power it can have over people and families. I choose not to be ruled by my money. Tyghe and I use it to honor God and support his work and make sure we are ok then make sure our families are ok, then make sure our friends are ok.

> Proverbs 3:27 (NIV)
  
> Do not withhold good from those to whom it is due, when it is in your power to do it.
> 
> Luke 6:30 (NIV)
  
> Give to everyone who asks of you, and whoever takes away what is yours, do not demand it back.
> 
> Luke 6:38 (NIV)
  
> Give, and it will be given to you. They will pour into your lap a good measure, pressed down, shaken together, and running over. For by your standard of measure it will be measured to you in return.

I believe God blesses and shows favor to Tyghe and I that we might bless and help anyone who needs it, family or friends, the church etc&#8230; And I believe this mentality shouldn&#8217;t just be money specific.

I left at 17 to go to college. I cannot count how many times I have been the lost puppy on someones doorstep for the holidays and they&#8217;ve taken me in, fed me or provided me a place to stay while in transition points in my life, or given me rides when I didn&#8217;t have a car. THIS is just as important as any money-lending. And I intend to reciprocate the generosity shown to me growing up by doing whatever I can to assist others. Success in my life is directly attributed to the generosity and support of others and my faith in God and probably a lot of drive or stubbornness on my own part (haha), this I believe strongly. Without any of that, I would not be where I am today&#8230;having the ability now to bless and help others and contribute to what will soon be a bigger &#8216;team&#8217; of family when needed.

When I think &#8220;Family and Finance&#8221; I don&#8217;t think of me or them in terms of who gets to &#8216;prosper&#8217;. Its about &#8216;us&#8217;&#8230;all of us, obeying what God has told us for the blessing of the entire family and the ability to help others in need whether it be through money, food, a roof, a car ride, helping them job hunt&#8230;this I believe is God&#8217;s heart for us.

> <span style="font-family: Arial,Helvetica,sans-serif;">Money never made a man happy yet, nor will it. There is nothing in its nature to produce happiness. The more a man has, the more he wants. Instead of its filling a vacuum, it makes one.<br /> <span style="color: #666600;">&#8211; Ben Franklin </span></span>

Ben Franklin is right. Money cannot bring happiness, but honoring God, family and friends can.
