---
title: 'There is nothing perfect, there’s only life: repost from Jun 22, 2006'
author: Mel
type: post
date: 2011-08-18T09:16:58+00:00
url: /2011/08/18/there-is-nothing-perfect-theres-only-life-repost-from-jun-22-2006/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Life or something like it
tags:
  - holocaust
  - quotes
  - washington dc

---
<span title="Read There's nothing perfect...there's only life.">Well&#8230;I am now back from my trip to D.C. I got my fill of museums, dancing, city life, good (expensive) food, and monuments. All in all, not bad for a break. I will only describe one experience here.</span><article>

<span style="font-family: Times New Roman,Times,serif;">I went to the Holocaust Museum. It was unbelievable well done. A lot of reading but absolutely amazing. I think that everyone should go through it despite the fears of going into a museum about such a depressing subject. It started with Hilter and Nazism&#8217;s rise to power and the political and economic situation of Germany at the time, goes through the inital arrests, then the Jews situation, into the Final Solution, and ends with a Final Chapter. You go in and they give you an ID card of someone who actually went through the Holocaust, it goes through their lives, and how they survived or did not survive the slaughter&#8230;</span>**<span style="font-family: Times New Roman,Times,serif;"><!--more-->Quotes and tidbits&#8230;</span>**</p> 

<span style="font-family: Times New Roman,Times,serif;"><em>First they came for the socialists, and I did not speak out, because I was not a socialist. Then they came for the trade unionists, and I did not speak out, because I was not a trade unionist. Then they came for the Jews, and I did not speak out, because I was not a Jew. Then they came for me- and there was no one left to speak for me.</em> <em><strong>&#8211;Martin Niemoller (anti-nazi german pastor)</strong></em></span>

<span style="font-family: Times New Roman,Times,serif;">There was a large room we walked into with nothing but shoes, piles everywhere collected from Auschwitz:</span>

_<span style="font-family: Times New Roman,Times,serif;">We are the shoes, we are the last witnesses, we are shoes from grandchildren and grandfathers, from prague, paris, and amsterdam, and because we are only made of cloth and leather and not of blood and flesh, each one of use avoided the hellfire. <strong>&#8211;Moses Schulstein, Yiddish Poet</strong></span>_

<span style="font-family: Times New Roman,Times,serif;">Quote:  <em>I know of course; it&#8217;s simply luck that we survived so many friends. But last night in a dream I heard those friends say to me; survival of the fittest&#8230;And I hated myself</em> <em><strong>&#8211;Bertolt Brecht</strong></em> </span>

<span style="font-family: Times New Roman,Times,serif;">Bertolt Brecht was one of the few Jewish &#8216;VIP&#8217;s&#8217; which included renowned artists, actors, dancers, and singers that were allowed U.S. VISA&#8217;s to escape the Holocaust. Other jews weren&#8217;t so lucky and eventually no country could take them.</span>

<span style="font-family: Times New Roman,Times,serif;">I chose to blog about the Holocaust Museum because it was very moving and because many of us still believe in the inherent goodness of mankind, including myself ,&#8217;people are really good at heart&#8217; (quoting Anne Frank of course). It is profoundly sad when &#8216;mankind&#8217; lets us down but it gives us a lesson on refusing to be complacent when such things happen.</span>

<span style="font-family: Times New Roman,Times,serif;">I will end with an interview I saw in the museum: </span>

_<span style="font-family: Times New Roman,Times,serif;">&#8220;&#8230;So I asked Yosef where Chaim was and he says he&#8217;s over by the quarry praying&#8230;And I go to Chaim and I ask him what he is praying. He says he is giving praise to God&#8230;and I look at him&#8230;praise?!!! In this place?!!! How can you praise God in this place, this place of starvation, beatings, rats, disease, fear, and death. Why does God deserve praise in this hell?&#8211;And he said, &#8216;I am giving God praise for he didn&#8217;t make me like the murderers around me.'&#8221;</span>_</article>
