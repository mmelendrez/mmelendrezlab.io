---
title: a typical lunchtime thought process…
author: Mel
type: post
date: 2012-03-06T05:11:15+00:00
url: /2012/03/06/a-typical-lunchtime-thought-process/
categories:
  - I will do Science to it!
  - Life or something like it
  - Poetry
tags:
  - hair
  - scientists

---
Mmmm&#8230;hungry time for lunch&#8230;
  
lunch, pork and vegis with sauce and rice
  
hair in food&#8230;
  
hmmmm&#8230;my hair or their hair
  
_\*pulls hair from food\*_
  
their hair
  
_\*flicks hair onto ground, keeps eating\*_&#8211;meh

pause

i wonder how many microbes are on a typical piece of hair
  
i wonder how many microbes are on my hair
  
i wash my hair
  
does she wash her hair
  
_\*looks at lady serving food\*_
  
maybe

I should culture/test my hair
  
that&#8217;d be interesting
  
no, not applicable&#8211;i need to test a food servers hair
  
_\*looks for dropped hair\*_
  
its on floor
  
floor contamination, damnit, can&#8217;t test it

someone in the US would wreak havoc if hair was in their food
  
is it that dangerous?
  
i should culture hair and see
  
_\*eats more food\*_

coffee sounds good
  
i should get around to reading R[ob Dunn, wildlife of our bodies][1]
  
he&#8217;d know if hair was dangerous
  
_\*finishes food, pays\*_
  
coffee

i read a lot of science blogs, scientists are getting &#8216;hotter&#8217; looking
  
we used to have to hide under rocks
  
my German friend was surprised after learning i was a microbiologist
  
i asked why
  
he said because i&#8217;m attractive

i should make a calendar of &#8216;hot&#8217; scientists
  
that&#8217;d be cool
  
with links to their publications and blogs
  
hot.

i should find someones hair and culture it&#8230;

 [1]: http://amzn.com/006180648X
