---
title: Stream-of-Christmasness
author: Mel
type: post
date: 2011-12-23T02:48:45+00:00
url: /2011/12/23/stream-of-christmasness/
featured_image: /wp-content/uploads/2011/12/Dachshund_head_in_Xmas_hat-400x288.jpg
categories:
  - Life or something like it
tags:
  - Christmas
  - family
  - friendship
  - gifts
  - history
  - love
  - memories
  - parties

---
I started wanting to write this blog based on one of the devotional entries in the book about what would happen if you took the &#8216;Christ&#8217; out of Christmas. My mind wandered into wikipedia reading about the history of Christmas itself. My mental wanderings continued into various conversations with friends and acquaintances talking about the mesh of pagan and religious traditions mixed into Christmas nowadays. Then of course that leads to the blatant commercialism that Christmas has become. I&#8217;ve only to travel 2 minutes by skytrain to see the influence of Christmas in Bangkok, a Buddhist country. Though they don&#8217;t officially celebrate the holiday itself by days off work, they encourage gift giving and the market places are bedecked in lights, fake trees, cardboard snowmen and other such holiday decor.

The rest of my mental wanderings are hazy at best and clarified eventually into a deluge of memories&#8211;as though I was being visited by the ghost of Christmas past&#8230;<!--more-->

  * Video proof during one Christmas as a 3 year old that I would not be ignored or tuned out as white noise. I must&#8217;ve said &#8216;mom&#8217; about 100 times before my mother&#8217;s sister was like &#8216;for pete&#8217;s sake Annette answer her!&#8217;
  * Running up the stairs at my aunts house on Christmas morning&#8230;she lived near Saddleback MT in California I believe. My cousins in footie pajamas.
  * My favorite activity as a child was to sit and look at the decorated, lit tree.
  * Inviting friends over in elementary school to bake cookies and have a sleep over.
  * Watching and helping my Mom bake copious amounts of sugar cookies. They literally covered the house!
  * My grandmother was and is always amazing at making sure we had gifts as children for Christmas&#8211;she still is amazing at this to this day. One year the gifts were late so we didn&#8217;t really have anything under the tree that year. During the night I hunted around the house and found random objects that I knew belonged to my sisters or Mom and Dad. I wrapped them and put them under the tree which was our decorated palm plant&#8211;Otis. Yes his name was Otis, I later christened a plant in college after him. He sat on our red and white cooler. Otis had lights and some ornaments and tinsel and now he had gifts under him and he was able to look out the sliding door to an amazing view of the Maui valley. I sat and watched him for a while then went to bed. The next morning there was a lot of confusion followed by laughter as my Dad opened his pair of slippers, my sister opened a box of her shells, my other sister opened some stuffed animals and my Mom opened a plate (yes, I actually just wrapped a plate for her) and something else I&#8217;d found in her purse. It wasn&#8217;t so much the &#8216;need&#8217; for gifts growing up, it was the need for laughter and family bonding. We couldn&#8217;t bless each other with new items that year but we didn&#8217;t need them&#8230;apparently unwrapping some old slippers can send my Dad into fits of laughter just as easy.
  * Watching old musicals, The Bishops Wife and Its a Wonderful Life in black and white with my Mom while eating popcorn. We&#8217;ve done this just about every year since when we lived in California, then in Washington, then in Hawaii and now whenever I go we try to watch at least one.

Fast Forward&#8230;

  * A picture of my parents and I with my sister Julia and brother Sam on Christmas Eve at church, in front of the churches Christmas tree.
  * Bagels and smoked salmon New Years mornings since I can remember. My sister Julia and I made a point of bringing or sending smoked salmon to Hawaii for the holidays since we both went to school in Washington State.
  * Christmas during college in Vegas with my grandfather and uncle where I got a velour sweat suit, a photo album and a Swiss army pocket knife. Oddest collection of gifts ever. That was also the same year I went to see my aunt sing in a lounge and they kicked me out because I was 20 yrs old, even though I was only there to see her sing and was drinking a coke.
  * Hosting Christmas parties in Bozeman with snow on the ground, hot chocolate with peppermint sticks, white elephant gift giving, copious amounts of food and the warmth of my basement apartment where I wrapped my door to the storage room like a gift and hung Christmas lights on the walls (those Christmas lights and ornaments stayed up til March at least).
  * My first Christmas with Tyghe and his family in Glendive, MT. It was the first time in many years the entire brood was there&#8230;brothers, their wives, their dogs, Tyghe, myself, Holly and Larry (Tyghe&#8217;s and his brothers Dad). Larry was channelling his inner Santa Claus sporting a white beard. Him and Holly being just so happy to have everyone under one roof. Holly cooking enough food to feed a small country, Pretzel (Tyghe&#8217;s dauschsy) being an old crone and snapping at another dog&#8217;s leg as she passed by&#8230;no reason that we could find why, she just felt like it (ahhh, crazy awesome dog). By far one of the coziest Christmases I&#8217;d ever experienced before.
  * The first time I brought a boy home to meet my folks in Hawaii was at Christmas. Tyghe and I wore reindeer headbands, walked along beaches and parks, my dad wanting Tyghe to give a &#8216;thumbs up&#8217; EVERYWHERE we went so he could take a picture. My mom asking Tyghe not to let cookie crumbs get everywhere&#8230;tyghe saying &#8216;no problem&#8217; as he shoved a whole sugar cookie angel into his mouth&#8230;no crumbs. Taking Sam to the park to toss the frisbee or to the beach to dig holes in the sand. Driving around to Kaneohe then up and around the beach road, stopping along the way so my Dad could take more pictures. Going on a dinner cruise that I&#8217;m sure Tyghe would like to forget because it ended with him dressed as a hula girl dancing with the other girls on stage with a bunch of other tourists. I found it funny. Yes I do have photo proof and no I won&#8217;t post it. When the sun set though it was beautiful out and we got to see whales. Just one of many spectacular moments I&#8217;ve gotten to have with him.

End stream of consciousness&#8230;

Few of my memories focuses on a single fancy gift but rather the interactions and feelings of those I got to spend Christmas with. You&#8217;d think with all the emphasis on gift giving in this holiday and the near sickening commercialism&#8211;gifts would play a bigger memory role, but really, they don&#8217;t.

What are your Christmas memories&#8230;I bet very few if any involve some awesome trinket you got and if they do then you are probably about 9 years old. The only true bonafide &#8216;trinket&#8217; memory I have is when I was perhaps 7 or 8 years old, in Federal Way, WA and my sister and I got a Nintendo game system with Mario Bros. I remember that specifically because there is a picture of both of us holding up the box victorious in our pursuit of getting this gift for Christmas and my parents generosity in actually picking it up for us. Its amusing seeing a 7 year old in a flowered nightgown, her sister in a matching yet different colored nightgown both holding up a gaming system as though we&#8217;d won a World Heavy Weight Boxing title&#8230;ah to be a kid again.

This year?

This year there is no travel in our future. We have a small fake Christmas club&#8230;I mean tree that sits on our living room table decorated with ornaments I&#8217;ve picked up in our travels and cards which we haven&#8217;t opened in its branches. I picked up a dachshund ornament while in Philly&#8211;our latest addition to our ornament collecting travels. Our families and friends have been faithful and wonderful to send us gifts which sit around and under the table near the tree&#8230;or in the fridge ala candy and cookies from Holly! The weather is predicted to be warm 84oF on Christmas Eve and 81oF on Christmas Day. We&#8217;ll attend a Christmas Eve party, sleep in on Christmas Day&#8230;eat bagels and smoked salmon in accordance with my family&#8217;s tradition, perhaps meet up with my boss for some Christmas afternoon shenanigans but generally just plan on enjoying each others presence in our lives.

Best gift I could&#8217;ve ever had came with me to Thailand&#8230;he&#8217;s about 6 feet 5 inches tall, dark and handsome, makes a mean homemade three cheese mac and cheese casserole, loves spicy Thai food even when it causes him to sweat lava, lays out in frisbee when he really shouldn&#8217;t (ahem!), rubs my head even when I don&#8217;t ask, tells me everyday he loves me. So perhaps I&#8217;ll stick a bow on his head for Christmas this year&#8230;although I&#8217;ll have to catch him when he&#8217;s sleeping because if he&#8217;s awake and standing up I won&#8217;t be able to reach his head and he might resist my attempts to bedeck him in bows. Who knows our Christmas morning may devolve into a wrestling match involving Christmas bows which eventually I&#8217;ll win because he&#8217;ll let me, so we can go eat some bagels and smoked salmon.

Merry Christmas All.

&nbsp;
