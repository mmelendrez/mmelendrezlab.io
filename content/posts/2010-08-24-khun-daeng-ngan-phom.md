---
title: Khun daeng ngan phom?
author: Mel
type: post
date: 2010-08-24T06:59:30+00:00
url: /2010/08/24/khun-daeng-ngan-phom/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"3995999c38";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Asia
tags:
  - Chonburi
  - gibbons
  - proposal
  - ring
  - Zip lining

---
So I&#8217;ve gotten a lot of requests to know how my Saturday, August 21st went&#8230;

We&#8217;d decided to go zip lining with my Thai friend Fon and another friend from frisbee AnnaRae. It was actually a pretty perfect day to head out, cloudy but not raining which was amazing considering it&#8217;s been raining pretty constantly here. It was fairly cool even&#8230;well as cool as Bangkok, Thailand can get (86 degrees and 88% humidity). We headed north-east to ChonBuri to a tour called Fly with the Gibbons and after a brief introduction and getting geared up we were off on a 24 platform romp through the jungle&#8230;

Our guide couldn&#8217;t say my name, Mel, so I became Miaw (Cat in Thai) and everytime I came to the platform prior to sailing off it, he felt the need to meow several times. Around platform 19 we asked if I was married, I said no, he asked it I had a boyfriend&#8230;I said you see the 6&#8217;5 man that you just sent off to the next platform? Him. He called Tyghe Yak (but don&#8217;t pronounce the &#8216;k&#8217; really and say it with emphasis on the &#8216;a&#8217; sound), it means giant apparently.  He then proceeded to &#8216;marry&#8217; AnnaRae on the next platform where you could go two at a time. AnnaRae said she&#8217;d fly over there with him and he surmised that now it would mean they were married.

<!--more-->The longest line was 300 m and I think the highest line was around 100 m? Our final repel, or rather they called it something else because we didn&#8217;t actually repel but were more &#8216;let down&#8217; by rope&#8211;was from 50 m. All in all it was pretty awesome, didn&#8217;t see any Gibbons but went to the zoo afterward and saw a ton of animals. Amazing how close they let you too the animals&#8230;we&#8217;re talking petting zoo close to a white rhino, a giraffe and a pygmy hippopatomus. A lot of the vendors sold food you could feed the animals with. And of course monkey&#8217;s galore. We came across what they call a &#8216;small bear&#8217;, I don&#8217;t really know what it really is, but it was quite cute&#8230;a trainer was holding it and let it walked all over us and pose for pictures. Tyghe and AnnaRae posted pictures of our excursion.

So in the sum of my life I have been walked on, sat on or peed on by the following animals: dogs, cats, small birds, squirrel monkeys, brown monkeys, macaws, elephants (I sat on it, it didn&#8217;t sit on me!), the little bear, an ecuadorian raccoon like animal, geckos, jackons (another type of lizard that changes color), the ecuadorian version of the &#8216;little bear&#8217;&#8211;it was golden, not black and cockroaches&#8230;awesome.

After the zoo we headed , changed and Tyghe suggested we head to the roof of our building. Our buidling has 35 floors, the top floor has a garden that overlooks the city, it&#8217;s a pretty fantastic view. We&#8217;d never been up there at night and you can even seen a bridge that changes colors. So we headed up there with snacks and drinks and to check ou the view&#8230;

We got the elevator and Tyghe said he&#8217;d forgotten his camera so he went back to the room while I attempted to hold the elevator which didn&#8217;t work. 5 min later he returned.

While we were chatting he asked me if I remembered anything about that day&#8230;that is was one year since he&#8217;d first told me he loved me. And with that he got down on one knee and proposed&#8230;at least that&#8217;s what I assumed given he pulled out a ring.

Why was I assuming? Because he did it in Thai 😀 and I had no idea what he&#8217;d said, haha!

&#8220;Khun daeng gnan gap phom?&#8221;

For all I knew I was committing to take out the garbage for the rest of our lives :).

And of course I said yes.

Photos of the ring are below. There isn&#8217;t one of it on my finger becasue my camera phone sucks, so these will have to do&#8230;

So&#8230;flying through the jungle, thai guide propositioning (and meow-ing), close encounters with wildlife and yet&#8230;

The highlight of my entire day occurred 35 stories up on a roof overlooking Bangkok when the man I love asked me to marry him.

All in all&#8230;a good day.

<3<figure id="attachment_19" style="width: 300px" class="wp-caption aligncenter">

[<img class="size-medium wp-image-19" title="Riddles Ring" src="http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0198-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0198-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0198-150x112.jpg 150w, http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0198.jpg 480w" sizes="(max-width: 300px) 85vw, 300px" />][1]<figcaption class="wp-caption-text">Riddles Jewelry in Montana... He sat on the ring for 4 months! Impressive</figcaption></figure> <figure id="attachment_20" style="width: 300px" class="wp-caption aligncenter">[<img class="size-medium wp-image-20" title="The Ring" src="http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0199-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0199-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0199-150x112.jpg 150w, http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0199.jpg 480w" sizes="(max-width: 300px) 85vw, 300px" />][2]<figcaption class="wp-caption-text">He hid the ring on top of our wardrobe taking advantage of my vertical challenge...</figcaption></figure> <figure id="attachment_21" style="width: 300px" class="wp-caption aligncenter">[<img class="size-medium wp-image-21" title="Side View" src="http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0200-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0200-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0200-150x112.jpg 150w, http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0200.jpg 480w" sizes="(max-width: 300px) 85vw, 300px" />][3]<figcaption class="wp-caption-text">He then got paranoid that I would sit on the couch and see the bag as I stare at the wardrobe...as I apparently do on occasion 🙂 so he tapped it back. Almost too far, that's why it took him 5 minutes to come to the elevator! He almost couldn't reach it himself!!! 😀</figcaption></figure>

 [1]: http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0198.jpg
 [2]: http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0199.jpg
 [3]: http://www.melaniemelendrez.com/wp-content/uploads/2010/08/IMGP0200.jpg
