---
title: Rights and responsibility
author: Mel
type: post
date: 2011-09-14T09:30:59+00:00
url: /2011/09/14/rights-and-resonsibility/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"01d9f0780c";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
wordbooker_extract:
  - |
    Devotional Blog:
    
    Topic: Rights and Responsibility, 9/14/11, Colossians 4:2-6
    
    So this was a discussion of role models and if by becoming  role model you give up your 'rights' to do certain things...ala movie or music stars having fits, getting int ...
categories:
  - Devotion
tags:
  - faith
  - God
  - rights

---
Devotional Blog:

Topic: Rights and Responsibility, 9/14/11, Colossians 4:2-6

So this was a discussion of role models and if by becoming  role model you give up your &#8216;rights&#8217; to do certain things&#8230;ala movie or music stars having fits, getting into drugs or whatnot. Granted getting into drugs isn&#8217;t a &#8216;right&#8217; for anyone let alone the famous but apparently it&#8217;s all the worse because those who are famous become role models whether they like it or not. It reminds me of the quote from Spiderman: &#8220;With great power comes great responsibility&#8221;; which surprisingly enough was not originally coined by Spiderman comics but rather [Thomas Francis Gilroy in 1892][1], it has also been attributed to Voltaire (Voltaire. Jean, Adrien. Beuchot, Quentin and Miger, Pierre, Auguste. &#8220;Œuvres de Voltaire, Volume 48&#8221;. Lefèvre, 1832). Whether history or marvel comic the statement rings true.

<!--more-->Often times growing up in the church I would find church-folk suddenly &#8216;disappear&#8217; from ministry&#8230;a youth pastor or leader, a worship leader and there is no explanation as to why. It was hammered into us as high schoolers that you shouldn&#8217;t let your life &#8216;stumble&#8217; others (Romans 14:13 and 1 Corinthians 8:13) and if it does you need to withdraw from positions of influence. Many times this included public ridicule and judgement on that person from the church and resulted in that person being ostracized. Sad that so many miss the first part of Romans 14:13 which, yes states, &#8216;don&#8217;t stumble others&#8217; in effect BUT also states &#8220;therefore let us not judge one another anymore&#8221;. I understand why those who falter step down from ministry but to be ridiculed and ostracized for making a good decision is simply sad.

I get confused by seemingly contradictory notions in the Bible. As mentioned, it says not to judge but then you come across verses like Galatians 6:1-6 which gives full support to &#8220;judging&#8221; your brothers persay. Rather is says that you should bring things to the attention of those in the church that are perhaps doing things unhealthy to themselves or their faith; however I&#8217;ve heard this quoted in sermon and used as a justification to high tail it into your own &#8216;high court&#8217; to &#8216;evaluate&#8217; your brother and brings all his faults to light. It really is quite incredible. I left that church soon thereafter. How many people actually respond well to a &#8216;Bible beating&#8217; would you say? Assuming you&#8217;ve chose to Bible beat someone that even cares about the Bible. I&#8217;ve watched people attempt to Bible lash a non-believer&#8230;with rarely favorable results. No wonder Christians many times get pegged as crazy, hell-fire &#8216;n damnation, Bible beating, holier than though &#8216;fools&#8217;&#8230;a little harsh of me? Absolutely not, I&#8217;ve heard that stigma from friends of mine when describing Christians. I&#8217;m sure it&#8217;s not &#8216;all our personal&#8217; fault for the stigma&#8230;non-believers judge and over-react just as much as believers do&#8211;we are all human after all. And I&#8217;ve heard the Christian &#8216;retorts&#8217; to this&#8230;&#8221;I&#8217;d rather be a dancing, jesus freak, &#8216;fool&#8217; than go to hell&#8230;like you will&#8221; Surprised? Don&#8217;t be, I&#8217;ve heard this too&#8230;Christians proud of their &#8216;alien-likeness&#8217; to non-believers. Many times citing we are to be in this world but not OF this world. Okay, I get that&#8230;but as Christians IN this world we have the responsibility to uphold Jesus&#8217; expectations of us&#8211;what might that be? Oh&#8230;I dunno&#8230;maybe LOVE ONE ANOTHER AND THY NEIGHBOR AS THYSELF for starters. I&#8217;m sure Jesus didn&#8217;t mean for us to love and &#8216;accept&#8217; other Christians only.

The devotional book talks mostly in respect to those in visual seats of &#8216;power&#8217; as role models&#8230;aka movie stars, sports heros, pastors&#8230; I started thinking about that and feeling like it really should apply to anyone, not just &#8216;visual&#8217; role models&#8230;just because 1/2 million people don&#8217;t watch you play sports or sing a song doesn&#8217;t mean you don&#8217;t impact someones life. And that thought is actually scary for me too&#8211;I can and I&#8217;m sure have been a &#8216;piss-poor&#8217; role model at times during my life. If I had to come up with a penny for every dumb decision I made that someone else was apart of or saw&#8230;I&#8217;d probably have no loan or credit card debt as well as a huge savings. If I instead gave that penny to the person, they&#8217;d be rich and I&#8217;d be saying &#8216;well shoot, I need to stop #$@!ing-up&#8217;.

This tags back to my previous blog about the impact you can have in someones life that may not be felt until you&#8217;ve passed away. Somehow, someone, somewhere in the world things you are the next best thing to sliced bread. Or better yet, I have a magnet on my fridge that quote by anonymous: &#8220;To the world you may be one person, but to one person you may be the world.&#8221; Does that mean I have to be constantly paranoid about what I do, what I say and &#8216;who I am&#8217;? I&#8217;m going to say, no. But it is our responsibility to try&#8211;believer and non-believer alike; whether for reasons of faith or if you are non-believer, simple humanity. No one is perfect and those around us know we do not walk on water&#8230;or they should know&#8230;and if they don&#8217;t&#8211;demonstrate that you indeed do not walk on water&#8230;

Warning: you may get wet.

 [1]: http://commons.wikimedia.org/wiki/File:TNYT11151892.pdf
