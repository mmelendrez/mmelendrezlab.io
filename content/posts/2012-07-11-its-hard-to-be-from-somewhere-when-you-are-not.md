---
title: It’s hard to be ‘from’ somewhere when you are not
author: Mel
type: post
date: 2012-07-11T13:51:45+00:00
url: /2012/07/11/its-hard-to-be-from-somewhere-when-you-are-not/
al2fb_facebook_error:
  - |
    |
        Get me: cURL communication error 28 Failed to connect to 2a03:2880:10:8f02:face:b00c:0:26: Network is unreachable:  Array
        (
            [url] => http://graph.facebook.com/me...
            [content_type] => 
            [http_code] => 0
            [header_size] => 0
            [request_size] => 268
            [filetime] => -1
            [ssl_verify_result] => 0
            [redirect_count] => 0
            [total_time] => 25.000732
            [namelookup_time] => 0.000575
            [connect_time] => 0.046391
            [pretransfer_time] => 0.153389
            [size_upload] => 0
            [size_download] => 0
            [speed_download] => 0
            [speed_upload] => 0
            [download_content_length] => -1
            [upload_content_length] => 0
            [starttransfer_time] => 0
            [redirect_time] => 0
        )
        
al2fb_facebook_error_time:
  - 2012-07-11T13:53:16+00:00
categories:
  - Life or something like it

---
Born in Los Angeles County, CA in a hospital that no longer exists, moved from California to Washington when I was 5, moved within WA a bit, moved to Hawaii when I was 11, moved around a bit within HI, by the time I reached high school I&#8217;d attended at least 10 schools that I remember distinctly. I get asked a lot if I&#8217;m a military brat&#8230;nope, my family just went where the work was.

Thankfully we settled in Maui long enough for me to attend all of high school in one place. Then what happened? I promptly started moving around again, summers working in HI, school years back in WA going to college, followed by a move for 2 years before settling in MT for grad school, followed by living in Thailand and now settled in Maryland.

Growing up I used to say I was equally from 3 states (CA, WA and HI)&#8211;after grad school I had to add MT because as far as length of time it was up there with where I was born (CA) and the other places I&#8217;d lived (WA and HI).

But length of time doesn&#8217;t dictate if you are &#8216;from&#8217; somewhere I&#8217;ve learned. Now I&#8217;ve always said my home (or where I am from) is where my family is and that&#8217;s true. I know I will always be supported and have a home where ever that may be with my folks. So currently, I am from Hawaii. Technically that&#8217;s not true though, I was born in Cali but then spent many of my formative years in Washington, albeit I hated grade school and jr. high, and then of course feeling like I actually &#8216;grew up&#8217; in Maui.

Montana also had a profound effect on me, I did live there longer than anywhere else unless you count my forays back to Hawaii in between school years and my two year hiatus there after college. But MT isn&#8217;t &#8216;home&#8217; to me. I have many great friends there and I loved Bozeman&#8230;it&#8217;s a great place, but it&#8217;s not &#8216;home&#8217;.

Is where you are &#8216;from&#8217;, where the highest density of friends and family are?

I used to envy kids that grew up their whole lives or most of their lives in one locale. They have friendships already rooted and as an outsider I can always come in and become apart of those friendships but never attain the life-long level that comes from being rooted I guess. And that&#8217;s ok, surprising at times when I discover I am not as &#8216;close&#8217; to someone as I&#8217;d previously thought, but understandible. No matter how close I get with someone or certain people, I don&#8217;t root&#8211;never have, it&#8217;s a habit for me not to root as the likelihood I&#8217;ll &#8216;move on&#8217; is usually pretty high&#8230;I do attach though (epiphytically for you geeks out there). So I&#8217;d be lying to say it doesn&#8217;t surprise when I am shed.

And wonderfully I have a few &#8216;unrooted&#8217; life long friends partly due to experiences and longevity that&#8217;s super glued us for life. If I asked them to travel 5,000 miles to see me for a few days&#8211;they&#8217;d seriously consider it and try and vice a versa and I so treasure those people in my life.

Should I say I am from nowhere? Or everywhere? Well I haven&#8217;t traveled so extensively as to say I am from everywhere, so I guess that leaves &#8216;nowhere&#8217;. Nah&#8230;that makes me feel like I came out of a test tube. I guess I&#8217;ll just stick with Hawaii as that&#8217;s where I long to be, I miss Hawaii, I miss my family, I miss the smells, the climate&#8230;perhaps that&#8217;s why I attached to Thailand. Although, Thailand is way more hot and humid than I would&#8217;ve liked but it was also &#8216;familiar&#8217; not to mention we just made some really awesome friends out there.

So my high school is planning another reunion&#8230;a 13 year reunion this October I want to say? Not sure&#8230;or maybe it&#8217;s a 15 year reunion being planned for 2014. I didn&#8217;t go to my 10 year reunion in 2007 as I was a poor graduate student that didn&#8217;t have the money to flit off to Hawaii, much as I may have wanted to. It&#8217;s interesting to see how lives drift apart and back together in some cases. Many of my high school friends and acquaintances did stints in the mainland sometimes for a decade or more and are now moving back to Hawaii. Whether you were born in Hawaii or just spent your formative years there&#8230;the islands always call you back.

In high school I used to say I&#8217;d never want to go back and settle in Hawaii&#8230;it&#8217;s expensive, it&#8217;s boring, its an island I can drive around in 8 hours (Maui)&#8211;this was my high school mentality&#8230;but then over the years I find myself yearning for the familiarity of it. While I have friends back there, I&#8217;d go back for my family and Hawaii in and of itself&#8211;having friends there is definitely a plus, having the chance to reconnect after so many years would be fun. Would I still consider Hawaii &#8216;home&#8217; if my family moved from there?

Excellent question, I haven&#8217;t decided yet.

Just some general musings of a nomadic child.
