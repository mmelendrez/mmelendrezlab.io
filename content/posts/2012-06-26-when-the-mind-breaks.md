---
title: When the mind breaks…
author: Mel
type: post
date: 2012-06-26T16:19:22+00:00
url: /2012/06/26/when-the-mind-breaks/
al2fb_facebook_link_id:
  - 43807988_808335542369
al2fb_facebook_link_time:
  - 2012-06-26T16:19:27+00:00
al2fb_facebook_link_picture:
  - post=http://www.melaniemelendrez.com/?al2fb_image=1
categories:
  - Devotion
tags:
  - God
  - life
  - pain
  - struggle

---
Devotioanl Blog:

&#8220;Looking for love in all the wrong places&#8221;, &#8220;You Can&#8221;, &#8220;There is a Plan&#8221;; 6/23/2012, 6/22/2012, 3/12/2012; 1 Peter 4:8, Luke 13:12, Jeremiah 29:11

You ever hear the joke that you should listen to country songs backwards? Why? Because then they become exceedingly happier&#8230;you get your house back, your dog back, your woman back, your tires un-slashed and your guitar un-smashed.

I&#8217;ve noticed a trend in many entries of this book. In many examples of people&#8217;s lives that she uses&#8230;when it rains it doesn&#8217;t just pour&#8211;it&#8217;s a fricken hurricane and it&#8217;s not &#8216;waves of life&#8217; that hit people, it&#8217;s a damn tsunami! <!--more-->Case in point, my comments/clarifications are not italicized and are enclosed with brackets:

> &#8220;Her story quickly tumbled out. She had been sexually molested by a member of her own family&#8230;a high school classmate used her in the same way&#8230;found herself pregnant at fifteen and married to him thinking he would rescue her. He moved her and the baby, then two babies into a small travel trailer&#8230;leave her and children for weeks without food, diapers, heat or money. She left her marriage [_for another man who&#8217;d rescued her from the situation_] [_only to end up_], he took her welfare, spent it on drugs, alcohol and gifts to other women. [_And to cap it all off?_] She discovered she had a sexually transmitted disease and he began to beat her&#8230;&#8221;

Damn&#8230;how about this one from a different entry:

> I asked a group of women I was speaking to if they would share stories of overcoming&#8230;this one stood out. &#8220;I realized I was out of control when I woke up next to a man I had no idea where I met him or what his name was. &#8230;I am getting better, learning I am worth of love, and looking at some hurts I have buried for years: incest, date rape and my own poor choices&#8230;&#8221;

Still not convinced&#8230;another story:

> I met Lisa at the bottom of her life. Her marriage was ready to break up; her spouse eventually left her for another woman. Lisa had a two year old and had experienced years of brokeness, abuse and abandonment. Her mother left her; the her stepmother couldn&#8217;t take living with Lisa&#8217;s drug addicted dad, so she left. Then Lisa&#8217;s biological mother wanted Lisa&#8217;s sister&#8211;but not Lisa. Her father was a drug dealer who went to prison, then died of an overdoes. She was on her own at sixteen&#8230;

Seriously, ready to ball your eyes out for these women? I mean damn! Next to these stories my life looks like a fricken tika-take parade, it makes my struggles look like crying over spilt milk. Now to be honest I know the use of these stories is supposed to be inspirational&#8230;&#8221;See! You don&#8217;t have it so bad!&#8221; There&#8217;s always someone with a story that will put your weenie-@$$-whining to SHAME! And all these stories come with happy endings of restoration, love, healing, guidance and happiness and that&#8217;s great&#8230;.but I cannot truly relate.

Now I&#8217;m sure the author [Pam] realized not all of us have these terrible stories and as such she made the entries more universal&#8211;the story being for illustration and obviously effect. If you look at the titles above, each of these excerpts came from different entries dealing with looking for love, having confidence in God&#8217;s plan and having a &#8216;you can&#8217; attitude when life is rough.

They were good entries in the book that ended all flowers with endings befitting an after-school special complete with a thumbs up as the credits roll.

My turn, now I&#8217;m going to tell some stories:

> A boy sits in his room, his whole life before him. Nothing especially &#8216;bad&#8217; in his life, it just drones on and on. He&#8217;s listless, he&#8217;s bored, he&#8217;s sad&#8230;so sad&#8230;so very sad. Why? He cannot even fully tell you. On his best friends birthday&#8230;he takes his own life.
> 
> &nbsp;
> 
> A girl starting to grow up. Nothing especially &#8216;bad&#8217; in her life aside from teenage discontent. Involved in things she cannot fully comprehend, the ramifications of which wreck her mind. A mental abyss opens in front of her, she falls. The girl she once was, gone. The life she once knew gone. Hurting, less, resentful, bitter, very few rays of light penetrate the wall she has now built. Desperate for a change, not knowing how, unable to accept &#8216;how&#8217; due to the pain of the &#8216;memories&#8217; she now has&#8211;so she floats, in and out, with highs and lows not sure anymore of what love truly is.
> 
> &nbsp;
> 
> A boy, watches his father torment his mother for years. Occasionally stepping into the fray of physical confrontations. Watches his father cheat, watches his mother hurt, watches as the church does nothing but pass judgement on them all. Hopeless, he asks God why&#8230;and receives no answer.

Are there after-school special endings for these stories? Sadly no for the first one as he took his own life. And no for the other two as their stories are not fully written yet. Are they at least &#8216;happy&#8217;? For the girl, in her own way, she finds her happy moments. For the boy&#8230;he is content, happy even in many respects and reserved in others. These stories are not nearly as involved as the ones described in the book and excerpt&#8217;d above, but no less detrimental in terms of their struggles.

You don&#8217;t have to be raped, beaten and addicted to drugs to &#8216;struggle&#8217;. Your husband doesn&#8217;t have to be a cheating, lying abusing b@st@!d for your problems to be considered &#8216;struggles&#8217;. You don&#8217;t have to be homeless to be considered &#8216;worthy&#8217; of the description &#8220;struggling&#8221; or to ask for help.

We all struggle. And while &#8216;terrible&#8217; stories from other people may help us put things into perspective it should never mitigate our own issues. Personally when someone comes to me with their story, I never try to &#8216;top it&#8217; with another story. Every mind has it&#8217;s own breaking point and no two minds are alike. When a mind is close to breaking, for whatever reason, the last thing you want to do is make that person feel ashamed or that their mind is weak compared to someone else.

When I was younger I never used to voice my problems, my frustrations or my hurts because there was always someone else worse off than myself and my issues were made out to be &#8216;insignificant&#8217;. When everything came to a head with me and my mind reached its breaking point, I sought counsel. I still struggle today, but when it gets to be too much I seek counsel, advice, comfort and I deal with it quickly rather than let it stew for years.

And I realized I could write my story like this:

> A girl, moved around much throughout her childhood, few friends, sad, lonely, poor, bitter, frustrated, angry; frequently fantasizing about death, hurting other people and the macabe. Judged by those she called friend, rejected by some who said they loved her, raped at 21 in a country not her own and no recourse for justice. Told by some she is no longer &#8216;good enough&#8217; because of that and the aftermath that followed. She asks God why and receives no reply.

Or like this:

> A girl, smart, clever, mischievous travels around as a child. Finds her best friend at 5 years old. Inquisitive, creative, giggled a lot. Dreamt of being a doctor and healing others pain around the world. One of the valedictorians of her high school, first in her family to go to college, first in her family to go to grad school, first in her family to get her Doctorate. Travels the world, swallows everything life gives her no matter what it is and still yearns for more. Wishes her capacity for love was greater. Thanks God for the adventure the life he gave her has brought her.

Both of these girls are the same girl. Life is full of struggle, you&#8217;ll never escape it&#8230;so embrace it and when it gets to be too much&#8230;don&#8217;t mitigate your suffering, don&#8217;t shove it deep inside you or feel ashamed as though your struggles aren&#8217;t &#8216;worthy&#8217;&#8230; I find comfort in this:

> No temptation has overtaken you except such as is common to man; but God is faithful, who will not allow you to be tempted beyond what you are able, but with the temptation will also make the way of escape, that you may be able to bear it. ~ 1 Corinthians 10:13

So pray, get advice or counsel from those you love/trust and perhaps&#8230;

Try re-writing your story.
