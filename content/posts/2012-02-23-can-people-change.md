---
title: Can people change?
author: Mel
type: post
date: 2012-02-23T05:21:14+00:00
url: /2012/02/23/can-people-change/
categories:
  - Life or something like it
tags:
  - change
  - friendship

---
Can people change?

When I ask this question I am surprised by the variation in responses I get. I suppose I assumed the general consensus would be &#8220;sure, yes&#8221;. After all we are all humans with the power of choice and ability to adapt and be flexible in our existence and relationships with others. I don&#8217;t think anyone doubts it&#8217;s &#8216;possible&#8217; for someone to change, when I get more specific with the question is when they &#8216;waffle&#8217;.

When I ask about people specifically in their own lives whom they&#8217;ve known an extended time&#8230;when I ask if &#8216;they&#8217; could change the response is usually &#8216;no, they are who they are, they&#8217;ll never change.&#8217; I found that interesting. I too believe that &#8216;anyone&#8217; can change should they choose to do so however, there are people in my life who I would be absolutely dumbfounded if they changed. They seem so staunch in their ways, their beliefs, their attitudes that nothing short of God or the devil himself could change them.

There are people that hold the belief that someone cannot/will not change from personal usually painful experience with that person. I have any number of stories of girlfriends of mine &#8216;played&#8217; by a guy. They fall hopelessly in love with him, he entertains that off and on for a while then loses interest and moves on. I&#8217;ve been the &#8216;girl&#8217; in that situation. Or worse he strings her a long, sometimes for years so that he has someone to &#8216;fall back on&#8217;&#8211;I&#8217;ve been the &#8216;girl&#8217; in that scenario as well. And it happens in reverse a girl cheats on her guy or vice a versa and we are so unwilling to entertain notions that the person who cheated on us can ever be anything more than just a lousy cheater. Isn&#8217;t that where the saying comes from? &#8220;Once a cheater, always a cheater.&#8221; If you fundamentally believe that people can change why stick with saying &#8216;once a cheater, always a cheater&#8217;. The same applies for &#8216;once a bitch/gossip always a bitch/gossip&#8217;; once stubborn always stubborn, once &#8216;disappointed&#8217; will always be disappointed&#8217;, once jaded will always be jaded, once wronged&#8211;now everyone &#8216;wrongs&#8217; them&#8230;.you get the idea.

I do know what it feels like to throw yourself 105% or more into a friendship and it crumbles anyway because that other person could not be bothered to acknowledge your existence any more for whatever reason. Reasons they never tell you. So what are you left to believe?

A friendship that you desperately attempted to keep alive despite different life paths and yet it died anyway (and that&#8217;s saying something given social networking today)&#8230;A person walks back into your life and decides they&#8217;d now like to be in your life again; you might have hurt them but they&#8217;ve also hurt you. Do you let them back in?

I believe people can change and if they want to and it makes their lives happier and I would be excited for them and wish them all the happiness in the world on their new outlook or view on life etc&#8230;but no matter how much I might want to be apart of that persons life as well, I am doubtful that I would let them back in. Why? Because I already threw everything I had available at the friendship&#8230;I&#8217;d feel I had nothing left to give.
