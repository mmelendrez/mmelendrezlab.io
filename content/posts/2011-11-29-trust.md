---
title: Scaling smooth inner walls of trust
author: Mel
type: post
date: 2011-11-29T03:54:21+00:00
url: /2011/11/29/trust/
categories:
  - Devotion
tags:
  - faith
  - family
  - friendship
  - trust

---
Ok after yesterdays sidetrack event of commenting on a blog I&#8217;d read entitled [&#8220;I&#8217;m Christian unless you&#8217;re gay&#8221;][1] (read it if you get a chance), now, back to the book&#8230;

Devotional Blog:

Topic: &#8220;Trust&#8221;, 11/27/2011, Jeremiah 31:1-6 and Ruth 3:5

In this section the author, Pam, goes into what it means to have a trusting relationship. She opens with something Ruth said in the Bible: &#8220;I will do whatever you say&#8221;&#8211;what guy wouldn&#8217;t want to hear that from a woman? Sorry guys, she was saying it to her mother-in-law. I find the concept of trust interesting in that I have some friends that are incredibly trusting and some that have some incredible walls built up&#8230;hell you need some seriously specialized climbing gear to get up the smooth face of their walls.

Then you inevitably ask the question &#8216;is it worth it?&#8217; Which is terrible I know, they are your friend after all. But it is exceedingly frustrating to think you are making progress only to find yourself on a temporary ledge with your friend laughing at you from above&#8230;continually saying &#8216;you don&#8217;t know me, you can never know me&#8217;. At that point I&#8217;d just rather rappel down and call it a day. Of course self-discovery and self-trust is an ongoing process and I&#8217;m sure I&#8217;ve frustrated many a friend as well, even though I wouldn&#8217;t say I put up walls&#8230;I think rather its just a fundamental misunderstanding of personalities. You build an image of what you think someone is in your head and when that turns out to be untrue it throws you for a loop. Not because they misled you but because you built this image that wasn&#8217;t who they were inside. Its not a matter of &#8216;good or bad&#8217;, its just not who they were and you have to step back and decide if you are going to take the time to dispense with all your, perhaps years of, preconceived notions and really get to know the person for who they are. Sometimes we are able to do that, sometimes circumstances prevent that option.

I used to say I was very &#8216;guarded&#8217; didn&#8217;t really trust anyone&#8211;but who am I kidding&#8230;its not who I am. At best I had phases of distrust that ended up evaporating as the event that triggered the distrust faded. Personally I&#8217;m a pretty open book, people don&#8217;t have to work too hard to read me. At first I was insulted because I thought of myself as a chameleon, I could put on whatever face was required and they&#8217;d never know &#8216;me&#8217;. So when people said  I was easy to read I was aghast&#8230;and here I thought I was this great actress. This was when it was &#8216;hip&#8217; to be mysterious&#8230;ya, no, I&#8217;m not mysterious haha. I was in theater from 6th grade up through high school and some in college and didn&#8217;t get bad reviews. As an actress, ok I didn&#8217;t suck, but as a person&#8211;who am I kidding&#8211;I suck at hiding my feelings. That doesn&#8217;t mean I wasn&#8217;t stubborn. Which I know probably drove some of my friends and boyfriends and family insane, knowing something was dreadfully wrong but not being able to truly pry it out of me.<!--more-->

As I got older I got better and worse about communicating what I really thought. I don&#8217;t enjoy upsetting people and I guess what it boiled down to is I didn&#8217;t trust them with my true emotions. As I got older as well, my mantra changed to &#8216;trust someone til they give you a reason not too&#8217;, within reason of course. And I had varying success personally with this view. Inherently I do trust pretty easily especially people I think are friends whether they are true friends or not. Probably why I got hosed a lot in elementary school. The girls would ask me to play with them so they could make fun of me where I just thought &#8220;hey girls want to play with me! trusting they really had no ulterior motives&#8221;. I wasn&#8217;t raised to be inherently wary of people, except strangers with candy that is.

Wariness is something I learned, sometimes the really hard way, as I grew up.

What about trust in God? That I was raised with&#8230;even when I didn&#8217;t or couldn&#8217;t understand it, it was right there. My family always always always trusted God for everything in our lives. And I can&#8217;t say it was a terrible way to grow up or live, we always had food, always had a roof over our heads, I was able to go to school, always had clothes that weren&#8217;t torn or ratty, we were always able to celebrate Christmas and thanks to Grandma always had at least one present, we had a car, we lived in a safe area&#8230; If you asked me back then, I&#8217;d probably do the &#8216;teenager whine&#8217; about all the things I did want and couldn&#8217;t have because of money or circumstance, but all teenagers whine and lament about that. But honestly, God has provided for my family all my life, one way or another. Dad&#8217;s jobs, sometimes via friends, sometimes via the church, sometimes via people we didn&#8217;t even know.

In college I&#8217;d pay my tuition and rent (when I lived off campus) and have no money left for food sometimes. I was terrible at praying back then, but when one has no food&#8230;one finds the time to send up a quick prayer that something comes through. And something always did, a friend would invite me to dinner, I&#8217;d find a $10 bill in the laundry in my pants&#8211;something would pop up. Perhaps you see this as coincidence, I see this as provision. I don&#8217;t believe in laying about doing nothing and &#8216;expecting&#8217; God to just show up and drop things from heaven. But I do believe when we work our hardest and do what we can, God is faithful to bless us and make sure we are provided for. Either God provides or my life is jam packed with coincidences difficult for me to rationalize.

There are only a few people in my life that I&#8217;d unabashedly say &#8220;I will do whatever you say/want&#8221; with no reserves and with complete trust. I can tick them off on one hand. Those same people I would say absolutely anything too, about absolutely anything&#8211;faith, love, death, God, insecurities and fears, doubts, happiness&#8211;anything. And I&#8217;d have complete trust that they would love and accept me no matter what even if they violently disagreed with what came out of my mouth. I can tick them off on one hand.

Feelings and emotions, sure I&#8217;m pretty easy to read meaning on the surface I am actually a very trusting person and it&#8217;s gotten me into trouble, yet I still do it. I&#8217;ll still trust someone for the most part until they give me a reason not to. But the very soul and heartbeat of who I am? Are there &#8216;walls&#8217; around that, no, actually, there are no walls&#8230;its just a long hike to reach that area and very few honestly stick around to walk that path with me whether they be friends or family. And I don&#8217;t blame then, its a huge investment to walk with someone long enough (theoretically or literally) to get to their center and some people do have sometimes insurmountable walls that would jade even the most devoted of friends or family. There are only a few people that I&#8217;ve invested the time and only they could tell you if I actually have reached the center of who they are&#8230;achieved that kind of trust. That&#8217;s why, in my life, I can tick those people that have reached that level of trust with me off on one hand&#8230;.and that&#8217;s ok.

 [1]: http://www.danoah.com/2011/11/im-christian-unless-youre-gay.html
