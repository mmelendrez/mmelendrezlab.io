---
title: Matches and candles start wildfires
author: Mel
type: post
date: 2011-10-06T13:32:29+00:00
url: /2011/10/06/matches-and-candles-start-wildfires/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"4bca7cefa0";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
wordbooker_extract:
  - |
    Devotional Blog:
    
    Topic:"Light": 10/5/11: Matthew 5:11-16
    
    There's a children's sunday school song that goes:
    "This little light of mine, I'm going to let it shine. This little light of mine I'm going to let it shine, let it shine, let it shine, l ...
categories:
  - Devotion
  - Life or something like it

---
Devotional Blog:

Topic:&#8221;Light&#8221;: 10/5/11: Matthew 5:11-16

There&#8217;s a children&#8217;s sunday school song that goes:

> &#8220;This little light of mine, I&#8217;m going to let it shine. This little light of mine I&#8217;m going to let it shine, let it shine, let it shine, let it shine&#8230;.&#8221;

The verse talks about holding fast when you are persecuted for your faith as you are called to be a &#8216;light&#8217; to others through the example of faith your life. The author talks about persecution in China and how believers there withstand a lot of harassment and yet still share their faith, assemble in their churches and strive to be light in a harshly and strictly controlled religious environment. I am fortunate to live in a country that does not persecute my faith but rather encourages respect for all faiths. Not to say that respect for faiths always happens in our American culture but we are free to believe, practice our beliefs and share our beliefs without fear of being dragged off to prison.

I have never personally experienced religious persecution&#8230;if I&#8217;ve been criticized or ridiculed it was more likely due to something inanely stupid that I did or said rather than a direct attack on my faith. I&#8217;ve always been open to sharing my faith with anyone curious but I&#8217;ve never been the type to walk out and just wallop a random stranger with my beliefs&#8211;&#8220;BELIEVE!!!&#8230;oh and by the way hello, my name is Mel&#8221;.

<!--more-->Being a &#8216;light&#8217; while emphasized in Christianity applies to other aspects of life. What does light do? Helps you visualize things you didn&#8217;t see before&#8230;perhaps didn&#8217;t comprehend before and never noticed prior to the light being there.

There is a movement in the U.S. right now called &#8220;The 99% movement&#8221; or &#8220;I am the 99&#8221; or &#8220;[Occupy Wall Street][1]&#8220;&#8230;[protestors all over the U.S. assembling against corporate greed which has caused so many downfalls for the common man and made the rich richer&#8230;][2] These people are thousands of &#8216;lights&#8217; attempting to visualize a fundamental &#8216;unfairness&#8217; in how our country is &#8216;controlled&#8217;. [Obama is attempting to address some of this through his new tax proposal measures][3]&#8230;with the basic justification of the rich and corporations should be taxed as much as the middle class but because of &#8216;loopholes&#8217; they are not. There&#8217;s a lot more too this and I encourage you to google it. The article above, from Reuters also mentions how companies like Google, Bank of America and Citicorp Group have said they will pay more if it&#8217;ll stimulate the U.S. economy and create jobs. Assuming their &#8216;offer&#8217; is sincere it seems that while corporate greed does exist&#8230;it doesn&#8217;t pervade every single corporate entity.

Am I the 99? Ya. I&#8217;ve struggled for health insurance, I&#8217;ve gone into debt attempting to get an education so I don&#8217;t have to work 3 minimum wage jobs to make ends meet&#8230;the allegations cited in [Keith Olbermann&#8217;s video][2] aren&#8217;t just characteristic of the U.S. they occur in other first world countries too. Is it unfair everything they cite? Definitely. Is it worth bringing light to&#8230;yes. There are several commentaries mentioning how the protestors are just protesting to protest without focus or a real understanding of why or not having all the information. The Nation has an interesting [FAQ][4] on this and Ezra Klein of the Washington Post talks about the movement in a [blog][5] with additonal links.

In essence I&#8217;ve just mixed politics with references to light having started with faith in a devotional blog&#8230;perhaps I shouldn&#8217;t mix the two, but I feel its applicable the idea of being light. Some people prefer the darkness, get frustrated or irritated with light whether that light is a person, a concept, a belief.

Whether for reasons of faith or humanity I think striving to be a &#8216;light&#8217; and not allowing anything to extinguish that is important. And I don&#8217;t use that as a &#8216;call to arms&#8217; or to protest or to Bible beat your neighbor. Lights are also beacons leading to refuge or leading away from danger. In this capacity you&#8217;ve no need to stand up and cry &#8216;foul&#8217;, but rather be there as a refuge for those coming out of their darkness, whatever it might be. If being an &#8216;activist&#8217; or &#8216;evangelist&#8217; isn&#8217;t your personality you can still demonstrate your faith and beliefs through your ability to comfort and extend a peaceful refuge. You don&#8217;t need floodlights to accomplish your purpose. I can&#8217;t count how many times as a child a nightlight or lighting of a simple candle immediately assuages fears&#8230;.as a child I liked to hypnotize myself in the flame.

What type of light are you? A match or a wildfire? What am I? Probably closer to a candle or match&#8230;though I may have some friends that characterize me differently. I&#8217;ve been questioned by other Christians as to why I am not more of a wildfire&#8230;do I not have as great of faith&#8230;do I not completely commit my life to God? Am I a lukewarm Christian?

My response?

> &#8220;Matches and candles start wildfires&#8221;

 [1]: http://blogs.wsj.com/law/2011/10/06/occupy-wall-street-and-the-constitution/
 [2]: http://current.com/shows/countdown/videos/special-comment-keith-reads-first-collective-statement-of-occupy-wall-street
 [3]: http://www.reuters.com/article/2011/10/05/us-usa-financial-taxes-idUSTRE7946Y820111005
 [4]: http://www.thenation.com/article/163719/occupy-wall-street-faq
 [5]: http://www.washingtonpost.com/blogs/ezra-klein/post/wonkbook-what-does-occupy-wall-street-want/2011/10/03/gIQAgCLgHL_blog.html
