---
title: When doing as you’re told becomes unhealthy
author: Mel
type: post
date: 2011-09-19T02:32:08+00:00
url: /2011/09/19/when-doing-as-youre-told-becomes-unhealthy/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"6df7692ddc";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
wordbooker_extract:
  - |
    Devotional Blog:
    
    Topic: Who is God to you?, 9/18-19/2011, Ezekiel 34:25-31 and Psalm 34: 8-14
    
    So I grew up your 'typical' Christian kid or perhaps 'typical' isn't the right word since I was raised more on the pentecostal/evangelical side and many ...
categories:
  - Devotion
tags:
  - blog
  - childhood
  - church
  - college
  - faith
  - God
  - life
  - purpose

---
Devotional Blog:

Topic: Who is God to you?, 9/18-19/2011, Ezekiel 34:25-31 and Psalm 34: 8-14

So I grew up your &#8216;typical&#8217; Christian kid or perhaps &#8216;typical&#8217; isn&#8217;t the right word since I was raised more on the pentecostal/evangelical side and many other Christian sects think we&#8217;re pretty nuts&#8230;fair enough. I grew up in with a Christian bent toward pentecostal/evangelical due to where my parents chose to go to church. We started out in Calvary Christian, fairly conservative along with Cornerstone Christian churches then moved into the Vineyard &#8216;movement&#8217; which was akin to house churches (they were usually small) and they popped up in random places whereever there was space&#8230;a strip mall vacant store, a school gym, another churches rec room, someones . To me the Vineyard churches felt very odd&#8230;sort of like the &#8216;hippy movement&#8217; for Christianity. But this was my perception as a young child&#8230;

_On a side note: It was in a Vineyard Sunday school where I learned about communion and received a piece a bread which is supposed to symbolize the body of Christ/Jesus&#8230;at which point I turned to my friend and squeezed the bread to &#8216;make it talk&#8217; telling my friend &#8216;jesus loves you&#8217;&#8230;my sunday school teacher was not amused&#8230;_

We attended such churches til I was 11 and moved to Hawaii. In Hawaii we attended a First Assembly church which was really small and has since expanded enormously to have satellite chapels all over the Pacific Rim and a congregation that I&#8217;ve seen attend at most 1,500 people&#8211;yowza! When we first started going I think 50 people on average&#8230;maybe 75-80 would attend. The church went from being a First Assembly Church to breaking off into it&#8217;s own entity now called [King&#8217;s Cathedral][1] headed by [Pastor James Marocco][2], a man with several degrees including a Ph.D. from reputable universities such as USC. The man knows his history and theology.

Why do I say all this? Because this is what I grew up in. I didn&#8217;t question my faith growing up, it just was what it was. People lifting their hands and dancing in church? Ok&#8230;sure. People getting prayed for and &#8216;falling out in the spirit&#8217;&#8230;ok no worries. People receiving prophecy from pastors or prophets&#8230;this was all on par with my upbringing and it wasn&#8217;t &#8216;alien&#8217; to me, though I&#8217;m sure all of this in one place might freak out a non-Christian or Christian with more sedate upbring in the faith. Our church in Hawaii wasn&#8217;t like this to begin with, they went through a series of &#8216;revivals&#8217; and before y&#8217;all have nightmares of some backwoods area of a southern state&#8230;it wasn&#8217;t like that&#8211;I think.

<!--more-->Point is, back then when I was a kid, I believed what I was told to believe. I accepted Christ, prayed, got bapitized, went to church regularly and tried to be the perfect child persay. I rarely thought for myself. And it was ok at the time. It was great being brought up with something in my life&#8230;even if later on I were to change my mind, I had a foundation to build from&#8230;assuming I found my own mind later. There are still many out there that blindly follow their faith without critically looking at it and examining WHY they believe what they believe and what role it plays in their life.

In my teens I went through a phase of &#8216;re-commitment&#8217; to Christ and my faith&#8230;but I was a teenager so while my heart was in the right place&#8211;I&#8217;m sure I messed up a long the way and didn&#8217;t fully live up to my re-commitment like I should&#8217;ve. Why? Mostly because I still didn&#8217;t fully understand what I believed and what that meant. In college, I did not attend church frequently and gave other Christians at my university a wide-berth. They seemed so &#8216;happy-go-lucky&#8217;  about their faith and were kind of &#8216;uppity&#8217; and it really turned me off. Many that I talked to had never had to deal with difficult things in their life; whereas my family had seen difficult times&#8211;we couldn&#8217;t relate. Many were wealthier kids and they rarely understood why I worked so much instead of going to/paying for retreats and spending all of my time with them and their &#8217;cause&#8217;. And you could only be in the &#8216;club&#8217; persay if you acted like them and only hung out with them. It was all very odd to me. I attended a small gathering at a Lutheran church just across the street from campus once a week for a long while. They called it Vespers and I loved it. It was so calming and it gave me a chance to express my faith without being bombarded by anyone. On the outside I seemed perfectly normal and calm, but on the inside I was a very angry individual during practically all of college&#8211;about many things I will not get into at this point.

I wouldn&#8217;t say I gave up my faith but it definitely was on the back burner, replaced by work, school, a boyfriend  here and there, the need for money and whatever extra work I could find to put myself through school, pay for school, eat at some point and when I wasn&#8217;t doing any of that&#8230;I was asleep. My faith and &#8216;God&#8217; didn&#8217;t enter my life again until after graduation when I had a minor meltdown and decided to &#8216;$%^#-it&#8217; and went home. My church might have been a little &#8216;crazy&#8217; to me sometimes, but I wanted home&#8230;it was my home and I loved it, I felt like I could figure my life out if I just went home for awhile.

And I did. To this day, I still struggle with paying attention to my faith. I am so friggin busy and constantly going, have been since I can remember and my Mom would probably agree. I&#8217;d go and go and go&#8230;then ultimately pass out for 2 days straight then go again. I literally cannot remember a time in my life when I wasn&#8217;t working or in school. I worked all my vacations from school, I worked during school, after graduation I immediately entered an internship and when life melted and I went home I had a job within 10 days of getting home. I went from that job straight into grad school and more work and literally straight from grad school to my post doc (within days of finishing my degree I was on a plane, arrived Saturday or Sunday&#8230;started work Monday, [see this blog][3]). That&#8217;s been my life&#8230;rarely quiet.

Amidst all that I continue to look for God, I continue to try and figure out what I believe and why I believe it. I could always fall back on the dogma I was taught but I don&#8217;t believe that is &#8216;real&#8217; faith and it saddens me when talking to Christians and the only argument for their faith that they have is &#8216;The Bible says so&#8217; or &#8216;God says so&#8217;. I call them religious &#8216;yes-men&#8217;, very automated Christians. I don&#8217;t doubt their faith, I&#8217;m sure they are very devout &#8216;yes-men&#8217; but it all seems very narrow-minded to me. Sure that works for you and your Christian friends and family but what about those who don&#8217;t give a rats @$$ about what the Bible says or don&#8217;t believe in God? How do you &#8216;reach&#8217; people like that? Or have you decided to stay within your &#8216;faith bubble&#8217; and only allow people that meet your Christianly requirements in?

Be who you are. Don&#8217;t be a religious &#8216;yes-man&#8217;. Figure it out for yourself. You&#8217;re faith should just &#8216;come through&#8217; naturally without the need to fancy Christian words or verses from the Bible. While it&#8217;s great to praise God you don&#8217;t have to &#8216;prove&#8217; you are Christian by always saying &#8216;praise God&#8217;&#8211;you&#8217;re not fooling anyone, especially if there are things in your life that scream &#8216;un-Christianly&#8217;. Now if you mean it then fan-tab-ulous, have at it. Personally, vocal evocations of my adoration of God aren&#8217;t inherent in my personality&#8211;does that make me less Christian? Absolutely not, I just have a different way of exploring and expressing my faith.

This blog is an effort toward exploring what I believe and why I believe it. A faith in progress that&#8217;s what I have&#8230;

&nbsp;

 [1]: http://kingscathedral.com/
 [2]: http://www.kingscathedral.com/jm_bio.php
 [3]: http://www.melaniemelendrez.com/2011/08/18/sealing-my-ph-d-fate-repost-from-sep-12-2007-with-updates/ "Sealing my Ph.D. fate: repost from Sep 12, 2007, with ‘updates’"
