---
title: Bird poop and buses to Bangkok
author: Mel
type: post
date: 2010-06-24T01:40:34+00:00
url: /2010/06/24/bird-poop-and-buses-to-bangkok/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"3995999c38";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Asia
tags:
  - Bangkok
  - Kamphaeng Phet
  - Sukhothai

---
So this past weekend I got to attempt to get in touch with my inner entomologist&#8230;sort of. I went up to the AFRIMS entomology facility in the North at Kamphaeng Phet. Their main purpose is to wait for patient blood samples collected from the villages to come back positive for Dengue virus then they mobilize and head out to the village. They enroll and collected mosquitoes from houses within a 200 m radius of the index house that has the patient. They use backpack aspirators to collect the mosquitoes record information about the house and give it a unique ID. Back at the lab, mosquitoes are sorted into their respective species, Ae. aegypti females being of most interest, and dissected as we are only interested in their head and thorax for Dengue PCR/isolation. The goal being cluster studies of Dengue in patients and mosquitoes and to track movement from village to village. Then I come in with my all-powerful, mostly open source programs and attempt to correlate/track the genetics vs. epidemiology of host/vector.

<!--more-->Now while learning about all of this and observing was very interesting there were a couple problems with the whole process&#8230;only one person there spoke English and at that, it wasn&#8217;t very good English. Sweetest girl, but communication was a struggle and they weren&#8217;t able to train me because they didn&#8217;t know enough English to tell me what to do really. So while I &#8216;learned&#8217; some things I didn&#8217;t involve myself with others because I didn&#8217;t want to mess up their protocol and they couldn&#8217;t explain it to me in English&#8230;so it was a week of mostly observation.

Tyghe came up end of the week and you can see his post about our trip up to Sukhothai at www.tygertown.us/blog as well as a ton of pictures on that blog and his picasa link. The ruins, place we stayed and bike riding were awesome. I&#8217;m not for redundancy and he pretty much covered it so I will only speak to a couple things that he missed or &#8216;blocked out&#8217; because they were so traumatizing :).

1. A bird pooped on his head.

2. When he talks about the bus ride he doesn&#8217;t quite do the insanity justice. So let me elaborate.

Bummer 1: Find the bus station, this was actually incredibly easy because we stayed at a fantastic little place near the old city and they arranged it for us, so no 1/2 hr walks for Tyghe or myself. Funny side note: In Kamphaeng Phet the bus station is a 1/2 hr away (about 3 km) as Tyghe said, it&#8217;s one of those through the woods, over the bridge, squeeze through the fence in a 100 degree heat type of experiences. Okay, so the bridge part is really the only true aspect of the trek. So when you catch a bus after making your way to this bus terminal that&#8217;s decently far from the city and you catch your bus&#8230;the bus GOES BACK through the city you just walked through to catch it. This &#8216;quirk&#8217; was not lost on me and elicited a &#8216;WTF&#8217; response.

Back to the story of the bus ride home from Sukhothai&#8230;

Bummer 2: Find the right bus. Now they&#8217;d told us the first bus left at 8am&#8230;ok so when we got there after repeating Bangkok til it sounded like we had turrets we got pointed to a counter that sold tickets back to Bangkok, Wintours. We figured we&#8217;d splurge and go first class. Now we though that meant more leg room&#8230;uh, no. Apparently 1 st class means you get a meal&#8230;maybe. If you can get off the bus, half way through the trip and scarf it down fast enough before the bus ups and decides to leave.

Bummer 3. Find seat. Tyghe was correct in saying the seats were constructed for little Thai people. He literally could not put his legs inside the seating area without sawing off his knee, he ended up having to twist so they were somewhat out in the aisle. AND we had all our bags that we somehow had to store in our seats with us (2 backpacks and my laptop bag) because the bus filled up. Why did we have to keep out bags with us you ask?&#8230;

Bummer 4: When we got to the desk prior to buying tickets a lady automatically gave us baggage checks for our backpacks to go under the bus. On further inspection of our tickets we noticed that they said for lost/stolen luggage they would only reimburse 300 Baht. 300 Baht roughly equals $10. Considering we both had laptops in our bags we said &#8216;fudge-that&#8217; (except we didn&#8217;t say fudge). So we ended up in tiny little seats with our backpacks&#8230;awesome

Bummer 5: Find the airconditioning&#8230;as if things weren&#8217;t already looking grim, turns out we got on a bus that didn&#8217;t have a particularly strong airconditioning ability&#8230;

Bummer 6: And we were on the side of the bus with the sun directly hitting us&#8230;

Bummer 7: We discussed hopping off and catching a different bus as the bus was starting on its way because the idea of a 6 hr bus ride back to Bangkok in this situation was far less than appealing. But the bus got moving and we said screw it, we&#8217;ll stay on&#8230;cant&#8217; get much worse can it?

then&#8230;

Bummer 7: They turned on the entertainment, no headphones&#8211;just speakers throughout the bus blaring whatever was playing. And what was playing?

Bummer 8: A Korean soap opera dubbed in Thai&#8230;

First thoughts? Kill me&#8230;kill me now.

Bummer 9: Not just Korean soap opera dubbed in Thai&#8230;BUT what turned out to be 6 hours of Korean soap opera dubbed in Thai. The main character of whom tried to kill herself several times during the course of the 6 hrs. But then end, we really wanted her to just die.

We pulled into Bangkok thoroughly traumatized, found a taxi to the BTS got home about 3p. Showered, changed, ate quickly then headed out to frisbee practice to wash off the traumitization with sweat.

Frisbee was pretty awesome despite not knowing really how to get to the fields and taking over an 1.5 hrs to get there but yay for google maps and after wandering randomly through neighborhoods we found them.

I think that covers the highlights mentally blocked by Tyghe 🙂
