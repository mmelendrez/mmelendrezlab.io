---
title: New years notes from a nomad…
author: Mel
type: post
date: 2012-01-04T07:57:06+00:00
url: /2012/01/04/new-years-notes/
featured_image: /wp-content/uploads/2012/01/fbvienna2-480x288.jpg
categories:
  - Life or something like it
tags:
  - life

---
So I don&#8217;t believe in resolutions because inevitably its not something that&#8217;s kept and its a waste of mind space&#8230;so I don&#8217;t do them. This year I opted to reflect on last year so I dug into my facebook (timeline is handy for this indeed) as well as some of the other emails, blogs or things that I&#8217;ve done to get a feel for last year&#8230;

Here are the highlights, anecdotes from my facebook wall, blog, reading, general musings, links etc:

<!--more-->

Jan 1, 2011: &#8220;learning to read/write Thai now in addition to speak&#8230;todays lessons 1-4 I learnt how to say &#8220;Grandma May is dancing drunk in our rice field&#8221;&#8230;awesome

Jan 4, 2011:

  * APO address successfully granted!
  * Serious Putty ala www.xkcd.co 
    <figure id="attachment_650" style="width: 300px" class="wp-caption aligncenter">[<img class="size-medium wp-image-650" title="serious putty" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/serious-300x100.png" alt="" width="300" height="100" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/serious-300x100.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/serious.png 441w" sizes="(max-width: 300px) 85vw, 300px" />][1]<figcaption class="wp-caption-text">www.xkcd.com</figcaption></figure></li> </ul> 
    
    Jan 5, 2011: Totally excited for this book: [Phylogenetic Networks: Concepts, Algorithms and Applications][2] (nerd alert!)
    
    Jan 6, 2011: I love my field: [When Zombies Attack: Mathematical modelling of an outbreak of zombie infection.][3]
    
    [<img class="aligncenter size-medium wp-image-651" title="zombies" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/zombies-300x240.png" alt="" width="300" height="240" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/zombies-300x240.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/zombies-1024x819.png 1024w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/zombies-375x300.png 375w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/zombies.png 1280w" sizes="(max-width: 300px) 85vw, 300px" />][4]
    
    Jan 21, 2011: Found him on the driveway outside our condo building and moved him to a plant.
    
    [<img class="aligncenter size-medium wp-image-654" title="fbbutterfly" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbbutterfly-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbbutterfly-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbbutterfly-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbbutterfly.jpg 640w" sizes="(max-width: 300px) 85vw, 300px" />][5]
    
    Jan 26, 2011: Title of poster presentation at a conference I attended: [Using craigslist messages for syphilis surveillance.][6] 
    
    Jan 31, 2011: &#8220;The difference between the right word and the almost right word is the difference between lightening and lightening bug&#8221; ~Mark Twain.
    
    Feb 3, 2011: I went to Vienna for a conference!
    
    [<img class="aligncenter size-medium wp-image-652" title="fbvienna1" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna1-225x300.jpg" alt="" width="225" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna1-225x300.jpg 225w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna1.jpg 480w" sizes="(max-width: 225px) 85vw, 225px" />][7]
    
    [<img class="aligncenter size-medium wp-image-653" title="fbvienna2" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna2-225x300.jpg" alt="" width="225" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna2-225x300.jpg 225w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna2.jpg 480w" sizes="(max-width: 225px) 85vw, 225px" />][8]Feb 7, 2011: Mel has: &#8220;emotional schizophrenia when it comes to her field. A mix of love, hat and turrets&#8230;&#8221;
    
    Feb 12, 2011: Bangkok Hat. Grey team, my team, loses the whole tournament only winning one game of all of them. That game we won though was against the team that won the tournament&#8211;ha HA!
    
    Feb 15, 2011: I attempt to set up a skype 3 way with two lovely ladies&#8230;schedule conflict derails the idea but it&#8217;s still the dream.
    
    March: We got to move to Ari, so much closer to both our work places and such a bigger nicer place!
    
    March 8, 2011: &#8220;A true friend is someone who thinks that you are a good egg even though he knows you are slightly cracked.&#8221; ~Bernard Meltzer
    
    March 24, 2011: &#8220;When you have come to the edge of all light that you know and are about to drop off into the darkness of the unknown, faith is knowing one of two things will happen. There will be something solid to stand on or you will be taught to fly.&#8221; ~Patrick Overton.
    
    March 25, 2011: Sometimes only Sonya truly understands me, she sent this to me&#8230;<figure id="attachment_655" style="width: 300px" class="wp-caption aligncenter">
    
    [<img class="size-medium wp-image-655" title="beauty" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/beauty-300x119.png" alt="" width="300" height="119" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/beauty-300x119.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/beauty-500x198.png 500w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/beauty.png 740w" sizes="(max-width: 300px) 85vw, 300px" />][9]<figcaption class="wp-caption-text">www.xkcd.com</figcaption></figure> 
    
    <p style="text-align: left;">
      April 3, 2011: My friend Som has her hen party!
    </p>
    
    <p style="text-align: left;">
      <a href="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbsomhenparty.jpg"><img class="aligncenter size-medium wp-image-657" title="fbsomhenparty" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbsomhenparty-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbsomhenparty-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbsomhenparty-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbsomhenparty.jpg 604w" sizes="(max-width: 300px) 85vw, 300px" /></a>April 5, 2011: &#8220;You think reviewers will notice if I write cluster-f$#@k of diversity? My brain hurts from trying to interpret this mess&#8221;
    </p>
    
    <p style="text-align: left;">
      April 12, 2011: Dengue hits Keanae, Maui&#8211;&#8220;Just got the Promed report, I feel like I should go change in a phonebooth and fly off&#8230;to collect data&#8221;
    </p>
    
    <p style="text-align: left;">
      April 13, 2011: We have a wet and wild time for Songkran in Chiang Mai!
    </p>
    
    <p style="text-align: left;">
      April 23, 2011: Som and Blondie&#8217;s wedding!
    </p>
    
    <p style="text-align: left;">
      <a href="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/somwed.jpg"><img class="aligncenter size-medium wp-image-658" title="somwed" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/somwed-300x223.jpg" alt="" width="300" height="223" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/somwed-300x223.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/somwed-402x300.jpg 402w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/somwed.jpg 720w" sizes="(max-width: 300px) 85vw, 300px" /></a>April 28, 2011: <a href="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/wineinfdis.jpg"><img class="size-full wp-image-656 aligncenter" title="wineinfdis" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/wineinfdis.jpg" alt="" width="232" height="27" /></a>May 2, 2011: Disheartened after reading <a href="http://www.amazon.com/Three-Cups-Deceit-Humanitarian-ebook/dp/B004XHVOW4">Three Cups of Deceit by Jon Krakauer</a>.
    </p>
    
      * I followed the story of Greg Mortenson for awhile, he has so much support in his state Montana, where I did grad school. I even went to listen to him speak. It&#8217;s an interesting back and forth, this book&#8230;ultimately I think his book ([Three Cups of Tea][10]) was a good piece, real or not it would&#8217;ve still motivated me. Alan Rose wrote a good opinion piece about this and memoirs in general for The Daily News: [3 cups of whatever][11], give it a read. If you are looking for a good book that will motivate you to get involved with amazing stories read [Half of the Sky by Nicholas D. Kristof and Sheryl WuDunn][12]. Great book with links in the back to the organzations talked about.
    
    May 8, 2011: Ah phdcomics makes me laugh<figure id="attachment_659" style="width: 300px" class="wp-caption aligncenter">
    
    [<img class="size-medium wp-image-659" title="phd050611s" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phd050611s-300x270.gif" alt="" width="300" height="270" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phd050611s-300x270.gif 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phd050611s-332x300.gif 332w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phd050611s.gif 600w" sizes="(max-width: 300px) 85vw, 300px" />][13]<figcaption class="wp-caption-text">phdcomis.com</figcaption></figure> 
    
    May 22, 2011: Apparently there was &#8216;a lot to do&#8217; about the world ending in May&#8230;&#8221;If the end fo the world had come like they said it would&#8230;I flew right through it to Bangkok&#8230;the apocalypse was crazy, white puffy clouds, clear skies and one massive bout of turbulence. Hmmm, maybe we ran over one of the horsemen? Sorry cultists, I think my plane ruined your apocalypse.&#8221;
    
    May 24, 2011: Wedding date set! August 18, 2012. &#8220;dum dum di dum&#8230;Why is it that when people sing the wedding song it just sounds like &#8216;dumb dumb di dumb&#8217;, I think it was planned that way&#8230;ha&#8221;
    
    May 25, 2011: I&#8217;m sorry, but this made me laugh really hard&#8211;ok I&#8217;m not sorry, it did make me laugh pretty hard. Christians have senses of humor too.
    
    [<img class="aligncenter size-medium wp-image-660" title="raptureservice" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/raptureservice-217x300.jpg" alt="" width="217" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/raptureservice-217x300.jpg 217w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/raptureservice.jpg 480w" sizes="(max-width: 217px) 85vw, 217px" />][14]June 15, 2011: Come stay with us in Thailand and meet our house pet Tyghe found this morning!
    
    [<img class="aligncenter size-medium wp-image-661" title="spider" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/spider-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/spider-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/spider-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/spider.jpg 640w" sizes="(max-width: 300px) 85vw, 300px" />][15]His name is Randy, he eats mosquitoes, beetles, the occasional rabbit or small child and he will be disposed of as soon as we stop pooping ourselves every time we look at him! Randy was set free off our balcony&#8230;hopefully never to return.
    
    June 18, 2011: Unsuccessful attempt to find a wedding dress in Thailand. &#8220;As if it weren&#8217;t enough the largest wedding dress size in Thailand is a 4, the lady felt the need to tell me that I simply had to lose weight. I like my curves I prefer not to be the size of a stick insect in order to fit into your vomit of ruffles and sequins, thank you very much.&#8221;
    
    July 4, 2011: We spent the long weekend at Koh Tao, jungle island of adventure, unintentional hiking and snorkeling! Tyghe has a great sum up of this with pictures in his [blog site][16].
    
    July 6, 2011: &#8220;Having an uber-useless day&#8230;go go gadget &#8216;give a shit!&#8217;&#8230;gadget fail&#8221;
    
    July 7, 2011: &#8220;live, love&#8230;eat cookies&#8221;
    
    July 11, 2011: &#8220;Doubt is a pain too lonely to know that faith is his twin brother&#8221; ~Khalil Gibran
    
    July 13, 2011: Oh&#8230;Adele you conjure up some deeply buried memories&#8230;[Someone Like You][17]
    
    July 16, 2011: International Hug an Ultimate Frisbee Player Day! Oh boy!
    
    July 19, 2011: A note from my friend Michelle: &#8220;We played killer bunnies last night, made me miss you lots and lots&#8221;
    
      * I miss her too.
    
    July 22, 2011: From my darling Sonya: &#8220;Wine was born of angel tears. The laughter of old friends and the lingering rays of an autumn sunset. The land remembers. We remember but we have to be asleep to see. The footsteps of the great beasts and the gentle step of a moccasin are imprinted in the earth&#8217;s memory. There was a time that grapes fed only these creatures, before the domestication of spirit animals and when mist was the veil of truth. This was a time when wine released only lust and bridged the gap between the worlds. Cities have since been built and the courage to conquer and expand beyond our circle of familiar fires has been fueled by the harvest. The masters, the chemists, the dreamers, all using the noble grape as their pallet, have given us fallen mortals a reason to dare, to love beyond boundaries, to imagine thoughts un-thought. Gods have been identified and devils acknowledged in the reflection of the wine&#8217;s surface. There is a mystery here in these bottles; there are stories unfolding, layering uninhibited clarity and numbness. There is a sadness and joy intermixed with gentle breezes. The wine rembmers a fire in the hills and early frost a fog disguised as warmth. The vines are intermixed with our blood, their roots reaching the earth&#8217;s heart. There is no separation. Only completeness and possibilities lay in the hills, awaiting the magician&#8217;s wand. Enjoy.&#8221;
  
    &#8211; Bob Kowalski, label on the back of the bottle
    
    [<img class="aligncenter size-medium wp-image-662" title="wine" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/wine-169x300.jpg" alt="" width="169" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/wine-169x300.jpg 169w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/wine.jpg 406w" sizes="(max-width: 169px) 85vw, 169px" />][18]
    
    Aug 4, 2011: Created a book list of 183 books&#8230;wow, I need to read faster!
    
    Aug 7, 2011: Nepal here I come! 😀 See blog post on Nepal [here][19].
    
    Aug 9, 2011: &#8220;I&#8217;d be lying on your breast right now, having blissfully damned what society would allow ear to heart&#8230;listening to the gentle rhythm of home-home, home-home&#8230;&#8221; ~Cypher, my amazing childhood friend! See whole [piece here][20].
    
    Aug 17, 2011: [<img class="aligncenter size-medium wp-image-663" title="booksuicide" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/booksuicide-300x259.jpg" alt="" width="300" height="259" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/booksuicide-300x259.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/booksuicide-346x300.jpg 346w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/booksuicide.jpg 555w" sizes="(max-width: 300px) 85vw, 300px" />][21][<img class="aligncenter size-medium wp-image-664" title="science" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/science-300x300.jpg" alt="" width="300" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/science-300x300.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/science-150x150.jpg 150w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/science.jpg 600w" sizes="(max-width: 300px) 85vw, 300px" />][22]Aug 26, 2011: Blog: [Power outage kill 34 million state BEAST run, investigator contemplates Linux homicide][23]&#8230;
    
    Sept 9, 2011:
    
      * I found a great website called [Dear Photograph][24] that overlays old and new photographs in the same place, it&#8217;s really cool, take a look!<figure id="attachment_665" style="width: 300px" class="wp-caption aligncenter">
    
    [<img class="size-medium wp-image-665" title="dearphoto" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/dearphoto-300x200.jpg" alt="" width="300" height="200" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/dearphoto-300x200.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/dearphoto-449x300.jpg 449w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/dearphoto.jpg 500w" sizes="(max-width: 300px) 85vw, 300px" />][25]<figcaption class="wp-caption-text">from dearphotograph.com</figcaption></figure> 
    
      * Started the section of my blog called &#8216;Devotion&#8217;&#8230;why? [Read here.][26]
    
    Sept 11, 2011: Anniversary of 9/11, wrote blog on reflecting on 9/11, [saved for something][27]&#8230;
    
    Sept 22, 2011: In talking with friends and family and doing my own &#8216;soul searching&#8217; I wrote a blog about this whole concept of people of differing faiths marrying and &#8216;how that works&#8217; because I grew up with the notion it &#8216;doesn&#8217;t work&#8217; etc&#8230;.my views have since changed. But it is interesting to reflect on this&#8230;Blog: &#8220;[Cascades of mistakes][28]?&#8221;
    
    Oct 4, 2011:
    
    [<img class="aligncenter size-medium wp-image-667" title="teddybears" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/teddybears-300x240.jpg" alt="" width="300" height="240" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/teddybears-300x240.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/teddybears-375x300.jpg 375w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/teddybears.jpg 600w" sizes="(max-width: 300px) 85vw, 300px" />][29]Oct 6, 2011: RIP Steve Jobs 🙁
    
    Oct 15, 2011:
    
      * Blog: In an effort to address the whole concept of &#8216;wanting people to like you&#8217; I wrote a blog on [pursuing praise][30] and how that&#8217;s affected my life and others I know.
      * This comic popped up on my facebook page and I couldn&#8217;t help but laugh.
    
    [<img class="aligncenter size-medium wp-image-668" title="fbmarriage" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbmarriage-214x300.jpg" alt="" width="214" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbmarriage-214x300.jpg 214w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbmarriage.jpg 400w" sizes="(max-width: 214px) 85vw, 214px" />][31]Oct 18, 2011: I love it! [Dr. Seuss takes on Malaria][32]!
    
    [<img class="aligncenter size-medium wp-image-669" title="ann-cover" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/ann-cover-243x300.jpg" alt="" width="243" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/ann-cover-243x300.jpg 243w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/ann-cover.jpg 320w" sizes="(max-width: 243px) 85vw, 243px" />][32]Nov 1, 2011: We went to a frisbee tournament in Phuket Thailand that was a both great and not so great trip. But the conversations that ensued were very interesting&#8230;read about it here: [The good, the bad, the beach][33]. Tyghe also has a [blog][34] about the trip with pictures!
    
    [<img class="aligncenter size-medium wp-image-681" title="phuket1" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phuket1-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phuket1-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phuket1.jpg 320w" sizes="(max-width: 300px) 85vw, 300px" />][35][<img class="aligncenter size-medium wp-image-682" title="melkissphuket" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/melkissphuket-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/melkissphuket-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/melkissphuket-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/melkissphuket.jpg 640w" sizes="(max-width: 300px) 85vw, 300px" />][36]Nov 9, 2011:
    
      * I love historical paleomicrobiology&#8211;its just&#8230;cool. Link to the article [here][37].
    
    [<img class="aligncenter size-medium wp-image-670" title="paleo" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/paleo-300x240.png" alt="" width="300" height="240" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/paleo-300x240.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/paleo-1024x819.png 1024w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/paleo-375x300.png 375w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/paleo.png 1280w" sizes="(max-width: 300px) 85vw, 300px" />][37]
    
      * Floodageddon hits Thailand, but we remained dry&#8230;though the water got close[.<img class="aligncenter size-medium wp-image-671" title="flood" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/flood-224x300.jpg" alt="" width="224" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/flood-224x300.jpg 224w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/flood.jpg 480w" sizes="(max-width: 224px) 85vw, 224px" />][38]
    
    Nov 15, 2011: FIFA, FIFA, FIFA&#8230;Australia versus Thailand. Thailand lost in the end but it was a fun game to watch!
    
    [<img class="aligncenter size-medium wp-image-672" title="FIFA" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/FIFA-200x300.jpg" alt="" width="200" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/FIFA-200x300.jpg 200w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/FIFA.jpg 480w" sizes="(max-width: 200px) 85vw, 200px" />][39]Nov 17, 2011: A friend sent me a video called Vines that I thought was a really nifty little video, well put together for what it was, I watched it several times&#8230;it was just&#8230;fun. [Link to video Vines][40].
    
    [<img class="aligncenter size-medium wp-image-673" title="vines" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/vines-e1325661368665-300x295.png" alt="" width="300" height="295" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/vines-e1325661368665-300x295.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/vines-e1325661368665.png 704w" sizes="(max-width: 300px) 85vw, 300px" />][41]Nov 21, 2011: There was a flood relief charity party on a rooftop that some friends and I went to. Picture snapped from camera phone has a retro feel doesn&#8217;t it?
    
    [<img class="aligncenter size-medium wp-image-674" title="rooftop80s" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/rooftop80s-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/rooftop80s-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/rooftop80s-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/rooftop80s.jpg 640w" sizes="(max-width: 300px) 85vw, 300px" />][42]Nov 23, 2011: &#8220;I have sneezed over 50 times in an hour and a half. I must be allergic to morning, time to go back to bed.&#8221;
    
    Nov 26, 2011: Tyghe and I host Thanksgiving at our place!
    
    [<img class="aligncenter size-medium wp-image-675" title="thanksgiving" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/thanksgiving-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/thanksgiving-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/thanksgiving-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/thanksgiving.jpg 720w" sizes="(max-width: 300px) 85vw, 300px" />][43]Nov 28, 2011: The blog post by SingleDadLaughing entitled: [I&#8217;m Christian unless you are Gay][44] gets over 8,000 comments and 228,000 Likes on Facebook and generates quite the flood of emotion and comments on the web. [I wrote a &#8216;sort of&#8217; response to this on my blog but it was more a reflection.][45]
    
    Dec 1, 2011: Tyghe and I have found a great group of friends in Thailand that love board games as much as we do&#8230;score!
    
    [<img class="aligncenter size-medium wp-image-676" title="settlers" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/settlers-224x300.jpg" alt="" width="224" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/settlers-224x300.jpg 224w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/settlers.jpg 480w" sizes="(max-width: 224px) 85vw, 224px" />][46]Dec 4, 2011: I jetted off the Philly for a conference which was great fun and I successfully procured a wedding dress without having to navigate &#8220;your fat&#8221; comments&#8230;woohoo! 😀
    
    Dec 15, 2011: My travels continued, this time with Tyghe as we went to Vietnam for a frisbee tournament see both our blogs for highlights! ([Vietnam: play by play][47]&#8211;Mine).
    
    Dec 22, 2011: In my devotional book thats apart of this series of blogs I write on &#8216;devotion&#8217; the subject was love. How often should you tell someone you love that you love them? It was an interesting question to ponder&#8211;if one should vocalize adoration and how much is too much? [Blog: Love and Riddles][48].
    
    Dec 24, 2011: A friend of ours had a great Christmas Eve Party at her house, it was a great night hanging out with friends.
    
    [<img class="aligncenter size-medium wp-image-677" title="xmaspairow" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmaspairow-300x225.jpg" alt="" width="300" height="225" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmaspairow-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmaspairow-400x300.jpg 400w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmaspairow.jpg 640w" sizes="(max-width: 300px) 85vw, 300px" />][49]Dec 25, 2011: Christmas! After church we made a huge breakfast, opened gifts and generally lazed about before hanging out with my boss and his wife for the evening.
    
    [<img class="aligncenter size-medium wp-image-678" title="xmasbrkfast" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmasbrkfast-300x224.jpg" alt="" width="300" height="224" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmasbrkfast-300x224.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmasbrkfast-401x300.jpg 401w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmasbrkfast.jpg 642w" sizes="(max-width: 300px) 85vw, 300px" />][50]Dec 31, 2011: It was a crazy year of travels, work, food, frisbee and what not. We rang in the new year at an outdoor restaurant with a friend of ours near our home in Ari. It was a great night, it&#8217;s been an adventure of a year and it was fun to reflect on everything that happened and to see how my views and thoughts may have changed.
    
    Next year promises to be just as busy and crazy with lab work, manuscripts, a planned trip to Cambodia, continuing to work through my devotion series to learn and figure out more about on my faith, finishing up wedding planning and oh ya&#8230;getting married. And of course you all know the world is supposed to end a-g-a-i-n when? I think this time it&#8217;s Dec 21st? So we&#8217;ve all that to look forward too as well!
    
    [<img class="aligncenter size-medium wp-image-683" title="maya" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/maya-252x300.jpg" alt="" width="252" height="300" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/maya-252x300.jpg 252w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/maya.jpg 300w" sizes="(max-width: 252px) 85vw, 252px" />][51]Sawasdii Bii Mai, Happy New Year.
    
    &nbsp;

 [1]: http://xkcd.com/840/
 [2]: http://www.amazon.com/Phylogenetic-Networks-Concepts-Algorithms-Applications/dp/0521755964
 [3]: http://www.mendeley.com/research/when-zombies-attack-mathematical-modelling-of-an-outbreak-of-zombie-infection/
 [4]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/zombies.png
 [5]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbbutterfly.jpg
 [6]: http://www.melaniemelendrez.com/2011/08/18/using-craigslist-messages-for-syphilis-surveillance/ "Using craigslist messages for syphilis surveillance…"
 [7]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna1.jpg
 [8]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbvienna2.jpg
 [9]: http://xkcd.com/877/
 [10]: http://www.amazon.com/Three-Cups-Tea-Mission-Promote/dp/0143038257
 [11]: http://tdn.com/rose-three-cups-of-whatever/article_605c5fb2-334d-11e1-995c-0019bb2963f4.html
 [12]: http://www.amazon.com/Half-Sky-Oppression-Opportunity-Worldwide/dp/0307267148
 [13]: http://www.phdcomics.com/comics.php?f=1431
 [14]: http://forums.macresource.com/read.php?1,1155221,1155233
 [15]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/spider.jpg
 [16]: tygertown.us
 [17]: http://www.youtube.com/watch?v=NAc83CF8Ejk
 [18]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/wine.jpg
 [19]: http://www.melaniemelendrez.com/2011/09/20/nepal-chaos-color-and-a-lot-of-monkeys/ "Nepal: chaos, color and a lot of monkeys…"
 [20]: http://www.youtube.com/watch?v=ZN_lpKz6FQk
 [21]: http://intrawebnet.com/pics/funny-pics/a-book-commits-suicide-every-time-you-watch-jersey-shore/
 [22]: http://twitpic.com/63lcfq
 [23]: http://www.melaniemelendrez.com/2011/08/26/power-outage-kills-34-million-state-beast-run-investigator-contemplates-linux-homicide/ "Power outage kills 34 million state BEAST run, Investigator contemplates Linux homicide"
 [24]: http://dearphotograph.com/
 [25]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/dearphoto.jpg
 [26]: http://www.melaniemelendrez.com/2011/09/09/devotion-as-devotion-does/ "Devotion as devotion does…"
 [27]: http://www.melaniemelendrez.com/2011/09/11/saved-for-something/ "saved for something…"
 [28]: http://www.melaniemelendrez.com/2011/09/22/cascades-of-mistakes/ "Cascades of mistakes?"
 [29]: http://www.flickr.com/photos/summermyass/6374189483/
 [30]: http://www.melaniemelendrez.com/2011/10/15/suck-less-suck-less/ "suck less, suck less…"
 [31]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/fbmarriage.jpg
 [32]: http://www.history.navy.mil/library/online/thisisann.htm
 [33]: http://www.melaniemelendrez.com/2011/11/01/the-good-the-bad-the-beach/ "the good, the bad…the beach"
 [34]: tygertown.us "Where is home?"
 [35]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/phuket1.jpg
 [36]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/melkissphuket.jpg
 [37]: http://schaechter.asmblog.org/schaechter/2011/11/a-pestis-from-the-past.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+schaechter+%28Small+Things+Considered%29
 [38]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/flood.jpg
 [39]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/FIFA.jpg
 [40]: http://www.youtube.com/watch?v=i1iiiEgqgT4
 [41]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/vines.png
 [42]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/rooftop80s.jpg
 [43]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/thanksgiving.jpg
 [44]: http://www.danoah.com/2011/11/im-christian-unless-youre-gay.html
 [45]: http://www.melaniemelendrez.com/2011/11/28/god-bless-no-strings-attached/ "God bless…no strings attached"
 [46]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/settlers.jpg
 [47]: http://www.melaniemelendrez.com/2011/12/20/play-by-play/ "Play by play, frisbee in Vietnam"
 [48]: http://www.melaniemelendrez.com/2011/12/22/love-and-riddles/ "Love and Riddles: How often do you say you love someone important to you?"
 [49]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmaspairow.jpg
 [50]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/xmasbrkfast.jpg
 [51]: http://www.melaniemelendrez.com/wp-content/uploads/2012/01/maya.jpg
