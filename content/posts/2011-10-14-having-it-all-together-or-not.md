---
title: Having it ‘all together’–or not.
author: Mel
type: post
date: 2011-10-14T05:25:42+00:00
url: /2011/10/14/having-it-all-together-or-not/
categories:
  - Life or something like it
tags:
  - PhD
  - work

---
I thought this was funny&#8230;and I could relate to some of the &#8216;evaluations&#8217; so I thought I would share. These quotes were taken from federal employee evaluations, crazily enough. I wonder if advisers/bosses knew that their employees could read the evaluations they&#8217;d still write things this harsh? Probably.

  1. Her women would follow her anywhere, but only out of morbid curiosity.
  2. Since my last report she has reached rock bottom and has started to dig.
  3. When she opens her mouth, it seems that this is only to change whichever foot was previously in there.
  4. This young lady has delusions of adequacy.
  5. This employee should go far, and the sooner she starts the better (My Ph.D. advisor said something akin to this to me!)
  6. She&#8217;s not the sharpest knife in the drawer. (THIS TOO!..My advisor said something similar to this to me as well!)
  7. She&#8217;s about as bright as Alaska in December.
  8. She&#8217;s so dense light bends around her.
  9. She has a full six pack but lacks the plastic thingy to hold it all together.

Ouch. We all have moments of blatant stupidity for sure and while I can laugh about these comments now as they are painfully funny&#8230;when my advisor was suggesting such things it was mortifying.
