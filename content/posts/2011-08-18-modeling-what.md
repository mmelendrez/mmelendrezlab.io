---
title: Modeling…What???….
author: Mel
type: post
date: 2011-08-18T09:32:32+00:00
url: /2011/08/18/modeling-what/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"7eddb45a9d";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - I will do Science to it!

---
## I found this note in my facebook archives and thought I would reshare it on this blog. It was following a trip to Imperial College, London where I took a Mathematical Modeling course. Posted: Sep 18, 2009.

&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;-

So I&#8217;ve been at a course at Imperial College in London for the past 2 weeks attempting to turn myself into somewhat of a mathematical modeler (please keep your uncontained laughter to a dull roar). It&#8217;s been quite intense and very enjoyable and frustrating, all in all doing exactly what it promised to do.
  
<!--more-->The following is a surmise of the ideal and reality of modeling infectious disease as I have learned from first hand experience at Imperial College:

This particular example comes from an attempt to model Avian Influenzae outbreaks in chicken farms in the UK, we have the raw data, we want to then look at it, develop a model, predict the outcome of the epidemic (if there is one) and then implement a &#8216;policy&#8217; that would prevent such an outbreak in the future&#8230;Right then&#8230;

THE IDEAL PLAN when you go into modeling is all kinds of optimistic:
  
![][1]The plan ideally.

So indeed we have this pie in the sky dream of going from (1) raw data as examined in Excell to (2) Modeling that data using Berkley Madonna (BM) a modeling program to (3) Constructing infectious trees (Using Haydon2 software) so that we will know where the first infectious farm was that ignited the epidemic then (4) Finally get into some spatial modeling of the transmission of the disease (R0), taking tea breaks along the way and at the end there will be much rejoicing and back patting.

THE REALITY IS&#8230;..MUCH DIFFERENT
  
![][2]Reality

Things to Note:
  
1. We did learn A LOT
  
2. The demonstrators (helpers) were fantastic
  
3. We didn&#8217;t finish, due to needing to run off and hear our next lecture
  
4. One of our group members attempted to tackle spatial modeling, it remains to be seen if the arrow pointing at her actually shot her dead sending her straight back down to our reality
  
5. Can you see where we all contemplated mass suicide?
  
6. There was some rejoicing
  
7. There were times when there was no rejoicing
  
8. Tea breaks were taken
  
9. Modeling is hard
  
10. The learning curve is a vertical ascent rather than a rolling hill&#8230;and I left my climbing gear in Bozeman.
  
11. In the end you don&#8217;t want tea so much anymore, rather a stiff drink.
  
12. After 2.5 days of this you start seeing spots and spelling things wrong like Excell&#8211;>Excel.

Cheers.

 [1]: http://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-snc1/10534_554925443239_43807988_33038840_4685812_n.jpg
 [2]: http://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-snc1/10534_554925917289_43807988_33038892_5104784_n.jpg
