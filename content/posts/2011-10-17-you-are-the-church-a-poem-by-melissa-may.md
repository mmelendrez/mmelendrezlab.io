---
title: You are the church, a poem by Melissa May
author: Mel
type: post
date: 2011-10-17T05:14:52+00:00
url: /2011/10/17/you-are-the-church-a-poem-by-melissa-may/
categories:
  - Poetry
tags:
  - church

---
A friend of mine forwarded this poem to me and I thought it was just really interesting and lovely, so I am sharing it here. I love poetry.

&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8211;<!--more-->

> When you realize that you are a church, you
  
> should be seated, so you can position
  
> yourself toward the sky
  
> Should give thanks to the hands that molded
  
> the bright arch of your cathedral shoulders
  
> Your pipe-organ throat
  
> Your padded and well worn thighs
> 
> You need to get comfortable with worship
  
> With the idea of the tide singing out its song
  
> of redemption in the house of your
  
> stained-glass belly
  
> You are an open sanctuary
  
> You are a gathering of sinners
  
> You are more than ritual, you are where
  
> God meets earth to kiss his kingdom electric
> 
> When you realize that you are a church, you
  
> should stop to examine the orange cherry of smoke
  
> between your fingers
  
> You are cedar and ceremony
  
> You are more clean healing than baptism and blood
  
> sacrifice
  
> You are an edifice
  
> You are a pilgrimage that believers will flock to
  
> They will kiss the knot of your belly button and believe again
  
> They will call the bells chiming in your pulse an experience
  
> They will want to return
> 
> When you realize that you are a church, you
  
> will play with the idea of sacrilege
  
> You will wonder if recognizing the divinity in your
  
> simple flesh is blasphemous
  
> If you will be labeled a heretic for your audacious joy, for
  
> the invitation to house broken things inside you
  
> You have to remember
  
> We get to choose what kind of place we become
  
> With so many train stations and death beds and dark corners
  
> in this world
  
> Realizing what your four walls were built for can never be an act that
  
> will separate you from Creation
> 
> You were made to be a church
  
> You are open doors and hallelujah
  
> You are a place where anyone can come and feel safe
  
> You were made this way
  
> You are the holy of holies
  
> You are a temple of righteous beauty
  
> You are a new tower where Babel loses its ability
  
> to divide us
> 
> When you realize you are a church, you can start
  
> appreciating how an imperfect thing can still
  
> have purpose
  
> You will start serving communion out of the cup in your
  
> hands
  
> You will mend socks and broken hearts
  
> You will be dirty
  
> You will trade your collection plate for
  
> a few stock pots
  
> You will start cooking when you are hungry
  
> Start feeding when you are lonely
  
> You will start seeing joy bud on the horizon
  
> like a reluctant Spring
> 
> When you realize that you are church
  
> A place where God can be found
  
> You will have to part with the idea that you
  
> are beyond redemption
  
> Will have to stop calling his house too big, too tall
  
> too fractured, too complicated
  
> You are a kingdom
  
> You are manifested beauty
  
> You are a spark of divine favor
  
> You are on every street corner, you are
  
> hope and wholeness
> 
> You are a CHURCH
> 
> When you realize it, throw open
  
> your doors.
> 
> We&#8217;ve all been waiting.
> 
> ~Melissa May
