---
title: Thanksgiving in…Liberia?
author: Mel
type: post
date: 2011-11-23T05:59:25+00:00
url: /2011/11/23/thanksgiving-in-liberia/
categories:
  - Life or something like it
tags:
  - history
  - indians
  - thanksgiving

---
I knew Thanksgiving was celebrated in the USA and Canada but I didn&#8217;t know they celebrated [Thanksgiving in Liberia, apparently it coincides with the Church&#8217;s harvest day][1]. Interesting. I wonder if there&#8217;s a history of celebrating Thanksgiving there or if this was a one time 2010 venture.

This year given I have been taken in many times for holidays we are hosting Thanksgiving at our place. Though, I am not making a turkey as they are scarce and expensive in Thailand. Thais do not celebrate Thanksgiving but its always nice to have an excuse to go to someone&#8217;s house to eat and partake in general merriment celebrating what American children learn as our &#8216;dinner with the Indians&#8217;. Indians who we then learn later in school, we decimated with smallpox, measles, typhus and plague among other diseases and war. DOH!<!--more-->

When I looked up Thanksgiving (in Wikipedia of course) it actually doesn&#8217;t mention a lot about the &#8216;traditional&#8217;  Thanksgiving story we learn as kids. There is one small mention is wikipedia involving Indians:

> While initially, the Plymouth colony did not have enough food to feed half of the 102 colonists, the Wampanoag Native Americans helped the Pilgrims by providing seeds and teaching them to fish.

Instead it talks abundantly about how Thanksgiving is and was celebrated as a harvest festival by the settlers of North America. New world, scarce food&#8230;when there&#8217;s a good harvest its cause to celebrate&#8230;.makes sense.

In Thanksgivings I&#8217;ve attended and when I was little we all went around the table and spoke of what we were thankful for that year&#8230;so I&#8217;ve compiled my list. I am thankful for many things, it has been an eventful year for sure. The obvious:

  * Finishing my Ph.D.

the perhaps less obvious but still very important:

  * Tyghe helping me maintain sanity many times
  * allergy medication
  * the amazing friends we&#8217;ve met here and still have back in the U.S.

Looking at the past year I have a lot to be thankful for. Some amazing things have happened and I&#8217;ve been given some great opportunities. I got a fellowship, I got to move around the world, I&#8217;m now published, I&#8217;m now engaged, I have a good relationship with my family and friends (far as I know) :-P, I have food and shelter (unlike many affected by the floods in Thailand)&#8230;

Sometimes during particularly difficult situations in my life I struggled to find anything good of truly happy in my life. This happened a lot to me in college. College wasn&#8217;t hell on earth for me but it certainly wasn&#8217;t the happiest part of my life for sure. The happiness was few and far between and consisted mostly of when I got to go home to Hawaii or go on forensics trips or hang out with certain friends. More and more I realize how much I disliked college, though it wasn&#8217;t horrible&#8211;I don&#8217;t go out of my way to &#8216;remember&#8217; it either.

BUT I am VERY thankful I got to go to college, again, something many people I know are never able to realize for financial reasons. I may be in debt from it, but I am so thankful I got to go.

I&#8217;m starting a new tradition for myself&#8230;hopefully it&#8217;ll turn into something I do every year. For myself, certain people I am just so thankful for their presence in my life and what they&#8217;ve been able to help me see, experience, understand or generally just being there for me. So I am writing them thank-you notes. And it changes every year I am sure as people walk in and out of our lives, not that we forget them but some years certain people are more involved in our lives than others and that&#8217;s ok. So this year I am going to sit down and reflect on the friends and family that have brought joy to my life and express my thankfulness through a really belated letter &#8230;as I just came up with this now and tomorrow is Thanksgiving&#8211;DOH!

I&#8217;ve written in several blogs that people often times don&#8217;t know and never know the impact they have on others. Everyone wants to feel like they matter to someone. Knowing that specific things they&#8217;ve done or said have made a difference, helped in some way or just been generally appreciated. So on Thanksgiving, I am just simply going to say thank you to the people that have done that for me this year in my life.

> <span style="font-family: georgia,bookman old style,palatino linotype,book antiqua,palatino,trebuchet ms,helvetica,garamond,sans-serif,arial,verdana,avante garde,century gothic,comic sans ms,times,times new roman,serif;"><span style="font-family: georgia,bookman old style,palatino linotype,book antiqua,palatino,trebuchet ms,helvetica,garamond,sans-serif,arial,verdana,avante garde,century gothic,comic sans ms,times,times new roman,serif;">We can only be said to be alive in those moments when our hearts are conscious of our treasures.  ~Thornton Wilder</span></span>

 [1]: http://www.emansion.gov.lr/press.php?news_id=1709&related=
