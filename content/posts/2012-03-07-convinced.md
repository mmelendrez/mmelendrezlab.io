---
title: convinced?
author: Mel
type: post
date: 2012-03-07T04:12:52+00:00
url: /2012/03/07/convinced/
featured_image: /wp-content/uploads/2012/03/word_of_god_jesus1-320x288.jpg
categories:
  - Devotion
tags:
  - Bible
  - debate
  - faith
  - history
  - Word of God

---
Devotional Blog:

The Word of God, 03/02/2012, 2 Timothy 3:16

> All Scripture is God-breathed and is useful for teaching, rebuking, correcting and training in righteousness. ~2 Timothy 3:16 (NIV)

So I figured I should start chipping away at all the dog eared pages in this devotional book that I&#8217;ve been neglecting. I keep up on my daily reading but somedays I have enough time to blog and other days I don&#8217;t so they end up dog eared for future contemplation. Oddly enough this entry is about &#8216;bearing each others burdens&#8217; rather than the word of God but when I read it, I realized I needed to address the &#8216;word of God&#8217; topic first. I&#8217;ll explain&#8230;

> From the devotional: &#8220;We all need to make time for Bible study. David writes in Psalm 73:26, &#8216;My flesh and my heart may fail, but God is the strength of my heart and my portion forever.&#8217; (NIV)&#8230;Only as I make God&#8217;s Word a priority do I have anything to give. A Bible study should be a refuge, a safe harbor.&#8221;

A week or so ago in a conversation with friends I stated that scripture will never &#8216;convince&#8217; me of whether someone is &#8216;right or wrong&#8217;. No one will ever &#8216;win&#8217; an argument with me only using scripture. I think I&#8217;ve even stated in previous blogs that I refuse to go tit for tat on Bible verses for various reasons aside from its just plain tedious and I feel its more &#8216;posturing your verse memorization prowess&#8217;, rather than attempting to make a valid point. For the first time now, I am wondering why that is. I am a Christian after all and I believe in God&#8217;s word, why isn&#8217;t God&#8217;s divinely inspired word enough to convince me of someone&#8217;s argument?

<!--more-->

What is the Word of God? My inner internet junkie takes over&#8230;

> &#8230;the Word of God is NOT a leather bound book. It is not ink on paper. It is the words and life of Jesus Christ through us. We manifest the Word of Christ, but it is not limited to being a book in our traditional physical formats. It’s not limited to digital methods of reading like on an iPhone or Kindle e-reader, either [[Link][1]]. (Steve B, missionary in Peru).

[This webpage][2] makes some interesting points looking at what Christian&#8217;s think of when they think of the &#8216;Word of God&#8217;, it has a lot of verses in it as well for reference, but in short it states:

> According to Scripture, the Bible contains the words (plural) of God. This does not in any way detract from the title of The Word of God that belongs to Jesus.
> 
> The trouble is that because we are so used to calling the Bible The W/word of God we automatically think of the Bible when The W/word of God is mentioned. How often have you heard the preacher or evangelist say &#8220;Let us open the word of God to..&#8221;
> 
> This is Scripturally incorrect. Because it is second nature for so many of us to think of the Bible as the W/word of God we often give it the place that Jesus should have. The result is that we become Bible dependant rather than Jesus dependant. We live in accordance with the Scriptures, which is good, but it leads us to depend on the Scriptures rather than the experiential life we should have with Jesus.

On [another Bible study site][3] a man justifies why God&#8217;s word can be &#8216;fallible&#8217; and yet still true:

> <span style="font-size: small;">&#8220;Much of Christianity works from the premise that the Bible as a whole is the infallible word of God. But no sooner does one begin to question some part of it than a Bible believing Christian indicates that to reject any one part of the Bible is to render the entire book irrelevant. The flow of logic supposes that if one part can be rejected, then it&#8217;s open season to reject any part one doesn&#8217;t like. Therefore the Bible as a whole is viewed as God&#8217;s flawless revelation to man. While on the surface this logic sounds reasonable, it is not necessarily true.&#8221;  &#8220;&#8230;it is also interesting to note that in demanding this standard, Christianity has tied its own noose. There are atheists who would like to discredit the entire book, and there are many Jews who would like to discredit the New Testament. So if either group can demonstrate even one little error in the New Testament they both win by Christianity&#8217;s own standards!  Christian teachers need to come to grips with the fact that there are numerous significant errors that cannot be reconciled with the classic bend-over-backwards apologetics of the past. Visit any atheistic or Jewish anti-missionary website and brace yourself. By demonstrating error in the Bible, those opposed to God and Christianity have proven that the New Testament is no longer the infallible word of God. These now have legitimate grounds on which to continue rejecting the truth that the Bible<strong> does</strong> contain. Christianity has to a large degree handed them this logic and win on a silver platter.  Speaking for myself, I no longer begin with the premise that the Bible is infallible cover to cover. Blind faith in any book is dangerous. But what I believe to be the truth is far more objective than being a simple matter of picking and choosing what suits any particular fancy. Here are the presuppositions that I work from;</span>
> 
>   * <span style="font-size: small;">The God of Abraham, Isaac, and Israel is the one true Most High God, and the creator of the heavens and earth. </span>
> 
>   * <span style="font-size: small;">His word is truth, and it can be found in the books of Moses, and the prophets, as well as in the words of Yahshua. What these men actually said never conflicts with the others. Yahshua&#8217;s words would be the same as those found in red in a red-letter edition Bible. </span>
> 
>   * <span style="font-size: small;"><strong>The truth will always be consistent with itself </strong>with <strong>no</strong> contradictions. It is therefore assumed, that in the rare cases when contradiction is found, it is due to man&#8217;s influence over the centuries. These contradictions are almost never more than one passage standing against numerous others, and the favor always goes with the majority</span>

<span style="font-size: small;">Google &#8220;word of God&#8221; or &#8220;using the Bible in debates&#8221; and you&#8217;ll get any number of opinions and sites that deal with this topic on the authority, fallibility, contradictions, truths within the Bible, the Word of God.</span>

> <span style="font-size: small;">For the word of God is living and active. Sharper than any double-edged sword, it penetrates even to dividing soul and spirit, joints and marrow; it judges the thoughts and attitudes of the heart.~Hebrews 4:12</span>

<span style="font-size: small;">What about me? Do I believe in the Bible? Yes. Do I believe it is the Word of God? Yes. Do I take it literally?&#8230;Ah, now comes a matter of definition, something my friends and I discussed. I think there is a difference in reading the Bible literally versus &#8216;flatly&#8217;. In my mind to read something flatly is to read it without context. The Word of God may have been divinely inspired but it was still written by the hand of man and I am sure in order to make the stories and lessons and history understandable to the masses it was written with them in mind. With the culture in mind. To read flatly would allow us to stone ladies for not wearing head coverings, something that we were released from in the New Testament for instance. Do I read literally? Yes, I believe the Israelites fought battles, I believe Jesus lived and healed people, died on a cross, rose again&#8230;I believe the Kings of the Old Testament existed, I believe God manifested himself in the various ways described&#8230;such as when the prophet <a href="http://en.wikipedia.org/wiki/Elijah">Elijah took on the prophets of Baal in the Old Testament</a>. I believe God created the world and science is the discovery of just how intricate and awesome his creation is. That being said I believe God created things to be able to evolve and adapt and I think religion and science co-exist just fine&#8211;but that is a conversation for a different blog. The Bible is history. The Bible is guidance from God but no one can claim to be the end all be all about what the Bible says and claim their &#8216;way of reading it&#8217; or interpretation is the &#8216;only&#8217; interpretation. There are scholars that spend their lives studying the Bible, the context and culture in which every chapter was written, who wrote it? why they wrote it? how they wrote it? The culture in which they wrote it&#8230;the original language that they wrote in. Until you understand everything surrounding the Bible you cannot claim to be the only authority on its interpretation, all you can do it read it, study it, seek counsel from those wiser than you in their study of the Bible and ask God for guidance on what it means.</span>

<span style="font-size: small;">So what bothers me about Bible verse banter? Perhaps its the attitudes of those that use the Bible as a weapon rather than inspiration. &#8220;Here is my Bible, now I shall beat you with it til you submit to my awesome ability to look up and memorize verses.&#8221; Perhaps its because for every verse lobbed at you in debate, you can most likely lob another verse to contradict it which then becomes a pissing battle of who can lob the most verses&#8211;it becomes a matter of the quantity of the Word of God rather than the quality of it. Perhaps its because I feel like people are replacing intimacy and a true relationship with God&#8211;replacing attempts to have a better understanding of God&#8212;replacing all that with black and white print, defaulting to a just a book rather than having that book among other things, influence and grow their actual faith.</span>

<span style="font-size: small;">Faith is individualized. In order for faith to be &#8216;real&#8217; one has to justify and figure it out for themselves. Blindly believing in a book concerns me in some ways. The Word of God is part of my faith, its not the sole factor defining my faith. My faith is my relationship, belief and attempts to seek after God. To do that, yes, I consult the Bible, I read it, I attempt to understand it but I will never justify a belief or action simply because &#8216;the Bible says so.&#8217; For those of you horrified at that statement let me give an example:</span>

<span style="font-size: small;">The Bible says &#8216;do not kill&#8217;, it&#8217;s a commandment part of the central tenants of Christian faith&#8211;follow God&#8217;s commandments. Ok, absolutely I will not kill, not ONLY because the Bible says so but because really you ought not kill someone&#8211;that&#8217;s my humanity (which God created) coming out. That&#8217;s also societies influence, society supports do not kill as well. So yes the Bible has influence over me but its never been the sole justification for my actions in life. Not so far at least.</span>

<span style="font-size: small;">I came across <a href="http://www.religiousforums.com/forum/religious-debates/100596-people-who-cite-biblical-verses-debates.html">a forum</a> that was discussing this issue of debating using the Bible. They make a good point that when you are having a religious debate you use religious sources. But when you are having a debate with issues that encompass secular views you cannot &#8216;expect&#8217; a person to be convinced by the Bible alone.</span>

> It&#8217;s frustrating how some people involved in religious debates always fill their rebuttals with biblical verses that no one can understand. What happened to being original and developing your own logical thought process to defend your beliefs? Hiding behind a book doesn&#8217;t make you credible &#8211; it only shows that you&#8217;re skillful with copy/paste. ~ElationAviation, Open Minded
> 
> <span style="font-size: small;">What I gets me are the ones who want to justify interfaith or secular topics on a specific set of scriptures. Using your scripture to explain your belief is one thing, but trying to use that passage to explain why another belief is wrong is another. Why would a non- Christian accept something from the New Testament as proof of anything, or a non-Muslim accept verses from the Qur&#8217;an? ~Tarheeler, Jewish Conversion Student.</span>

I&#8217;m young, whose to say someday someone won&#8217;t successfully &#8216;zing&#8217; me with a Bible verse in an argument and I am like &#8216;oh wow, profound&#8217; but in the meantime, I&#8217;m a Christian&#8230;I believe in the Bible, I just also believe that a book should facilitate rather than replace an actual relationship or intimacy with God and I believe that people should be able to think for themselves and form compelling logical arguments. God gave us the gifts of reasoning and intellect&#8230;we should use them.

 [1]: http://www.fireonyourhead.org/2011/07/07/what-is-the-word-of-god/
 [2]: http://www.all4god.net/word_of_god.htm
 [3]: http://www.judaismvschristianity.com/word.htm
