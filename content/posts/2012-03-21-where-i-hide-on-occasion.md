---
title: where i hide on occasion…
author: Mel
type: post
date: 2012-03-21T07:49:28+00:00
url: /2012/03/21/where-i-hide-on-occasion/
categories:
  - I will do Science to it!
  - Life or something like it
tags:
  - life

---
Now to be fair, this was a particularly hectic week and the &#8216;busiest&#8217; my desk has been but upon reflection it pretty much sums up my existence as a postdoc&#8230;&#8221;semi-organized chaos&#8221;.

[<img class="aligncenter size-full wp-image-820" title="desk" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/03/desk.jpg" alt="" width="1504" height="992" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/03/desk.jpg 1504w, http://www.melaniemelendrez.com/wp-content/uploads/2012/03/desk-300x197.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/03/desk-1024x675.jpg 1024w, http://www.melaniemelendrez.com/wp-content/uploads/2012/03/desk-454x300.jpg 454w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" />][1]

Click on the picture to enlarge.

What you don&#8217;t see is the 3 pairs of shoes under the desk along with 2 bags and flat rate mailing boxes. On the cubicle wall out of view are several postcards from friends in Boston, Jackson, WY&#8211;now Australia and Great Falls, MT, 3D dengue sequence structures and a post it note to pay bills&#8211;the line between normality and nerd is definitely a blurry one.

 [1]: http://www.melaniemelendrez.com/wp-content/uploads/2012/03/desk.jpg
