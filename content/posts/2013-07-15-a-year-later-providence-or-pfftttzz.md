---
title: A year later…providence or pfftttzz…
author: Mel
type: post
date: 2013-07-15T14:05:10+00:00
url: /2013/07/15/a-year-later-providence-or-pfftttzz/
al2fb_facebook_error:
  - 'Get me: Facebook error: Error validating access token: Session has expired at unix time 1347626611. The current unix time is 1373898302.'
al2fb_facebook_error_time:
  - 2013-07-15T14:25:03+00:00
categories:
  - Life or something like it
tags:
  - dreams
  - life
  - purpose
  - quotes

---
So in a random turn of events I just decided to check this blog&#8230;today is July 15, 2013. My last entry was July 16, 2012. Coincidence?

I find it humorous that this blog has taken the form of most of my journals (diaries) where I have fits where I write everyday and periods of no activity until one day I randomly decide to write again. I don&#8217;t particularly advertise this blog aside from the facebook linking for family and friends that might be interested and I think you can find it via google searching. But this white screen that I type into is more for reflection than anything else, if others derive benefit from that, great.

Perhaps it&#8217;s providence that I&#8217;ve decided to check my blog. My last entry was about &#8216;dreams&#8217; and where I was at. Since then life has been eventful but I still struggle with what will make me happy in my work&#8230;similar musings as to my last entry. Since then, I&#8217;ve gotten married, been quite productive if not incredibly frustrated at work, been back to Thailand to teach a workshop, been to Europe to attend a workshop, started a teaching blog for things I learn at workshops, started online newsletters for my field so others can tap into what I find on the internet, finished up a teaching fellowship, explored DC during free time&#8230; Life is clipping along as it should&#8230;

Somethings I&#8217;ve figured out in the past year:

<!--more-->

  1. Some of us are West-Coasters, Some of us are East-Coasters&#8230;Some of us can coast any Coast&#8230;I am a West-Coaster and will seek to move back eventually.
  2. Bat viruses are cool.
  3. Analyzing influenza makes me fall asleep&#8230;I seriously don&#8217;t know what it is with that virus but my mental capacity looks at the data and just wants to take a vacation immediately.
  4. I am not as awesome a multi-tasker as I once thought.
  5. I&#8217;m a pretty good teacher.
  6. Perhaps I should go into teaching.
  7. Blogging/writing about science is frightening&#8230;and&#8230;I can&#8217;t pinpoint why. I envy Carl Zimmer&#8217;s ablilities!
  8. I still want a sabbatical.
  9. DC can rival Bangkok in the summer.
 10. &#8230;

One thing I&#8217;ve learned this year is how few people there are available out there who do what I do. My field now combines, virology, biology, microbiology, molecular biology, programming, software development, Linux environments, mathematics and statistics&#8230;The field of bioinformatics is growing so rapidly as we&#8217;ve produced way more data than we can ever analyze and we don&#8217;t have enough people trained to analyze it. So I&#8217;m in a high demand field&#8230;that I&#8217;m decent at&#8230;that I only sort of like&#8211;doh! Why is that?

The last thing I&#8217;ve learned is that I need to stop being a vain little b*tch and stop wondering if &#8216;I matter&#8217;. It really is a petty thing to wonder. So many of us value our lives based on some sort of metric. In my case, if I don&#8217;t feel like what I do matters, then by proxy I don&#8217;t &#8216;matter&#8217;&#8230;and I want to matter. I&#8217;ve struggled for years with what I do because I can&#8217;t see the tangible difference it makes, if any. I&#8217;m the lab rat&#8230;I&#8217;m the girl behind the computer (or pipette) generating pictures from data&#8230;I may never see the benefits if any of the things I produce in my field. Doctors, nurses, humanitarian relief personnel, freakin party planners! etc&#8230;they see everyday how their work matters. They see how it affects/helps people. Ok, so I have no desire to be a party planner.

I&#8217;ve been at my job now for a year, it has taught me one thing&#8230;how to prove to yourself you matter. We are constantly having to justify why we get paid on grants and reports. I get paid to conduct surveillance on infectious diseases (mostly dengue and flu at the moment) that circulate globally. I get paid to study viruses from a genetic perspective in order to inform vaccine development efforts for dengue virus. In truth, I like only 30% of my job (what I just mentioned above + my opportunities to teach others). The other 70% is stress, paperwork, red tape, more red tape, more paperwork, being saddled with more than I feel I can handle and yet still being expected to get it all done, more stress, having to deal with areas I am not an expert or qualified to deal with, having to do other peoples jobs that they won&#8217;t do themselves, feeling like more of a &#8216;pet&#8217; with a needed skill set than a researcher (though this is lessening which is good)&#8230;

That&#8217;s up from 20/80 this time last year so that&#8217;s progress at least.

Overall though, I get to do some pretty cool stuff&#8211;so while I cannot guarantee staying where I am at long term, I am here for now and I will make the most of it. First step? Stop my whining&#8230;because it&#8217;s starting to f*ckin annoy me. ha.

I used to console myself with Emerson&#8217;s quote when I was unhappy about the trajectory of my life:

> “The purpose of life is not to be happy. It is to be useful, to be honorable, to be compassionate, to have it make some difference that you have lived and lived well.”
  
> ― [Ralph Waldo Emerson][1]

It&#8217;s a good view, although a bit of a let down for those of us who&#8217;d also like to actually &#8216;be happy&#8217;&#8230;.I think perhaps Eleanor Roosevelt&#8217;s view is a little more up my alley now.

> <div>
>   “The purpose of life is to live it, to taste experience to the utmost, to reach out eagerly and without fear for newer and richer experience.”<br /> ― <a href="http://www.goodreads.com/author/show/44566.Eleanor_Roosevelt">Eleanor Roosevelt</a>
> </div>

&nbsp;

 [1]: http://www.goodreads.com/author/show/12080.Ralph_Waldo_Emerson
