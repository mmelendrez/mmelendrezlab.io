---
title: Tribute to Edgar Allen Poe on his Birthday…
author: Mel
type: post
date: 2012-01-19T07:36:11+00:00
url: /2012/01/19/tribute-to-edgar-allen-poe/
categories:
  - Poetry
tags:
  - childhood
  - edgar allen poe
  - poetry
  - the raven
  - tribute

---
I wrote this in high school as an English project and figured I would post it as my tribute to Edgar Allen Poe on his birthday, which is today January 19th. It&#8217;s a parody of his poem [&#8216;The Raven&#8217;.][1] Now it was an English project so I had to copy the meter and style and everything&#8230;looking back I think it was a pretty decent job. That and well&#8230;I was an odd teenager, I still am odd&#8230;just not a teenager anymore.<!--more-->

> > ###### **The Cake: A Dieters Nightmare**
> > 
> > ###### A parody of &#8220;The Raven&#8221; by Edgar Allen Poe
> > 
> > ###### By: MCM
> > 
> > ###### (copyrighted MCM 1996 because it&#8217;s mine, not yours)
> > 
> > &nbsp;
> > 
> > ###### Once upon a nightime glowing, down the stairs sleek and slowly,
> > 
> > ###### To the dark and foreboding basement of long before,
> > 
> > ###### As I reached for the light so sordid, suddenly there came the dreaded,
> > 
> > ###### A shattering I soon regretted, regretted the mess my broccoli made on the dusty floor.
> > 
> > ###### &#8216;What&#8217;s this?&#8217; I questioned, no one answered-
> > 
> > ###### Only a giggle, and then no more
> > 
> > &nbsp;
> > 
> > ###### Yes a giggle, clear as crystal, the vegetables tossed out by the fistful,
> > 
> > ###### And each root, weed, bean and mushroom formed its mush upon my floor.
> > 
> > ###### Speechless I stood out in contempt,
> > 
> > ###### My refrigerator cold, empty and smelling potent.
> > 
> > ###### All sustinence gone forever except-except decadent chocolate of whom I loved and called Stouffer-
> > 
> > ###### A twenty year old cake so perfectly carved
> > 
> > ###### And so I loved my dear Stouffer-
> > 
> > ###### Not to be eaten, no nevermore.
> > 
> > &nbsp;
> > 
> > ###### Sighing at my prized possession, watching it move with a rippling sensation,
> > 
> > ###### Had not spared me, scared me with a new found horror never experienced before.
> > 
> > ###### So that now I stood reciting, to calm my heart which was exciting,
> > 
> > ###### Tis only my imagination enticing, my cake lying in my refrigerator door-
> > 
> > ###### Can only be this, cannot be more.
> > 
> > &nbsp;
> > 
> > ###### Mustering up my weakened courage, the strength within me began to flourish,
> > 
> > ###### &#8220;Mr. or Mrs. Stouffer, I could have your forgiveness down to the core;&#8221;
> > 
> > ###### &#8220;I did not mean to forget you for so many a year,&#8221; Just then the cake did jeer;
> > 
> > ###### I saw a wicked smile appear-
> > 
> > ###### To become a contorted face to peer&#8211;to peer at me from the floor.
> > 
> > ###### Laughing it edged closer, beckoning me everso lower, to the floor.
> > 
> > ###### &#8220;Eat me,&#8221; it whispered and then no more.
> > 
> > &nbsp;
> > 
> > ###### &#8220;NO!&#8221; I screamed panicked and stiff, yet tempted I pondered quick and swift,
> > 
> > ###### Nearly yielding to the provoking mound of lactose which dared me from the floor.
> > 
> > ###### My mind then came up with a resolve,
> > 
> > ###### &#8220;You shall tempt me no longer,&#8221; to the cake I did call-
> > 
> > ###### Out into the rain you shall fall, into utter darkness, good bye Stouffer.
> > 
> > ###### This I cried out but no echo dare surmount the sacred name Stouffer.
> > 
> > ###### Only a giggle, and then no more.
> > 
> > &nbsp;
> > 
> > ###### Back into my basement turning, all my stomach within me churning,
> > 
> > ###### Up the stairs I flew all the way up to my bedroom chamber door.
> > 
> > ###### With all my strength only could I lumber, into my bed, into delightful welcome slumber,
> > 
> > ###### And when I recovered I was on my basement floor-
> > 
> > ###### My cake smiling next to me on my basement floor-
> > 
> > ###### &#8220;Eat me,&#8221; it whispered and then no more.
> > 
> > &nbsp;
> > 
> > ###### &#8220;This cannot be,&#8221; I did shout, &#8220;I threw you out, I threw you out!&#8221;
> > 
> > ###### Over it slided in lordly grandeur,
> > 
> > ###### The temptation became to strong, I knew it was wrong-
> > 
> > ###### Yet I grabbed my knife and tongs and pursed the cake all around my basement floor-
> > 
> > ###### As it slided sluggishly back and forth laughing on my floor-
> > 
> > ###### &#8220;Eat me,&#8221; it chanted and then no more.
> > 
> > &nbsp;
> > 
> > ###### And then it was gone, and so went its provoking song,
> > 
> > ###### Satisfaction showed by the smile I wore,
> > 
> > ###### Back up the stairs I went, to the TV where many an hour I have spent.
> > 
> > ###### Then I set my heavy frame upon my carpeted floor-
> > 
> > ###### My heavy, protruding frame sounded slightly against my floor-
> > 
> > ###### A giggle, and then no more.
> > 
> > &nbsp;
> > 
> > ###### But to every channel I did turn, it showed people thin with fat and calories burned.
> > 
> > ###### My heart did guilt within me strike and shame also inside me bore,
> > 
> > ###### Disgruntled I got up pondering, to here and there sauntering,
> > 
> > ###### Til I found myself wandering, up to my bedroom chamber door-
> > 
> > ###### When the image came back to me, above my bedroom chamber door-
> > 
> > ###### &#8220;Eat me,&#8221; it chanted and then no more.
> > 
> > &nbsp;
> > 
> > ###### &#8220;Impossible,&#8221; I gasped in terror &#8220;It&#8217;s back to haunt me, my torture bearer,
> > 
> > ###### &#8220;Let me be, haunt me no more,&#8221;
> > 
> > ###### Back down into the basement darkness, back to my refrigerator tarnished,
> > 
> > ###### On my knees looking at the varnish my chocolate cake made on my basement floor-
> > 
> > ###### Then it appeared again to me, &#8220;NO!&#8221; I screamed, my face against the floor-
> > 
> > ###### &#8220;Eat me,&#8221; it chanted&#8230;forevermore.
> > 
> > &nbsp;

<p style="text-align: left;">
  Happy Birthday Mr. Poe.
</p>

 [1]: http://www.heise.de/ix/raven/Literature/Lore/TheRaven.html
