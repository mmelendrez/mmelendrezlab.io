---
title: Dark Crystal
author: Mel
type: post
date: 2011-11-02T06:14:15+00:00
url: /2011/11/02/dark-crystal/
categories:
  - Devotion
tags:
  - darkness
  - name

---
Devotional Blog: &#8220;Signpost&#8221;, 10/24/2011, Psalm 71:14-17

In this entry entitled &#8220;Signpost&#8221; she discusses the meaning of names using her son&#8217;s as an example. She discusses how her son has grown to live up to his name and suggests that even before our parents knew us, God did. That a parent&#8217;s chosen name for their child no matter how it comes about foreshadows who that child is or will be.

My name? My mother originally wanted to name me Bianca but was met with so much resistance that she turned to her sister and asked her what her middle name was and so I was named after my Aunt&#8217;s middle name.

> <p style="text-align: center;">
>   Melanie
> </p>
> 
> <p style="text-align: center;">
>   Derived from the Greek (Melania): Dark, blackness
> </p>
> 
> <p style="text-align: center;">
>   Introduced by French Form as Melanie
> </p>

<p style="text-align: left;">
  When I went to Brazil my nickname Mel meant honey. When I lived in Hawaii it was associated with the word Mele which means song. In Christianity they have biblical meanings for names&#8230;and its very difficult to find my particular name. Several books and sites say it means &#8216;gentle&#8217; or in Hebrew it means &#8220;Gods grace lifts me up&#8221;. I suppose I have lived up to my name in terms of being dark-haired with olive skin tone. And I definitely believe God&#8217;s grace has lifted me out of some pretty dark places.
</p>

<p style="text-align: left;">
  Funnily enough I looked up Bianca&#8230;Italian origin, meaning &#8216;white, shining&#8217;. I had to laugh, so it was either Black or White&#8230;one monochromatic color or the other. Spiritually like Melanie, Bianca was a difficult name to find&#8230;closest I got was &#8216;purity&#8217;. Probably best I was named Melanie&#8211;Bianca definitely doesn&#8217;t seem to describe me at all&#8230;LOL.
</p>

<p style="text-align: left;">
  Together with my middle name&#8211;Melanie Crystal would be &#8220;Dark Crystal&#8221; as there&#8217;s no other real meaning for Crystal than well&#8230;crystal, the hard rock that can be cut with facets and made pretty.
</p>

<p style="text-align: left;">
  Do I think my name successfully &#8216;foreshadowed&#8217; who I&#8217;ve become? Perhaps, there are aspects of my personality that can be dark and harder and also aspects which are gentle. The name Crystal is usually associated with clear beauty so it&#8217;s appropriate that mine is dark crystal as my faith and personality aren&#8217;t exactly stark clear but rather more complicated. So in the end I believe my name is quite fitting.
</p>
