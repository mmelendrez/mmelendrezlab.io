---
title: 'When it’s dark outside you can see the stars: repost from Mar 22, 2007'
author: Mel
type: post
date: 2011-08-18T09:02:34+00:00
url: /2011/08/18/when-its-dark-outside-you-can-see-the-stars-repost-from-mar-22-2007/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"680f804b9e";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Life or something like it
  - Not Asia
tags:
  - life
  - living
  - passion
  - purpose

---
I&#8217;m back from Brazil! I am not going to relate the specifics of my trip if you want to know that stuff check out the blog I kept: [http://springinbrazil.blogspot.com][1] but suffice it to say it was absolutely amazing.

When I decided to go to Brazil I went to refocus and clarify somethings in my life about my future and where I am supposed to be following the completion of my thesis. I got down there and the people were so wonderful, so passionate about their work and what really matters in life, it was so intoxicating and contagious. They were so full of faith and perserverence. It was such a priviledge just to be down there and help out with their ministries and be with them in general.  Being down there re-confirmed what I already knew in my mind. I had lost my passion, my direction which I know may sound odd since I&#8217;ve been on this PhD path for the past 4 years and have never really detoured from it. But I was beginning to forget why I was doing it&#8230;who I was doing it for&#8230;

I think it&#8217;s healthy to every once in awhile really take stock of your life and if you are living it the way you were intended to live it. Idealistic, faith driven&#8211;I am&#8230;I believe there is a reason for everything, even the shittiest things that happen to us in life. Going to Brazil allowed me to darken my mind and pay attention to the spots of light in my life that I&#8217;d thought I&#8217;d lost.

I made some amazing contacts and I will be pursuing the possibility of returning to Brazil one day after I graduate or going to the Tenwek mission hospital in Kenya to see about the research ward there. Currently they do not have one, I would love to establish one. The hospital is the only one for 100&#8217;s of miles in either direction and it&#8217;s a good hospital but I need to look into their staffing for monitoring of infectious disease/epidemiology, outbreaks in the area, and what the possibilities are.

I am so excited for the future, I cannot even begin to describe how much easier I breathe now. I don&#8217;t know honestly where I will be when the chips fall, but I walk through the doors that open to me, I don&#8217;t question the &#8216;plan&#8217; for my life, I simply walk it, run it, drag myself through it if necessary, easy or hard&#8211;the way I always have and I believe that&#8217;s only way I will be content in this life.

It took darkening my mind to see the pinpoints of light representing the good things and promising things in my life that drive my passion.**__**

 [1]: http://springinbrazil.blogspot.com/
