---
title: anger management…
author: Mel
type: post
date: 2017-06-02T07:05:26+00:00
url: /2017/06/02/anger-management/
categories:
  - Life or something like it
tags:
  - anger

---
> “Holding on to anger is like grasping a hot coal with the intent of throwing it at someone else; you are the one who gets burned.”
  
> ― [Gautama Buddha][1]{.authorOrTitle}

I&#8217;m not an &#8216;angry&#8217; person. Ask just about anyone that knows me or has known me for any extended period of time and they&#8217;ll probably say I&#8217;m pretty easy going. I usually care more about how I am making others feel and that trumps how they are making me feel. Sure I get hurt, annoyed or pist like the next person but then time goes by and it&#8217;s supposed to go away. Well, while it doesn&#8217;t consume my days and nights (usually), it never goes away. I&#8217;m still pist about stuff from elementary school &#8211; yes, elementary school. From about the 4th grade through 8th grade I was a very angry, depressed, rather morbid child and it manifested in creative ways at home and in school it did not lend itself to making friends. I was made fun of, I was bullied, I was ostracized&#8230;the kids pretty much hated me and at  the time I had no idea why.  I didn&#8217;t think  I was such a terrible person, but y&#8217;know schoolyard politics dictate who the misfits are. Which is why I am forever grateful my best friend (of 30+ years now!) stuck it out with me.

Because I was such a little shit as a child, when we moved to Hawaii and I got the chance to &#8216;start over&#8217; in  high school, I made a concerted effort to remake myself and suck less as a human being. To a large degree it worked and my inner anger dissipated. Being in a stable location for all of high school helped. Going to church helped and aside from nearly getting my ass kicked freshman year of high school &#8211; I was able to make an amazing group of friends, hopefully, many of whom I&#8217;ll see this summer at our 20 year high school reunion.

When I got to college I saw kids from elementary school&#8230;they were going to the same college&#8230;they were in MY dorm &#8211; karma!? We were civil, had our own circles, wasn&#8217;t a big deal &#8211; but on seeing them, the anger came back. You think you have something resolved, you&#8217;ve moved on with your life and low and behold&#8230;

&#8220;Mother! f\*\*@ss chomping monkey vomit son of a heartless goat sh\*\**bag gahhhhhh&#8230;i hate you&#8230;&#8221; &#8211; No not a stream of consciousness, I think at some point this sentence actually came out of my mouth.

<!--more-->

Over the years various versions of this turrets-inspired outburst would surface. Never in public &#8211; in the recessed safety of my room or office if no one was there. College had it&#8217;s own challenges. It can get stressful putting yourself through school, working and trying to live up to some probably pretty ridiculous ideal you&#8217;ve set up for yourself. My relationships were tense, my friendships sometimes strained, the academics a shock, I worked too many jobs&#8230; I sought refuge in speech and debate and study abroad. I was angry about my grades. I was angry that I was working so much and that&#8217;s why my grades were slipping but I had to work to pay for school. I was angry I was in debt. I was angry I wasn&#8217;t making friends or catching onto things as readily as I did in high school. The laundry list goes on.

I caught a reprieve, after an internship I did following college, I just went home. Anger is exhausting.

Grad school was incredibly awesome and beyond frustrating&#8230;yet again, rage inducing at times. That feeling of being totally out of control in an area you feel you &#8216;should&#8217; have some control. Or being told I have control only to have it whipped away. Assumptions were made on my abilities and, as it were, limitations as a scientist and it bled over into assumptions about &#8216;who I was&#8217; as a human being. You want to flip the rage switch? Assume you know someone, base decisions on that&#8230;then &#8216;notify&#8217; that someone later.

Don&#8217;t get me wrong, I have seen a lot of beauty, I haven&#8217;t spent my entire life in a blind rage &#8211; of course not and perhaps to balance out this blog, my next one will expound on one of my heart-shaping experiences.

I&#8217;m not perfect, but I am honest. I&#8217;m typically the first person, especially if it&#8217;s brought to my attention, to say I&#8217;m sorry. But I am also an over-thinker and I will turn situations around in my head til they are devoid of all color and rather than cooling off and letting go, I just get angrier &#8211; it&#8217;s a slippery slope. You ever done this?

Louis CK has a sketch you can probably find on youtube about &#8217;email fights&#8217;. The build up of writing everything down, the hours you spend writing perhaps having others read it too; then the more hours you spend fantasizing about said person&#8217;s reaction when in reality that person may not give you a second thought, oh gee.

Louis CK is pretty crude in his humor but really he says a lot of things we all think and tells us about our most terrible habits. I don&#8217;t do the fantasy part&#8230;but I do the email part. Often times I&#8217;ll write out my anger, and never send it, or send a version that&#8217;s been proofread by my mom or now my husband. I&#8217;m not looking to &#8216;end&#8217; relationships I care about &#8211; but you need an outlet right? Of all the things I have raging in my head, 20% I write down and about 3% actually get sent.

And then there is my upbringing (if you&#8217;ve read my previous posts under &#8216;devotion&#8217; or &#8216;life or something like it&#8217;). &#8220;Give it to  God.&#8221;

> &#8220;My dear brothers and sisters, take note of this: Everyone should be quick to listen, slow to speak and slow to become angry, because human anger does not produce the righteousness that God desires&#8221;  ~James 1:19-20
> 
> &#8220;Fools give full vent to their rage, but the wise bring calm in the end.&#8221;  ~Proverbs 29:11

Well&#8230;I am a damn fool.

At least in my head. It used to infuriate me growing up how &#8216;easily&#8217; my peers seemed to be able to give things to God. Why couldn&#8217;t I let go of stuff too. They all seemed &#8216;released&#8217; and &#8216;free&#8217; and happy go lucky and I just was like well&#8230;fuck me. I&#8217;d pray, I felt, as earnestly as they did. I&#8217;d sing, I&#8217;d read my Bible, talk to my youth pastors, be involved in youth and church events. Sure I was learning about my faith&#8230;but I wasn&#8217;t learning how to truly take care of my anger.

I also grew up in a culture where kids were not &#8216;diagnosed&#8217; with everything. Maybe a psychiatrist would have had a hey day diagnosing whatever disorders I had and prescribing all manner of potion to control my pre-adolescent-continuing-into-adulthood issues. Nope, when I was growing up the medicine was &#8216;get over it&#8217; &#8216;suck it up&#8217; &#8216;well, sucks to be you&#8217; &#8216;find a way to fix it&#8217; &#8216;stop belly aching&#8217; &#8216;go outside and play&#8217; &#8216;go take a nap&#8217;&#8230; Would I have preferred popping a pill? No, because I also have this insane control issue where I want to be the one to &#8216;fix&#8217; my issues and not depend on anyone or anything to do it for me. This is the part where my church upbringing says &#8216;ah ha! there&#8217;s your problem, you need to give your control issues to God&#8217;. Duly noted Christian upbringing.

I can count on one hand how many times I&#8217;ve actually blown up or given &#8216;vent&#8217; to my rage. But that&#8217;s just the problem, when you don&#8217;t let it out what&#8217;s the result?

> “Speak when you are angry and you will make the best speech you will ever regret.”
  
> ― [Ambrose Bierce][2]{.authorOrTitle}

So what is my recourse? Nothing, I do nothing, I stew, I don&#8217;t go after the &#8216;closure&#8217; I probably need to let go of whatever anger I have.

Why? Well, what purpose would it serve? I&#8217;ve already lost. Don&#8217;t really have the desire to lose more. What would be gained by hammering on the issue? Some self-righteous perverse sense of needing to be &#8216;justified!&#8217; Well, just because you are justified doesn&#8217;t mean you are innocent.

So now my anger and I are at an impasse and my husband is having none of it. He knows my anger isn&#8217;t with him so this emboldens him to speak forth and we have the following conversation.

Husband: &#8220;Go get a gym membership, my friend goes to this great one, only $10/mo and has a pool! You like swimming remember?&#8221;

Me: &#8220;Meh, it&#8217;s going to cut into my depressed, self loathing, lazi-anger time&#8221;

Husband: &#8220;Didn&#8217;t you used to compete? Don&#8217;t you want to do that again? It&#8217;ll be good to release some of that tension!&#8221; (He&#8217;s saying this with way too much hopeful support in his voice. I alternately love him more and slightly hate him now).

Me: &#8220;Yes, I had a goal when I am 40 to compete again&#8230;but it&#8217;s so much effort, I am so far gone&#8230;&#8221; (He cuts me off&#8230;)

Husband: &#8220;You&#8217;re going.&#8221;

Me: &#8220;Well damnit.&#8221;

I get my membership and as a &#8216;freebie&#8217; they give you a trainer consult &#8211; because y&#8217;know the best medicine for depression and anger is being told you are 37% body fat. Oh holy God. I danced ballroom, jazz and hip hop for 15 years, I used to compete in triathlon and cycling&#8230;I USED to be in decent shape. Now I feel like a female Michelin man. For some people body condition reality checks are supposed to be motivating &#8211; never been the case for me, rather it&#8217;s just demoralizing. Kind of goes along with those ridiculous expectations I have of myself? Ya ya I know, more stuff I just need to &#8216;let go of&#8217; right?

So what is my motivation? I&#8217;d love to sit here and write that I am motivated, not angry anymore and life is just gloriously spurting cotton candy now that I am going to the gym. All of that would be a lie and I gave up lying a long time ago &#8211; I&#8217;m pretty terrible at it so made sense to give it up. I hate the gym&#8230;but I hate the state of my health even more (mentally, spiritually and physically).

All I can say is I&#8217;m taking steps &#8230;perhaps it helps that I have a husband behind me holding a wine bottle on a stick just out of reach so that I&#8217;ll keep walking. Ok &#8211; that&#8217;s a lie, but you get the metaphor. I never thought I was a prideful person &#8211; but perhaps I am, or else why would I hold on in &#8216;self righteous anger&#8217; to anything? If you can&#8217;t master pride you don&#8217;t really have self-control. Being humble isn&#8217;t weakness &#8211; it&#8217;s the balance of mind and heart &#8211; each compromising with the other so the resolution can become clearer. I want to be a good role model, a good teacher, a good leader and I know pride gets in the way of that. If you can&#8217;t swallow your pride, you can&#8217;t lead&#8230;John Weatherford wrote that when he was talking about Ghengis Khan. Just because my rage is inner &#8211; doesn&#8217;t mean I&#8217;m practicing &#8216;self-control&#8217;.

I have a beautiful daughter &#8211; I want to be strong, inspiring and most importantly happy for her. Life is a gift, I basically need to stop pissing all over it and start taking care of my gift.

I have faith that the other issues will just fade. Life has a way of bringing the important people and events into your life that help keep you going &#8211; help make it all worth it. When push comes to shove, I have realized that if something is fucking with my family or close friendships&#8230;it becomes incredibly easy to let go, throw my hands up and walk away so that those closest to me are not hurt or don&#8217;t continue to get hurt. Some things are too precious, life and loved ones. Everything else is filler.

> “You can’t selectively numb your anger, any more than you can turn off all lights in a room, and still expect to see the light.”
  
> ― [Shannon L. Alder][3]{.authorOrTitle}

Heading toward the light&#8230;slowly but surely.

&nbsp;

 [1]: http://www.goodreads.com/author/show/2167493.Gautama_Buddha
 [2]: http://www.goodreads.com/author/show/14403.Ambrose_Bierce
 [3]: http://www.goodreads.com/author/show/1391130.Shannon_L_Alder
