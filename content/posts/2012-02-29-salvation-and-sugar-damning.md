---
title: Salvation and Sugar Damning…
author: Mel
type: post
date: 2012-02-29T04:30:49+00:00
url: /2012/02/29/salvation-and-sugar-damning/
featured_image: /wp-content/uploads/2012/02/sugarsmacks-e1330489007184.jpg
categories:
  - Devotion
tags:
  - church
  - "culture'"
  - faith
  - family
  - God
  - judgement
  - relationships
  - salvation

---
Devotional Blog:

Christian &#8216;Culture&#8217;, Depravity and Salvation, 2/29/2012, Ephesians 2:8-9

I love meeting new people, hearing new points of view listening to life adventures, life realizations and commiserating on mutual experiences. So 2012 is a leap year and there was no entry in my devotional book for this day so this is my own devotional. Honestly I do have any number of pages in the book dog eared to write about as I am fantastically behind in my posting, but I find I enjoy writing more when I am writing about something that is currently bothering or inspiring me. Enter today&#8217;s topic.

I had the pleasure of meeting someone with a very similar upbringing to myself and we bantered back and forth about being children having grown up in the church. Children who grew up in the church, most likely said their &#8216;salvation&#8217; prayer at a young age, went through the &#8216;Christian&#8217; motions growing up, sunday school, youth camp, retreats, revivals, door to door evangelism, whatnot. We &#8216;shunned&#8217; the people we were supposed to shun or hate, we accepted the people that fit into the Christian box and we were encouraged that the greatest calling in life is that of ministry. Christian culture surrounded us, we memorized verses, held our hands up in deference to God during worship, allowed people to pray for us, we prayed for people, we knew all the ins and outs of the culture and we really didn&#8217;t have an understanding of what true &#8216;salvation&#8217; was&#8230;but of course we were saved&#8230;weren&#8217;t we?<!--more-->

Christian culture leads many kids to grow up leading double lives. Appearances are so important sometimes that families and individuals will gloss over the real needs and hurts in their lives and in their families in order to keep up the Christian middle/upper class &#8216;everything is hunky dory holy&#8217; image to their fellow church goers. The perfect Christian life is paramount&#8230;so make it look that way even if its not true. This is especially true of Pastor&#8217;s families, having to live their entire family life above reproach, no mistakes, no imperfections, nothing the churchgoers can gossip about. Fact of the matter is parents and children are far from perfect or capable of complete control of each other and this concept of perfect appearances and ignorance is positively toxic.

It&#8217;s OK to suck sometimes.

> For by grace you are saved through faith, and this is not of yourselves, it is the gift of God; it is not of works, so that no one can boast. Ephesians 2:8-9

And heaven knows I&#8217;ve sucked a lot in my life.

How do you understand your faith, your salvation, your &#8216;mere Christianity&#8217; if you will? Well this blog was to start a walk through my own faith so lets go there. What makes me choose Christianity over a different religion? After all Christianity and many religions share the same moral fiber of good/evil, right/wrong as defined by religions and their holy book or in the case of atheists&#8211;perhaps defined by societal norms and values. Why am I a Christian?

I&#8217;m going to rephrase a few things here because I think whenever someone hears Christian they immediately expect judgement and retribution to reign down on them&#8230;heck I AM a Christian and I expect judgement and retribution to reign down on me everyday from other Christians&#8211;not the awesomest way to live is it? I like the way my new friend phrased this.

Rephrase: Why did I choose the God of the Bible over any other god or the option of no god? When did I come to a greater understanding of salvation&#8230;when do I think I got &#8216;saved&#8217;?

I was raised in the church. Said my salvation prayer as a 4 or 5 year old at Calvary Christian in Costa Mesa, CA. Baptized at 15 in high school. Supposedly recommitted my life to Christ while on a youth missions trip to California at 16 or 17, can&#8217;t fully remember. Perhaps my story of salvation should end there&#8230;not very fulfilling or inspiring is it? Why do you think I shied away from ever &#8216;sharing my testimony&#8217; when asked in youth group, haha&#8211;it was BORING!

Actual &#8216;salvation&#8217; for me?

I haven&#8217;t led a picture perfect life to say the least. I was bitter and sad about my sister leaving the family, I was bitter and angry at the lack of support I got during college from my supposed advisers, my boyfriend and I did the whole break up/get back together thing a couple times which creates a seesaw of emotion, I was worried about finances as all young adults are. I was dealing with the absolute mind f$%!king effects of having become a rape statistic several months prior, I&#8217;d been awarded a Rotary Ambassadorial scholarship that was proving difficult to implement because no one would email/call me back, the visa was an issue, the school entry was an issue, the money was an issue. Capped off with a medical issue that my doctor advised I shouldn&#8217;t even travel culminating in me having to turn down the scholarship. 22 years old, sitting alone in my apartment, I had a mental break, depressed out of my gourd, entertaining thoughts of suicide in Seattle, Washington, U District. Did I &#8216;say the prayer of salvation&#8217; then? Did I see the bright light of God on my path leading me (saying that in my best southern baptist voice)? No. When I was 4 and 17 I said words/a prayer out loud. When I was 22, I believed it, and realized it in my heart&#8230;I believe that&#8217;s when salvation was mine. Does that mean I wasn&#8217;t saved til 22? Perhaps I was saved when I said that prayer at 4 years old, I just didn&#8217;t come to grips with God&#8217;s salvation and truly take it as my own until 22. Depravity in my soul, no amount of attempted perfection on my part, no amount of raising my hands, speaking in tongues, praying &#8216;like a pro&#8217;, proclamations of God&#8217;s goodness, teaching of Sunday school, ministering to friends etc. satisfied my soul because I was attempting to be good enough, to &#8216;save&#8217; myself if you will. Instead what I needed was the reality of a savior&#8230;I needed the God of the Bible.

Did it &#8216;fix&#8217; me? (raucous laughter)&#8230;absolutely not. In fact it took a good 7 years to get over many things, some of which to this day I still struggle with and I continue to constantly make mistakes. The difference? I am free to make those mistakes, I am free to ask forgiveness, I am free to be honest about who I am&#8230;completely f$%ked up and loved by God and thankfully loved and supported by my family no matter how many mistakes I make. And it took a long time but I started to heal and decided that rather than &#8216;force&#8217; my way through life creating the agenda I thought God wanted me to follow, rather I would just let him open the doors and I would just walk through them. My life has been impressively straightforward since. I may not know his ultimate plan for me, but &#8216;where&#8217; I&#8217;m supposed to go has since been crazy and clear. I&#8217;ve never been able to see the road ahead, but I always know where to turn.

Did Christian &#8216;culture&#8217; ruin me? Yes and no. Being raised in the Christian religion gave me an impressive foundation of belief, faith and moral fiber. Could I have attained that in other religions? Of course, but in my case it was Christianity. Do I have friends with impressive moral fiber that aren&#8217;t Christians&#8211;absolutely! There are aspects of religious culture or societal culture, if you are atheist, that ingrain moral fiber in you&#8230;hopefully. Have I seen Christian culture ruin people? Yes, unfortunately I have. I have watched close friends say F-you to the church and its establishments due to unfair judgement that&#8217;s been doled out them. My family has been the victim of unfair church gossip and judgement. I&#8217;ve had friends leave the God of the Bible completely because of Christians and their &#8216;culture&#8217;.

Did it ruin me? In some ways, though I never &#8216;hated&#8217; anyone persay, I had to learn to &#8216;love&#8217; everyone for who they were and leave &#8216;judgement&#8217; out of the equation. Christian &#8216;culture&#8217; teaches us/encourages us to judge others, especially other Christians, its in the Bible after all (Matthew 18:15-17, Galatians 6:1-2, 2 Timothy 3: 16-17, Titus 3:10). We call it accountability and its the excuse we use to wreak havoc on others, destroying them. Is accountability a good thing? Yes, when administered correctly. Problem is, very few Christians understand the difference between accountability and damning someone in the &#8216;sweetest&#8217; voice possible. I call it &#8216;Sugar Damning&#8217; and its the most toxic form of Christian &#8216;counseling&#8217; out there. Many times a lot of emphasis in Christian counseling is on &#8216;sugar damning&#8217; and pointing out all the faults that person has but doling out judgement and correction this way is &#8216;ok&#8217; because you&#8217;ve put on a &#8216;sweet&#8217; rather fake &#8216;I just care about you&#8217; disposition.

Does this mean we should sit idly by as our friends and family hurtle into a harmful chaos of a situations? No, getting involved because you love someone deeply and want to help is different than sitting on your pillar of judgement.

> And let us consider how to stir up one another to love and good works, not neglecting to meet together, as is the habit of some, but encouraging one another, and all the more as you see the Day drawing near.
> 
> **~Hebrews 10: 24-25**
> 
> Brothers, if anyone is caught in any transgression, you who are spiritual should restore him in a spirit of gentleness. Keep watch on yourself, lest you too be tempted.
> 
> **~Galatians 6:1**
> 
> Do nothing from rivalry or conceit, but in humility count others more significant than yourselves. Let each of you look not only to his own interests, but also to the interests of others. ****
> 
> **~Philippians 2: 3-4**
> 
> But he gives more grace. Therefore it says, “God opposes the proud, but gives grace to the humble.
> 
> **~James 4:6**
> 
> And we urge you, brothers, admonish the idle, encourage the fainthearted, help the weak, be patient with them all.
> 
> **~1 Thessalonians 5:14**

Are you wrapped up in the appearance of your life rather than what it is? Do you have a grasp on your own salvation, or has the &#8216;culture&#8217; sucked you in. Do you have an honest love and desire to support, encourage, advise, have relationship with and help those around you? Or are you the best &#8216;Sugar Damn-er&#8217; out there?

I think grasping and understanding ones salvation and relationship with God is a lifetime endeavor for Christians and I don&#8217;t believe you have to &#8216;hit rock bottom&#8217; to start finding it as I did, everyone is different. People like me have thick skulls&#8211;doh! Life, religion or lack thereof, culture, faith, understanding&#8230;it&#8217;s all apart of our walk, our internal search, our humanity.

> ##### I took a walk
> 
> ##### It got me thinking
> 
> ##### Left foot, right foot
> 
> ##### smiling, blinking
> 
> ##### Breathe in, breathe out
> 
> ##### Somehow linking
> 
> ##### My soul to my mind and my heart to my mouth
> 
> ##### The lost to the found and the North to the South.
> 
> ##### ~Lyrics from Needle in the Dark by Passenger
> 
> &nbsp;
> 
> &nbsp;
