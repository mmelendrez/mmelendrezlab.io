---
title: Witches Broom in Vietnam…
author: Mel
type: post
date: 2011-08-19T04:15:21+00:00
url: /2011/08/19/witches-broom-in-vietnam/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"2e805efd20";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - I will do Science to it!
tags:
  - Asia
  - cult
  - longan
  - ProMed
  - Vietnam
  - witches

---
I decided to repost this from ProMed because I thought it was interesting&#8230;who comes up with these disease names!? When I saw it in my inbox of course I had visions of cult activities involving longan fruit in the jungles of Vietnam&#8230;yes I have an active imagination.

Witch&#8217;s broom gets its name from a deformity in a woody plant, typically a tree, where the natural structure of the plant is changed. A dense mass of shoots grows from a single point, with the resulting structure resembling a broom or a bird&#8217;s nest. [[Source][1]]

A quick shout out to [ProMed][2] which is a great resource for hearing about disease outbreaks of known or unknown etiology around the world&#8230;check it out!

But really, [Longan fruit][3] is quite prevalent in Thailand as well and it is quite delicious. So, fantastical imagination aside, see below, feel free to read the culti-c disease activities plaguing Longan in Vietnam!

<!--more-->WITCHES&#8217; BROOM, LONGAN &#8211; VIET NAM: MEKONG DELTA


  
\***\***\***\***\***\***\***\***\***\***

<div id=":1ll">
  A ProMED-mail post<br /> <<a href="http://www.promedmail.org/" target="_blank">http://www.promedmail.org</a>><br /> ProMED-mail is a program of the<br /> International Society for Infectious Diseases<br /> <<a href="http://www.isid.org/" target="_blank">http://www.isid.org</a>>Date: Mon 15 Aug 2011<br /> Source: Vietnam Today, Vietnam News report [edited]<br /> <<a href="http://www.dztimes.net/post/social/witches-broom-casts-spell-on-crops.aspx" target="_blank">http://www.dztimes.net/post/<wbr>social/witches-broom-casts-<wbr>spell-on-crops.aspx</wbr></wbr></a>></p> 
  
  <p>
    Witches&#8217; broom disease is blighting longan crops around the Cuu Long<br /> (Mekong) Delta [southern most region of Viet Nam including several<br /> provinces], causing disastrous losses to farmers. In the delta,<br /> around 11 570 ha [hectares] have been blighted by the disease,<br /> figures from local agriculture departments show.
  </p>
  
  <p>
    A farmer in Soc Trang Province said longan was the main fruit in his<br /> area, but a witches&#8217; broom epidemic had hit harvests. In Dong Thap<br /> Province famous for longan cultivation, almost the entire 3680 ha of<br /> the crop had been infected, the local Agriculture and Rural<br /> Development Office said. In some communes, most orchards have been<br /> affected reducing longan output by 70 to 90 percent. In Tien Giang,<br /> Vinh Long, and Tra Vinh provinces, the disease has spread rapidly.
  </p>
  
  <p>
    Nguyen Minh Chau, Southern Fruit Research Institute, said witches&#8217;<br /> broom spread quickly and caused extensive damage. But farmers&#8217;<br /> awareness of this disease remained low though his institute had<br /> dispatched officials to educate them about it. Another major concern<br /> was that few farmers had cut down diseased trees and branches, making<br /> it difficult to contain the spread of the disease, he added.
  </p>
  
  <p>
    &#8212;<br /> Communicated by:<br /> ProMED-mail<br /> <<a href="mailto:promed@promedmail.org">promed@promedmail.org</a>>
  </p>
  
  <p>
    [Longan (_Dimocarpus longan_) is an important fruit crop in parts of<br /> Asia. Witches&#8217; broom (or rosette shoot) is a serious disease of the<br /> crop with reported yield losses of up to 50 percent. It was first<br /> described from China in 1941 where in some areas up to 100 percent of<br /> trees were infected, with higher incidence in mature trees. Since<br /> then it has also been reported from other countries in the region,<br /> for example Thailand and Taiwan.
  </p>
  
  <p>
    Symptoms appear on branches and leaves but not on fruit. They may<br /> include distorted mature leaves, unexpanded young leaves, dense<br /> clusters of shoots, poorly developed flowers, as well as abnormal<br /> development of flowers and panicles resulting in &#8220;broom-like&#8221;<br /> appearance of the inflorescences. Similar symptoms have been reported<br /> from litchi and a close relationship between the 2 diseases is indicated.
  </p>
  
  <p>
    The disease is still considered to be of unconfirmed aetiology, but<br /> is suspected to be caused by a filamentous virus transmitted by<br /> vectors such as litchi stink bug (_Tessaratoma papillosa_), longan<br /> psyllid (_Cornegenapsylla sinica_), and a new species of gall mite<br /> (_Eriophes dimocarpi_). The disease is also spread through grafting,<br /> and preliminary evidence suggests that it may be seed transmitted.<br /> Disease management may include orchard sanitation (removal of<br /> potential pathogen reservoirs), phytosanitary measures (disinfecting<br /> cutting tools), and use of clean planting and grafting material.<br /> Resistant crop varieties are available for some areas.
  </p>
  
  <p>
    The term &#8220;witches&#8217; broom&#8221; is descriptive of symptoms only and is<br /> associated with a range of plant diseases caused by different pathogens.
  </p>
  
  <p>
    Maps<br /> Viet Nam and neighbours:<br /> <<a href="http://www.lib.utexas.edu/maps/middle_east_and_asia/vietnam_pol01.jpg" target="_blank">http://www.lib.utexas.edu/<wbr>maps/middle_east_and_asia/<wbr>vietnam_pol01.jpg</wbr></wbr></a>> and<br /> <<a href="http://healthmap.org/r/01CY" target="_blank">http://healthmap.org/r/01CY</a>><br /> Viet Nam provinces:<br /> <<a href="http://en.wikipedia.org/wiki/File:VietnameseProvincesMap.png" target="_blank">http://en.wikipedia.org/wiki/<wbr>File:VietnameseProvincesMap.<wbr>png</wbr></wbr></a>>
  </p>
  
  <p>
    Pictures<br /> Witches&#8217; broom symptoms on longan flower spikes:<br /> <<a href="http://www.fao.org/docrep/003/x6908e/x6908e09.jpg" target="_blank">http://www.fao.org/docrep/<wbr>003/x6908e/x6908e09.jpg</wbr></a>><br /> Healthy longan:<br /> <<a href="http://www.bijlmakers.com/fruits/longan/longan_fruits_in_tree.jpg" target="_blank">http://www.bijlmakers.com/<wbr>fruits/longan/longan_fruits_<wbr>in_tree.jpg</wbr></wbr></a>> (fruit),<br /> <<a href="http://www.bijlmakers.com/fruits/longan/longan_flowers.jpg" target="_blank">http://www.bijlmakers.com/<wbr>fruits/longan/longan_flowers.<wbr>jpg</wbr></wbr></a>> (flowers), and<br /> <<a href="http://www.fruitipedia.com/Longan%20tree%204.jpg" target="_blank">http://www.fruitipedia.com/<wbr>Longan%20tree%204.jpg</wbr></a>> (tree)
  </p>
  
  <p>
    Links<br /> Longan witches&#8217; broom information via:<br /> <<a href="http://www.fao.org/docrep/003/x6908e/x6908e0c.htm#b7-10.7%20Pests%20and%20Diseases" target="_blank">http://www.fao.org/docrep/<wbr>003/x6908e/x6908e0c.htm#b7-10.<wbr>7%20Pests%20and%20Diseases</wbr></wbr></a>>,<br /> <<a href="http://www.daff.gov.au/__data/assets/pdf_file/0014/164003/ll_final_a.pdf" target="_blank">http://www.daff.gov.au/__<wbr>data/assets/pdf_file/0014/<wbr>164003/ll_final_a.pdf</wbr></wbr></a>>, and<br /> <<a href="http://www.agnet.org/library/eb/417b/" target="_blank">http://www.agnet.org/library/<wbr>eb/417b/</wbr></a>><br /> Research on cause and spread of longan witches&#8217; broom:<br /> <<a href="http://www.actahort.org/books/558/558_66.htm" target="_blank">http://www.actahort.org/<wbr>books/558/558_66.htm</wbr></a>>,<br /> <<a href="http://en.cnki.com.cn/Article_en/CJFDTOTAL-ZBDX402.008.htm" target="_blank">http://en.cnki.com.cn/<wbr>Article_en/CJFDTOTAL-ZBDX402.<wbr>008.htm</wbr></wbr></a>>, and<br /> <<a href="http://www.actahort.org/books/558/558_65.htm" target="_blank">http://www.actahort.org/<wbr>books/558/558_65.htm</wbr></a>><br /> Information on longan cultivation and diseases (with pictures):<br /> <<a href="http://www.bijlmakers.com/fruits/longan.htm" target="_blank">http://www.bijlmakers.com/<wbr>fruits/longan.htm</wbr></a>>,<br /> <<a href="http://www.tradewindsfruit.com/longan.htm" target="_blank">http://www.tradewindsfruit.<wbr>com/longan.htm</wbr></a>>, and<br /> <<a href="http://www.fruitipedia.com/longan_dimocarpus_longan.htm" target="_blank">http://www.fruitipedia.com/<wbr>longan_dimocarpus_longan.htm</wbr></a>>. &#8211; Mod.DHA]<br /> &#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;<wbr>&#8230;..dha/mj</wbr>
  </p>
  
  <p>
    *#############################<wbr>#############################*<br /> ******************************<wbr>******************************<br /> ProMED-mail makes every effort to  verify  the reports  that<br /> are  posted,  but  the  accuracy  and  completeness  of  the<br /> information,   and  of  any  statements  or  opinions  based<br /> thereon, are not guaranteed. The reader assumes all risks in<br /> using information posted or archived by  ProMED-mail.   ISID<br /> and  its  associated  service  providers  shall not be  held<br /> responsible for errors or omissions or  held liable for  any<br /> damages incurred as a result of use or reliance upon  posted<br /> or archived material.<br /> ******************************<wbr>******************************<br /> Donate to ProMED-mail. Details available at:<br /> <<a href="http://www.isid.org/ProMEDMail_Donations.shtml" target="_blank">http://www.isid.org/<wbr>ProMEDMail_Donations.shtml</wbr></a>><br /> ******************************<wbr>******************************<br /> Send  all  items  for  posting  on  the  PRO/MBDS  list  to:<br /> <<a href="mailto:promed-mbds@promedmail.org">promed-mbds@promedmail.org</a>>    (NOT   to   an    individual<br /> moderator).    If  you  do  not  give  your  full  name  and<br /> affiliation,  it  may  not  be posted.  If you are concerned<br /> about  repercussions  if  your  name  appears  on a posting,<br /> please advise us to  withhold  your identifying  information<br /> when  you  submit  a  piece  for  posting.  Send commands to<br /> subscribe / unsubscribe,  get  archives,  help,   etc.   to:<br /> <a href="mailto:majordomo@promedmail.org">majordomo@promedmail.org</a>.  For assistance from a human being<br /> send mail to: <a href="mailto:owner-promed@promedmail.org">owner-promed@promedmail.org</a>><br /> ##############################<wbr>##############################<br /> ##############################<wbr>##############################</wbr></wbr></wbr></wbr></wbr></wbr>
  </p>
</div>

 [1]: http://en.wikipedia.org/wiki/Witch%27s_broom
 [2]: http://www.promedmail.org/pls/apex/f?p=2400:1000
 [3]: http://myseedgarden.blogspot.com/2008/06/euphoria-fruit-longan.html
