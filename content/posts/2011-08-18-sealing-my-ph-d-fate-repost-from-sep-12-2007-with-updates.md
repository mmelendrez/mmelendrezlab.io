---
title: 'Sealing my Ph.D. fate: repost from Sep 12, 2007, with ‘updates’'
author: Mel
type: post
date: 2011-08-18T08:52:45+00:00
url: /2011/08/18/sealing-my-ph-d-fate-repost-from-sep-12-2007-with-updates/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - I will do Science to it!
tags:
  - graduate
  - PhD
  - process

---
So I am procrastinating on a piece of analysis right now that would basically seal my fate for my Ph.D. time-wise here in Bozeman.  Following a surprisingly productive meeting with my advisor, this morning it turns out that I am 90% sure I will not finish ontime&#8230;and all the Ph.D. students said &#8220;surprise surprise&#8230;&#8221; Yeah I know, no student ever finishes ontime&#8230;but man I had hopes. I am allowed to hope now aren&#8217;t I?<article>This seems like the longest Ph.D. in the history of LIFE itself!</article> 

<p style="text-align: center;">
  <strong>By way of Update: I ended up finishing my dissertation not in 4-6 mos as this post later implies but 3 YEARS LATER! Such is the Ph.D. process&#8230;.*sigh*</strong>
</p><article>

<!--more-->BUT, and I hate admitting this but my advisor was right, when we took a hard look at the numbers and what I need for a robust thesis&#8230;I need more data. Problem is getting/analyzing that data will add another 4 mos to my Ph.D. So hence, another semester essentially&#8230;and that&#8217;s not even including writing up. DOH! Well&#8230;at least I like my research&#8230;there are those that work on something they hate for years upon years, at least I like what I am doing &#8230;yes, I&#8217;ve come to grips with my insanity.</p> 

<h1 style="text-align: center;">
  By way of update: My insanity got worse as the years dragged on, there was little hope to come to grips with it around year 6&#8230;
</h1>

SO, the rest of this blog is dedicated to those in an epic struggle to finish!

<h1 style="text-align: center;">
  <strong>By way of update: And it was an &#8216;epic&#8217; struggle in the end</strong>
</h1>

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel11.gif" alt="" width="467" height="174" />

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel7.gif" alt="" width="600" height="260" />

Of course there was the first three years of my Ph.D. which technically didn&#8217;t end up counting for my thesis, so I guess I can look at it as I&#8217;ve only spent or will have only spent 3 yrs total on my actual thesis&#8230;

<h1 style="text-align: center;">
  <strong>By way of Update: I told myself a lot of lies during the end of my degree&#8230;anything to keep the caffeine IV flowing and homicidal tendencies at bay</strong>
</h1>

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel8.gif" alt="" width="455" height="205" />

And you know given I&#8217;ve spent so much time here I can serve as a beacon of knowledge to undergrads and new prospective grad students&#8230;

<h1 style="text-align: center;">
  By way of Update: I was optimistic back then.
</h1>

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel2.gif" alt="" width="535" height="180" />

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel4.gif" alt="" width="531" height="171" />

Eventually there has to be a light at the end of the tunnel right???

<h1 style="text-align: center;">
  By way of Update: Sure&#8230;350 pages and 3 years later
</h1>

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel10.gif" alt="" width="483" height="199" />

Y&#8217;know&#8230;as I wonder where my life went&#8230;

<h1 style="text-align: center;">
  By way of Update: &#8216;Bout a year into your postdoc when you think back to your Ph.D. process you&#8217;ll still wonder this&#8230;
</h1>

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel14.gif" alt="" width="543" height="194" />

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel13.gif" alt="" width="565" height="182" />

And of course receiving the occasional encouragement&#8230;

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel0.gif" alt="" width="475" height="192" />

<img src="http://i61.photobucket.com/albums/h70/bluemm79/mel9.gif" alt="" width="530" height="175" />

Eventually your Ph.D. begins to feel abit like this:

<img src="http://i61.photobucket.com/albums/h70/bluemm79/stickstuck.gif" alt="" width="147" height="122" />

<h1 style="text-align: center;">
  By way of Update: Ya that&#8217;s about right.
</h1>

SO, projected graduation now is December 2008&#8230;

<h4 style="text-align: center;">
  <strong>By way of Update: Defended March 4, 2010 (after several cancellations), Dissertation turned in March 12, 2010, </strong>
</h4>

<h4 style="text-align: center;">
  <strong>Flew out to Hawaii to see family March 15, 2010, </strong>
</h4>

<h4 style="text-align: center;">
  <strong>Flew to Bangkok, Thailand March 19, 2010, </strong>
</h4>

<h4 style="text-align: center;">
  <strong>Started PostDoc Monday, March 22, 2010</strong>
</h4>

Goal: To finish before I am 30! Think I can do it?

<h1 style="text-align: center;">
  <strong>By way of Update: I finished prior to my 31st birthday&#8230;YIKES!</strong>
</h1>

Well&#8230;one can hope.

Much Thanks for [Ph.D. comics][1] for showing the realities of graduate study!</article> 

&nbsp;

 [1]: http://www.phdcomics.com/comics.php
