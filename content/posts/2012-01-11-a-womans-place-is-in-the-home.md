---
title: A womans place is in the home…???
author: Mel
type: post
date: 2012-01-11T04:33:16+00:00
url: /2012/01/11/a-womans-place-is-in-the-home/
categories:
  - Devotion
tags:
  - 
  - children
  - family
  - God
  - men
  - women

---
Devotional Blog:

12/21/11, Topic: &#8220;Workers at Home&#8221;, Titus 2: 3-5

I&#8217;ve heard this verse used to justify a woman&#8217;s calling to &#8216;stay at &#8216; and raise a family as opposed to being independent and working. I&#8217;m not going to say much about this books devotional entry except that I like how the author deals with this and I agree with what she says&#8230;amazing given I rarely agree with what she has to say.

> Then they can train the younger women to love their husbands and children, to be self-controlled and pure, to be busy at home, to be kind and to be subject to their husbands, so that no one will malign the world of God. (Titus 2:4-5, NIV).

Now fundamentally I do not disagree with anything in this verse. I do believe that women should love their husbands and children and make them first in their lives. I do believe that women (and men) should practice self control, be kind and be subject to their husbands (or wives). Purity is a subject for another blog but I don&#8217;t necessarily disagree with what the verse says about that. And of course there&#8217;s the &#8216;busy at home&#8217;. In a previous blog I addressed [what &#8216;home&#8217; is for me][1]&#8230;personally, growing up I was never taught that I should aspire to go to college to get my M.R.S. (Mrs.) degree. My parents were always encouraging us to go out there live our lives, get jobs and accomplish our goals and aspirations with no expectation that we should marry and have babies right away&#8230;although I know my mother would love to be a grandmother.

I think at one point my mom, when she had my little brother whose 21 years younger than me&#8230;she joked that she was tired of waiting for me to have grandbabies for her so she made another of her own.

The author of the devotional, in her studies, found that the definition of &#8216;busy at home&#8217; is &#8220;worker at home&#8221;, &#8220;a guard of the home; keeper of the home; domestically inclined&#8230;&#8221; She read more opinions, studies and commentaries about the subject and they all had one common feature, a woman&#8217;s <span style="text-decoration: underline;"><strong><em>heart</em></strong></span> is at home. Her priorities and heart reflect God and a family focused value system. I can agree with this and not just because it&#8217;s &#8216;convenient&#8217; for me to agree with this because of my life choices.

God can use women same as men in every aspect of life. God is an equal opportunity &#8217;employer&#8217; and I don&#8217;t believe it&#8217;s &#8216;left up to the men&#8217; to do &#8216;all God&#8217;s work&#8217;. Yes, God made women to bear children and we have a more natural &#8216;maternal&#8217; (obviously) disposition which makes us suited to having kids and running a home. And I am not opposed nor ever have been opposed to having kids. I love kids, I&#8217;ve just never had a &#8216;biological clock&#8217;. If I have kids, great, if I don&#8217;t, I don&#8217;t. And I know my own disposition, I&#8217;d go nuts from cabin fever if I had to spend 18 years at home only &#8216;allowed&#8217; to raise my kids and &#8216;keep house&#8217;. I don&#8217;t think God intends that. We all have gifts outside the &#8216;home&#8217; and while my priorities will always be my family, I think its possible and encouraging to have other &#8216;callings&#8217; as well.

I agree with her when she says:  &#8220;Your feet can be anywhere but your heart should be &#8216;at home&#8217;.&#8221;

 [1]: http://www.melaniemelendrez.com/2011/12/27/where-is-home/ "Where is home?"
