---
title: wandering another year later
author: Mel
type: post
date: 2014-09-21T13:37:24+00:00
url: /2014/09/21/wandering-another-year-later/
categories:
  - Life or something like it
tags:
  - life
  - reflection

---
<img src="http://www.melaniemelendrez.com/wp-content/uploads/coffeeMel.jpg" alt="coffeeMel" width="174" height="226" class="alignright size-full wp-image-920" />

So the husband pointed out that it&#8217;s been over a year since my last post and perhaps I was due to start blogging again. Oddly, the past few years since coming back to the US from Bangkok &#8211; specifically to Maryland/DC I just haven&#8217;t really been inspired to blog&#8230;as I&#8217;m sure you can tell from the copious lack of posts freakishly apparent on this blog. 

It&#8217;s difficult for me to write without a reason. I typically don&#8217;t update my teaching blog for months on end because I only update it when I feel I have something useful to relay. I typically don&#8217;t update this blog unless I have something, again, useful to mull over.

Fact of the matter is, while I may not be wandering geographically much anymore I am still very much wandering inside. Struggling with the pros and cons of my current life, debating what will really make me happy, both loving and hating what I do, turning 35 in a couple weeks and coming to grips with the decision of whether to have kids during the last portion of my 3rd decade of life because I&#8217;d rather not have them in my 40&#8217;s if I have them at all, being told I&#8217;m over thinking everything &#8211; yet that&#8217;s my nature&#8230;

I&#8217;m finally seeming to get my legs under me in this new field I jumped into during my postdoc&#8230;took awhile. Realizing the majority of what I do and how I live is to make those around me happy and accommodated; but not necessarily myself &#8211; and yet making other&#8217;s happy does make me happy, but then it doesn&#8217;t, but it does&#8230;it really is a vicious circle.

I haven&#8217;t blogged because I&#8217;ve been in limbo &#8211; eating, sleeping, working, rinse, repeat&#8230; And what is there to say when you are in limbo? &#8220;Wow, it&#8217;s been 2 years and I&#8217;m still in limbo.&#8221; &#8211; inspiring right? Not really.

I have a bookshelf and Kindle full of books I am uninspired to read, I have a book of recipes yet I am not inspired to cook, a book for programming yet I am not inspired to program, I have a basket full of partially finished cross-stitch that remains in the basket, I have a gym membership languishing, Manuscripts and blogs &#8211; yet I am uninspired to write&#8230; sounds depressing? It really is not &#8211; I&#8217;m just in limbo, internally wandering, going through the motions, until I find a renewal of passion for something again.

Mid-30&#8217;s life crisis, maybe. 

I could blame my phone, those infernal addictive app games, social networking sites, LOL Cats, youtube etc&#8230;for becoming the unproductive &#8216;filler&#8217; in my life &#8211; how many hours can you spend just on youtube videos or looking through buzzfeed posts, but while I&#8217;ve lost time to those at some point or another, most of my time loss stems from just &#8216;existing&#8217; and being unable to put what&#8217;s going on my head into coherent form. The husband really is a saint for putting up with this by the way.

I&#8217;m not really looking for answers here. I&#8217;m not happy nor unhappy, just reflecting&#8230;yet another year later. 

PS. Happy one-day-early birthday Mom, I love you.
