---
title: My definition of good must be broken…
author: Mel
type: post
date: 2012-06-15T16:32:52+00:00
url: /2012/06/15/my-definition-of-good-must-be-broken/
al2fb_facebook_link_id:
  - 43807988_804537154369
al2fb_facebook_link_time:
  - 2012-06-15T16:32:56+00:00
al2fb_facebook_link_picture:
  - post=http://www.melaniemelendrez.com/?al2fb_image=1
categories:
  - Devotion
tags:
  - faith
  - God
  - purpose

---
Devotional Blog:

6/14/2012: &#8220;When good isn&#8217;t good enough&#8221; Romans 8:28

This isn&#8217;t out of the book I am going through which coincidentally arrived in the mail from Thailand in our first round of boxes&#8211;this is one I am just going to write.

If you&#8217;ve read my previous blogs in the devotion section or perhaps in my life section or such you&#8217;ll know how I was raised. In a faith that stresses undying, unwavering faith in God and that everything works toward his plan and purpose. That no matter how &#8216;rough&#8217; life gets &#8216;it&#8217;s all good&#8217; because God knows how it&#8217;ll all turn out and that&#8217;ll be what&#8217;s &#8216;good&#8217;. I was raised in the midst of prophets who communicated God&#8217;s promises to my family, the plans he had, the constant assurance that he hasn&#8217;t forgotten us that his plans will come to pass&#8211;that we will walk into the awesomeness he has in store for us. I was raised to pray. I was raised to believe. No matter what happens, how many years pass, how many people hurt in the meantime&#8230;it&#8217;ll all &#8216;come out in the wash&#8217; persay.

I&#8217;m currently encountering an all out mental rebellion occurring due to the clash of what I feel logically should be done and faith.

My mother says when things get difficult and things don&#8217;t seem to come to pass its because of choice. Everyone has free choice and certain people choose to &#8216;bar your way&#8217; in essence, toward achieving the plans God has for your life. It doesn&#8217;t mean you won&#8217;t achieve it, it means it&#8217;ll take longer. My question, if God knows all this, knows people will stand in the way, knows this will drive you mad with frustration, why does he put you on that particular path to your purpose? Why does he pick that path for you?  Why does he put you at the mercy of people who could give two sh*ts or less about God and his plan? Is it supposed to be character building? Faith building? Ok, so God wants us to &#8216;learn&#8217; something&#8230;but what happens when a &#8216;lesson&#8217; controlled by the choices of people who don&#8217;t care about God and his &#8216;plans&#8217; but directly affecting your &#8216;promise&#8217; seems to be destined to extend to the end of your days. Ok perhaps that&#8217;s a bit melodramatic, but when you are getting past 20 years and have seen no end leading you to the goal of the plan, or even worse, you &#8216;see&#8217; endings that get taken away from you, I really want to look up at the sky and say WTF.

I was ready to jump ship after 7 years at my Ph.D. it seemed like it was just becoming &#8216;impossible&#8217; to finish but I knew a &#8216;finish&#8217; was there, I saw it, it was tangible and I knew exactly what I had to do to obtain that finish. What happens when you don&#8217;t know HOW to obtain the finish line? What happens when the &#8216;finish line&#8217; turns into a moving target of which you cannot predict the direction? What happens when your best, your devotion, your walk, isn&#8217;t good enough&#8211;why after 20+ years you must still continue to walk through hell, taking it on faith that the promise is somewhere&#8230;out there&#8230;on the other side? The response I&#8217;ve gotten from friends and family in these situations is&#8230;well we&#8217;ve walked this far for this long we &#8216;have&#8217; to keep going.

My logic: this situation must change, change it

My faith: keep walking or let them keep walking

My &#8216;power&#8217; over the situation: i am but a &#8216;spectator&#8217; in this

My duty: to just pray

My frustration: Magnified a 1000 fold.

My understanding: Apparently very little

My desire: for the situaiton(s) to change or the promise become realized.

There are the examples of Joseph and Job. Joseph who was sold into slavery by his brothers for decades, God gave Joseph dreams and visions and he ended up an advisor for Pharoh, rich and well situated. Job who lost absolutely everything; his family killed, his land and assets taken, his health stricken and he still never denied faith in God and God rewarded that and restored everything to him in the end. There are miraculous stories within the Bible of God coming into peoples lives and speaking to them directly or such other manifestation of his presence/power. But God doesn&#8217;t manifest himself, his power or his promises like he used to&#8230;such as in the old testmant or new testament&#8230;making &#8216;faith&#8217; that much more important and easier to lose I suppose too. Honestly though, in this day and age I think if God threw a bolt of lightening down and spoke people would shrug or run screaming thinking we were being invaded by aliens. Such is the skepticism of our time.

Perhaps I have to just &#8216;come to peace&#8217; with the fact I cannot or am not being allowed to actually change situations myself. Perhaps the first step is to understand that we are not &#8216;promised&#8217; our purpose. We are promised God&#8217;s purpose and it&#8217;s our decision to accept or reject that. And apparently accepting that opens us up to all the &#8216;events&#8217; that come with it whether its personal or mental injury, lessness, starvation, watching those we love suffer&#8230;

Therefore, God&#8217;s plan and purpose can come with a fantastic potential grab bag of human crap that is out of our control and his because of human choice.

Human choice. It frees us and enslaves us all at the same time.

> “_And we know that in all things God works for the good of those who love him, who have been called according to his purpose.” ~Romans 8:28_

 That&#8217;s a hard verse to swallow when those you love most suffer. And when life crashes down, and our own expectations of what&#8217;s “good” or &#8220;fair&#8221; even, fall apart, perhaps we think God has failed us. Perhaps we think he does not love us. Perhaps “all things” don’t work for the good, just some things&#8230;

I don&#8217;t doubt God&#8230;but I do have a lot of unanswerable questions.

I wish I had a summation that says &#8220;Never Fear! God is Here! HUZZAH and POOF, everything will work out and your dreams and promises WILL all come true, keep fighting the good fight you are almost there!&#8221; Why can&#8217;t I say that? Because it hasn&#8217;t happened yet, because I don&#8217;t know if &#8216;you&#8217;, &#8216;I&#8217;, or &#8216;they&#8217; are almost there and I am personally not privy to God&#8217;s plans.

What I can say is that, of all the people in my life that do things by the purpose they believe God has given them, the people that walk through every fire imaginable in their lives, and they spit out the other end&#8230;I haven&#8217;t encountered one who regrets it. No matter how hard everything got&#8230;in the end, they don&#8217;t regret it and most are quite happy and successful at the end. So along with the virtual grab bag of human crap you inevitably have to deal with&#8230;apparently, in the end, it&#8217;ll all be worth it.

Doesn&#8217;t make it any easier to watch at present but I suppose its of some small consolence.

&nbsp;
