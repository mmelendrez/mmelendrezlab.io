---
title: God said wha???
author: Mel
type: post
date: 2012-01-24T09:31:09+00:00
url: /2012/01/24/god-said-wha/
featured_image: /wp-content/uploads/2012/01/hearingGodpics-e1327396818834.png
categories:
  - Devotion
tags:
  - faith
  - God
  - "God's voice"
  - prayer
  - purpose

---
Devotional Blog:

Topic: &#8220;Confused?&#8230;go Back&#8221;, 01/24/2012, Jeremiah 24: 6-7

How does one &#8216;hear&#8217; from God? This is a loaded topic for me&#8230;

> &#8220;I will give them hearts that will recognize me as the Lord. They will be my people, and I will be their God, for they will return to me wholeheartedly&#8221; **-NIV, Jeremiah 24:7**
> 
> &nbsp;
> 
> &#8220;After the earthquake came a fire, but the LORD was not in the fire. And after the fire came a gentle whisper.&#8221; **-NIV, 1 Kings 19:12**
> 
> &nbsp;
> 
> &#8220;He who has an ear, let him hear what the Spirit says to the churches.&#8221; **-NIV, Rev 3:22**

Confused? Always. I bumble my path routinely in an effort to &#8216;hear&#8217; God and know what his plan and purpose for my life is. I say the prayers, I keep so still as to nearly faint from not breathing enough, I close my eyes and meditate during worship, think of things I&#8217;ve learned, listen to pastors and mentors more learned than me&#8230;

Majority of the time I think I must be on the wrong channel&#8230;cause all I ever get is static and I rarely know what exactly it is I am doing wrong.<!--more-->Todays devotional entry talks about going back to where you first heard God if you come upon times in your life when you are confused. What if you cannot pin point a time, place or person you were with where you &#8216;heard&#8217; God distinctly? I know of TONS of places to go where God spoke to other people&#8211;will that work? When I was younger God spoke to everyone but me it seemed. I&#8217;ve spoken on this briefly in previous blogs, where the power and plan of God seems to drop like a big neon sign for my friends and I sit&#8230;in the dark.

I&#8217;m not saying God&#8217;s power isn&#8217;t at work in my life, I have plenty examples of how he&#8217;s blessed me and &#8216;led&#8217; me apparently without &#8216;speaking&#8217; to me and I&#8217;m not saying I&#8217;ve never &#8216;felt&#8217; God&#8217;s presence or peace in my life&#8211;because I have on all accounts. But I quite literally walk through life with a flashlight on my path never really knowing where God wants me to go, where God wants me to be. I follow a concentrated beam of light in front of me that illuminates maybe 2 feet, just enough to avoid any major rocks or falls&#8211;though not all of them. I don&#8217;t know the destination, I don&#8217;t even know the next turn. My life changes in bursts&#8230;I&#8217;m not kidding. That&#8217;s why I use the analogy of a flashlight because I take one turn and think I&#8217;m going to be on a path for awhile and suddenly it veers off in a completely different direction. And vice a versa, I turn onto a path I think will have many turns and I amp myself up and prepare for craziness ahead&#8230;and it stays flat and boring for what seems like an eternity and I feel like perhaps I took the wrong turn somewhere wondering why I&#8217;m in a &#8216;holding pattern&#8217;. Mind you all this time, I can only see 2 feet or so in front of me.

Perhaps I need a better antenna for hearing God, perhaps I need to find the right &#8216;channel&#8217;, perhaps I need stronger batteries in my flashlight, perhaps I should psycho-analyze why, in the world that contains my path,&#8230;its always dark?! Psychologists would probably have a heyday in my mind.

Once again I turn to google for internet answers&#8230;&#8221;Oh pastors who post on the internet! What am I doing wrong?!&#8221;

<p style="text-align: center;">
  <a href="http://www.google.com/search?client=ubuntu&channel=fs&q=hearing+from+God&ie=utf-8&oe=utf-8"><img class="aligncenter  wp-image-736" title="hearingGod" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/hearingGod-e1327392998875.png" alt="" width="892" height="784" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/hearingGod-e1327392998875.png 892w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/hearingGod-e1327392998875-300x263.png 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px" /></a>Look at the 6th one down&#8230;seriously??? Sounds like a total advertisement&#8230;when I read it my brain read it in the cheesy salesman voice. &#8220;Hear God&#8217;s Voice!&#8211;Guaranteed&#8221;, just three easy instalments of $99.95, this offer will expire at the end of this commercial&#8230; Of course I had to click on it&#8230;
</p>

[<img class="aligncenter size-large wp-image-737" title="christianU" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/christianU-e1327393400582-1024x803.png" alt="" width="584" height="457" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/01/christianU-e1327393400582-1024x803.png 1024w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/christianU-e1327393400582-300x235.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/01/christianU-e1327393400582.png 1219w" sizes="(max-width: 584px) 85vw, 584px" />][1]Huh&#8230;wasn&#8217;t too far off. They are selling &#8216;school&#8217; essentially and books, although they do offer the free streaming audio which consequently is also the top hit on the google search. If I don&#8217;t hear God&#8217;s voice&#8230;do I get my money back?

The search results were interesting&#8230;

  1. [4 Keys to Hearing God suggests][2] (i) that God&#8217;s voice is in our heart and often sounds like a spontaneous stream of thoughts. (ii)  we should become still so we can sense and feel God&#8217;s flow of thoughts and emotions within. (iii) As you pray, fix the eyes of your heart upon Jesus, seeing in the Spirit the dreams and visions of Almighty God. (iv) Journaling, writing out of your prayers and God&#8217;s answers.
  2. Another hit from [About.com][3], I find it amusing that a top hit for hearing God&#8217;s voice is from About.com. In short it highlights advice from [Karen Wolff][4], a business woman turned motivational writer. She states that it&#8217;s a learned skill. She states that God talks to us through his Word, other people, our circumstances and most of the time manifests as a &#8216;still small voice&#8217; a prompting in our hearts. She ends everso tritefully by stating &#8220;when God talks, shut up and listen&#8221;&#8211;profound Karen&#8230;thank you.
  3. CBN to the rescue (Christian Broadcasting Ministries)&#8230;[7 keys to hearing God&#8217;s voice][5]. Ok&#8230;not 4&#8230;7. A rather long post from Craig von Buseck which I won&#8217;t get into but it echos a lot of what I have previously read: (i) scripture (ii) holy spirit speaking to your heart (iii) prophetic words (iv) Godly counsel (v) confirmation (vi) peace of God (vii) circumstances or timing.

All these sites caution the &#8216;new&#8217; Christian from misreading/mishearing God&#8217;s voice. Well I have news for them, I&#8217;ve been a devout believer in Christ for my entire life and I STILL have issues discerning his voice. Just because you &#8216;grow up&#8217; doesn&#8217;t mean you become the end all be all in hearing God&#8217;s voice. My mother is a reverend and she still is cautious when seeking/hearing God&#8217;s voice&#8211;as it should be. Now the advice on these sites isn&#8217;t bad or incorrect&#8230;I suppose it just frustrates be because out of the 11 total suggestions I&#8217;ve done/experienced all of them and I still feel like a bat flying around without sonar!

  * Stream of thoughts? I have plenty and I am super cautious in interpreting where they are from&#8230;simply because my mind is rarely quiet. So perhaps I pass off God&#8217;s voice because I am overly cautious about my ADD-like brain.
  * Stillness&#8230;did you not hear how I almost fainted from trying to be still to &#8216;hear&#8217; God&#8217;s voice?
  * Prayer&#8230;check. Dreams and visions? Not so much&#8230;sigh.
  * Journaling&#8230;does blogging count? I&#8217;m trying, I really am!
  * Scripture&#8230;Ok, I am historically terrible at this but I can remember distinct times I have specifically gone to the Bible and come away more confused or with nothin. Hopefully this blogging with devotional book and Bible will help clarify my mind.
  * Holy spirit speaking to your heart&#8230;There have been times I&#8217;ve done things simply on a &#8216;feeling&#8217; but my analytical brain has difficulty ascribing that to the Holy Spirit. Because it was a human feeling, a human desire to do something, to help etc&#8230;
  * Prophetic words&#8230;another loaded topic as I rarely got prophesized over and when I was, they kept telling me God wanted me in school.
  * Godly counsel&#8230;I grew up in the church I had tons of access to counsel and I took advantage of it quite a bit, not to mention my mother who was and is a great resource for all things Biblical/spiritual that I am confused about. I don&#8217;t always agree with her, but it does get the juices flowing.
  * confirmation&#8230;I must just keep a lot of what I think God wants me to do to myself because I&#8217;ve never really gotten confirmation from other people that I&#8217;m supposed to be somewhere or do something. My own mom who routinely prays for me and my life doesn&#8217;t even know where God&#8217;s plan is supposed to take me.
  * peace of God&#8230;This is the closest I come to &#8216;hearing God&#8217;s voice&#8217;. When I have a &#8216;peace&#8217; about something I take it as a sign that&#8217;s the path to take. It doesn&#8217;t always come immediately upon making the decision but it&#8217;s there and it gives me confidence I made the right decision.
  * Circumstances or timing&#8230;I can&#8217;t think of any situations in my life of &#8216;being in the right place at the right time&#8217;. I&#8217;m just &#8216;in places&#8217; and get things done in those places then move on&#8230;that&#8217;s been my life.

The last link I appreciated more that the previous ones: it&#8217;s just a [Bible Study posted to the net about hearing God&#8217;s voice based on scripture from Acts][6].  It doesn&#8217;t advertise, it doesn&#8217;t cite other people mostly with Bible verses &#8216;sprinkled&#8217; in for &#8216;flavor&#8217; and it gives a practical example I can follow and understand making nebulous concepts such as God&#8217;s &#8216;still small voice&#8217; a littler clearer. This study started with a story then went straight for the Bible and stayed there introducing examples a long the way. I will repost only the illustrative story as I think it hits the whole idea of trying to discern and hear Gods voice on the head, but if you are interested in the rest, click the link.

[**Sandy Gregory&#8217;s Story Of The Remote Employee**][6]

Imagine you are hired to open up an office in Anchorage, Alaska. Your new boss gives you a high-tech looking two-way radio, a policy and procedure manual, and tells you that you will receive instructions once you arrive, and off you go. Upon arrival you hear your boss&#8217;s voice over the radio, saying, &#8220;I will communicate to you through this radio unit. But take note: our competitors, our enemies, also have access to this channel. They will try to impersonate my voice with false messages to thwart our purposes.&#8221; &#8220;Oh no!&#8221; you panic, &#8220;Then how will I know if it is you or the enemy giving me instructions?&#8221;

Your boss&#8217;s voice comes back over the radio: &#8220;Three ways. First, considering the situation, _check every message_ supposedly from me against the policy and procedure manual. Since I wrote it, I&#8217;m not likely to ask you to violate it, right? Also, if I am not talking, don&#8217;t focus in on the noise, pretending that I am. If I am not speaking, let the manual be your guide. Don&#8217;t let any impersonating voice mislead you, or your own overactive imagination.&#8221;

&#8220;Second, since the Manual does not cover every situation, you will have to _get to know my voice._ I know, this will take time, and so I am not likely to ask you to do anything radical until we both have some low-risk successes under our belts. Remember, I understand the situation perfectly well, so I&#8217;ll go slow at first. A time will come when I will be able to tell you to do the wildest things, and you will know it is me. In the short-term, you must be trained through low-risk experience.&#8221;

&#8220;Third, over time, _my overall purpose_ for your work will begin to come into focus. You will begin to see the grand strategy in the policy and procedure manual, and the overall pattern of my true instructions. When this happens, you&#8217;ll know instantly if what you hear through your unit is &#8216;of me&#8217;, just your imagination, or enemy misinformation. False instructions will begin to appear silly to you then. So take heart, and get to work.&#8221;

After reflecting on this a few moments, you hear your boss&#8217;s voice again on the radio unit. &#8220;Take all of the money from petty cash and give it the next person that walks in, no questions asked.&#8221; Hmmm&#8230; You look in the policy and procedure manual, and this is specifically forbidden. Besides, you know your boss wouldn&#8217;t tell you to do something that risky right off. And also there was an certain &#8220;twang&#8221; to the voice, an appeal to something different within you, and a plan that was not in the long-term interests of the company. So, even though you are on a hostile channel, you are beginning to have hope that you can indeed do this job&#8230;

&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;

I told you this whole idea of hearing God&#8217;s voice was a &#8216;loaded&#8217; topic for me.

I have been pounded with so much frustration at not hearing God&#8217;s voice the way other&#8217;s do. Fact of the matter, I am not them and they are not me and the craziness inside my head is something only God can manage. And I know it all starts with prayer. I should pray more, that is a fault in me.

> <span style="font-family: Arial,Helvetica,sans-serif;">I have been driven many times to my knees by the overwhelming conviction that I had absolutely no other place to go. &#8212; Abraham Lincoln </span>

I&#8217;ve no other place to go, so I go to my knees and I hope that my decisions lead me where God wants me, I hope my flashlight doesn&#8217;t give out. I know God leads me even though I never know whether or not I hear him or if he even &#8216;talks&#8217; to me. Perhaps he doesn&#8217;t need to talk to me persay but rather simply open doors. I have yet to decline a door that&#8217;s been opened to me. God knows my heart, my desires, my frustrations, my limitations, my fanatical desire to be logic and faith based. I live at odds with myself quite frequently. I hope I can eventually still my mind and soul to perhaps one day hear him clearly and know its him.

 [1]: http://www.cluonline.com/Hear-Gods-Voice-2.htm
 [2]: http://www.cwgministries.org/Four-Keys-to-Hearing-Gods-Voice
 [3]: http://christianity.about.com/od/womensresources/a/hearingfromgod.htm
 [4]: http://christianity.about.com/od/womensresources/p/biokarenwolff.htm
 [5]: http://www.cbn.com/spirituallife/BibleStudyAndTheology/Discipleship/CVB_Seven_Keys_Hearing_God.aspx
 [6]: http://www.acts17-11.com/hearing.html
