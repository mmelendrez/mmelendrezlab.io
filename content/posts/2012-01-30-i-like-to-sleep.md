---
title: Apparently I like to sleep and stare at computers…
author: Mel
type: post
date: 2012-01-30T08:05:01+00:00
url: /2012/01/30/i-like-to-sleep/
featured_image: /wp-content/uploads/2012/01/sleep.jpeg
categories:
  - Devotion
  - Life or something like it
tags:
  - church
  - faith
  - sleep
  - time

---
Devotional Blog:

Topic: &#8220;Spending Time&#8221;, 12/27/2011, Ecclesiastes 3:1-8

This entry encouraged us to log our time for one week to see where our &#8216;priorities and passions&#8217; were in our lives.

So a week from Sunday to Saturday (7 days) has 168 hours in it. Of those 168, I was alseep 53.5 hours&#8230;an average of 7.6 hrs/day. Although Saturday skews that as I slept 11 hours on Saturday and and if I just look at the work week, I slept an average of 6.9 hours/day. Apparently on the weekends I like my sleep, not to mention as the week progressed I got worse at getting up on time. My alarm goes off at 6am, I managed to get up and go work Monday and Tues at 6:30am. Weds and Thurs that got later and I managed to get up and go to work at 7am&#8230;on Friday it was 730am. A lot of wake-up fail!

So 168 hours in a week minus 54.5 sleeping = 113.5 to account for waking hours&#8230;which we&#8217;ll soon find out isn&#8217;t an altogether true statement&#8230;doh!<!--more-->

Sunday consists mainly of church and frisbee followed by games with friends assuming we aren&#8217;t all dead from playing ultimate.

Monday-Friday is my typical work week where I spend a hefty amount of time answering emails and running analyses. When I have a week of cloning or lab work then that takes over any empty space I might have in the office/lab

Saturdays normally consist of breakfast and Badminton again followed by potential games with friends. This was not the case this week however. This past Saturday Tyghe and I played the super lazy card and stayed pretty much all day. I didn&#8217;t get up til 11am at which point we ate lunch, then he took a nap while I ran to the store and practiced some Viola followed by waking him up, dinner and then 4 hours of watching movies, checking email here and there. I did have a video chat scheduled&#8230;but alas sickness intervened and my friend had to cancel. I hope she&#8217;s feeling better!

So by the hours total&#8230;I spent the following according to Google calendar in any given week:

2 hours in Church, 6 hours playing frisbee, 3.25 hours eating lunch (apparently I only eat for like 15 min during the workday), 11 hours playing board games, 3.67 hours chatting with people on gmail chat during the workday whilst doing other things,  8.4 hours work related emailing (YIKES!), 18 hours on data analysis, 3 hours on Wikipedia, 6 hours looking up other things not using Wikipedia, 3 hours reading on google reader which I subscribe to several science blogs on and looking at NSF funding schemes, 4 hours video chatting (although this isn&#8217;t typical of every week), 2 hours shopping for food or other, Apparently I spent 2 hours in any given week &#8216;lost&#8217; in some part of Bangkok&#8211;this week I got lost on the way to Kasetsart University while attempting to meet Tyghe for dinner, 1 hour on work related phone calls, 2 hours watching downloaded TV shows, 3 hours practicing my Viola, 6 hours figuring out my taxes, 7 hours on facebook, 4 hours writing this blog in any given week, 5 hours asking Tyghe for help on one thing or another techy related (example, he rooted my HTC this week then I didn&#8217;t know how to work some things on it), 2 hours phasing out (aka, watching status lines run on my ClonalFrame analysis), 6 hours on fellowship related paperwork/reports, and finally, in addition to the 54.5 hours I sleep in a week&#8230;I also apparently nap an additional 8 hours!

Add it all up? 110.32 hours. That leaves 3.18 hours unaccounted for, lost to the twilight zone or probably amounts to the time I spend using the bathroom, showering, walking from office to office at work, getting to/from work etc&#8230;

So out of 168 hours what percentage am I:

Unconscious: 37.1%

Staring at a computer: 42%

Absorbing light other than flourescent: 3.6% although this is a bit misleading as I absorb sunshine whilst walking to/from work and to/from lunch.

Watching &#8216;TV&#8217; or movies: 3.6%

Zoning out: 1.2%

Playing boardgames: 6.5%

Playing Viola: 1.8%

Lost: 1.2%

Asking others for help: 3.0%

Interesting, I spend more time staring at a computer than sleeping even! If anything is going to &#8216;compete&#8217; for my job its not going to be activities or hobbies or being social with others&#8230;nope it&#8217;s going to be the desire to just sleep&#8230;nice.

> There is a time for everything, a season for every activity under heaven. NIV, Ecclesiastes 3:1

So going back to the blog, where does my passion lie? In sleeping and doing stuff on computers! How&#8230;sad. If we break down the computer stuff you&#8217;ll see much of it is job related so I do spend a great deal of time on my job&#8230;which makes sense as that is what I am paid to do here. As you can also see, there was no lab work this week as I am primarily doing analysis for manuscripts. The shutdown of Wikipedia didn&#8217;t effect me much as I only spend about 3 hours a week on Wikipedia. Most of the time I am on scientific literature search engines such as PubMed, Biological Abstracts or GoogleScholar.

I am a little encouraged that I spend 4 hours on this devotional blog as that was the whole point of starting this in the first place so that I might spend some time during the week contemplating my faith etc. Prior to this blog, church was pretty much it for contemplation about my faith. So I&#8217;ve found about 4 additional hours during the week for God and things of faith. I hope to increase that in the future.

Highlights to note for this past week:

  1. I picked up some Chinese new years cards.
  2. I wiki&#8217;d caucasian mountain dogs&#8230;I don&#8217;t remember why&#8230;
  3. Chatted w/ a friend about her engagement
  4. Watched Tyghe hack my phone
  5. napping
  6. Emailing potential wedding photographers
  7. Playing frisbee
  8. Video chatting with my sister
  9. and of course&#8230;staring at status lines on a Windows DOS Run command screen

That&#8217;s right, be jealous of my life&#8230;
