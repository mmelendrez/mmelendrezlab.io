---
title: A space cadets moment
author: Mel
type: post
date: 2011-10-04T13:09:02+00:00
url: /2011/10/04/a-space-cadets-moment/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"504f210c62";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
wordbooker_extract:
  - |
    Devotional Blog:
    
    Topic: "Nothing to show for it", 9/27/11, Psalm 90: 1-6, 12-17
    
    After reading this verse and section I found myself taking an almost opposing stance to what the author suggests. The take  message coming from Pslam 90:12.
    "Tea ...
categories:
  - Devotion

---
Devotional Blog:

Topic: &#8220;Nothing to show for it&#8221;, 9/27/11, Psalm 90: 1-6, 12-17

After reading this verse and section I found myself taking an almost opposing stance to what the author suggests. The take message coming from Pslam 90:12.

> &#8220;Teach us to make the most of our time, so that we may grow in wisdom.&#8221;

She goes on to discuss that instead of wasting our time watching TV we could make lists, clean, pray, write a book&#8230;use the time to &#8216;prepare&#8217; for something. She goes on to say that the Bible has over twenty references to a &#8216;moment&#8217; which is just an instant of short piece of time and that if a moment is important to God, then it can be important to us. That much I agree with&#8230;moments matter.

<!--more-->What do I do in &#8216;moments&#8217;, honestly&#8230;I don&#8217;t really watch TV, I space cadet out, often times brought back to reality by someone flicking their fingers or waving their hand in front of me. One of these days I should try and count the myriad of thoughts that go through my head in that moment. Often times people will ask what in the world was I thinking that made me zone out like that. Most of the time I cannot answer them as it&#8217;s more of a steam of consciousness or stream of emotions if you will than any intelligible thoughts. But it quiets  my mind.

You&#8217;ll notice I am writing this blog &#8216;from the future&#8217; as the time stamp on it is October 4th and the reading from the 27th of September. In that time, Tyghe&#8217;s step-mother who he is quite close to and grew up with passed away from Cancer; mantel cell lymphoma for those curious to know. She&#8217;d been battling the disease for 2 years. I suppose you can guess what my thoughts have been consumed by recently.

A list of things I ponder in a moment: death, love, faith, BEAST analysis, ladybugs, landscapes, the mist, snow, the current book I am reading, the reason I opened my last bottle of wine, art/crafts, music, scents that transport me back in time&#8230;.inevitably everything digressing into a haze of color and feeling with a lull of melody in the background.

The author mentions not wasting moments with things like TV, be productive instead. I disagree in a sense. I think everyone needs quiet moments, while not actively being productive on the outside I believe that a quiet haze of thought taking up moments in time is important. It calms the mind, something our rushed busy chaotic society of individuals desperately needs sometimes.

&nbsp;
