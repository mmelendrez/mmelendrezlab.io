---
title: 'Nepal: chaos, color and a lot of monkeys…'
author: Mel
type: post
date: 2011-09-20T04:03:22+00:00
url: /2011/09/20/nepal-chaos-color-and-a-lot-of-monkeys/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"722bbeb83e";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
wordbooker_thumb:
  - http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np1.jpg
wordbooker_extract:
  - "So kudos to my cousin Lauren and my friend Heather for reminding me I still haven't posted about Nepal...but the Masala tea I sent them did make it to them...woohoo! Sometimes mail even from a US APO address may take a lifetime to reach its destination ..."
categories:
  - Asia
tags:
  - Asia
  - Nepal

---
So kudos to my cousin Lauren and my friend Heather for reminding me I still haven&#8217;t posted about Nepal&#8230;but the Masala tea I sent them did make it to them&#8230;woohoo! Sometimes mail even from a US APO address may take a lifetime to reach its destination, it might as well have been put on the slow boat to China for all I know&#8230;except maybe that wouldn&#8217;t work&#8211;China is actually quite close to me. But you get the idea.

So back in August I traveled to Kathmandu, Nepal to attend a conference/workshop on Flu in SE Asia and it was quite interesting. Though the majority of the workshop didn&#8217;t really deal with my specific field I did learn a lot about epidemiology and analysis that goes with that kind of data which is supplemental and valuable to know. So that was neat.

The first day I arrived I was really tired and though I arrived in the afternoon I had little energy to venture beyond the hotel, [Dwarika&#8217;s Hotel][1]. I made it as far as the intersection. I don&#8217;t know what I expected, it was a lot more chaotic than I&#8217;d expected and there were tons of people. In my head I was thinking&#8230;damn if I can&#8217;t handle this how would I ever survive going anywhere in India?! Haha. I think it was just a lot to take in at first. The hotel was amazing, thank God for per diems and conference discounts that allowed me to stay there, it really was beautiful.

And just a note: In keeping with the chaos that Nepal was this blog is written in an organized chronological manner with absolute chaos with regard to the pictures and text. I&#8217;d like to think its my underlying &#8216;motif&#8217; or &#8216;commentary&#8217; on the crazy&#8230;but alas, I am not that smart to have thought that up prior. Instead its more a result of my lack in ability to use wordpress to post pictures in a non-chaotic format with respect to the text. I got better toward the end though.<!--more-->

[<img class="size-full wp-image-284 alignleft" title="np1" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np1.jpg" alt="" width="96" height="128" />][2]

[<img class="size-full wp-image-287 alignleft" title="np4" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np4.jpg" alt="" width="128" height="96" />][3][<img class="size-full wp-image-286 alignleft" title="np3" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np3.jpg" alt="" width="128" height="96" />][4][<img class="size-full wp-image-285 alignright" title="np2" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np2.jpg" alt="" width="96" height="128" />][5]The conference took up most of my time while in Nepal so unfortunately I wasn&#8217;t able to get out of Kathmandu except to get to Bhaktapur which was beautiful. And sadly I didn&#8217;t get to see the Himalayas due to the monsoon season. But I did see a lot of beautiful things. My walking/taxi adventures took me all over Kathmandu and while there I met Laveta and her husband. She&#8217;s been studying in the Terai for nearly a year, spoke decent Nepali and I had dinner with them a few times and her and I hiked up to one of the temples which was a lot of fun, yay for meeting new friends.

My adventures: don&#8217;t worry i&#8217;ll zoom in as we go&#8230;[link to the actual map][6]

[<img class="aligncenter size-medium wp-image-289" title="Screenshot1" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot1-300x239.png" alt="" width="300" height="239" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot1-300x239.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot1-375x300.png 375w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot1.png 900w" sizes="(max-width: 300px) 85vw, 300px" />][7]My first adventure was in Thamel, just walking around and souvenir hunting&#8211;aka pashmina hunting&#8211;oh me and soft things, it took a lot of restraint to not jump into the piles of pashminas in the store I bought them from. And it took all my strength to actually MAIL the ones I&#8217;d bought for my friends and family&#8211;haha! The map is of my Thamel and Durbar Square walk&#8230;I didn&#8217;t do all this in one day 🙂 rather over a few days&#8230;

[<img class="aligncenter size-medium wp-image-290" title="Screenshot2" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot2-300x240.png" alt="" width="300" height="240" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot2-300x240.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot2-375x300.png 375w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot2.png 900w" sizes="(max-width: 300px) 85vw, 300px" />][8]<figure id="attachment_291" style="width: 96px" class="wp-caption alignleft">

[<img class="size-full wp-image-291" title="np5" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np5.jpg" alt="" width="96" height="128" />][9]<figcaption class="wp-caption-text">Main road in Thamel</figcaption></figure> <figure id="attachment_292" style="width: 128px" class="wp-caption alignleft">[<img class="size-full wp-image-292" title="np6" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np6.jpg" alt="" width="128" height="96" />][10]<figcaption class="wp-caption-text">some really interesting tall palms i'd never seen before</figcaption></figure> <figure id="attachment_293" style="width: 96px" class="wp-caption alignleft">[<img class="size-full wp-image-293" title="np7" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np7.jpg" alt="" width="96" height="128" />][11]<figcaption class="wp-caption-text">Temple in the city</figcaption></figure> 

The fair trade souvenir shop that Laveta&#8217;s husband took me to was called [Mahugathi&#8217;s][12] and it had a really large selection of pahsminas and other items and all the proceeds go back to the villages and people that make the items so that was really neat.

Laveta and I then found a time to head up to Pashupatinath Temple which was actually quite close to the Hotel. It is where last rites and cremation ceremonies are conducted. You are not allowed in unless you are Hindu but we were able to take in the views and different sites around the Temple compound area and it was really cool. Oh and there were a lot of monkey&#8217;s on the way up.[<img class="aligncenter size-medium wp-image-330" title="Screenshot3" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot3-300x239.png" alt="" width="300" height="239" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot3-300x239.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot3-375x300.png 375w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot3.png 900w" sizes="(max-width: 300px) 85vw, 300px" />][13]<figure id="attachment_298" style="width: 128px" class="wp-caption aligncenter">

[<img class="size-full wp-image-298 " title="bp14" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/bp14.jpg" alt="" width="128" height="96" />][14]<figcaption class="wp-caption-text">The Himalayas!...can't you see them?</figcaption></figure> 

[<img class="size-full wp-image-304 alignleft" title="np9" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np9.jpg" alt="" width="128" height="96" />][15]

[<img class="size-full wp-image-306 alignleft" title="np11" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np11.jpg" alt="" width="128" height="96" />][16][<img class="size-full wp-image-307 alignleft" title="np12" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np12.jpg" alt="" width="128" height="96" />][17]<figure id="attachment_308" style="width: 128px" class="wp-caption alignleft">

[<img class="size-full wp-image-308 " title="np13" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np13.jpg" alt="" width="128" height="96" />][18]<figcaption class="wp-caption-text">The temple from afar...we had to climb up and over to see it...</figcaption></figure> 

[<img class="size-full wp-image-305 alignright" title="np10" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np10.jpg" alt="" width="96" height="128" />][19]<figure id="attachment_312" style="width: 128px" class="wp-caption alignleft">

[<img class="size-full wp-image-312" title="np19" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np19.jpg" alt="" width="128" height="96" />][20]<figcaption class="wp-caption-text">hands down prettiest temple I'd seen yet...</figcaption></figure> 

[<img class="size-full wp-image-310 alignright" title="np17" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np17.jpg" alt="" width="128" height="96" />][21][<img class="size-full wp-image-309 alignright" title="np16" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np16.jpg" alt="" width="128" height="96" />][22]<figure id="attachment_313" style="width: 128px" class="wp-caption alignleft">

[<img class="size-full wp-image-313 " title="np20" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np20.jpg" alt="" width="128" height="96" />][23]<figcaption class="wp-caption-text">ta da! Pashputi...as they call it</figcaption></figure> 

[<img class="alignleft size-full wp-image-316" title="np23" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np23.jpg" alt="" width="128" height="96" />][24]

&nbsp;

[<img class="size-full wp-image-311 alignleft" title="np18" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np18.jpg" alt="" width="128" height="96" />][25]

&nbsp;

[<img class="alignleft size-full wp-image-314" title="np21" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np21.jpg" alt="" width="96" height="128" />][26]

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

It was a long day but it was really interesting to see what was going on at the temple. Never exactly seen cremations taking place. Prior to cremation they would wash themselves in the river. A cleansing ritual I assume. Before heading the temple we detoured up the mountain some more and found this red and white temple in this stupa area which was really neat. There weren&#8217;t a ton of people there but there were a ton of monkeys and they decided to attack one of the ladies scarves as it hung too close to the ground and proved too tempting. [<img class="alignright size-full wp-image-317" title="np24" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np24.jpg" alt="" width="128" height="96" />][27]From there we headed into Thamel for some dinner where I got this shot just as everything was closing up&#8230;at around 9pm I&#8217;d say. Everything pretty much shuts down early in Kathamandu, it was pretty interesting. I&#8217;m sure the bars at the guesthouses stocked with trekkers stayed open but tourism as it were closes down early.

Next stop: Swayambunath Temple

[<img class="aligncenter size-medium wp-image-331" title="Screenshot4" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot4-300x240.png" alt="" width="300" height="240" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot4-300x240.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot4-375x300.png 375w, http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot4.png 900w" sizes="(max-width: 300px) 85vw, 300px" />][28]My next adventure I did solo. It was to hike up the stairs to Swayambunath Temple, known as the Monkey Temple&#8230;but I&#8217;ve seen monkey&#8217;s EVERYWHERE at this point&#8230;but hey&#8230;lets go for a hike&#8230;[<img class="size-full wp-image-318 alignleft" title="np25" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np25.jpg" alt="" width="96" height="128" />][29]

Up a MILLION stairs! A kid decided to join me for about 75 stairs attempting to be my tour guide and informing me that it was 365 stairs to be exact and asked if I wanted to know more history, be taken to some souvenir shops and I said &#8220;Thank you, you are very nice but I am not looking for a tour guide&#8221;. Was it the lonely planet guide I was carrying that gave me away as a tourist? Or my blatantly white skin! Haha&#8230;[<img class="alignleft size-full wp-image-319" title="np26" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np26.jpg" alt="" width="96" height="128" />][30]

[<img class="alignleft size-full wp-image-322" title="np29" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np29.jpg" alt="" width="96" height="128" />][31][<img class="size-full wp-image-320 alignleft" title="np27" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np27.jpg" alt="" width="96" height="128" />][32][<img class="alignleft size-full wp-image-323" title="np30" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np30.jpg" alt="" width="96" height="128" />][33][<img class="alignleft size-full wp-image-324" title="np31" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np31.jpg" alt="" width="128" height="96" />][34][<img class="alignleft size-full wp-image-325" title="np32" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np32.jpg" alt="" width="128" height="96" />][35][<img class="alignleft size-full wp-image-326" title="np33" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np33.jpg" alt="" width="128" height="96" />][36][<img class="alignleft size-full wp-image-327" title="np34" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np34.jpg" alt="" width="96" height="128" />][37][<img class="alignleft size-full wp-image-328" title="np35" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np35.jpg" alt="" width="96" height="128" />][38][<img class="alignleft size-full wp-image-329" title="np36" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np36.jpg" alt="" width="81" height="128" />][39]

&nbsp;

&nbsp;

&nbsp;

&nbsp;

It was a heinously hot day &#8230;don&#8217;t get me wrong, blue skies, puffy white clouds&#8230;that&#8217;s great but when you are climbing a million stone stairs the blazing sun does not become ones friend. The Stupa was pretty cool though and there happen to be a festival going on that day so it was SUPER crowded, a lot of candles were lit, a lot of red powder (tikas?) being put on people and everyone paying their respects. I don&#8217;t know what festival it was but it looked pretty interesting. On the way down I saw these monk boys hiding from the sun&#8230;I thought it was pretty cute.<figure id="attachment_336" style="width: 128px" class="wp-caption alignright">

[<img class="size-full wp-image-336" title="np41" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np41.jpg" alt="" width="128" height="96" />][40]<figcaption class="wp-caption-text">the bead shops</figcaption></figure> 

[<img class="size-full wp-image-332 alignleft" title="np37" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np37.jpg" alt="" width="96" height="128" />][41]The following day I ventured down south of Thamel area to Durbar Square Kathmandu&#8230;there are actually more than one Durbar Square, its the &#8216;seat&#8217; of government for various towns&#8230;so Kathamandu has one and so does Bhaktapur and many other towns in Nepal. See above Thamel map, southern portion that was more or less my walk. During my walk I looked down various alleyways and came across this stupa area which was kind of neat, literally buried in the city area. It was here I found prayer flags for some friends which I have yet to mail! DOH! Need to find a box or something big enough since they are full sized. [<img class="alignleft size-full wp-image-333" title="np38" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np38.jpg" alt="" width="96" height="128" />][42]I also found this random blue door. I don&#8217;t know why I liked it so much but I did and of course I therefore had to take a picture of it. It was the only blue item in the entire road I was walking on. It was cool.[<img class="alignright size-full wp-image-334" title="np39" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np39.jpg" alt="" width="96" height="128" />][43][<img class="alignright size-full wp-image-335" title="np40" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np40.jpg" alt="" width="128" height="96" />][44][<img class="size-full wp-image-340 alignleft" title="np45" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np45.jpg" alt="" width="128" height="96" />][45][<img class="alignleft size-full wp-image-337" title="np42" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np42.jpg" alt="" width="96" height="128" />][46][<img class="alignleft size-full wp-image-338" title="np43" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np43.jpg" alt="" width="128" height="96" />][47][<img class="size-full wp-image-342 alignright" title="np47" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np47.jpg" alt="" width="128" height="96" />][48] I kept walking and came across some puppet makers a few other shrines that were set up with candles and pictures and of course quite a few rickshaws all offering me a ride.

&nbsp;

[<img class="size-full wp-image-341 alignleft" title="np46" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np46.jpg" alt="" width="96" height="128" />][49]Then entered Durbar square which was actually pretty interesting, I had to pay a fee to enter, but it didn&#8217;t bother me too much and there were a lot of tourists there, many with their own guides. I walked through the square and had lunch at a rooftop restaurant which gave me a good view of the area and one of the old palaces which sadly I did not get a picture of as my camera ran out of battery.

One of our satellite labs for  AFRIMS is located in Kathmandu and Dr. Sanjaya came and took me and one of the other ladies on a tour of the facilities at their building as well as one of their field sites up in Bhaktapur where we screen for dengue and influenza. Bhaktapur was very different from Kathmandu as it was made mostly from red brick and the contrast between the brick and the green valleys was striking. And of course there was a festival going on so there were copious amounts of people when we made it to Bhaktapur&#8217;s Durbar Square. It was crazy but really cool.

[<img class="alignnone size-full wp-image-346" title="np50" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np50.jpg" alt="" width="128" height="96" />][50][<img class="alignnone size-full wp-image-347" title="np51" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np51.jpg" alt="" width="128" height="96" />][51][<img class="alignnone size-full wp-image-344" title="np48" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np48.jpg" alt="" width="128" height="96" />][52][<img class="alignnone size-full wp-image-345" title="np49" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np49.jpg" alt="" width="128" height="96" />][53]

The chicken cracked me up, her baby chick was hiding by her leg so I got a closer picture or rather attempted to get a closer picture.

[<img class="alignnone size-full wp-image-348" title="np52" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np52.jpg" alt="" width="128" height="96" />][54][<img class="alignnone size-full wp-image-349" title="np53" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np53.jpg" alt="" width="96" height="128" />][55]

The Durbar square in Bhaktapur seemed bigger than the one in Kathamandu, the town when we walked through it was pretty much empty because everyone was here for the festival.

[<img class="alignnone size-full wp-image-351" title="np55" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np55.jpg" alt="" width="96" height="128" />][56][<img class="alignnone size-full wp-image-353" title="np57" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np57.jpg" alt="" width="128" height="96" />][57][<img class="alignnone size-full wp-image-354" title="np58" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np58.jpg" alt="" width="128" height="96" />][58][<img class="alignnone size-full wp-image-350" title="np54" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np54.jpg" alt="" width="96" height="128" />][59]

[<img class="alignnone size-full wp-image-355" title="np59" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np59.jpg" alt="" width="96" height="128" />][60]

As we walked away and back to the hospital to ride back into to Kathmandu I saw something I couldn&#8217;t resist taking a picture of. All I could think was &#8220;one of these things is not like the other thing&#8230;&#8221; I know the picture is small but perhaps you can see it.

[<img class="aligncenter size-full wp-image-357" title="np61" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np61.jpg" alt="" width="128" height="96" />][61]you are a goat&#8230;not a motorcycle&#8230;but nice try.

My last adventure in Nepal before flying out was to see the Boudhanath Stupa which is another Stupa nestled within the city surrounded in fact by a huge circular wall. Apparently pilgrims come to pray, chants, sing, prostrate themselves, play instruments and walk in a clockwise motion around the Stupa. It was pretty amazing. This was my last night in Kathmandu. After walking around the stupa and taking some pictures I found a rooftop terrace restaurant and prayed for the rain to hold off. I sat at the table over looking the stupa and the valley. I could see mountains on the left and right and the clouds were literally rolling off them&#8230;but it didn&#8217;t storm and I stayed well into the evening, writing letters/postcards, eating a Nepali dinner, reading and drinking Masala tea, it was a really nice quiet time for me. Perfect way to end my trip.

[<img class="alignnone size-full wp-image-358" title="np61" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np611.jpg" alt="" width="128" height="96" />][62][<img class="alignnone size-full wp-image-359" title="np62" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np62.jpg" alt="" width="96" height="128" />][63][<img class="alignnone size-full wp-image-360" title="np63" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np63.jpg" alt="" width="128" height="96" />][64][<img class="alignnone size-full wp-image-361" title="np64" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np64.jpg" alt="" width="96" height="128" />][65][<img class="alignnone size-full wp-image-362" title="np65" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np65.jpg" alt="" width="128" height="96" />][66]

[<img class="alignleft size-full wp-image-363" title="np66" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np66.jpg" alt="" width="96" height="128" />][67]And for those of you that may think I ripped these pictures off 😉 Yes, I truly was in Nepal, here I am apparently photographed by a nice lady who thought she&#8217;d get artsy with the photo haha. If you are interested in seeing the actual pictures with full detail there are in my [picasa album][68] which is public access.

Cheers! <3

 [1]: http://www.dwarikas.com/
 [2]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np1.jpg
 [3]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np4.jpg
 [4]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np3.jpg
 [5]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np2.jpg
 [6]: http://g.co/maps/pwwnq
 [7]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot1.png
 [8]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot2.png
 [9]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np5.jpg
 [10]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np6.jpg
 [11]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np7.jpg
 [12]: http://www.lonelyplanet.com/travelblogs/165/23351/Fair+Trade+Shops+in+Kathmandu+%26+Patan?destId=357120
 [13]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot3.png
 [14]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/bp14.jpg
 [15]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np9.jpg
 [16]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np11.jpg
 [17]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np12.jpg
 [18]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np13.jpg
 [19]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np10.jpg
 [20]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np19.jpg
 [21]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np17.jpg
 [22]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np16.jpg
 [23]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np20.jpg
 [24]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np23.jpg
 [25]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np18.jpg
 [26]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np21.jpg
 [27]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np24.jpg
 [28]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/Screenshot4.png
 [29]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np25.jpg
 [30]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np26.jpg
 [31]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np29.jpg
 [32]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np27.jpg
 [33]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np30.jpg
 [34]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np31.jpg
 [35]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np32.jpg
 [36]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np33.jpg
 [37]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np34.jpg
 [38]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np35.jpg
 [39]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np36.jpg
 [40]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np41.jpg
 [41]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np37.jpg
 [42]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np38.jpg
 [43]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np39.jpg
 [44]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np40.jpg
 [45]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np45.jpg
 [46]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np42.jpg
 [47]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np43.jpg
 [48]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np47.jpg
 [49]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np46.jpg
 [50]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np50.jpg
 [51]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np51.jpg
 [52]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np48.jpg
 [53]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np49.jpg
 [54]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np52.jpg
 [55]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np53.jpg
 [56]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np55.jpg
 [57]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np57.jpg
 [58]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np58.jpg
 [59]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np54.jpg
 [60]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np59.jpg
 [61]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np61.jpg
 [62]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np611.jpg
 [63]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np62.jpg
 [64]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np63.jpg
 [65]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np64.jpg
 [66]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np65.jpg
 [67]: http://www.melaniemelendrez.com/wp-content/uploads/2011/09/np66.jpg
 [68]: http://picasaweb.google.com/mmelendrez/Nepal?authuser=0&feat=directlink
