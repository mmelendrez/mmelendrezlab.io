---
title: greener grass?
author: Mel
type: post
date: 2011-11-04T03:03:20+00:00
url: /2011/11/04/greener-grass/
categories:
  - Devotion
tags:
  - Asia
  - childhood
  - church
  - envy
  - God
  - graduate
  - life
  - living
  - PhD
  - reality

---
> All your life you live so close to truth it becomes a permanent blur in the corner of your eye. And when something nudges it into outline, it&#8217;s like being ambushed by a grotesque.
> 
> ~Tom Stoppard, Rosencrantz and Guildenstern are Dead

Devotional Blog: 10/22/2011, 10/26/2011, and 10/28/2011; &#8220;Being true to yourself&#8221; and &#8220;Fact and fiction&#8221;; Romans 12: 12-18, Acts 2: 32-39, and 2 Peter 1:15-21

I&#8217;m lumping these entries together because they speak of similar things regarding how we see ourselves compared to others, how others make us feel about ourselves and the alternate realities we concoct of a life &#8216;we want&#8217; rather than the life we are supposed to be living.<!--more-->

In Romans 12:12-18 its all about attitude&#8230;your attitude to other people and in general.

> &#8220;Be joyful in hope, patient in affliction, faithful in prayer&#8230;bless those who persecute you, rejoionce with those who rejoice, mourn with those who mourn. Live in harmony with one another. Do not be proud&#8230;Do not be conceited&#8230;Do not repay anyone evil for evil&#8230;&#8221; (NIV)

I can be a stubborn creature, wanting what I want and wanting right now. Do I sound like a two year old yet? When I don&#8217;t understand life I pout and drown my confusion and frustration usually in a bottle or two of wine. Though I do that less now. I&#8217;d focus on what I didn&#8217;t have, how unfair life was, how lonely I was (even when I was in relationships), how I felt no one understood me but also wasn&#8217;t willing to share certain parts of who I was&#8211;thereby not even giving them a chance to even try and understand. My family grew up under the arm of judgement from many people. When I grew up and left for college and life&#8230;I took that judgement and fear of more judgement with me. I came to expect it from people. I focused a lot on what I did and how others perceived me and less on being who I was.

I&#8217;m not saying I wasn&#8217;t &#8216;me&#8217;, I totally was&#8211;but I suppose a more limited version of me. I came out of that mindset in grad school but it still hung around occasionally mostly when I was alone with myself and it limited my &#8216;total abandon&#8217; of being who I was. Depending on the circumstance I can still be like that&#8230;clamming up my true feelings or personality because I am too tired to &#8216;debate it&#8217; or rather I don&#8217;t want to debate it. I go back and forth between speaking my mind and being willing to debate with those who disagree not caring what they think of me and really caring what someone thinks of me even though I shouldn&#8217;t, thereby clamming up. I went through a phase of &#8216;whatever&#8217; and trying not to care about what others thought. I think anyone who uses this method is lying to themselves, as much as you don&#8217;t want what someone else thinks of you to effect you&#8230;it just does even though you don&#8217;t show it.

Further on the author (Pam) discusses how we&#8217;ve all become walking resumes and rather than being who we are or &#8216;doing what we are&#8217;&#8230;we&#8217;ve become what we do. She cautions against defining yourself by what you do. I&#8217;ve done a lot of things. Some of which I am proud of, others I&#8217;d rather forget forever and wish had never been a part of my life. Is my life my work? Depends on which my friends you ask LOL.

Yes, my work is a huge part of my life but I wouldn&#8217;t define myself by it mainly because I am not inherently adept at what I do. It&#8217;s not inherently who I am. Some people are amazing at the same work I do. They think perfectly, get things done 2x faster than I do because their brains and lives work better in this environment. I do this work because it&#8217;s challenging and I love it, but it&#8217;s not who I am.

Ever looked at other lives and been envious? I have! Its the interminable grass is always greener mentality. I&#8217;d look at the things my friends that I grew up with are doing now with their lives and I&#8217;d &#8216;want&#8217; that. In my mind I saw them making a difference and I wanted that so bad. I wanted the instant gratification of the work they did which showed them the &#8216;fruits of their labor&#8217; right away. My work doesn&#8217;t do that. I may never know the overall impact my work has while I am alive.

Examples:

  * I have a high school friend that is married to a Dentist and they live in Africa and are medical missionaries. They go out into the bush and perform dental procedures for the tribes and anyone in need out there. She helps her husband with this work. They have a beautiful daughter. If this interests you and what they do is pretty cool, you can google &#8216;mozambique medical missions&#8217; and it should be the top video hit, their last name is Costa. I&#8217;d put the link but my internet is &#8216;tarding&#8217; out. I&#8217;ll be happy if this blog posts today!
  * When I was in grad school it was really bad. I felt like I was langushing as my high school friends went off and became doctors, lawyers, got married, joined the peace corp etc&#8230;I imagined showing up to my high school reunion, unmarried, childless and still in school&#8230;talk about feeling inadequate.

I wanted my life to be more &#8216;impactual&#8217;. When I was little I desperately wanted to be a medical missionary or work for an NGO. My last year in graduate school I fought to graduate and during that time, many times I contemplated quitting. What graduate student doesn&#8217;t? Many times I&#8217;d ask myself, did I miss the mark for my life? Was I supposed to take another path? I&#8217;d often day dream about another life a mentally simpler one perhaps&#8230;working at an NGO mostly, humanitarian relief etc. I felt I didn&#8217;t &#8216;need&#8217; my Ph.D. to be happy, that I could be just as happy or happier even doing something else, anything else. I think for a graduate student there comes a point when you realize that its just a piece of paper and its not the fricken holy grail of happiness, that you can do without it&#8211;that&#8217;s when you graduate LOL. Science was never my strongest subject in school, languages and literature were. My high school friends were really surprised when I ended up in hard science. I think I was surprised too. I loved the challenge of science, I liked that it made me think&#8211;and frequently made my head hurt, that I liked less.

I would fantasize about the lives of others that I envied, which I know I shouldn&#8217;t do but when life was frustrating for me, it was a form of escape. I&#8217;d make up &#8216;other lives&#8217; alternate realities, fiction in effect. For many years I lost the joy of the reality of my own life and what I did.

I&#8217;ve never been a &#8216;spiritual&#8217; God speaks to me everyday kind of person. Many Christians would say that&#8217;s my fault, and perhaps it is, for not seeking my faith and relationship with God harder. But I&#8217;ve always believed God puts people exactly where they are supposed to go as long as you aren&#8217;t stubborn or picky. If a door opens I walk through it.

  * I hated my undergraduate institution for the most part. Forensics and some very awesome friends were my saving grace while there. I applied to several colleges, PLU was the only conceivable one I got accepted to. The other was in Maine and much more expensive. So the door opened&#8230;I went to PLU.
  * My family couldn&#8217;t pay for college. I paid myself, anyway I could, including working up to 4 jobs at one point and applying for any and all scholarships and funding. Many times I&#8217;d pay tuition and have no money left for food, oddly enough a friend would invite me to dinner that night, it was pretty funny at the time&#8211;somehow I always ate, always had a roof, money would come from the oddest places. One year I got a very late tax refund&#8230;and it showed up exactly when I needed it. I&#8217;d fantasize about transferring to to other schools and would try, the doors always closed. Things always seemed to work out in favor of staying at PLU. So, much as I disliked my school, inside I felt that was where I was just supposed to be. Because I&#8217;d tried to &#8216;get out&#8217; and it wasn&#8217;t working LOL.
  * After college I got a rotary ambassadorial scholarship to Costa Rica. Logistics became a nightmare then I got hit with a medical condition where I couldn&#8217;t travel out fo the country&#8230;door closed. I contemplated going home to Hawaii instead but didn&#8217;t know if I&#8217;d have the money for a ticket&#8230;I hopped online, $383 (tickets are normally heinously more expensive to Hawaii), hmmm, k, guess I&#8217;m supposed to go home&#8230;I&#8217;ll admit that was a depressing moment though. I got frustrated and complained to God why did I even GET the stupid scholarship if I wasn&#8217;t supposed to go&#8211;enter pouting and bottle of wine. Bottom-line the door just closed, I couldn&#8217;t argue it.
  * Sick of working in Hawaii I sent out 60 inquiries for graduate positions in the US in the field of microbial ecology. I got three hits in Montana. Montana&#8230;yes, do you know how many times when I tell people I am from Hawaii they do a double take and ask what the hell am I doing in Montana!!?? The door opened in Montana, I took it&#8230;one of the best decisions of my life I think&#8211;odd as I felt it was that I was in Montana haha.
  * I applied for 4 different fellowships after grad school, got high marks on all my proposals but no one bit&#8230;until Thailand. I&#8217;d never imagined living in Asia&#8230;I&#8217;m a latin girl bilingual in Spanish, I thought if I ended up out of the country I&#8217;d be back in Central or South America! Spanish doesn&#8217;t exactly help me here. The door opened here, I took it.

I&#8217;ve always just followed the open doors/windows. And perhaps that&#8217;s how God speaks to me. He knows that if a door opens I&#8217;ll walk through it without much question&#8230;so perhaps he doesn&#8217;t have to &#8216;talk&#8217; to me or throw down a neon sign, he just opens the door.

I am learning to like/love who I am for who I am. I am learning not to envy others nor fantasize about a life I know I am not supposed to have. Overall I am happier doing what I am doing rather than doing what I think I want to be doing. I think if I was working for an NGO I&#8217;d be fantasizing about science&#8230;because that&#8217;s just how the greener grass works. It&#8217;s an interesting reality check for me when I chat with some friends or sisters and they tell me how exciting and/or cool my life must be. As the one living it, I don&#8217;t immediately see it that way but yes it is cool I am out in Thailand, it is cool I got my Ph.D., it is cool that I&#8217;ve been blessed to get my education, travel to several places in the world, have experiences others dream about&#8230; I believe God has shown a lot of favor in my life and blessed me immensely and I am thankful for that.

I think the fact/reality of my life is better than any fiction I could&#8217;ve made up and I have to remind myself of that. I need to let myself be who God made me to be and live the life that was meant for me and only me to live.
