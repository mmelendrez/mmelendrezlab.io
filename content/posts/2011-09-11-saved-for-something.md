---
title: saved for something…
author: Mel
type: post
date: 2011-09-11T07:28:12+00:00
url: /2011/09/11/saved-for-something/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"0001b49998";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";s:17:"wordbook_new_post";s:1:"1";}'
wordbooker_extract:
  - |
    Devotional Blog:
    
    Topic: A reason you are 'here'... 9/11/11: 1 Timothy 2:1-7
    
    I thought it uncanny and eerie that I should read about this today...it being the 10th Anniversary of the World Trade Center tragedy. Last week in my perpetual hunt for b ...
categories:
  - Devotion
tags:
  - 9/11
  - existing
  - faith
  - holocaust
  - life
  - purpose
  - war

---
Devotional Blog:

Topic: A reason you are &#8216;here&#8217;&#8230; 9/11/11: 1 Timothy 2:1-7

I thought it uncanny and eerie that I should read about this today&#8230;it being the [10th Anniversary of the World Trade Center tragedy][1]. Last week in my perpetual hunt for books I came across several accounts of 9/11, no doubt because the anniversary was coming up so they were encouraging people to read the first person accounts and stories surrounding that day. There are a lot of first person accounts from survivors and from those who watched and tried to help. BBC wrote an article asking the question [&#8220;Is there a novel that defines the 9/11 decade?&#8221;][2] and sums up the novels and stories that have come out of 9/11 since it happened. I ended up downloading to my Kindle &#8220;[102 Minutes][3]&#8221; by Jim Dwyer and Kevin Flynn which has had incredibly high ratings about the collapse of the world trade centers towers and &#8220;[Who they Were][4]&#8221; by Robert Schaler, which discusses those who &#8216;jumped&#8217; from the towers during that day and others which forensics teams struggled to identify; which has received mixed reviews. It is widely supported that [&#8220;Tower Stories: An Oral History of 9/11][5]&#8221; is also one of the best books about what happened as well.

Simple perusing of the news pops up hundreds of accounts from survivors all of whom struggle with memories from that day, people, friends, family they lost and why they survived&#8230;why them. Artie Van Why wasn&#8217;t actually in the towers but in a building next to them, him and co-worker ran out to see what happened then ran toward the towers to help amidst falling debris and people. He says that he always thought that when you fall from high enough you are dead before you hit the ground&#8230;but he realized that these people were very much alive holding their arms out as if to cushion the impact as they fell. When the towers collapsed they all ran&#8230;he made it, his co-worker did not. [his story [here][6]].

Survivors of any terrible experience grapple with survivors guilt&#8230;the perpetual question of why me? Why did I survive. I am sure soldiers go through this trauma as well after coming from a fight where they saw fellow soldiers fall and die. The documentary [Restrepo][7] deals with this in their portrayal of a group of Marines who were sent to the most dangerous part of Afghanistan, The Korengal Valley, for deployment&#8230;not all came back.

Survivors of the holocaust oftentimes will tackle the emotions and convictions that come with being the only survivor in their family and having witnessed such atrocities enacted on them and their friends. [I wrote about this in an earlier blog after having visited the Holocaust museum in D.C.][8]

Though we all might not ascribe to the same belief I am sure many of us wonder about our purpose for being here, why we survive things while others do not, how watching someone die makes you realize how infinitesimal your life can seem and how easily it can be snuffed out. There are those of us that ascribe survival and such as &#8220;God&#8217;s providence&#8221;&#8230;we have a purpose in life and we will not be taken, not die, til that purpose is completed. This is the general thinking. But as soon as that purpose has been accomplished&#8211;poof&#8230;time for snuffing and many accept that, although the prospect of death is still hard to grapple with. Not so much because it&#8217;s &#8216;death&#8217;&#8230;I think many people are more terrified of &#8216;how&#8217; they might die than actual death itself.

In the end, for those left behind or those that survived, the question remains&#8230;were you saved/spared for a reason? Do you have a purpose to accomplish greater than yourself though you may not know it?

<!--more-->Often times growing up I&#8217;d get so frustrated&#8230;so so frustrated. Our church believes in prophecy and once a year there would be a conference with speakers that were prophets that would pray over you and confirm things in your life or speak to you of things in your life&#8230;the purpose God has for you. My purpose? Always always always stay in school&#8230;keep going to school. Part of me was like&#8230;well gee, that&#8217;s dull and kinda sucks. In the meantime my friends next to me would be getting these &#8216;huge words of knowledge&#8217; about their lot in life, Gods purpose, who they were supposed to marry, why they matter. I felt like, WTF, what am I? Kimchi?&#8211;fermenting with no real purpose for an undetermined amount of time. I was jealous of my friends and others I saw accomplishing things in their lives and I felt stagnant&#8230;God doesn&#8217;t &#8216;speak&#8217; to me&#8211;heck he doesn&#8217;t even speak to prophets who are supposed to have a &#8216;direct&#8217; line about me&#8211;yeesh! My skepticism grew.

As I got older I decided I didn&#8217;t &#8216;have&#8217; to know why I was here&#8230;just that there was a reason I was here. I decided to think of my life as less like Kimchi and more like wine. The longer it ages the better (supposedly) it gets and when its opened or needed its usually a special scenario. Yes, I just brought the topic of wine into this devotional.

I don&#8217;t know &#8216;why&#8217; I am here. Occasionally I still fall into friend-envy seeing others lives, the impact they seem to be having and then musing about how ineffectual my life must be. The ensuing depression invariably leads to the opening of another bottle of wine&#8230;hey, I&#8217;m just being honest here. And then I think has God &#8216;saved&#8217; me from anything? I dunno&#8230;when I was really little I was in a huge car wreck that totaled the car and put my mom in the hospital. I was with my dad who was seat-belted in and he bear hugged around me and kept me safe. I was in another car wreck in college that totaled the car and left me bruised but no worse for wear. In 1st grade I fell head first on an iron spike and came out no worse for wear&#8230;but kids are made of rubber right? Now I have a vertical scar on my forehead that turns white when I am flushed&#8230;its my &#8216;racing-stripe&#8217; it makes me fast. On a flight into Seatac one time the landing gear wouldn&#8217;t go down and we circled three times to try and get it down. The first time only 1 gear came down, the second time they tried to lift that gear to try again and the gear wouldn&#8217;t come up&#8230;the final circle they got it up and were able to get both down. Another flight into San Fran during a heinous thunderstorm the pilot did a spiral descent which was quite unnerving and the plane shook and made noises as if it was ready to come apart steadily for what seemed ages&#8230;everyone clapped for him when we landed safely. I&#8217;ve had other horrifying experiences where perhaps my life wasn&#8217;t immediately in danger but could&#8217;ve easily turned that way in a few choice moments.

Yes I am here for a reason&#8230;though I don&#8217;t know what. Perhaps to interact with certain people during the course of my life. I don&#8217;t believe in accidents&#8230;I believe everyone you come into contact with is for a reason&#8211;even if its for a brief moment in time and you never see them again. My &#8216;morbid&#8217; fascination with survivor accounts such as the Holocaust, 9/11, war-time&#8230;has led me to read countless biographies about people who passed away, their stories told by others. The impact of their lives only being felt after they died.

I think if you live to see the impact your life has on this world and those in it, that is a blessing to be cherished&#8230;most people will never know their purpose and how it ultimately impacted others. [Mohammed Bouazizi&#8217;s death &#8216;sparked&#8217; a revolution][9], I am sure we can all think of people who&#8217;ve impacted our lives and are now gone forever.

Daryl, a high school friend&#8230;who passed away while on vacation with his parents in a car wreck. Pierre, brother of my neighborhood best friend in elementary school&#8230;passed away in a car wreck when I was in high school. I still have their school pictures. My grandmother, a future father-in-law, a friend and mentor Rich who turned me on to the tastiness that is [Hornsbys cider][10]&#8230;all of whom have passed. I remember a ski trip with some amazing friends&#8230;one of whom ([Collin Keck][11]) passed away when his bike was hit by a truck. I remember another friend [Colin Craig][12] who inspired me in cycling and competing who is now gone&#8230;I think about these lives and its easy to say some of them went too young. Its maddening sometimes to know that such things are out of your control. They may not have started a revolution&#8230;but they&#8217;ve inspired me and made my life and many other lives better by their existence in it and that matters just as much.

One of my favorite quotes&#8230;it sits on my homepage on my blog is by JRR Tolkein &#8220;Not all who wander are lost&#8221;. I may not know my purpose in life, I may never know my purpose in life&#8230;I wander through it just as anyone else&#8230;but I know I am here for a reason and while I may get sidetracked I will never be &#8216;lost.&#8217;

 [1]: http://www.bbc.co.uk/news/magazine-14790016
 [2]: http://www.bbc.co.uk/news/entertainment-arts-14682741
 [3]: http://amzn.com/B005569FBY
 [4]: http://amzn.com/B000FCKJ0Y
 [5]: http://amzn.com/B002PY72SW
 [6]: http://www.bbc.co.uk/news/world-us-canada-14439342
 [7]: http://restrepothemovie.com/
 [8]: http://www.melaniemelendrez.com/2011/08/18/there-is-nothing-perfect-theres-only-life-repost-from-jun-22-2006/ "There is nothing perfect, there’s only life: repost from Jun 22, 2006"
 [9]: http://www.bbc.co.uk/news/world-africa-12241082
 [10]: http://www.hornsbys.com/
 [11]: http://www.collinscoalition.org/index.php?option=com_content&view=article&id=98&Itemid=54
 [12]: http://colincraigmemorial.org/index.html
