---
title: 'Singing Softly: repost from Jul 12, 2007'
author: Mel
type: post
date: 2011-08-18T08:55:05+00:00
url: /2011/08/18/singing-softly-repost-from-jul-12-2007/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"7eddb45a9d";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";s:17:"wordbook_new_post";s:1:"1";}'
categories:
  - Poetry

---
<h4 align="center">
  <strong><em><span style="text-decoration: underline;">Singing Softly</span></em></strong>
</h4>

<h4 align="center">
  <strong><em></em></strong>
</h4>

<h4 align="center">
  <em>Singing</em> softly
</h4>

<h4 align="center">
  <em>Loving </em>loudly
</h4>

<h4 align="center">
  Life being
</h4>

<h4 align="center">
  Only what life is able
</h4>

<h4 align="center">
  To be
</h4>

<h4 align="center">
  <em>Caring</em> deeply
</h4>

<h4 align="center">
  <em>Abandoning</em> reproach
</h4>

<h4 align="center">
  <em>Making</em> wishes like a child
</h4>

<h4 align="center">
  <em>Knowing</em> the hurt
</h4>

<h4 align="center">
  Of reality
</h4>

<h4 align="center">
  <em>Giving</em> favor
</h4>

<h4 align="center">
  <em>Learning</em> grace
</h4>

<h4 align="center">
  <em>Touching</em> without fear
</h4>

<h4 align="center">
  <em>Trying</em> despite cost
</h4>

<h4 align="center">
  <em>Diminishing</em> life&#8217;s
</h4>

<h4 align="center">
  Trivialities
</h4>

<h4 align="center">
  <em>Still singing softly</em>
</h4>

<h4 align="center">
  <em>Still loving loudly</em>
</h4>

<h4 align="center">
  <em>Life still being</em>
</h4>

<h4 align="center">
  <em>What only life</em>
</h4>

#### _Can be._
