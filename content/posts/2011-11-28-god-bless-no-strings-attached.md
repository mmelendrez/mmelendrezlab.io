---
title: God bless…no strings attached
author: Mel
type: post
date: 2011-11-28T07:07:12+00:00
url: /2011/11/28/god-bless-no-strings-attached/
categories:
  - Devotion
tags:
  - God
  - homosexuality
  - judement
  - love
  - sin

---
Unofficial Devotional Blog: (not in book, but I&#8217;m gonna write it anyway)

Topic: &#8220;love, judgement, right and wrong&#8221; (verses&#8230;many, see below)

Since I started this devotional &#8216;section&#8217; to my blog I&#8217;ve talked about a lot of different topics introduced to me by this rather &#8216;fluffy&#8217; devotional book that I&#8217;ve been making my way through. And I actually was going to write another entry based in that book but as I opened the link to start a new blog&#8230;all this came flowing out instead. For an introduction to how this all got started in all the &#8216;devotion&#8217; stuff see the [first blog about my attempt at keeping regular devotions and analyzing my faith][1]. Topics ranged in this book from finding your &#8216;hidden sin ([blog post][2])&#8217;, leadership and mentorship ([blog 1][3], [blog 2][4]), family and finances ([blog][5]), wishing for a different life ([blog][6]), acceptance ([blog][7]), love and forgiveness ([blog][8]), relationships with non-believers ([blog][9]), trauma ([blog][10]), life purpose/being saved for something I wrote on the anniversary of 9/11 ([blog][11]) and many of the things I&#8217;ve said, done or written have [gotten me pegged throughout life as a &#8216;lukewarm Christian&#8217;][12].

I read a blog post entitled [&#8220;I&#8217;m Christian unless you are Gay&#8221;][13] written by a guy whose blog I follow because he has interesting things to say. Since it&#8217;s been written it&#8217;s gotten 74K plus facebook &#8216;likes&#8217; and has been shared I&#8217;m sure countless times to &#8216;mixed&#8217; reviews sometimes. I am one of those that shared this post on facebook and now I am sharing it here with my own take. I encourage you to read his post (linked above) in its entirety as well as some of the responses to the post both negative and positive. He&#8217;s caused quite the firestorm and some of the responses were very powerful.

After reading his post and all the responses&#8230;two quotes stuck with me.<!--more-->

  1. &#8220;Some of the most Christlike people are those with no religion at all&#8221; and
  2. [In a response a woman discussed how she&#8217;d ostracized her son for being gay stating she wouldn&#8217;t accept him til he got past this sin. After reading the blog post she broke down and immediately wanted to call and apologize to her son for not showing the unconditional love she should&#8217;ve despite disagreeing over his life choices. She ended with &#8216;God bless&#8230;no strings attached&#8217;.][14]

My response to the first, how sad. How sad that many of those that claim to bear the name Christian cannot be bothered to try and live that. I&#8217;ve mentioned before that many times I was treated better by my non-Christian friends than Christian friends. I&#8217;ve mentioned before that I&#8217;ve seen judgement from the Church &#8216;rain&#8217; down on those I am close with&#8230;more often then not causing people to walk away from God rather than toward him. How sad it is.

My response to the second, it stuck with me. How many of us &#8216;bless&#8217; others with &#8216;strings attached&#8217;? We are only worthy of acceptance and love if we follow &#8216;the rules&#8217;. [In one of the more negative responses a woman stated that we are supposed to &#8216;judge lest we be judged&#8217;&#8230;as it &#8216;apparently&#8217; states in the Bible][15] (she says it in the 6th paragraph down beginning with &#8216;God commanded&#8230;&#8217;) Ok, I&#8217;m no Bible scholar here&#8230;but I believe the Bible states: Judge NOT, lest ye be not judged&#8211;lets see if I&#8217;m right shall we? GOOGLE AND MY BIBLE TO THE RESCUE&#8230;so I googled it: Google says it talks about this in Matthew&#8230;ok Google&#8230;heading to Bible:

Oh look I&#8217;M RIGHT: Matthew 7:1-5 says judge NOT, that ye be NOT judged, lets go to the NIV (my Bible version):

> Do not judge, or you too will be judged. For in the same way you judged others, you will be judged, and with the measure you use, it will be measured to you. Why do you look at the speck of sawdust in your brother&#8217;s eye and pay not attention to the plank in your own eye? How can you say to your brother, &#8216;let me take the speck out of your eye, when all the time there is a plank in your own eye? You hypocrite, first take the plank out of your own eye, then you will see clearly to remove the speck from your brothers eye. (NIV, Matt 7: 1-5)

Later on in Matthew something more sobering is stated:

> The good man brings brings good things out of the good stored up in him and the evil man brings evil things out of the evil stored up in him. But I tell you that men will have to give account on the day of judgement for every careless word they have spoken. For by your words you will be acquitted, and by your words you will be condemned. (NIV, Matt 12: 35-37)

Yikes!

Now I&#8217;ve stated before and I&#8217;ll state again&#8230;I am no good in a Bible &#8216;firefight&#8217; simply because I don&#8217;t believe in going verse for verse toe to toe. The Bible has verses on love and judgement, forgiveness and retribution/revenge. In the end I try to live my life to the example Christ has set&#8230;that is what being a Christian is&#8230;isn&#8217;t it?

I will be the first to admit I am a Christian and I am an imperfect one. I love my God, I love my faith and I love my brothers and sisters in Christ and I will love those who do not believe in Christ.

I find it interesting and I am FULLY guilty of this&#8230;that many Christians I have come across simply avoid the volatile subjects like homosexuality when it comes to discussion. Because inevitably what they turn into is full on debates. They are seriously charged topics where life and emotion collide with spectacular chaos.

[When I talked about &#8216;flirting with sins&#8230;&#8217; I was bothered by the whole &#8216;guilt by association&#8217; mentality that the author in my devotional book seemed to be promoting and I&#8217;m going to apply it here since she&#8217;s applied it to &#8216;me&#8217; in essence][2]. Hateful words or actions are  just as effective weapons as a gun or knife. It pushes children off buildings, into hangman&#8217;s nooses, drug overdoses and drives homicidal gunning down of kids in schools. Guilt by association&#8230;your words have in effect murdered someone. Do you think I am &#8216;stretching&#8217; it too much? I don&#8217;t.

When I was growing up I remember being horrified at the lack of distinction in sin that was told to me at church. For instance, a lie is seen the same to God as murdering someone. To God, sin is sin is sin&#8230;it all sucks and needs to be cleaned away no matter what. This is why God looks at the heart. Do I believe a mass murderer can get saved? Yes, absolutely. Does that pardon him from his crimes, no&#8211;he still ended up paying for those crimes via life in prison or other&#8211;but God can wash him of that sin and make him a new person. Do I equate shoplifting with murder&#8230;no&#8211;how can I? Its beyond my human reasoning that shoplifting is on par with taking someone&#8217;s life. Do both suck? Sure, yes, absolutely. But its always been beyond me how God considers them equal, so I end up just accepting that ALL sin no matter how small sucks and needs to be cleaned away by his grace and mercy. In some countries you can receive the death sentence for smuggling drugs or life imprisonment for saying things out of turn. Sin is sin is sin&#8230;

You don&#8217;t agree or support homosexuality as a life choice or you think it is a sin?&#8230;Ok. You need to get it together then and find a way to ALSO LIVE God&#8217;s second greatest commandment: love one another (Matthew 22: 36-40; 1 Cor 13:13; Mark 12:28-31; Galatians 5:14; Colossians 3:12-14). If sin is sin is sin and you believe homosexuality is a sin and you believe that would put it on par with lying, stealing, killing whatnot&#8211;and you are capable of &#8216;loving&#8217; thieves and killers&#8230;why not those who are gay? One of the churches I was in had an avid thriving prison ministry ministering to the prisoners, helping them, praying with them, teaching them&#8230;this same church treated gays as untouchables that couldn&#8217;t be &#8216;reached&#8217;. Why? I didn&#8217;t understand, I couldn&#8217;t fathom their treatment of these people, such disdain, such disgust. They&#8217;ll show love and concern for the murderer or thief but nothing for a human being whose done nothing to merit their disdain aside from breathing and living a life they simply &#8216;don&#8217;t agree with&#8217;.

My views on homosexuality, marriage, kids, marijuana (medicinal or otherwise), evolution, different cultures, different faiths, what I had for breakfast&#8230;are just that, my views, my opinions. Yes, they are shaped by my life, my experiences, my faith, what I&#8217;ve been brought up with&#8230;but they do not give me permission to judge others and &#8216;choose&#8217; who is &#8216;worthy&#8217; of my concern and love. For those of you Christians, does this sound familiar?

> Hate the sin, love the sinner.

I am grateful my parents raised me with that&#8230;I am a sinner, we all are. My parents raised me with so much love and acceptance no matter what I did. They offered me guidance when I was &#8216;sinning&#8217; without condemnation, everything they did was in love. Now I am not gay, but I am 100% certain that if I&#8217;d &#8216;come out&#8217; as gay to them I am sure that part of them would&#8217;ve been like &#8220;well shit&#8230;&#8221; (except they don&#8217;t swear&#8230;so it&#8217;d be some other exclamation). But then they&#8217;d love on me, talk to me, accept me, guide me&#8211;do all the things parents do for their children while they grow up and find their own path, their own way&#8230;and they would pray for me. They cultivated a child who emphatically believes in the power of prayer, peace, guidance and love in Christ&#8217;s message rather than hate, condemnation and intolerance.

I may not agree with peoples life choices and by all means if they request help I will do anything in my power to help them or show them to someone who can help them. It is possible to have strong views on these controversial issues and discuss them without hate. It is possible to love someone who you don&#8217;t agree with&#8230;we all do it everyday with our families don&#8217;t we? I can&#8217;t say I agree with every word my father and mother have said to me, but I honor them, I love them, I would do anything for them. I can&#8217;t say I agree with everything my best friend (who is atheist) has to say, nor does she agree with everything I believe or have to say, but I&#8217;d take a bullet for her in a second and I will love her like a sister til the day I die.

It is not my place to damn people to hell, [only God knows the heart][12]. And I am so thankful that God can read the heart. My job on this earth is to show Christ&#8217;s love, tell people about him should they wish to know and continually strive to live by the commands and guidance God gives in the Bible.

I want to be able to bless anyone that comes into my path&#8230;no strings attached. If you are incapable of accepting those different from yourself, that differ from what you believe Christ has laid out in the Bible&#8230;then pray. Pray for them, do you believe God needs to &#8216;open their eyes and heart?&#8217; Then pray for that. Then turn inwards on yourself, look at the sawdust in your life&#8230;the planks of wood even.

Yes, pray for them&#8211;but also pray for yourself, pray for God to open your eyes and heart too.

 [1]: http://www.melaniemelendrez.com/2011/09/09/devotion-as-devotion-does/ "Devotion as devotion does…"
 [2]: http://www.melaniemelendrez.com/2011/11/21/flirting-with/ "flirting with…"
 [3]: http://www.melaniemelendrez.com/2011/11/18/minions-and-mentorship/ "Minions and Mentorship…"
 [4]: http://www.melaniemelendrez.com/2011/09/14/rights-and-resonsibility/ "Rights and responsibility"
 [5]: http://www.melaniemelendrez.com/2011/11/09/having-an-out-of-money-experience/ "Having an out of money experience"
 [6]: http://www.melaniemelendrez.com/2011/11/04/greener-grass/ "greener grass?"
 [7]: http://www.melaniemelendrez.com/2011/10/15/suck-less-suck-less/ "suck less, suck less…"
 [8]: http://www.melaniemelendrez.com/2011/10/14/love-covers-a-multitude-of-sins/ "Love covers a multitude of sins"
 [9]: http://www.melaniemelendrez.com/2011/09/22/cascades-of-mistakes/ "Cascades of mistakes?"
 [10]: http://www.melaniemelendrez.com/2011/09/15/trauma/ "Trauma"
 [11]: http://www.melaniemelendrez.com/2011/09/11/saved-for-something/ "saved for something…"
 [12]: http://www.melaniemelendrez.com/2011/11/01/the-good-the-bad-the-beach/ "the good, the bad…the beach"
 [13]: http://www.danoah.com/2011/11/im-christian-unless-youre-gay.html
 [14]: http://www.danoah.com/2011/11/few-more-powerful-responses-to-im-christian-unless-youre-gay.html/5/
 [15]: http://www.danoah.com/2011/11/powerful-responses-to-im-christian-unless-youre-gay-blog.html/3/
