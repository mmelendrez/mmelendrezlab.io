---
title: Gur e m’ anam is m’ eudail
author: Mel
type: post
date: 2012-05-29T02:48:42+00:00
url: /2012/05/29/gur-e-m-anam-is-m-eudail/
al2fb_facebook_link_id:
  - 43807988_797915319579
al2fb_facebook_link_time:
  - 2012-05-29T02:48:56+00:00
al2fb_facebook_link_picture:
  - post=http://www.melaniemelendrez.com/?al2fb_image=1
categories:
  - Life or something like it
  - Poetry
tags:
  - Gaelic
  - love

---
## It was my love and my treasure

Short post&#8230;sentimental post. Over the past couple years I&#8217;ve neglected my love of Gaelic music. I got this love from my Mom who dabbled in learning Gaelic a bit. I remember singing along with some Gaelic songs trying to memorize what the lyrics mean while in high school, college and grad school&#8211;out of view of most people haha! My vocabulary is limited to what I learned through the music and I&#8217;m sure my pronunciation is pretty poor, but I&#8217;m slowly finding my passion for this music again.

Digging around Youtube I came across Julie Fowlis and posted one of her songs that I absolutely love. It&#8217;s a love song. A friend asked about the lyrics&#8230;haha, I had to use google translate for most of it to concoct the rough translation, then dug around Julie&#8217;s site and found the actual lyrics, they are posted below in Gaelic then English.

<p style="text-align: center;">
  <em><strong>I love re-discovering parts of me that were lost in the fray of life.</strong></em>
</p>

<p style="text-align: center;">
  <strong><em>I love re-discovering love.</em></strong>
</p>

My source for this is: www.juliefowlis.com/songs

The youtube video where you can actually hear the song if you aren&#8217;t one of my &#8216;facebook friends&#8217; is: http://youtu.be/ez1O5swf1IM

## Bothan Àirigh am Bràigh Raithneach (A sheiling on the Braes of Rannoch)

* * *

Gur e m&#8217; anam is m&#8217; eudail
  
chaidh an-dè do Ghleann Garadh:
  
fear na gruaig&#8217; mar an t-òr
  
is na pòig air bhlas meala.

O hi ò o hu ò, o hi ò o hu ò,
  
Hi rì ri ò hu eile
  
O hì ri ri ri ò gheallaibh ò

Is tu as fheàrr don tig deise
  
de na sheasadh air thalamh;
  
is tu as fheàrr don tig culaidh
  
de na chunna mi dh&#8217; fhearaibh.
  
Is tu as fheàrr don tig osan
  
is bròg shocrach nam barrall:
  
còta Lunnainneach dubh-ghorm,
  
is bidh na crùintean ga cheannach.

An uair a ruigeadh tu &#8216;n fhèill
  
is e mo ghèar-sa a thig dhachaigh;
  
mo chriosan is mo chìre
  
is mo stìomag chaol cheangail.

Thig mo chrios à Dùn Eideann
  
is mo bhrèid à Dùn Chailleann,
  
gheibh sinn crodh as a&#8217; Mhaorainn
  
agus caoraich à Gallaibh.

Is ann a bhios sinn &#8216;gan àrach
  
air àirigh am Bràigh Raithneach.
  
ann am bòthan an t-sùgraidh
  
is gur e bu dùnadh dha barrach.

Bhiodh a&#8217; chuthag &#8216;s an smùdan
  
a&#8217; gabhail ciùil duinn air chrannaibh;
  
bhiodh an damh donn &#8216;s a bhùireadh
  
gar dùsgadh sa mhadainn.

It was my love and my treasure
  
who went yesterday to Glengarry,
  
the man with hair like gold
  
and kisses that taste of honey.

You suit your clothes
  
better than any man on earth;
  
you look better in your garments
  
than any man I&#8217;ve ever seen.

You look better in stockings
  
and comfortable laced shoes,
  
a dark blue London coat
  
that cost many crowns to buy.

When you arrive at the fair,
  
you&#8217;ll bring my gear,
  
my small belt and my comb
  
and my little narrow fastening
  
head-band.

My belt will come from Edinburgh
  
and my marriage head-dress from
  
Dunkeld,
  
we&#8217;ll get cattle from the Mearns
  
and sheep from Caithness.

And we&#8217;ll rear them in a sheiling
  
in Bràigh Raithneach,
  
in the brush-wood enclosed hut of
  
dalliance.

The cuckoo will sing
  
its song to us from the trees,
  
the brown stag and its roaring
  
will wake us in the morning.
