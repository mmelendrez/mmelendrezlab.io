---
title: Digesting fictional fluff…
author: Mel
type: post
date: 2011-09-15T03:45:19+00:00
url: /2011/09/15/digesting-fictional-fluff/
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"0e271d3e5a";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";}'
wordbooker_extract:
  - |
    The Help by Kathryn Stockett
    
    So if you read any number of reviews on this book, say in Goodreads for Amazon it seems readers have a 'love/hate' relationship with this book. The vast majority--of those at least writing reviews loved it. I myself grew ...
categories:
  - 'Summer Reading List: Amature Thoughts from an Avid Reader'
tags:
  - Asia
  - Bangkok
  - "culture'"
  - help
  - history
  - maids

---
[The Help][1] by Kathryn Stockett

So if you read any number of reviews on this book, say in [Goodreads][2] for [Amazon][3] it seems readers have a &#8216;love/hate&#8217; relationship with this book. The vast majority&#8211;of those at least writing reviews loved it. I myself grew hot and cold during the course of reading it. It deals with such an interesting subject and important awful time in history and it felt to me like it was written to go straight to movie&#8211;tragic and &#8216;feel good&#8217; all at the same time, great movie fodder. But does that necessarily make good fiction? And low and behold where is this book now? In movies! I&#8217;ll be interested to see how they interpret the book in the movie. I have a rule of reading books before I see them in the movies as much as I can&#8230;and I&#8217;m not one of those people where the movie has to have every last exhaustive detail from the book for it to be &#8216;good&#8217;. I&#8217;m always interested in adaptations. And I think Kathryn Stockett makes a good point in her quote:

> “Everyone knows how we white people feel, the glorified Mammy figure who dedicates her whole life to a white family. Margaret Mitchell covered that. But no one ever asked Mammy how she felt about it.”

To say the least, its a little discussed area of history&#8230;how &#8216;the maid&#8217; feels. I found myself more excited about her blurb at the end about her physical experiences growing up in the 60&#8217;s in Mississippi and I found myself more compelled and wishing she&#8217;d written about that rather than this book. But to be fair I have always been more partial to non-fiction unless it&#8217;s a literary &#8216;classic&#8217; ala Wuthering Heights or the Secret Garden. Is this book a &#8216;classic&#8217;? Um&#8230;no. It&#8217;s not bad&#8230;but it&#8217;s kind of a let down. I wish she&#8217;d developed some characters more and played down others. But I recognize the difficulty she must&#8217;ve faced writing characters she could not relate too.

I loved the relationship the author built between Aibileen and the little girl Mae Mobeley, my favorite part of the book and an important one as no child is ever born racist, it&#8217;s taught&#8211;many times harshly. And my favorite parts of the book had Mae in them. When she starts school her teacher Miss. Taylor shames her to no end because she drew a black child as something that makes her happy. Aibileen had been teaching her that there is &#8216;no color&#8217;, we are all the same and can love each other as such. While Mae is playing with her little brother she makes her little brother be the &#8216;black child&#8217; and tells him no matter what she does he has to sit there and take it or he&#8217;ll go to &#8216;jail&#8217; and then she proceeds to throw dolls at him, pour crayons on him then tells him lets play back of the bus like Rosa Parks etc&#8230;Mae&#8217;s father watches this and asks her who taught her this and she lies and says it was her teacher, when in fact it was Aibileen that&#8217;d been telling her stories&#8230;&#8217;secret&#8217; stories.

Surprisingly the ending was not what I was expecting which is good, but I&#8217;m not sure I liked it either&#8230;I dunno, it was both sad and hopeful I suppose.

<!--more-->One final comment, what the hell was up with the random naked pervert in that popped in for a portion of one chapter and was never heard from again. That was really &#8216;odd&#8217;. I suppose Kathryn Stockett wanted to find a way to add a different dimension to Celia&#8217;s character? But she never develops Celia in the novel anyway&#8230;why start now? Celia ends up being a caricature of a woman who can apparently kick a random perverts ass but can&#8217;t boil water&#8230;\*sigh\*

In the end, the book came off as more like fluff than a serious look inside the lives of African American domestic help in the 60&#8217;s and I still wish Kathryn Stockett had written her own story with her own voice then manifest a concoction of fiction pieced together from childhood memories using a voice she wasn&#8217;t admittedly comfortable with and with very little research into other memoirs of African American women during that time. She even states in her epilogue she never even asked her own maid growing up what it felt like to serve a white family.

The book did make me think of my own situation in Thailand. When Tyghe and I moved into our apartment the owner said that there was a maid that would come 3 times a week for about $60 USD and clean, do laundry, dishes and whatnot. In fact many families in Bangkok have full-time or part-time &#8216;help&#8217; in apartment upkeep or caring for their children. Having a maid was an odd feeling for us, downright weird in fact. I&#8217;ve never in my life had a maid. Part of me wants to learn enough Thai to ask her about her experiences being a maid. And I know that some families treat their maids/nannies quite strictly or harshly here. A friend told me that Thai maids like working for white foreigners because it&#8217;s a lot &#8216;easier&#8217; than working for a Thai or Asian family. In effect he was telling me that we are &#8216;taken advantage&#8217; of by our help. While I can get on board with the whole &#8216;nanny&#8217; possibility when you have kids and work and need the help; I don&#8217;t know that I&#8217;ll ever get used to the whole&#8211;having your own personal maid. I&#8217;ve always done things for myself and its weird when I see her doing our laundry or washing dishes. Interestingly enough, when its in a hotel or restaurant obviously there are maids to clean and people to cook your food and wash dishes&#8211;which if fine, but when its my &#8216;own personal maid&#8217; coming into my house&#8230;I dunno its just &#8216;interesting&#8217;, definitely not something I&#8217;m used to.

Do I recommend this book? Meh.

What do I suggest instead? [Coming of age in Mississippi by Anne Moody][4] which I&#8217;ve read rave reviews for and have downloaded, granted its an autobiography and I&#8217;m partial to those. I found it in the &#8216;suggested books&#8217; when looking at The Help. Read The Help for fictional fluff&#8230;Read Coming of age in Mississippi for the real deal.

 [1]: http://amzn.com/0399155341
 [2]: http://www.goodreads.com/book/show/4667024-the-help
 [3]: http://www.amazon.com/Help-Kathryn-Stockett/product-reviews/0399155341/ref=dp_top_cm_cr_acr_txt?ie=UTF8&showViewpoints=1
 [4]: http://amzn.com/0440314887
