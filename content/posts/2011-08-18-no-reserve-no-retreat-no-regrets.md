---
title: No reserve, no retreat…no regrets
author: Mel
type: post
date: 2011-08-18T09:21:43+00:00
url: /2011/08/18/no-reserve-no-retreat-no-regrets/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Asia
  - Life or something like it
  - Poetry
tags:
  - faith
  - quotes

---
#### William Borden was born into great wealth and attended Yale in 1905, but if you do a search on William Borden on the internet you won&#8217;t find great business dealings or a discovery of a new drug…he died at 25.  And to many his life was a great waste.

<!--more-->William Borden wrote the quote: &#8220;No reserves, no retreat, no regrets…&#8221; To some he had so much potential in his life which they felt was wasted, since he died so young, to others he was an inspiration. He&#8217;s an inspiration to me. His parents sent him on a trip around the world following his graduation from Yale. While abroad he fell in love with Asia and decided that he wanted to spend his life helping the Muslim Kunasa people in China. Many back saw that as such a waste of education, of affluence, of his upbringing…but he was devoted to his cause. He used his money to set himself up in Egypt to learn the language and study prior to going to China (1910)…during this time he wrote in his Bible &#8220;No reserves.&#8221; While in Egypt he contracted spinal meningitis, he wrote in his Bible &#8220;No retreat.&#8221; Right before he died, the last entry was, &#8220;No regrets.&#8221; He died at 25.

#### The trajectory of my life has taken me far away from my family in Hawaii and friends I&#8217;ve come to know and love, but it means I am pursuing the life I was put here for and I am excited for that.
