---
title: 'Living vs. Existing: repost from Jan 31, 2007'
author: Mel
type: post
date: 2011-08-18T09:07:14+00:00
url: /2011/08/18/living-vs-existing-repost-from-jan-31-2007/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Life or something like it
tags:
  - existing
  - life
  - living
  - purpose

---
So while I was during Christmas I encountered many friends I have had since moving to the islands when I was 11 (10? 11?&#8211;ah doesn&#8217;t matter)&#8230;and I saw almost all of them between Oahu and Maui and talked to the rest via phone.  And between the people I know from home, all the friends I met and have through school and family&#8211;I have realized there are really two types of people. Those who live and those who exist. Not that one is particularly better than the other, just depends on the person.<article>

<!--more-->Some have families or are starting families (we are all in the 25-35 range now), some are working at their dream job, some are traveling the globe, while others like myself are still in school. For the most part everyone seems happy, healthy and more or less self-collected but for some; they insist on just existing rather than living and that bothers me, how can one be happy just existing.</p> 

Point in case: I have a friend, she&#8217;s an amazing, intelligent, sensitive, wonderful person. She&#8217;s in a job she hates but it pays well and she gets along with her coworkers. She went to school to be a nurse, but due to issues with family and finances and medical things she went back home and just found work which she is now more or less settled into. It&#8217;s not what she wants to do and yet she continues to do it rather than pull out and actually start searching for the job she wants that she knows will make her so much happier. And yet she&#8217;ll talk about her aspirations and what she would love to do and yet will do nothing to make that happen for herself and I don&#8217;t understand why? She&#8217;s more or less financially stable enough to give it a go and still she does not. She wakes up same time every morning, she showers, goes to work, half-heartedly does her job bantering with her coworkers throughout the day, takes her lunch break, comes back, finishes the day, drives home&#8211;maybe she rents a movie, maybe she goes out to dinner with friends, she seems happy and yet when I talk to her you know somethings missing, you know she&#8217;d rather be somewhere else living her life, rather than existing in it.

I&#8217;m guilty of it too, I did the same thing after college, I went home for similar reasons and found a job. Started as a secretary (yeah, how close to biology or spanish is that???), ended up a patient care coordinator of a cosmetic (he didn&#8217;t do reconstructive) plastic surgery clinic (ok, so I&#8217;m getting closer to the bio side). I was paid well, I made commission, I was almost completely out of debt from undergrad, I got up at the same god awful early hour each morning, got coffee/tea, drove to work, opened the clinic, bantered with the nurses, the doctor, the patients, the techs, made phone calls, typed on my computer, took my 45 min lunch break, finished my day and went home. Maybe I went out with friends, rented a movie, went to church&#8230;but there was something missing. I should&#8217;ve been happy-hell I had a good job in Hawaii, albeit I didn&#8217;t like the job very much. My coworkers and I got a long, I had a good relationship with my parents and siblings, I had friends. But I got tired and restless knowing I should be doing something else rather than preying on the vanity of insecure people. So I left&#8211;and ended up in Bozeman, much to the surprise of anyone who finds out I&#8217;m from Hawaii!

If &#8216;exsting&#8217; makes you happy, great&#8211;but everyone I&#8217;ve ever met who seems to me to be &#8216;existing&#8217; isn&#8217;t necessarily happy persay. What&#8217;s the difference between living and existing? Passion. Existing is passionless&#8211;your neither here nor there, you don&#8217;t really have an opinion, you are satisfied but you are not, you are kind of a walking contradiction to your own life. Not that my life is non-stop passion&#8211;that&#8217;s not how I meant it. But I can feel, my emotions are there, I am not a daily automon, I know I am doing something I want to do and will be exactly where I am supposed to be and that gives me contentment and purpose, a life&#8211;not just existence.

That&#8217;s why when any of my friends or family express a passion about ANYTHING, travelling, a future job aspiration, dancing, school, having their own family&#8230;I would hope I am the kind of person who is like &#8216;GO!&#8217; have fun&#8211;if that&#8217;s what makes you feel alive then you have to do it. This does not mean I will be following you: ie. base jumping, sky diving, bungee jumping&#8230; but I&#8217;ll cheer from the side and pray your parachute opens quick enough&#8230;metaphorically and literally!

Existence isn&#8217;t a bad thing, but one thing sticks out about my conversation with my friend. She kind of shrugged her shoulders and said: &#8216;perhaps you are right, perhaps I am not happy persay&#8230;but that&#8217;s not the same as being unhappy.&#8217;

It makes me abit sad, I see so much inside of her, so much she could do and it&#8217;s hard not seeing her bother to take a step in a direction that she knows will make her happy or happier, will make her alive&#8211;because she&#8217;s scared or indifferent of leaving the status quo.

I guess I&#8217;ve gotten to a point where I need to feel alive rather than exist&#8211;and I hope she eventually reaches that point too.</article>
