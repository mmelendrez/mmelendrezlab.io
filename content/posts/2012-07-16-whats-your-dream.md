---
title: what’s your dream?
author: Mel
type: post
date: 2012-07-16T14:48:30+00:00
url: /2012/07/16/whats-your-dream/
al2fb_facebook_link_id:
  - 43807988_815056613289
al2fb_facebook_link_time:
  - 2012-07-16T14:48:36+00:00
al2fb_facebook_link_picture:
  - post=http://www.melaniemelendrez.com/?al2fb_image=1
categories:
  - Devotion
tags:
  - confidence
  - dream
  - life
  - love
  - purpose
  - work

---
Devotional Blog:

&#8220;Regroup&#8221;, 7/15/2012, Proverbs 9:9

So last weekend I was talking to my mom and last night I was talking to my sister and with both conversations I found myself pondering my choice of &#8216;life path&#8217;. If you&#8217;ve read previous entries in this section of my blog you will know that I&#8217;ve said that I&#8217;ve always just walked through the paths I feel like God has opened to me assuming that&#8217;s direction he wants me to go. It is after all the only path that&#8217;s opened up, so I just walk through it. Did I think &#8216;this&#8217; is where my path was leading? Actually no.
  
<!--more-->

So what was my original plan. Well, and I didn&#8217;t figure this out until I was half way through my Ph.D., turns out I probably wanted to go more into public health/epidemiology. The things epidemiologists were doing&#8230;that&#8217;s what I wanted to be doing. Don&#8217;t need a Ph.D. for that, a Master&#8217;s is more than adequate. So HOW did I end up here?

In addition it seems that Biology is moving toward a &#8216;harder&#8217; science with the advent of bioinformatics&#8230;.in the sense of a lot of math, modeling, statistics. My mind is not naturally adept at &#8216;thinking&#8217; this way, never has been. I think I&#8217;ve mentioned this in previous blogs as well. I am much much better at things like literature or languages and YET here I am, Chief of Bioinformatics at a research institute in Maryland. And yes, I have been trained for this, I can do it&#8230;and it can be rewarding, but it&#8217;s tiring, it&#8217;s challenging and I am plagued by constant &#8216;ghosts&#8217; of inadequacy left over from my Ph.D. process.

Now to be fair I am getting better in the sense of self confidence in my field than I used to be. Coming out of my Ph.D. I felt like I knew nothing, which was pretty scary walking into my Post Doc. Now I knew that I &#8216;knew&#8217; stuff or I wouldn&#8217;t have finished and graduated with my Ph.D. but my scientific self confidence took a beating for a good 6 years, difficult to undo that right away.

And perhaps that&#8217;s my problem, the compilation of coming into a job where I was immediately overwhelmed with the &#8216;needs&#8217; of the institution that had to be dealt with right away. Being thrust into a career field that moves so quickly and I have trouble already digesting everything associated with it. I am also jumping fields from the &#8216;environmental&#8217; to the &#8216;medical&#8217;. Jumping from microbial/viral ecology and evolution to infectious disease so I voluntarily walked into a career where I was already starting &#8216;behind&#8217;. Well, not surprising as I&#8217;ve never been one to take an &#8216;easy&#8217; path.

In a previous blog I mentioned I battled with envy of others paths. Friends of mine that seemed to be doing amazing things and yes, I was jealous. I wished my life was so fulfilling as theirs. It was the classic &#8216;grass is greener&#8217; mentality and I snapped myself out of it pretty quickly. No I am not on the path I had originally thought I&#8217;d be on and yes, I won&#8217;t lie, that is frustrating.

Am I on a better path? I know my answer should be yes after all its the path I feel God put me on. But my answer is neither yes nor no. Why am I not &#8216;netting&#8217; happiness out of the path God has put me on? If it&#8217;s the right path shouldn&#8217;t I be overall pleased and happy with it? Perhaps not? So there are several very reasonable reasons why I am in my current state of re-evaluation:

  1. Hormones&#8211;ya I&#8217;m a woman, I admit it, it&#8217;s possible I am over-reacting.
  2. Lack of self-confidence (thank&#8217;s Dave)&#8230;although honestly I can only blame him for so long, eventually I have to extract my own head from my own @$$ (lovely image I know) and take responsibility for my own growth and abilities.
  3. I really am not adept at this field, perhaps I should change to something that&#8217;s not science?
  4. I am burnt out. I&#8217;ve been constantly going, constantly learning, constantly working for about 9 years now with no break&#8211;ya literally&#8230;no break. Perhaps I need a &#8216;sabbatical&#8217; in a sense where I don&#8217;t have to worry about work for a determined amount of time. Perhaps that&#8217;ll help me rediscover my &#8216;passion&#8217; again for what I worked so hard for in the first place. Fabulous, so now I just need to magically make that time appear.
  5. I&#8217;m imbalanced. NO, not mentally (ok maybe a little mentally) but mostly it&#8217;s literal. In order to for me to function I have to have &#8216;ying and yang&#8217; in my life. My friends say I am quintessentially Libra (I need weights on either side of me balanced or I tip, mentally and physically). Right now, work is all I have and stuff associated with the wedding, but mostly it&#8217;s just work work work. I need work and &#8216;the creative&#8217;, something I haven&#8217;t had since moving to MD. I literally eat, sleep, and work. To do things outside of that costs money and time we don&#8217;t currently have. And we keep telling ourselves that after the wedding it&#8217;ll be better, more balanced&#8230;

I fantasize about not having to work, not having to learn, picking up my viola again&#8230;I fantasize about sitting at and cross-stitching&#8211;dammit apparently I&#8217;m fantasizing about being an 80 year old woman&#8211;DOH!

I think it&#8217;s a combination of factors because of course it&#8217;s never really one thing that triggers stuff like this. But I think mostly it&#8217;s just that I&#8217;m burning out. When you burn the match at both ends for too long, you run out of matchstick; then you are trying to keep the flame alive with no fuel&#8211;which is impressive but not sustainable. I suppose I am coming to grips with my own limitations.

> Teach the wise and they will be wiser. Teach the righteous and they will learn more. ~Proverbs 9:9

So what am I doing&#8230;re-evaluating my dream? I am obviously on a different path than I&#8217;d originally thought I&#8217;d be on when I was back in high school. I&#8217;ve invested so much time and energy in this dream, this path, and quite frankly I wouldn&#8217;t change my path no matter how frustrating it got. The &#8216;life&#8217; experience has been invaluable. That doesn&#8217;t mean that its &#8216;netted&#8217; me happiness necessarily then. Stupid huh? I wouldn&#8217;t change my past path, but I wouldn&#8217;t say it&#8217;s necessarily netted me happiness either&#8211;haha. This is the part where all the guys roll their eyes, throw their hands up and say &#8220;There&#8217;s just no way to &#8216;win&#8217; with you!&#8221; and all the ladies shake their heads knowing exactly what I mean :).

My dreams? To work with an NGO someday, to teach, to be useful, to satisfy whatever purpose I am hear to satisfy, to inspire, to love like tomorrow isn&#8217;t promised&#8230;

There&#8217;s a quote in the devotional book that I actually liked:

> &#8220;&#8230;going for the dream by a different path is better than no dream at all.&#8221;

Ya I have dreams&#8230;ya I&#8217;m on a different path than I originally thought I would be&#8230;and sometimes it&#8217;s good to remember and reflect on the path that&#8217;s taking you toward those dreams.
