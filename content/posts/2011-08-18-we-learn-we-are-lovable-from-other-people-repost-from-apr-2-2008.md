---
title: 'We learn we are lovable from other people: repost from Apr 2, 2008'
author: Mel
type: post
date: 2011-08-18T08:27:37+00:00
url: /2011/08/18/we-learn-we-are-lovable-from-other-people-repost-from-apr-2-2008/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - 'Summer Reading List: Amature Thoughts from an Avid Reader'
tags:
  - faith
  - love
  - marriage

---
## We learn we are lovable or unlovable from other people&#8230;

Book: [Blue Like Jazz by Donald Miller][1]

&#8220;My friend Kurt used to say finding a wife is a percentage game. He said you have to have two or three relationships going at once, never letting the one girl know about the others…Kurt believed you had to date about twenty girls before you found the one your were going to marry. He just believed it was easier to date them all at once. Kurt ended up marrying a girl from Dallas, everybody says he married her for her money. He is very happy…&#8221;

<!--more-->&#8220;My friend Mike Tucker reads books about dating and knows a lot on the subject. He says thinks like &#8220;you know, Don, relationships are like rubber bands…when one person pulls away well that just draws the other one even closer.&#8221; That sort of thing is interesting to a guy like me because I know nothing about dating. What little I know about dating is ridiculous and wouldn’t help rabbits reproduce.

Here’s a tip I never used: I understand you can learn a great deal about girldom by reading Pride and Prejudice, and I own a copy, but I have never read it. I tried. It was given to me by a girl with a little note inside that read: What is in this book is the heart of a woman. I am sure the heart of a women is pure and lovely, but the first chapter of said heart if hopelessly boring!

&#8220;I asked my friend Paul, ’how is married life?’, ’It’s good. It’s tough, but it is good…it isn’t what you think it is.’ ’Marriage?’ ’It isn’t fulfilling in the way you think it is.’ I asked, ’Are you happy?’ ’Define happy.’ ’Are you happy you married Danielle?’ Paul put the stem of his pipe back in his mouth. ’I am very happy.’ Paul continued…’The scary thing about relationships, intimate relationships, is that if somebody gets to know us, the us that we usually hide, they might not love us; they might reject us…I’m saying there is stuff I can’t tell Danielle, not because I don’t want to, but because there aren’t words. Marriage is the closest two people can get and it’s amazing, but they can’t get all the way to that place of absolute knowing. Marriage is the most beautiful thing I have ever dreams of, Don, but it isn’t everything. It isn’t Mecca. Danielle loves everything about me; she accepts me and tolerates me and encourages me. She knows me better than anybody else in the world, but she doesn’t know all of me, and I don’t know all of her. There are some places in our lives that only God can go.

One of the ways God shows me he loves me is through Danielle and because she loves me, she teaches me I am lovable. We learn that we are lovable or unlovable from other people. That is why God tells us so many times to love each other.

~This book has a lot of interesting insight especially about christianity and ’religiousity’&#8211;there are some chapters about that too. He calls it nonreligious thoughts on Christian spirituality.

 [1]: http://www.amazon.com/Blue-Like-Jazz-Nonreligious-Spirituality/dp/0785263705
