---
title: Where is home?
author: Mel
type: post
date: 2011-12-27T06:54:46+00:00
url: /2011/12/27/where-is-home/
categories:
  - Devotion
  - Life or something like it
tags:
  - 
  - family

---
Devotional Blog:

Topic: &#8220;Home Base&#8221;, 12/12/2011, 1 Chronicles 16:43 NIV

So while the devotional entry gave me the idea of this blog&#8230;it&#8217;s actually got nothing to do with what she wrote in the book and I am citing a different verse. But I have taken her topic title of Home Base because it inspired what I will write about.

> &#8220;Then all the people left, each for their own , and David returned home to bless his family.&#8221;  ~1 Chronicles 16:43 NIV

Since leaving Hawaii for college I always get the question of &#8220;so when are you going home next?&#8221; Where are you from? Where is home for you? Everytime I get asked this question it prompts me to ponder about &#8216;what IS home&#8217; exactly. For me, home has always been where my folks are which over the years has changed locations many times. When we are in school we never think to call our college towns &#8216;home&#8217;&#8211;I certainly wouldn&#8217;t call Parkland, WA (near Tacoma) home&#8211;blech! When I moved to Bozeman, MT at first home was Hawaii&#8230;&#8221;Are you going home for the holidays?&#8221;&#8211;&#8220;Yes, I am going home.&#8221; That was years 1, 2 and 3. Around year 4 and in later years in MT I started noticing a change&#8230;&#8221;Are you going back to Hawaii for Christmas?&#8221;&#8230;&#8221;Yes, I am going back to spend Christmas with my folks.&#8221; Had I decided that Hawaii was no longer my home?<!--more-->

Truth be told both MT and HI both became home&#8211;claiming two parts of my heart. Hawaii because it was where my parents lived. Montana because it&#8217;s where I feel my life &#8216;started&#8217; persay.

Some people think that where ever you lived a long time&#8230;that&#8217;s home. Well that screws me too&#8230;6 years in Cali, 6 years in Federal Way, Washington, 6 years in Maui, 4 more years in Parkland, WA with trips back to Maui in the summers, 1.5 more years in Maui, 7 years in Montana&#8230;now 1.5 years in Thailand (how old am I?&#8230;ya I think that&#8217;s right). By all counting&#8230;I am from CA, WA, HI and MT because I lived for 6 or more years in each place. Still others believe home is where your &#8216;heart&#8217; is. Well as I&#8217;ve previously described that would&#8217;ve pinned me between HI and MT.

Talking to a friend of mine here who is a &#8216;farang&#8217; (white person). He thought it was interesting how Thai&#8217;s always ask if you are going home for Christmas&#8230;even if Thailand is your home. For him, Thailand is now his home, he now &#8216;visits&#8217; the U.S. but  Thailand is home and yet he still gets asked if he&#8217;s going home for the holidays. Thailand for me is a temporary home as it has an expiration date for me (2013), but it&#8217;s my home for now. Why?

Now I believe home is where soul is at rest. As &#8216;cheesy&#8217; as saying &#8216;home is where the heart is&#8217;, we say it because it&#8217;s true. But it depends on what you think of as home. For some home is a physical location like Bozeman, MT. Beautiful, snowy in winter, sunny in summer, the smells the people, the houses, the activities, the mountains, the microbrews&#8230;yes, that&#8217;s &#8216;home&#8217; to some people, to &#8216;be&#8217; anywhere else no matter for how long won&#8217;t feel like &#8216;home&#8217; to them. They associate home with all those things in addition to the emotion it draws. Others don&#8217;t really care much about the location but rather, its their family or friends, where ever they are, that&#8217;s home. I&#8217;d say in that respect Montana and Thailand have become 2nd and 3rd homes for me as I&#8217;ve made some many amazing friends in both places in addition to what would then be considered my 1st home (Hawaii) as that&#8217;s where my family lives.

But &#8216;home&#8217; for me is purely a &#8216;feeling. A feeling that can occur in any location or be associated with any person. And it changes as my circumstances change. When I think of Hawaii now, I love it and my parents live there but I no longer say &#8220;I&#8217;m going home to Hawaii.&#8221; Rather I say I&#8217;m going to visit my folks for Christmas in Hawaii or whatnot. I miss my family&#8230;I miss Maui&#8230;but I don&#8217;t associate Hawaii with the &#8216;feeling&#8217; of home anymore. Thinking of home this way is convenient for me as I&#8217;ve spent the greater part of my life travelling around living in various places and it assists in not getting attached to the physicality of anything. Currently my feelings and motivations are attached to Tyghe so home is where Tyghe and I are&#8230;and that home is still changing and will continue to change yet for several years I am sure, so I guess&#8230;

Right now I think the best description of home in my own life is: &#8220;Everyday is a journey, and the journey itself is home&#8221; ~[Matsuo Basho][1]

&nbsp;

 [1]: http://en.wikipedia.org/wiki/Matsuo_Bash%C5%8D
