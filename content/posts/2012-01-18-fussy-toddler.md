---
title: I am a fussy toddler…
author: Mel
type: post
date: 2012-01-18T05:04:47+00:00
url: /2012/01/18/fussy-toddler/
categories:
  - Devotion
tags:
  - A.A. Milne
  - Asia
  - existing
  - God
  - graduate
  - life
  - memories
  - passion
  - peace
  - quiet
  - train

---
Devotional Blog:

Topic: &#8220;Cultivating the quiet&#8221;, 01/08/12, Psalm 23: 1-6

Hey&#8230;I&#8217;m into January in this book&#8230;well actually I&#8217;m backlogged and still have some blogs from December&#8217;s month in the book to write but I kind of just dog-ear them and will get to them, eventually.

Interestingly this one came up. In a previous entry in the book the author had encouraged us to spend our time productively and not waste it and I wrote a [blog about how taking moments to &#8216;space out&#8217;][1] and how valuable that can be for ones mental health. Now in this entry she encourages moments of quiet stating that a &#8216;quiet heart is a receptive heart&#8217;. 1 Peter 3:4 states, pulling from the previous verse&#8211;beauty&#8230;&#8221;should be that of your inner self, the unfading beauty of a gentle and quiet spirit, which is of great worth in God&#8217;s sight.&#8221; Pslam 23: 2 states: &#8220;He lets me rest in green meadows; he leads me beside peaceful streams&#8230;&#8221;

Flipping through the book this morning to find an entry dog-earred to write about I came across this and it hit a nerve for me. In the past few weeks my life has taken a tremendous turn.<!--more--> A lot has happened in a short period of time with family, work, life in Thailand&#8211;everything all at once and it&#8217;s overwhelming. Speaking to my mother out of frustration I stated that I cannot remember the last time I had to &#8216;breathe&#8217;. My life goes from one end to the next at full speed. I come to the end, I flip around and keep going, no time to actually take a break. For instance, I have been trying to get &#8216;time off&#8217; since graduating college. I started bemoaning my inability to to &#8216;find myself&#8217; like so many of my friends got to do by travelling, having no work obligations, no real family obligations, no school obligations&#8230;just having that time to &#8216;be&#8217;. All during high school I worked. All during college I worked, after college I went directly into an internship, after my internship I went &#8230;less than 10 days after having arrived home I was working full-time again, 2 years later still working I applied for grad school. I&#8217;d saved up money, figured I&#8217;d have some free time before going&#8230;nope. I got the acceptance in November after travelling to MT to visit/interview, had one month to pack up my life, while still working, and move to MT and be ready to start school spring semester in January. Started school in January and worked odd jobs all through grad school&#8230;7 years. As my grad school years were coming to a close I was relishing the thought of a break between grad school and my first postdoc. A break, any break&#8230;something! Unfortunately my degree process extended into my whole buffer prior to the start of my fellowship so I defended, turned in my dissertation, moved and left for Bangkok within 2 weeks and started my postdoc. Now I&#8217;ve been presented with a job opportunity after 2 years of post-docing which is amazing and wonderful, but it starts immediately and once again I get no time to breathe. If I accept it, once again I hit the ground running to make life happen.

And I was frustrated, there seems to be no &#8216;quiet&#8217; in my life. I&#8217;ve lived in Asia for 2 years and given how busy work keeps me we haven&#8217;t been able to travel within Asia that much. So many misconstrue the life of working overseas&#8230;it&#8217;s not a backpacking vacay that&#8217;s for sure. We&#8217;re not &#8216;finding ourselves&#8217; over here, we are making life happen just in a different country. So like all humans, as grateful as I was for the job opportunity I was being a fussy toddler about it&#8211;I&#8217;m not usually a fussy toddler about much, I think it was just all so overwhelming when compounded on top of everything else going on in my life.

So where does the fussy toddler find time to &#8216;be quiet&#8217; and have a &#8216;time out&#8217;? Turns out it was on a train coming back from Chiang Mai (in Northern Thailand). We&#8217;d decided to take a mini-break for the weekend and head up 16 hrs by train to Chiang Mai to attend a frisbee one-day hat tournament. Ok, so it&#8217;s a long way to go for a weekend but it was a timely decision. I&#8217;m in the top bunk coming back and I absolutely cannot get to sleep. So I am writing postcards and listening to my ipod and my ipod has a grand mix of everything on it and a song comes on by [Barlow Girl called Enough][2] (they are a Christian band) and my ipod literally got stuck on repeat in that song&#8230;uncanny and well, my ipod is old with quirks. But it struck me&#8230;when has life ever been &#8216;enough&#8217; for me, has what God given me in life been &#8216;enough&#8217;? THIS was my time to sit back and breathe&#8230;and be quiet. So I was.

I actually took that time to sit and reflect on all the times in my life I&#8217;ve taken to be &#8216;quiet&#8217;, times that I&#8217;ve felt at total peace, that I can remember.

  * While in the Ecuadorian Amazon our group went out in canoes at night on the river to look for caymans (smaller alligators). We had to be very quiet or we&#8217;d scare them off. Our boat didn&#8217;t have very many kids in it. We floated into an open space of water, a break in the jungle, a million stars overhead lighting up the jungle. I was 20 years old.
  * In Costa Rica we went up and stayed in a massive cabin in the cloud forest while doing environmental conservation work. It was loud and hot in the cabin so I can outside and sat on the steps which a cup of hot cane juice. The forest was lit up with fireflies, some of the boys were out trying to catch them. I was 18 years old.
  * During the day we&#8217;d (in Costa Rica) gone to the local village and some of our group started to play soccer with the local kids. I sat on a wall to watch. The village was situated on a bridge of sorts between two mountains/large hills? As they played the fog rolled in quickly muffling all the sounds of the game.
  * Cross-stitching in my basement apartment on my futon couch with a million blankets, snowing outside. I was 26 years old.
  * Walking down Babcock St. in Bozeman, MT when it starts to snow HUGE snowflakes. I was 27 years old.
  * New years 2010 in Maui, on the beach. I was 29 years old.
  * Sitting atop Pete&#8217;s Hill after sledding with hot cocoa and peppermint schnapps in Bozeman, MT. I was 24 years old.
  * Sitting up in Ulapalakua on a mountain side overlooking Maui valley after driving up into the forest and finding a clearing. I was 16 years old.
  * Sitting at a beach restaurant in Koh Samet, Thailand listening to the ocean. I was 31 years old.
  * Sitting in a train at sunset watching the rice fields whiz by. I am 32 years old.

As I sat there I kept thinking of other times where I&#8217;ve had my &#8216;quiet&#8217; times. Fact of the matter is, my life is busy, always has been, there&#8217;s always something happening or that needs to happen, always someone that will need company. I wrote this in a postcard to a friend while sitting in that top bunk by myself unable to sleep.

> &#8220;I have had such a peace living in Thailand the past two years. It comes from knowing and experiencing exactly where God has placed me in life. A life I am learning can change at a moments notice. A life where I have to cherish those quiet moments reflecting on how awesome God is for giving me the life I have. Though I may never truly know his plan for my life, though I may walk through doors that make me cry&#8211;ultimately, in the end, peace overrides the pain at having to end one chapter of my life to start another.&#8221;

Its in these quiet moments that I &#8216;find myself&#8217;. My life has had so many very interesting chapters&#8230;all chapters have to come to an end at some point, thankfully in some cases, sadly in others. I&#8217;m hoping I&#8217;ll have many more chapters to write and that I&#8217;ll find the &#8216;white space&#8217; the &#8216;quiet&#8217; to reflect and center myself and &#8216;revel&#8217; in the life I have been so generously given. Where is the &#8216;quiet&#8217; in your life?

So I was sent a sad note from the climbing community facebook page that I am apart of that a girl had passed away while climbing in Patagonia. She&#8217;d written some blogs for [highline online][3] and out of curiosity I clicked over to her blog and she had an entry on the [Sound of Silence][4]. In it she posts a poem by A.A. Milne that I will repost here:

> <h5 style="text-align: center;">
>   <strong>The Island by AA Milne</strong>
> </h5>
> 
> &nbsp;
> 
> If I had a ship,
  
> I’d sail my ship,
  
> I’d sail my ship
  
> Through the Eastern seas;
  
> Down to a beach where the slow waves thunder-
  
> The green curls over and the white falls under-
  
> Boom! Boom! Boom!
  
> On the sun-bright sand.
  
> Then I’d leave my ship and I’d land,
  
> And climb the steep white sand,
  
> And climb to the trees,
  
> The six dark trees,
  
> The coco-nut trees on the cliff’s green crown-
  
> Hand and knees
  
> To the coco-nut trees,
  
> Face to the cliff as the stones patter down,
  
> Up, up, up, staggering, stumbling,
  
> Round the corner where the rock is crumbling,
  
> Round this shoulder,
  
> Over this boulder,
  
> Up to the top where the six trees stand….
> 
> And there would I rest, and lie,
  
> My chin in my hands, and gaze
  
> At the dazzle of the sand below,
  
> And the green waves curling slow,
  
> And the grey-blue distant haze
  
> Where the sea goes up to the sky….
> 
> And I’d say to myself as I looked so lazily down at the sea:
  
> ‘There’s nobody else in the world, and the world was made for me.’

Where do you find your &#8216;peace&#8217;&#8230;describe it, write it down, fold it up, place it in a special place for those days when life overwhelms you and you cannot find the peace in it. Then find a spot&#8230;lay in your bed, find a hill to sit on, find an empty swing set&#8230;unfold your piece of paper and reflect on what has brought you peace&#8211;remember, feel it, revel in it&#8211;let it center you.

Then jump back into life and attack it with voracious passion.

 [1]: http://www.melaniemelendrez.com/2011/10/04/a-space-cadets-moment/ "A space cadets moment"
 [2]: http://www.youtube.com/watch?v=zvRrWqLsoxk
 [3]: http://highlineonline.ca/
 [4]: http://highlineonline.ca/2011/12/23/the-sound-of-silence/
