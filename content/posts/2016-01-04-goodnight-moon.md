---
title: Goodnight Moon…
author: Mel
type: post
date: 2016-01-04T08:11:24+00:00
url: /2016/01/04/goodnight-moon/
categories:
  - Life or something like it

---
So it&#8217;s about 1:30 in the morning and I&#8217;m sitting on husband&#8217;s computer writing this reflecting on my past year. I&#8217;m actually pretty proud this post is less than a year from my last post, granted only by a month but hey &#8211; it&#8217;s progress right? I&#8217;m drinking hot limeade with honey, my own home remedy for the raging cough and sore throat that I&#8217;ve had the past several days now. Caveat of kids&#8230;when they go to daycare they bring home all manner of viral madness that you and the spouse get to take turns doing battle with&#8230;

Oh ya&#8230;so I had a baby this year.

In fact my last post on Macchu Picchu I was nearly 2 mos along. It made that trip all that more special for my husband and I to be able to travel and cross a bucket list item off my list with little peanut tagging along. While I did not relish the insane nausea that kept me from enjoying so much Peruvian cuisine the experience was nonetheless amazing.

 <img class="wp-image-964 alignleft" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150118_063602.jpg" alt="IMG_20150118_063602" width="306" height="408" /><img class="wp-image-965 alignleft" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150118_063406.jpg" alt="IMG_20150118_063406" width="308" height="411" />

Following that adventure began the adventure of getting larger and larger as peanut became kumquat became grapefruit eventually on her way to small watermelon.  Pregnancy treated me well and occupied most of my year. Skip ahead 9 mos, I&#8217;m not going to go into too much detail other than to relay what I learned by 40 weeks of pregnancy:

  * naps are awesome
  * waiting for said child to debut is tedious especially when one cannot (or shouldn&#8217;t) work
  * baby movement is awesome and will be what I miss most
  * morning sickness is not awesome &#8211; even if you aren&#8217;t vomiting constant nausea blows
  * toward the end you will hear &#8216;Wow, ready to pop yet&#8217; about a MILLION times!!!!! and you&#8217;ll reply &#8216;Nope, still XX <span class="il">weeks</span>/months to go&#8221; about a MILLION times!!!
  * Is it just East Coasters like to schedule their babies? &#8211; I&#8217;ve been asked a ton of times when my scheduled induction or cesarean is and am met with surprise when I say; nope just waiting for her to come on her own. It&#8217;s like I&#8217;m a cave woman!
  * pillows are awesome
  * food is awesome
  * ice cream is awesome (husband! so you&#8217;re fault!)
  * pregnancy in the hot humid summer is not awesome
  * people smile A LOT at pregnant women &#8211; it gives me a warm fuzzy feeling to know I can make someone smile just by cooking a minion and waddling past them.
  * pregnancy doesn&#8217;t have to &#8216;slow you down&#8217; like I&#8217;ve been told. Long as you listen to your body the world is still your oyster! HOWEVER, if you over do it your body will definitely rein you in sometimes not nicely!

I had lofty goals of what I was planning to accomplish before heading out on maternity leave because while I had hopes, I had no delusions about the fact I was most likely going to get nothing done during maternity leave. Figured I&#8217;d be lucky to get a shower. I managed to finish up analyses so others could write manuscripts and literally the day before I went into labor I managed to submit a manuscript detailing a study I did during my Ph.D. work!

My daughter arrived Sept 8 &#8211; and my husband and I found ourselves in the tailspin of sleep deprived newborn stage. Thank God my mom and then my husband&#8217;s mom showed up to help us navigate for a few weeks! FMLA allowed me up to 12 weeks off my job only part of which was paid at 60% of my salary. Welcome to the U.S. land of no paternity leave and in my case maternity leave was called temporary disability &#8211; y&#8217;know because expanding your family is a disability!? But enough about my discontent with &#8216;the system&#8217;. Just because that&#8217;s &#8216;how it is&#8217; all over the country doesn&#8217;t make it right though. And I was fortunate, there are several jobs where there&#8217;s no FMLA or anything set up for women having kids &#8211; so I have to count my blessings where I can.

My precious girl is now 4 mos old&#8230;I&#8217;ve since gone back to work, had fantasies about going all &#8216;office space&#8217; on my breast pump, {{< youtube N9wsjroVlu8 >}}

managed to see my name in print again after a coons age of languishing publications &#8211;

  * <a href="http://journal.frontiersin.org/article/10.3389/fmicb.2015.01540/abstract" target="_blank">Recombination does not hinder formation or detection of ecological species of <em>Synechococcus</em> inhabiting a hot spring cyanobacterial mat</a>
  * <a href="http://www.ijidonline.com/article/S1201-9712(15)00261-1/abstract" target="_blank">Feasibility and effectiveness of a brief, intensive phylogenetics workshop in a middle-income country</a>
  * <a href="http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0840-5" target="_blank">Pathosphere.org: pathogen detection and characterization through a web-based, open source informatics platform</a>

and as of recent, caught a nasty cold accompanied by a heinous cough &#8211; hence the home remedy I am currently drinking.

4 months&#8230;having children is challenging. Not to go all Dicken&#8217;s about it but the husband summed it up nicely with &#8220;It is the best of times, it is a the worst of times.&#8221;  And I&#8217;ll expand on that by adding some more Dickens&#8221;&#8230;it is the age of wisdom, it is the season of Light&#8230;it is the spring of hope&#8230; we have everything before us&#8230;&#8221;

We call her our incredi-baby capable of being unbelievably challenging and adorable all at the same time!
  
{{< youtube IaCjLOe55UY >}}

In keeping with what I learned with 40 weeks of pregnancy, here&#8217;s what I wish with 4 mos of parenthood under my belt:

  * I could bathe in caffeine
  * My boobs would automagically fill bottles during the night so I can sleep rather than pump.
  * I wish I could freeze moments of time when <span class="il">my daughter</span> has a huge smile and gives me massive eyes.
  * I could find the &#8216;off&#8217; button on my body and conversely the &#8216;on&#8217; button when 2 am pumpings come up
  * I could be at work and at daycare or home with <span class="il">my daughter</span> at the same time &#8211; so apparently I want to be in several places at once&#8230;what&#8217;s that thing called where Hermoine turns back time again so she can be in several places at once Harry Potter fans?
  * I had more tolerance. There are days when it&#8217;s just not there and I feel horrible about it.

We are getting to know our daughter and she is getting to know us and even on our most challenging days &#8211; her smile and laughter just seems to erase any difficulties. When she&#8217;s cuddly you just want to hold her forever. Currently, she&#8217;s discovered she loves to stand up &#8211; I think she may skip crawling altogether!

I&#8217;m learning to love routine, the predictable &#8211; unusual given most of my life I&#8217;ve associated routine with mundane. Having a child is teaching me the beauty of routine, the small things, the rituals, the prayers, the quiet and loud moments&#8230;

Tonight my daughter was very tired and pitching a fit as husband was attempting to get her into pajamas. I took over to give him a breather as she was hollering up a storm which can grate on the most zen of nerves sometimes. There is such solace in being able to tag-team with your child &#8211; ensures some modicum of calm which is absolutely essential when attempting to quiet a baby in the midst of an emotional riot! Part of me couldn&#8217;t blame her, she has been catching back to back viruses from daycare for over a month now! And she&#8217;s been a trooper for most of it.

I read in a book, (great read by the way &#8211; <a href="http://amzn.com/0345440927" target="_blank">Secrets of the Baby Whisperer for Toddlers by Tracy Hogg et al. </a>) that, in general, there are no &#8220;bad&#8221; parents and no &#8220;bad&#8221; children &#8211; just learning on both fronts. Learning each others cues, learning to communicate &#8211; it&#8217;s a steep curve but one that you master over time and can&#8217;t really rush.

I picked up my little minion still in the midst of her meltdown. As soon as I held her she started to quiet down. We went to the TV &#8211; &#8220;good night TV&#8221; and shut it off, we went to the baby changing area &#8220;good night baby changing area&#8221; and turned off the light, we went to the extra bedroom &#8220;good night playroom&#8221; and turned the light off, we went to the kitchen &#8220;good night kitchen&#8221; and turned the light off, we went to the bathroom and and looked in the mirror &#8220;good night baby in the mirror&#8221; and we blew kisses to the baby in the mirror as we shut off the light. With each good night she grew quieter and quieter. We grabbed her bottle and sat on the couch and sang as she settled down for the night and for bedtime.

<img class="alignleft wp-image-974" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20151225_132344.jpg" alt="IMG_20151225_132344" width="260" height="347" />

&#8220;Good night sweetheart well it&#8217;s time to go, good night sweetheart well it&#8217;s time to go, I hate to leave you but I really must say &#8211; good night sweetheart goodnight.&#8221;

She drifted off and started snoring &#8211; because lets face it she&#8217;s got my husbands genetics 🙂

I put the bottle down picked her up still singing and placed her in her crib.

> **I&#8217;ll love you forever,**
  
>  **I&#8217;ll like you for always,**
  
>  **As long as I&#8217;m living**
  
>  **my baby you&#8217;ll be.**

Goodnight moon&#8230;goodnight 2015.
