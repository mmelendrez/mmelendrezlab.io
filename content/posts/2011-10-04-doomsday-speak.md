---
title: Doomsday speak?
author: Mel
type: post
date: 2011-10-04T13:47:08+00:00
url: /2011/10/04/doomsday-speak/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"504f210c62";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:20:"wordbook_comment_get";s:2:"on";s:17:"wordbook_new_post";s:1:"1";}'
wordbooker_extract:
  - |
    Devotional Blog:
    
    Topic: The Middle, 9/28/11; Ecclesiastes 12:9-14
    
    I don't know which book is more 'happy go lucky', Lamentations (everything is sad) or Ecclesiastes (everything is meaningless). A lot of time in this book is devoted to talking abo ...
categories:
  - Devotion

---
Devotional Blog:

Topic: The Middle, 9/28/11; Ecclesiastes 12:9-14

I don&#8217;t know which book is more &#8216;happy go lucky&#8217;, Lamentations (everything is sad) or Ecclesiastes (everything is meaningless). A lot of time in this book is devoted to talking about how meaningless is everything and how we should live in the gifts of the moments as they are from God. It goes into detail on how Solomon feels that nothing is worth it&#8230;Ecc 2:17, 19; 5:10; 6:2; and 11:10 for instance. The author ascribes this to &#8216;midlife disillusionment&#8217; and mentions at the end that the only positive Solomon mentions is &#8216;fearing God&#8217;.

So life is meaningless and fear God? Ya, gave me warm fuzzies all over too.

How do you stay optimistic in light of all that? Solomon had an amazing amount of wisdom, a gift from God. He looked beyond his wealth and stature to the very nature of life and what that meant. If all is fleeting: wealth, knowledge, health, public adoration&#8230;what is left? God&#8230;faith, something that can never be taken from you.

If all were stripped of me what would I have left&#8211;assuming I had presence of mind&#8230;my faith. Even in my deepest depressions and I&#8217;ve had some deep ones&#8230;my faith is always there&#8211;at times I reluctantly look at it asking myself &#8216;how is it helping me now&#8217;? I&#8217;m depressed, nothing is helping me now. Is he so disgusted with my life that he&#8217;s averted his eyes or turned his back no longer willing to comfort me? Depression has amazing &#8216;suck power&#8217; like quick sand only it&#8217;s black mud and you sit and wallow in it because it&#8217;s easier than dragging yourself from the muck and searching for any light&#8211;eventually if you don&#8217;t manage to drag yourself out, you suffocate in it.

I don&#8217;t know if Solomon was depressed when he spoke in Ecclesiastes but even with the &#8216;God given wisdom&#8217; he had&#8230;everyone ponders &#8216;is it worth it&#8217;. Everyone finds their own sense of meaning, everyone drags them-self out of muck because to wallow in it is a dark place to be. I&#8217;ve been told by a few seemingly eternally pessimistic friends that its better to just say &#8216;meh&#8217; and not care&#8230;but why lie to yourself? I cannot count how many times in response to hurtful things I&#8217;ve flippantly said &#8216;whatever&#8217; or &#8216;I don&#8217;t care&#8217; which is a total and utter lie and in looking back I kind of wished I&#8217;d stomped back in saying &#8220;I do care, this does matter.&#8221;

Even the coldest heart as an &#8216;achilles heel&#8217;&#8211;something that gets to them. And when its hit, on the outside you say &#8216;meh&#8217; and keep up the cold composure while on the inside you are boiling in muck. What do you do? Lots of times I&#8217;ve wallowed away in stubborness and what did it serve me in the end? Absolutely nothing.
