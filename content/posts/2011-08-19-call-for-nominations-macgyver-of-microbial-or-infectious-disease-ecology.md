---
title: 'Call for Nominations: MacGyver of [Insert Your Field]'
author: Mel
type: post
date: 2011-08-19T05:30:53+00:00
url: /2011/08/19/call-for-nominations-macgyver-of-microbial-or-infectious-disease-ecology/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - I will do Science to it!
tags:
  - infectious disease
  - MacGyver
  - microbial ecology

---
[<img class="aligncenter size-full wp-image-184" title="260px-MacGyver_intro" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/08/260px-MacGyver_intro.jpg" alt="" width="260" height="200" />][1]So I can&#8217;t claim complete originality of this idea, I was inspired by a facebook post from a colleague of mine [Dr. Jennifer Biddle][2] who consequently is my personal nomination (see below). In the resource strapped world of research in general and in fields that bounce routinely between NIH and NSF funding, like my field: Infectious Disease Ecology (is it ecology? is it medical? is it ecology? is it medical?) innovation is key! But lets not snub the other fields of science where innovation is key! I simply address this field because it&#8217;s my field. Any nominations are welcome! Also for those working in the developing world where money can be even scarcer it pays to &#8216;figure shit out&#8211;old school&#8217;.

Remember the days when grandpa would walk to school, uphill in the snow both ways and amused himself for hours with nothing but a stick and a string? Where we learned how to &#8216;make life happen&#8217; with nothing but a few coins in our pockets&#8230;things have gotten expensive nowadays! Then the 1980&#8217;s came a long and who should present himself but MacGyver&#8230;my fiance currently owns every episode of [MacGyver][3] created. Then man who could set off bombs with bubble gum, attack and defeat countless terrorists and other assorted bad guys with baking soda and a swiss army knife. The man was scientific &#8216;magic&#8217; if you will.

So in honor of the world of money strapped scientific research I&#8217;d like to open nominations for who you believe to be the MacGyver of your field and why with links to their professional profiles if you so desire. Let&#8217;s laud the achievements of our innovative colleagues and strive for their ingenuity borne out of sheer force of will and desperation&#8211;&#8220;do I buy food or that sequencing kit&#8230;&#8221; Yes I am guilty of placing a sequencing kit above food at times&#8230;but I drew the line at mouth pipeting and just bought some damn pipets!

<!--more-->My nomination would be Dr. Jennifer Biddle who inspired this blog with her facebook status:

_ ****_

<p style="text-align: center;">
  <em><strong>&#8220;experiments today included an aquarium bubbler and zip ties. Ghetto science: building a gassing setup for no money!&#8221;</strong></em>
</p>

This was followed by several comments answered by Dr. Biddle in this manner: _****_

<p style="text-align: center;">
  <em><strong>&#8220;[paraphrase]: Bubbler from Doug, tubing from Bill, left over zip-ties from fieldwork&#8230; My goal is to be macGyver and sequence a genome with a well placed paper clip ;)&#8221;</strong></em>
</p>

<p style="text-align: left;">
  Dr. Biddle&#8217;s research focuses on Microbial populations and processes in subseafloor marine environments and her projects and publications can be seen <a href="http://www.ceoe.udel.edu/people/profile.aspx?jfbiddle">here</a>.
</p>

<p style="text-align: left;">
  So I put the question to you all&#8230;who would you nominate and why? Nominations will remain open until I decide to close them as this is completely for fun and given my fiance is the all-knowing MacGyver enthusiast, he will most likely help me decide who wins. Or perhaps no one wins but rather we highlight all those innovators out there&#8230;
</p>

<p style="text-align: left;">
  <span style="text-decoration: underline;"><strong>I salute you all!</strong></span>
</p>

 [1]: http://www.melaniemelendrez.com/wp-content/uploads/2011/08/260px-MacGyver_intro.jpg
 [2]: http://www.ceoe.udel.edu/people/profile.aspx?jfbiddle
 [3]: http://en.wikipedia.org/wiki/MacGyver
