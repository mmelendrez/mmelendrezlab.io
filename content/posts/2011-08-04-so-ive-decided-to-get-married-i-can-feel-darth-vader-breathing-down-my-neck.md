---
title: So I’ve decided to get married, I can feel Darth Vader breathing down my neck…
author: Mel
type: post
date: 2011-08-04T12:20:08+00:00
url: /2011/08/04/so-ive-decided-to-get-married-i-can-feel-darth-vader-breathing-down-my-neck/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Wedding Planning Bliss-sters...
tags:
  - complex
  - marriage

---
Yes I just linked getting married with Darth Vader, I&#8217;m officially an un-recoverable geek; I&#8217;ll explain further down&#8230;

So I got engaged last August 2010 on the top of a building in Thailand, interested? [read here][1]. Amidst the flurry of people who were happy for me and those who were substantially peeved they didn&#8217;t hear from me personally but rather through facebook&#8211;doh!&#8211;even when you think you are &#8216;winning&#8217; by using social networking, you&#8217;re not; accept that now. Anyway, I started mulling the &#8220;oh crap, now I&#8217;ve actually got to plan this&#8230;from Thailand&#8230;in a state I don&#8217;t live in, where I have no family&#8211;although I do have a couple friends.&#8221; But, never fear&#8230;I have a year or more to do it. If you didn&#8217;t know I live in Thailand&#8230;[read here][2]&#8230;.[and here][3]&#8230;[and here][1]&#8230;and if you&#8217;ve decided you&#8217;d like to be my stalker [check here][4]. Yes I actually live and work in Thailand.

I never dressed up as a bride as a child&#8230;not once. I liked to get white dresses dirty. All through elementary, junior high and high school, not once did I ever give weight to the thought of getting married. My type also wasn&#8217;t exactly in &#8216;high demand&#8217; either, so that might have had something to do with it. I went through phases of being too immature, too geeky or that girl who your friends with but the thought of anything more makes you heave ho-ho&#8217;s or twinkies or whatever the heck you had for lunch that day.

<!--more-->When I finally did start dating, I was a serial monogamist emotionally attached until I had a reason not to be a.k.a. they cheated on me or didn&#8217;t want to be with me anymore. I&#8217;ve only broken up with two men in my life and both are better off without me I&#8217;m sure. I think as a young adult what scarred me the most was a phone call where an on/off boyfriend mentioned wanting to be with me but not wanting to marry me; y&#8217;know be with me til someone that was better for him came about. Firstly, I had to give the guy props for having the cojones to actually say that. Secondly, as politely as I could, I declined all the while thinking &#8220;I&#8217;m gonna die alone.&#8221; I&#8217;ve been dumped a lot of times in my life, some very creative ways, but this is the one instance I actually remember well.<figure id="attachment_71" style="width: 740px" class="wp-caption aligncenter">

[<img class="size-full wp-image-71" title="commitment" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/08/commitment.png" alt="credit www.xkcd.com" width="740" height="194" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/08/commitment.png 740w, http://www.melaniemelendrez.com/wp-content/uploads/2011/08/commitment-300x78.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/08/commitment-500x131.png 500w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px" />][5]<figcaption class="wp-caption-text">credit to www.xkcd.com</figcaption></figure> 

But this blog isn&#8217;t about my crazy past dating life&#8230;at least this entry isn&#8217;t, its about the reality that I did find someone who accepts and love the quirk I am and has decided to forever bind himself to that. Sounds weird and creepy huh? We&#8217;ll come to that too&#8230;in later entries. Suffice it to say I never thought and I&#8217;m sure many friends and family were skeptical that I&#8217;d ever marry.

It is now August 2012 and what have I done in the past year&#8230;gradually eased myself under the hot breath of the wedding industrial complex&#8230;that&#8217;s what I&#8217;ve done. A simple internet search sent me to the Ibuprofen bottle multiple times. I felt like it was me against a galactic empire of wedding starships pounding me with solicitations to join them&#8230;join the darkside&#8230;give in to the wedding industrial complex. As with anything in my life if I&#8217;m doing something or going to do something I read&#8230;a lot. And thanks to an amazing friend of mine, Piper, I received some actually helpful books on wedding planning on a budget or from far away. As with any good bride to be I promptly subscribed to a bridal magazine, no not 5, just one. Just doing the internet search on weddings nearly gave me an aneurism and a strong desire to run off and elope&#8230;with or without the husband to be. I figure this meant I should start slow.

Hands down the most hopeful, helpful book for maintaining my sanity amidst wedding planning on a budget is the [Bridal Bargains Guide][6] (I have the 10th edition). It&#8217;s become my wedding planning Bible and has helped me weed through the money hungry, glitz&#8217;d out, icing infused through an IV industry that weddings are. Anotherwards&#8230;it&#8217;s cool. My brain has the tendency to go into A.D.D. mode&#8211;though I&#8217;ve never been formally diagnosed with such, there&#8217;s something flighty about your brain when you&#8217;ve written half a blog post and during that time have checked your email 6 times, googleplus once, your downloading show twice and facebook 4 times. I need all the organizational focusing power I can get; books, lists, spreadsheets and semi-regular video chats with said maid of honor help.

Bridal Bargains is sectioned into several chapters each quite detailed in the area of the wedding dress, bridesmaid dresses, caterer, photography, videography, flowers, venues, scheduling, DJ&#8217;s&#8230;you name it they cover it along with a nearly exhaustive look at vendors with ratings as to how good they are on an A-F scale. Definitely worth the money.

When originally asked what I wanted in a wedding I immediately wanted to go back to Hawaii, have it outdoors or on a beach, and a small wedding. And we started planning to that effect until life got in the way and we realized having a wedding in my hometown was not feasible. Enter plan B. My fiance is from Wisconsin&#8230;Wisconsin it is. Fabulous so now I&#8217;m planning a wedding from Thailand in a state I have now only visited once, last May. Last May I had a whirlwind 7 days to attend 1 graduation, 1 graduation party, see and meet for the first time copious amounts of family to be and select a wedding venue&#8211;&#8220;go go gadget wedding venue!&#8221; It was a successful venture and we landed a golf/country club outside of town in a beautiful area. I had a brief fiery romantic affair with a barn venue but the flame blew out when I realized how far it was from civilization and how inconvenient or impractical it might be. Maybe if I lived in WI I could&#8217;ve pulled off a barn venue&#8230;but I needed a place that would do the majority of organization for me. Living abroad has it&#8217;s difficulties for sure.

But to finish up my entry into wedding bliss-sters&#8230;some other books that were quite useful:

  1. [The complete outdoor wedding planner][7] by Sharon Naylor: Very thorough guide to all the questions you might have with respect to having an outdoor wedding from venue, time of year, weather, legalities to wedding dress and shoes.
  2. [The best of Hawaii wedding book][8] by Tammy Ash Perkins: Piper got this for me when I was still dreaming of a wedding in Hawaii and having lived there I can say this book does quite well and telling you the yay&#8217;s and nay&#8217;s of throwing a wedding in Hawaii. It gives specific venues which I can vouch are pretty beautiful and lists vendors, some of whom I actually know! Most of the sites they talk about are quite pricey but to their credit they do list &#8216;free&#8217; possibilities too on beaches or parks or gardens around Hawaii. It&#8217;s not exahustive but for wedding planning in Hawaii the book is a good start.
  3. I&#8217;m a craft nut so when I got the book [Handmade Weddings][9], I was super excited. Then I thought when am I EVER going to have the time to do all this. The main thing in a &#8220;craft&#8221; wedding that ALL the planning sites and past brides say is start early. Well I have year&#8230;we&#8217;ll see how it goes. This book is themed according to the type of wedding you want or kind of girl you are&#8211;ie. there&#8217;s a chapter on &#8216;girlie romantic weddings.&#8217; Many of the crafts are pretty easy to do and look really nice. Specialty craft shops are hard to come by unless you can ask how to get to one in good Thai so this&#8217;ll be another challenge. And I could always go the Thai manufacturing route and buy in bulk from [J-J market (Chatuchak).][10]..paper fans for everyone! We&#8217;ll see how long my motivation holds out.

So now I embark on my quest for a somewhat simple, fun, semi-low shindig amidst the red barns of Wisconsin armed with 4 books a subscription to BRIDES magazine, 3 bottles of Ibuprofen and an ever doting fiance willing to bring me wine when the IV of icing gets to be too much.

At 31, according to some cultures I am &#8216;past my shelf-life&#8217; but happily I am not defined by my &#8216;sell-by date&#8217; and I have a feeling I will enjoy being married for many reasons one of the funnier ones echoing Rita Rudner:

<p style="text-align: center;">
  <strong><em>&#8220;I love being married.  It&#8217;s so great to find that one special person you want to annoy for the rest of your life.&#8221;</em></strong>
</p>

 [1]: http://www.melaniemelendrez.com/2010/08/24/khun-daeng-ngan-phom/ "Khun daeng ngan phom?"
 [2]: http://www.melaniemelendrez.com/2010/04/22/a-month-in-thailand/ "A Month in Thailand"
 [3]: http://www.melaniemelendrez.com/2010/06/24/bird-poop-and-buses-to-bangkok/ "Bird poop and buses to Bangkok"
 [4]: http://maps.google.com/maps?q=thailand&hl=en&sll=13.723419,100.476232&sspn=0.676376,1.234589&z=5
 [5]: http://www.melaniemelendrez.com/wp-content/uploads/2011/08/commitment.png
 [6]: http://www.amazon.com/Bridal-Bargains-Americas-best-selling-ebook/dp/B004GKMTLS
 [7]: http://www.amazon.com/Complete-Outdoor-Wedding-Planner-Everything/dp/0761535985
 [8]: http://www.amazon.com/Best-Hawaii-Wedding-Book-Destination/dp/1930722648
 [9]: http://www.amazon.com/Handmade-Weddings-More-Crafts-Personalize/dp/0811874508
 [10]: http://www.bangkok.com/shopping-market/popular-markets.htm
