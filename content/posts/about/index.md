---
title: About Blogger
author: Mel
type: page
date: 2010-02-15T11:43:48+00:00
wordbooker_options:
  - 'a:10:{s:18:"wordbook_noncename";s:10:"680f804b9e";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'

---
I am an aspiring infectious disease ecologist attempting to learn some mathematical modelling just to frustrate and excite my intellect more and&#8230;because all the cool kids are doing it. <span style="text-decoration: underline;"><em><strong>This blog is strictly a personal blog and NOT a professional blog.</strong></em></span>

For a link to my academic profile: <http://www.linkedin.com/pub/mel-melendrez/3/ba9/629>

For interest in my academic publications:

  1. R Takhampunya, A Kengluecha, T Monkanna, A Korkusol, S Leepitakrat, B Tippayachai, **MC Melendrez**, BP Evans and JH Richardson. _submitted._ Characterization of Orientia tsutsumagushi Strains Isolated from Leptotrombidium Mites and the Rodent Host Post-Transmission. Vector-Borne and Zoonotic Diseases.
  2. DM Ward, **MC Melendrez**, ED Becraft, CG Klatt, JM Wood and FM Cohan. 2011. Metagnomic approaches for identification of Microbial Species. _In: Handbook of Molecular Microbial Ecology, Volume 1: Metagenomics and complementary approaches_. First Edition, Ed. FJ de Brujin. John Wiley & Sons. pp. 105-109.
  3. JV Conlan, RG Jarman, K Vongxay, P Chinnawirotpisan, **MC Melendrez,** S Fenwick, RCA Thompson and SD Blacksell. 2011. [Hepatitis E virus is prevalent in the pig population of Lao People&#8217;s Democratic Republic and evidence exists for homogeneity with Chinese genotype 4 human isolates.][1] Infection Genetics and Evolution. 11: 1306-1311.
  4. **MC Melendrez,** RK Lange, FM Cohan and DM Ward. 2011. [Influence of molecular resolution on sequence-based discovery of ecological diversity among _Synechococcus_ populations in an alkaline siliceous hot spring microbial mat.][2] Applied and Environmental Microbiology. 77: 1359-1367.
  5. D Bhaya, AR Grossman, A-S Steunou, N Khuri, FM Cohan, N Hamamura, **MC Melendrez**, MM Bateson, DM Ward and JF Heidelberg. 2008. [Population level functional diversity in a microbial community revealed by comparative genomic and metagenomic analyses.][3] ISME Journal 1: 703-713.
  6. A-S Steunou, D Bhaya, MM Bateson, **MC Melendrez,** DM Ward, E Brecht, JW Peters, M Kuhl and AR Grossman. 2006. [In-situ analysis of nitrogen fixation and metabolic switching in unicellular thermophilic cyanobacteria inhabiting hot spring microbial mats.][4] Proceedings of the National Academy of Sciences USA. 103: 2398-2403.

&nbsp;

 [1]: http://www.ncbi.nlm.nih.gov/pubmed/21550423
 [2]: http://www.ncbi.nlm.nih.gov/pubmed/21169433
 [3]: http://www.ncbi.nlm.nih.gov/pubmed/18059494
 [4]: http://www.pnas.org/content/103/7/2398.short
