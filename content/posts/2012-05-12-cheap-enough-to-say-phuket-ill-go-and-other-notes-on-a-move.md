---
title: Cheap enough to say ‘Phuket’ I’ll go, and other notes on a move.
author: Mel
type: post
date: 2012-05-12T13:12:28+00:00
url: /2012/05/12/cheap-enough-to-say-phuket-ill-go-and-other-notes-on-a-move/
al2fb_facebook_nointegrate:
  - 1
al2fb_facebook_link_id:
  - 43807988_790996070809
al2fb_facebook_link_time:
  - 2012-05-12T13:12:31+00:00
al2fb_facebook_link_picture:
  - post=http://www.melaniemelendrez.com/wp-content/uploads/2012/05/airasia.jpg
categories:
  - Asia
  - Life or something like it

---
Perhaps, I&#8217;ll title this blog after I&#8217;m done writing. Yeesh&#8230;last post was end of March and it is now May! It&#8217;s been an eventful April/beginning of May to say the least. For those who are unaware or just joining &#8216;us&#8217; now&#8230;after living in Thailand for 2 years Tyghe and I have now relocated back to the U.S. abandoning our somewhat transient lifestyle for 401K&#8217;s and adventures we fully intend to have on the outskirts of Washington D.C. For instance this weekend we are heading to Virginia Beach, VA to take a final joy ride in our rental before giving it up for public transit&#8211;our foreseeable future.

I have mixed emotions with respect to leaving Thailand. We made an amazing group a friends there, I loved my work; while living there wasn&#8217;t the &#8216;constant&#8217; adventure that many people envisioned us having every day we&#8217;d made it our . Albeit hot and humid home, but home nonetheless and I find myself missing it.

Times like this I find it useful to make a pro/con list or rather things I miss and things I don&#8217;t miss:

Things I miss:

  * Our friends, I&#8217;d write this several times in the list because we really did connect with another &#8216;family&#8217; while out there and it was amazing. So many times I&#8217;d come across people I worked with that were counting the days til their time there was over and many times its because they hadn&#8217;t hooked into a network of friends that fueled them like a second family. If we go back to Thailand to visit, 90% of the reason will be our friends, 10% would be the see the parts of the country we missed seeing while living there.
  * The fact I can stuff a 6&#8217;5 man full on about $1.50. We now live in one of the most expensive counties in Maryland&#8212;DOH!
  * The ease and cheapness of traveling with SE Asia. You can&#8217;t beat a $30 roundtrip ticket on AirAsia&#8230;sure the airline is like a Thai bus, I think they&#8217;d have people standing in the aisles holding handles if they could. Sure Tyghe never fit in the seats as they were made for people not over 5&#8217;4 so he had to sit sideways. But you can&#8217;t beat $30 to get out of the city for the weekend.
  * The convenience and ease of public transit, the skytrain, subway, bus system, taxis everywhere, motorbike taxis everywhere, rapid bus transit system&#8230;if you wanted to go somewhere you really had no excuse&#8211;you could get there so really the only thing holding you back would be your laziness or it could be the 104oF heat and 50-60% humidity&#8230;ya that&#8217;d promote laziness for sure!
  * The smell of fruit, flowers or right before a storm. There were some amazing thunderstorms. The first year we watched them as they passed along the Chao Phraya river which we had a view of from our apartment&#8230;on the 12th story I believe that&#8217;s where we were. The second year, in our apartment on the 2nd floor you could see the lightening bursts and the trees outside our window would go ballistic, it was pretty cool.
  * Thais are happy people that will honestly try to help you even if they don&#8217;t understand a word you are saying.
  * Frisbee and frisbee tournaments..I hear the parties were pretty awesome too but in the two years I went to tourneys I succeeded in making only ONE party&#8211;doh! It&#8217;s the laying down after my shower while Tyghe showers that gets me every time.
  * The beaches&#8230;oh the beaches&#8230;the islands (except for Phuket, I strongly strongly dislike Phuket). Though I am amused by AirAsia&#8217;s ads for going to Phuket which by the way is pronouced (poo-ket)&#8230;but playing off how foreigners pronounced &#8216;Ph&#8217;&#8230;.

<img class="aligncenter size-full wp-image-835" title="airasia" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/05/airasia.jpg" alt="" width="500" height="375" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/05/airasia.jpg 500w, http://www.melaniemelendrez.com/wp-content/uploads/2012/05/airasia-300x225.jpg 300w, http://www.melaniemelendrez.com/wp-content/uploads/2012/05/airasia-400x300.jpg 400w" sizes="(max-width: 500px) 85vw, 500px" />

  * The food, even though I couldn&#8217;t eat more than half of it because of the spiciness&#8211;I still tried.
  * Our apartment, it definitely had quirks but it was spacious and we could easily have friends over. Our apartment now in Bethesda is nice, though not as spacious&#8211;we have a den/office which is pretty cool&#8230;it&#8217;s very&#8230;.white. I&#8217;ll need to remedy that when our boxes get shipped from Thailand. But it&#8217;s in a nice safe building that has a gym, business center, concierge, one parking spot in the garage and is almost spitting distance to the metro and bus stop.
  * Board game nights. I think it&#8217;s funny that when we left Thailand and posted everything that we had to get rid of, our friends were all worried about what would happen to all our boardgames&#8211;who gets custody of our boardgames since they knew we couldn&#8217;t take them all back. We managed to bring back [Killer Bunnies][1], [Pandemic][2], a chess game I bought (now we need to learn to play&#8211;well play and not suck so much at it, haha).
  * Dinner nights. In Thailand you don&#8217;t normally cook as its so hot, basically no one has an oven. Most have hot plates and a microwave. Food is so cheap on the street so that&#8217;s what most people do, but their dinner on the street on the way home. And that&#8217;s what we did, it was expensive to cook anything western and more expensive to go out and eat western food. Plus we loved Thai food so it wasn&#8217;t a big deal. But there were nights I just felt like cooking and often we would invite our roaming bachelor friends or couples to come join because I have a habit of making too much food anyway.
  * The Thanksgiving we had 20+ people over.
  * Inexpensive maid service you often times just &#8216;get&#8217; with your apartment rental, after moving back I was like &#8220;wow, the dishes and laundry aren&#8217;t magically getting done!&#8221; That spoiled us.

Things that make me glad to be back in the U.S.

  * No language barrier, in Thailand though the majority of those in Bangkok spoke a modicum of English&#8211;when you live there you have to deal with things like electric bills that are sometimes late, internet that may not be working and you have to get serviced, banking, public transit etc&#8230;that makes getting things fixed a definite challenge. By the end of our two years Tyghe and I were functional in Thai&#8211;ie. could get ourselves around via Taxi or public transit, order food, hold basic conversations that included a lot of hand waving and &#8216;arai na?&#8217; (&#8216;what?&#8217;).
  * The heat/humidity. Now I lived in Hawaii a long time and traveled to Costa Rica, Brazil, Ecuador&#8230; so I thought I was prepared for what Thailand might bring. I actually prefer heat and humidity to cold and dry&#8211;I survived Montana to the hum of a humidifier. I thought I was prepared&#8230;not in the least.
  * The smells. Sometimes they were awesome like cooking food, but many times walking down the street you&#8217;d come across an odor that could corrode steel.
  * I will not miss sweating. Sweating when? ALL THE TIME. Sweating midday, ok understandable, sweating during frisbee workouts&#8211;sure of course, sweating during my morning commute to work at 7am&#8211;ok a little annoying, sweating at 2am in the morning&#8211;say Wha??? Yes, literally. I got up at 2am to leave for my flight and it was 95oF and 60% humidity. That&#8217;s messed up. I will not miss sweating so much.
  * We are closer to family. We had some unfortunate things happen within family during our two years in Thailand and living abroad makes it challenging to respond to such things although NOT impossible as we demonstrated by getting back to the states within 24 hours taking a series of direct flights with little to no layovers.

Overall the move was a smart decision I feel as it allows me to gain experience and grow in my own career and Tyghe gets the chance to hold a normal job again which was frustrating for him living in Thailand. I&#8217;m sure you can imagine the ire of a computer programmer who can&#8217;t program because the internet keeps going out at his apartment and is no more reliable at a coffee shop and he can&#8217;t get it fixed because the technicians can&#8217;t narrow down what&#8217;s wrong and of course the language barrier. Upon moving back he found out we could get 3 to 4G on phones and Verizon FIOS at our apartment and probably had a small orgasm&#8212;oh computer geeks.

Now that we are more or less settled I intend to pick up the blogging again. Interestingly I checked my google analytics and since starting this blog in April 2010 I&#8217;ve had 990 page views&#8230;that&#8217;s not to say that amount of people actually stayed on the page but 990 people have been directed to the site and that&#8217;s pretty fun. I have a friend that just got over 1000 pages views which was really cool for her. Her blog is actually really neat&#8211;so I&#8217;m going to shamelessly plug it. Lately it&#8217;s encompassed adventures in serving tables in Hawaii. Her anecdotes are priceless and she&#8217;s quite a good storyteller so head over there when you get a chance.

http://upsellyoursoul.blogspot.com/

Our adventure in Thailand has concluded&#8230;our adventure back in the states now begins&#8230;

 [1]: http://boardgamegeek.com/boardgame/3699/killer-bunnies-and-the-quest-for-the-magic-carrot
 [2]: http://boardgamegeek.com/boardgame/30549/pandemic
