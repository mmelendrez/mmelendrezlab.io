---
title: The Plan…or lackthereof
author: Mel
type: post
date: 2012-02-20T07:49:01+00:00
url: /2012/02/20/the-plan-or-lackthereof/
categories:
  - Life or something like it
tags:
  - Bangkok
  - childhood
  - future
  - life
  - memories
  - nomad
  - Thailand

---
> # “It is not the strongest of the species that survives, nor the most intelligent that survives. It is the one that is the most adaptable to change.” ~Charles Darwin

It&#8217;s been awhile since I&#8217;ve posted anything&#8230;

Amazing how quickly life can go from a single path to a different path in mere days. It would be nice if the paths in our lives to success was straight and we merely had to find the right puzzle pieces in the path&#8230;

[<img class="aligncenter size-full wp-image-760" title="success2" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/02/success2.jpg" alt="" width="251" height="189" />][1]Of course we all know reality is a little different&#8230;is it not?

[<img class="aligncenter size-full wp-image-761" title="organization" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/02/organization.png" alt="" width="517" height="602" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/02/organization.png 517w, http://www.melaniemelendrez.com/wp-content/uploads/2012/02/organization-257x300.png 257w" sizes="(max-width: 517px) 85vw, 517px" />][2]So a plan that originally starts out straightforward can easily change directions and present us with decisions we didn&#8217;t think we&#8217;d have to make all at once.

[<img class="aligncenter size-full wp-image-762" title="success" src="http://www.melaniemelendrez.com/wp-content/uploads/2012/02/success.jpg" alt="" width="320" height="234" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2012/02/success.jpg 320w, http://www.melaniemelendrez.com/wp-content/uploads/2012/02/success-300x219.jpg 300w" sizes="(max-width: 320px) 85vw, 320px" />][3]On that note&#8230;we are officially leaving Thailand. I have taken a job offer at the [Walter Reed Army Institute of Research in Bethesda, Maryland][4]&#8230;back in the ole U.S. of A. The start date for being onsite is May 1st, 2012 so we essentially have a couple months to pack up our lives here, finish up whatever work I have, travel around SE Asia as we haven&#8217;t been able to do thus far, move to a new state, start new jobs and plan a wedding all at once&#8230;ah damn, I still have to file my taxes too doh! Turns out our wedding couldn&#8217;t be at a worse time given it&#8217;s now right in the middle of all this change that is occurring. When we&#8217;d originally scheduled our wedding we thought it&#8217;d be in the middle of nothing else happening. We couldn&#8217;t be more wrong.

The timeline for those interested:

  * Februrary: Cope with the idea of leaving in 2 months
  * March: Plan and execute packing, wedding planning, job details, plane tickets, travel all free weekends, wrap up work at AFRIMS
  * April: potential travel for Mel to US to apt. hunt, more packing, more wedding planning, wrap up Tyghe&#8217;s class (his final is April 11), wrap up NRC fellowship requirements. Then spend the rest of April traveling before returning to Bangkok the last week and flying to the US the last weekend in April
  * May: Be onsite the 1st to start processing to work at WRAIR and move into apt.
  * June/July: Settle, start work, plan wedding
  * August: Get married, go back to work (we&#8217;ll have to take a honeymoon another time most likely).

Is the job a good one? Yes in fact it is right up my alley working with people I enjoy working with and can continue to grow under. Am I wild about living in Maryland for the forseeable future as this job is pretty much a &#8216;permanent&#8217; job. Well I&#8217;ve never lived on the East coast and I know it will definitely be different. There were a lot of factors to consider in taking this job. I was sad that it&#8217;d take away my last year in Thailand. I love working in Thailand, I&#8217;ve built good relationships and I&#8217;ll be sad to leave. I was sad that it&#8217;d take away the potential travel time Tyghe and I would&#8217;ve had, if we&#8217;d had another year here. Now instead we are having to jam it all in at the end before heading back to the states, so we&#8217;ll have to pick and choose and cross the rest off our bucket list some other time in the future. The factors go on and on: Tyghe can get a good job in Maryland, the weather is more &#8216;agreeable&#8217; in Maryland than out here especially for Tyghe, the job offer I got was a good one, its working on things I am interested in, its a smart career move, its a stable job which is becoming quite important in the ever fluctuating job market of the US&#8230;

So why am I lamenting&#8230;what have I to lament? Absolutely nothing realistically speaking. I think its the &#8216;child&#8217; in me lamenting. Stupid right? I think it has a lot to do with my upbringing. I yearn for the nomadic. Most of my friends from high school and college have done a lot traveling, they had their backpacking Europe or Asia experiences, they worked for NGOs or in the peace corp&#8230;and now with the &#8216;nomadic&#8217; urges satisfied they&#8217;ve moved onto working jobs and having families. Some of them are still traveling, some are having their families abroad. One of the things I&#8217;ve had to learn is &#8216;a break between life stages&#8217; is a luxury that most of us may never have or afford. I came across an interesting blog about PhD graduates that decided to forgo the job offers and travel for their foreseeable future, their blog here: [PhD nomads][5].

What do I mean &#8216;break&#8217;. The &#8216;child&#8217; in me wanted a break. I wanted a time in my life where I wasn&#8217;t working, no responsibilities, no pending due dates&#8230;I wanted a time to sit and absorb, travel. Unfortunately such a dream requires money and time, neither of which I grew up with. Like many kids in my situation, we worked in high school, we worked our way through college, after college we worked then went to grad school or just got hooked into a good job right away. Most of us never having that [&#8216;gap&#8217; year][6] as the Brits call it. Never having that time to discover new things, with very few financial pressures or responsibilities. I wasn&#8217;t looking for a year off by any means, I would&#8217;ve been pleased with 3 months off. I couldn&#8217;t between high school and college, too busy working and getting things together to start school that fall. I didn&#8217;t between college and grad school, I went directly into a job, from there went to Hawaii and worked more, from there directly into grad school. I&#8217;d planned to after grad school&#8230;but of course that didn&#8217;t happen and I immediately started my fellowship out here in Bangkok&#8230;.

This is the part where I slapped myself for being an idiot. What did I want in life? Was I happy as a nomad? Minimally, sure I could be happy&#8211;as exciting as travel and being a nomad is, I really wanted a home base. Growing up, that stability wasn&#8217;t there all the time, we moved around quite a bit, perhaps I didn&#8217;t want to leave the nomad life because that&#8217;s what I knew, I am good at being a nomad and the prospect of settling and sticking somewhere permanently is scary&#8230;visions of a restless suburban nightmare pop into my head&#8230;damn you reality TV, haha.

Fact is, I might not have had the blessing or good fortune of having that gap year, but I have still had the wonderful experience of traveling, experiencing new places, meeting incredible people&#8211;sure I had to work the entire time but that doesn&#8217;t mean I didn&#8217;t get to experience some crazy awesome and crazy awful stuff in my life. I have gotten to travel to 10 countries and live/work abroad twice. That&#8217;s more than many people get to experience in a lifetime and I&#8217;ve done it before the age of 35.

I think what&#8217;s so &#8216;scary&#8217; persay for me is the fact I am now completely leaving a type of lifestyle behind that I&#8217;d grown up with, grown accustomed too and now have to explore a completely different part of who I am. It&#8217;s always sad to close one chapter of your life to start another, but the following chapters are what I am living for&#8230;not the past. I was sad to leave Montana after 7 years and move to Thailand, but it was one of the best decisions I ever made&#8211;being out here has been amazing.

I think the speed at which things happened and changed for us is a little daunting as well. It makes you second guess if you made the &#8216;right&#8217; decision. But I&#8217;ve always walked through the doors that have opened to me and I&#8217;ve never regretted one of them, so I am walking through this one. The thought of living in Maryland doesn&#8217;t exactly get the heart racing but then again I&#8217;ve never tried living there or anywhere on the East coast so I don&#8217;t really know now do I?

Going to Maryland and taking this job weren&#8217;t in the original plan..but that&#8217;s ok, our adventure will continue to unfold back in the states. The perk with this job is that I&#8217;ll still be collaborating with AFRIMS here in Bangkok which will give me the opportunity to come back and work/visit.

The next several months are going to be a whirlwind of chaotic activity, perhaps now sitting at my computer on my day off&#8211;I should enjoy the relative peace I have sitting here, balmy outside, drinking a coke zero, birds singing like crazy, flower infused air, food I can barely eat due to the spice but is so damn good&#8230;yes, I will miss Thailand but I look forward to going back to the U.S., going back and creating a new home and starting a new chapter.

 [1]: http://autusmasterminds.com/HOME_files/Pathway%20to%20success.jpg
 [2]: http://www.wardwired.com/wp-content/uploads/2012/01/org-chart-mess.png
 [3]: http://3.bp.blogspot.com/-uO_IEWWNrv4/ToIJZ9w1HXI/AAAAAAAABH8/adek7lGu_aQ/s1600/pathway+to+success.jpg
 [4]: http://wrair-www.army.mil/
 [5]: http://www.phdnomads.com/
 [6]: http://en.wikipedia.org/wiki/Gap_year
