---
title: 'Love and Riddles: How often do you say you love someone important to you?'
author: Mel
type: post
date: 2011-12-22T02:33:11+00:00
url: /2011/12/22/love-and-riddles/
featured_image: /wp-content/uploads/2011/12/googleimagelove-e1324518455906.png
categories:
  - Devotion
tags:
  - communication
  - friendship
  - love
  - passion
  - relationships

---
Devotional Blog:

Topic: &#8220;Love and Riddles&#8221;, 12/6/2011, Song of Songs (Solomon) 2:1-17

So I&#8217;ll be jumping around a bit as I play catch up in my devotional blog &#8216;series&#8217; out of this book. I&#8217;m combining two entries in this blog. So Song of Songs or Solomon as its called in some Bibles is quite the &#8216;lovers&#8217; book. It&#8217;s very short, only 8 short chapters (about 4 Bible pages) and sits between Ecclesiastes and Isaiah. I kept missing it when I was flipping through my Bible trying to find it. And the book is all about love and how to treat your lover. And how can you not think this books is about desire with verses like:

> 1:2 Let him kiss me with the kisses of his mouth&#8211;for your love is more delightful than wine. 1:13 My lover is to me a sachet of myrrh resting between my breasts.  1:16 My love is mine and I am his&#8230; 7:9-12 May the wine go straight to my lover, flowing gently over lips and teeth. I belong to my lover and his desire is for me. Come, my lover, let us go to the countryside let us spend the night in the villages. Let us go early to the vineyards to see if the vines have budded, if their blossoms have opened,and if the pomegrantes are in bloom&#8211;there I will give you my love.

In spite of the lack of explicitly religious content, Song of Songs can also be interpreted as an allegorical representation of the relationship of God and Israel, or for Christians, Christ and the Church or Christ and the human soul ([Cited from Wikipedia][1]).

What struck me was the sincere passion these two characters within this book had for each other. Like all good internet junkies I googled &#8220;love&#8221;&#8230;<!--more-->

Top Hits in Everything:

[<img class="aligncenter size-medium wp-image-625" title="googlelove" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googlelove-e1324518527214-300x289.png" alt="" width="300" height="289" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googlelove-e1324518527214-300x289.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googlelove-e1324518527214.png 883w" sizes="(max-width: 300px) 85vw, 300px" />][2]Top Hits in Images:

[<img class="aligncenter size-medium wp-image-626" title="googleimagelove" src="http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googleimagelove-e1324518455906-300x204.png" alt="" width="300" height="204" srcset="http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googleimagelove-e1324518455906-300x204.png 300w, http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googleimagelove-e1324518455906-1024x698.png 1024w, http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googleimagelove-e1324518455906.png 1248w" sizes="(max-width: 300px) 85vw, 300px" />][3]There&#8217;s so much in life that complicates &#8216;love&#8217; and communication&#8230;especially communication about love and what it is. It&#8217;s different to every person. I know people that believe that if you say you love someone &#8216;too much&#8217; it loses all meaning and thereby becomes worthless. Others I know believe you should tell someone you love them everyday of your life because you never know if it&#8217;ll be the last day you have that opportunity (a bit on the morbid sad side, but ok). Still others believe words are inadequate and actions are the only way to express love. I&#8217;ve told people I&#8217;ve loved them only to have them scoff assuming me insincere or joking or using the word &#8216;losely&#8217;. And of course there are many types of &#8216;love&#8217;. Those you love like family versus those you love romantically for instance. Or perhaps those you regard with conditional love versus those that would receive unconditional love.

In Song of Songs 2:3 it says: &#8220;Compared to other youths, my lover is like the finest apple tree in the orchard. I am seated in his delightful shade, and his fruit is delicious to eat.&#8221; Later on she talks about &#8216;starving&#8217; or &#8216;being faint&#8217; for his love.

10 years ago I would&#8217;ve laughed outright at these verses as impractical, not real, mushy and impossible to achieve with respect to love. Back then I was of the mindset of rarely to never telling someone I loved them because I thought it&#8217;d &#8216;sully&#8217; the meaning. Plus, honestly I didn&#8217;t know what unconditional love was aside from what my parents showed us growing up. But that&#8217;s parental love. Over time my views have changed; not in the sense I now believe you &#8216;should&#8217; tell someone you love them every moment of every day, but rather if you truly do love them the way they deserve to be loved then you wake up actually <span style="text-decoration: underline;"><em>wanting</em></span> to tell them you love them everyday. It becomes second nature&#8230;even when the &#8216;passion&#8217; has worn off.

Now feeling it is one thing&#8230;communicating it is another. Not everyone is a &#8216;great&#8217; communicator. I myself can be a jumble of &#8216;stream-of-consciousness-confusion&#8217; sometimes. The funny thing is, I don&#8217;t think we give those that love us in our lives enough credit. They&#8217;ve been around us, they&#8217;ve seen us sometimes at our worst and best and we somehow think they won&#8217;t understand? So we say nothing at all. I&#8217;ve talked to people who pride themselves on their ability to be a &#8216;mystery&#8217;. &#8220;No one can know me! ha HA!&#8221; And yes, there is &#8216;allure&#8217; in that&#8230;the challenge of trying to figure someone out&#8230;but at least for me, after while I just want the damn mystery solved and to move on. It&#8217;s so fricken tiring to constantly be told&#8211;&#8216;so close but no cigar&#8217;. In the end it makes me bitter, not intrigued and it ceases to be sexy. I get to the point of &#8220;just say what&#8217;s on your damn mind!&#8221;

In an earlier entry in the book, the author (author of the devotional book I&#8217;m in) talks about all the little things that she notices that gets inbetween relationships such as:

  1. Holding on to negative attitudes
  2. Using harsh or critical words
  3. Taking others for granted
  4. Not valuing the other person&#8217;s time, talent or treasure
  5. Having unspoken or unrealistic expectations
  6. Not dealing with your own past baggage
  7. Never saying &#8220;I&#8217;m sorry&#8221;.

The last three have bitten me in the a$$ so many times I cannot even count. Only with respect to #7 I think I said I&#8217;m sorry too much. It became my go to response in early relationships&#8230;I just assumed it was my fault whatever was happening so the response was automatic. I think baggage and unrealistic expectations are huge ones as well. It took me several years to deal with the bulk of my baggage and I couldn&#8217;t really enjoy or truly be myself in a relationship til that was dealt with. My baggage kept me lonely no matter how awesome my relationships were.

I think &#8216;knowing&#8217; someone is a lifetime process and we go into relationships thinking we &#8216;know&#8217; someone only to find we know them very little and have based our expectations on a &#8216;person&#8217; that doesn&#8217;t really exist. Then we get pissy that this person isn&#8217;t who we&#8217;d hoped they were and mostly its because we didn&#8217;t give them the flexibility of character to be who they are around us. I have tried to fit into boxes in relationships that weren&#8217;t truly me for the sake of the relationship because I wanted that person as my friend or romantic interest. It ALWAYS ends in disaster&#8230;that I&#8217;ve finally figured out.

How do you fix all this? Hell, I dunno but I know it starts with opening your mouth and making words come out. I&#8217;ve said a **load** of oddities to Tyghe in the 2 and half years we&#8217;ve been together! We don&#8217;t always agree, our life is not always &#8216;happy go lucky&#8217; and sometimes were are silent&#8230;but eventually we (sometimes awkwardly) try and explain or talk about what&#8217;s going on. Knowing the other person is sincere and is just trying to figure things out rather than piss you off helps too.

My view now&#8230;I will always make sure people in my life know I love them, adore them, appreciate them, believe they are important, that I think of them in all the ways I can. I&#8217;m not perfect at it, I screw it up frequently I&#8217;m sure but hopefully those who know me well know I am sincere at least and hopefully that helps.

Today is Dec 22nd, Christmas is coming up&#8230;y&#8217;know peace, love, joy and goodwill to all men? Watch yourself some &#8220;Its a wonderful life&#8221; and perhaps think of someone who needs to hear that you love them, adore them, appreciate them, simply think of them&#8211;perhaps you should let them know.

Relationships&#8230;romantic or otherwise are bumpy, sometimes you have to say _ouch_ and sometimes you have to say _I&#8217;m sorry_.

 [1]: http://en.wikipedia.org/wiki/Song_of_Songs
 [2]: http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googlelove.png
 [3]: http://www.melaniemelendrez.com/wp-content/uploads/2011/12/googleimagelove.png
