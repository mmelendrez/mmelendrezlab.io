---
title: Blog Updating and Reposting…
author: Mel
type: post
date: 2011-08-18T09:40:45+00:00
url: /2011/08/18/blog-updating-and-reposting/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - Life or something like it
tags:
  - blog
  - posting

---
So after combing the myspace, facebook and personal archives I have compiled everything now onto this site with handy category tags listed to the right. If you are interested in a particular topic click a tag and you are off!

So if posts are entitled and have a repost note and date you know its my invocations from a previous life&#8230;most likely my Ph.D. life; which you may find highly amusing or slightly depressing. Quite a few Ph.D. skeletons fell out of the closet when I opened it!

Cheers!
