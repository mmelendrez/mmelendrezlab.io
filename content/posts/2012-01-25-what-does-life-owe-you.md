---
title: What does life owe you?
author: Mel
type: post
date: 2012-01-25T06:22:04+00:00
url: /2012/01/25/what-does-life-owe-you/
categories:
  - Devotion
tags:
  - excuses
  - life
  - relationships
  - responsibility

---
Devotional Blog:

Topic: &#8220;Take Responsibility&#8221;, 01/25/2012, Romans 13:1-7, Galatians 6:5, 2 Corinthians 5:10

It&#8217;s her fault. It&#8217;s his fault. It&#8217;s their fault. The dog did it&#8230;

[Kid&#8217;s Excuses:][1] 

  1. One child to another: _Whenever you tell a big fat lie to get away with breaking an ornament, vase etc.  Tell your mum, dad etc. that it threw itself off of the mantle piece, table etc and then add in that you always believed that there was something out there!_
  2. Mom:  Where&#8217;s your report card?  You:  Um, mommy I&#8217;m really sorry and everything.  But I didn&#8217;t do it or anything but you know how I walk to school?  Well, the bell rang and I went to my locker to put it in my backpack and some very mean kids took it and started playing monkey in the middle then someone yelled that there was a big fight so the kids dropped it and ran outside.  Then, I was walking & was looking at it and a dog chased me and got it and chewed it up!  Sorry!
  3. I do what the cheerios tell me.
  4. When I was little, I cut up my sheets, my excuse was, &#8220;Jesus did it!&#8221;****
  5. The clowns made me do it&#8230;I sware!

[Excuses given to police officers][2]:

  1. #### One night many years ago I was on patrol and observed a vehicle blow through a red light at a major intersection. There had been plenty of time to stop, yet the vehicle had not even slowed down. I stopped the car and asked the young female driver why she had done that. The girl told me she had just had her brakes repaired, it had been very expensive, and she DIDN&#8217;T WANT TO WEAR THEM DOWN! Usually I give people a pass if I haven&#8217;t heard their excuse before, but in this case she got the ticket. &#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;. _Submitted by&#8230; Dave Hoffman, Sergeant, Naperville IL PD_ 

  2. #### I stopped a car in a rural area of our county for going 80 MPH in a 55 MPH zone. The driver explained that he had a bee flying around his head so he sped up to 80, hoping that the bee couldn&#8217;t fly that fast and would not be able to fly out of the back seat area to get at him&#8230;.._Submitted by&#8230;Gary Lenon, Mecosta County Sheriff Department, Michigan._

Amazing what we say to avoid blame not only as children but as adults as well. <!--more-->The author mentions that people that come into her office with grievances against others have any number of excuses why they are the victim&#8230;and only the victim in the scenario such as:

  * &#8220;Everyone picks on me&#8221;
  * &#8220;My mom [or dad] is so unreasonable [or crazy etc.]&#8221;
  * &#8220;They don&#8217;t know what my life is like.&#8221;

When the friends and family come in&#8230;she hears a different story:

  * &#8220;She won&#8217;t get a job, she&#8217;s 27&#8230;&#8221;
  * &#8220;She acts so deprived&#8230;&#8221;
  * &#8220;He expects us to bail him out every time he get in trouble.&#8221;

At what point do you have to get to in order to take responsibility for your own life and stop blaming other people for your situation, your losses, your anger, your hurt, your &#8216;righteous&#8217; indignation. Sure what do I know? More than you probably think or assume unless you&#8217;ve read my previous blog posts. I&#8217;ve been a victim, I&#8217;ve been angry, I&#8217;ve experienced loss and righteous indignation, embarrassment&#8230;you name it I probably have a story for you. But I rarely if ever use those experiences as excuses for what I do, what I say or who I am.

I was horrifically taunted in elementary school. It&#8217;d be easy for me to say, it was all the mean girls faults, I was the total victim in the situation. But lets be honest, sure most of the taunting was probably unprovoked but I could definitely be a little shit and that probably didn&#8217;t &#8216;endear&#8217; me to them either. When people accuse me or you or anyone of things, our natural instinct is defensiveness, deflect the blame, the accusation, the insult&#8211;if we can&#8217;t deflect it onto someone else, we excuse it with our own &#8216;mental&#8217; justifications. No one likes to be &#8216;whacked&#8217; in the side of the head, it doesn&#8217;t feel good, but sometimes it&#8217;s needed.

When relationships break down&#8230;whose fault is it? Most of the time its both parties fault. And I&#8217;m not talking about dangerous situations, in that case get yourself the hell out of that situation! I&#8217;m talking about a breakdown in a relationship whether it be family or otherwise that&#8217;s non-life threatening. Why can&#8217;t we take responsibility for our part in the breakdown? The worst lie in break-up history is &#8220;It&#8217;s not you, it&#8217;s me&#8221;&#8211;really? really? You expect them to buy that? Obviously it WAS you, not that you are at fault necessarily but there was something about you the person couldn&#8217;t cope with. Its an even worse lie if that person that breaks up with you hooks up shortly thereafter&#8230;especially if they&#8217;ve told you they want &#8216;me&#8217; time&#8230;&#8217;too think&#8217;. Yep&#8230;I&#8217;ve been there, more than once on the receiving end of that lie. They do it to &#8216;spare your feelings&#8217;&#8211;no, honestly, it&#8217;s worse, you&#8217;re not sparing my feelings&#8230;you are pissing me off through your lack of honesty and balls.

What about family&#8230;ahhh family, we have any number of grudges we hold against family, its our &#8216;blood right&#8217; to be all kinds of pist at family and still &#8230;well, be family. What about our part in those grudges? Again are we simply the victim, nothing else? Could our grudge be remedied simply by letting go of our &#8216;injustice&#8217; and talking to said family member(s) or is it more comfortable to sit in a haze of miscommunication and anger? I&#8217;ve never enjoyed relationship or family drama and I try to steer clear of it if it happens. To entertain it only encourages more drama and it just gets tiring.

> For each one should carry his own load -NIV, Galatians 6:5

Who answers for your grudge in the end? You do. When asked why you lived your life the way you did, what excuse can you offer? None. Why? Because in the end you are responsible for your own life and what comes of it regardless of the family, friends or romantic relationships that enter and leave your life. I&#8217;m sorry if you have this problem or that problem&#8230;but there comes a point where enough time has passed that its no longer your mom&#8217;s fault, your dad&#8217;s fault, your ex-boyfriends fault&#8230;.your dogs fault, the policeman&#8217;s fault. You let the problem lie for so many years. You were the one who chose to ignore it. You were the one who chose to let the relationship falter, for whatever reason, you let it go. You were the one who decided not to communicate, make your point, then listen to what they had to say. Sounds like you should take as much responsibility as the other party involved. And if you decide not too, then you should never be able to use that situation as an &#8216;excuse&#8217; for the crap in your life.

> &#8220;We&#8217;re all accountable for ourselves. Life is not a free ride. Life doesn&#8217;t owe you anything. It is a healthy day in a teen&#8217;s life when he or she realizes no one ever has to do anything for you &#8211;any help you are given is a gift. It is tragic when that day of realization hit people in their 20&#8217;s, 30&#8217;s, 40&#8217;s&#8230;later&#8221; -Devotional book

I am attempting more and more in my life to make sure I take responsibility for my decisions, actions, words. To apologize when I need to apologize. To stand up for myself when I need to stand up for myself. To help others when I see or find out they need help. To communicate rather than shut people out. To &#8216;let go&#8217; when I need to let go even though it&#8217;s difficult. To absorb critique or criticism without getting down on myself, ripping their heads off or making excuses.

Life doesn&#8217;t owe me anything&#8230;it is an awesome gift that more than once and I&#8217;m sure many times in the future will punch me in the gut. I wish I could come up with a snazzy inspirational quote to end on&#8230;but what do you say to people who&#8217;ve had the crap beaten out of them by life after you ask them to take their fair share of responsibility for it? Nothing really.

&nbsp;

 [1]: http://madtbone.tripod.com/kids_excuses.htm
 [2]: http://www.funnysnaps.com/iexcuse.html
