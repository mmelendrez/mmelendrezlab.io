---
title: The ominous projector screen of ‘life’
author: Mel
type: post
date: 2012-06-25T15:44:01+00:00
url: /2012/06/25/projector-screen-of-life/
al2fb_facebook_link_id:
  - 43807988_807904296589
al2fb_facebook_link_time:
  - 2012-06-25T15:44:06+00:00
al2fb_facebook_link_picture:
  - post=http://www.melaniemelendrez.com/?al2fb_image=1
categories:
  - Devotion
tags:
  - God
  - life
  - secrets

---
Devotional Blog:

&#8220;Lights Flashing&#8221;, 6/21/2012, 1 Corinthians 4:5

Ok, back to the book after quite the hiatus.

> When the Lord comes, he will bring our deepest secrets to light and will reveal our private motives&#8230; ~1 Corinthians 4:5

Well now isn&#8217;t that a scary thought? How many secrets does one carry throughout life? How many thoughts do we think would we be ashamed of if someone were to actually crawl into our head and listen to. Thankfully people can&#8217;t crawl into each others heads and truly hear the thoughts that roll through them.

Funny thing is, growing up this verse made us wary of the &#8216;big&#8217; secrets&#8230;infidelity, sex before marriage, stealing, physically hurting someone etc. Our pastors would often times use the image of a huge projector screen showing all our sins and evil thoughts to the world for all our family and friends to see. Oh the embarrassment! Oh the judgement! Oh the pain we would cause ourselves and other people with our hidden sins that haven&#8217;t been brought to light and forgiven. I used to live in fear as a kid, paranoia even that when I died God would put me on a stage and broadcast my entire &#8216;evil&#8217; life of every little thing I&#8217;d ever done that wasn&#8217;t 100% &#8216;saintly&#8217;, down to beating the crap out of a stuffed animal because I was angry. Yes, as a child I occasionally beat the crap out of dolls and stuffed animals out of frustration. I ripped pages out of my journals and threw tantrums in my room out of sight and earshot of anyone. Much of my anger and frustration growing up I kept inside, in fact all of it I kept inside. And while these &#8216;tantrums&#8217; and stuffed animal beatings seem harmless enough at first sight, my thoughts got darker as I grew up.

What about the little &#8216;secrets&#8217; the thoughts no one hears about, the thoughts that will never be voiced but are nonetheless there&#8230; the dark thoughts.<!--more-->

Have you fantasized about the elaborate death of someone; be them friend, foe, family or complete stranger?

Have you fantasized about your own death?

Have you &#8216;given up&#8217; on someone in your life and they don&#8217;t know it because you&#8217;ve chosen not to tell them?

How many times have you thought people truly close to you were idiots but telling them as much would hurt the relationship so you say nothing or lie&#8230;a white lie of course.

Ooohhh, the white lie&#8230;to spare their feelings, to give them false hope, to make them believe you care when you don&#8217;t, to pass the blame to someone else so you don&#8217;t have to deal with it, to avoid confrontation, because to say nothing at all would cause them to harp on you til you blurt out the truth in anger potentially wrecking a relationship you may have truly wanted but was not in a place to deal with at that moment&#8230;

What if people could see the truth behind our white lies? Would you be embarrassed? Ashamed? Defiant? Angry?

If all of the dark thoughts in my life and white lies came to light people may think I am a truly f%$ked up person. And they probably aren&#8217;t all that far off. We can all be pretty messed up in our own way. Everyone has their &#8216;achilles heel&#8217; in life. Many of us are full of false contentment trying desperately to believe we are happy when perhaps we are not, and we don&#8217;t know how to fix it. So we continue with the status quo, we pray to God for guidance and we continue on.

I hesitate to follow the advice of some Christians that suggest we &#8216;arrest&#8217; our minds or ask God to control our minds so as to not have evil, impure, vicious or dark thoughts. Do I necessarily want to live in the midst of dark or depressing thoughts? Not really but simply praying for God to take said thought away doesn&#8217;t deal with the root of the thought to begin with, meaning it can and will come back.

So how do you fix it? How do you live a happy life free of the white lies and secrets? In my church, they have a very strong belief in openly confessing, getting prayer, being cleansed etc. The problem with that and often times the aftermath of that in my experience has been lingering judgment that forever taints you in the eyes of the &#8216;upstanding&#8217; individuals in the church and you are systematically and slowly ostracized. Penalized for your honesty, for your imperfect humanity. No wonder the &#8216;white lie&#8217; is such an important aspect of human interaction.

I attempt to purge my mind of lies, secrets and dark thoughts. Firstly, I&#8217;m not very good at lying so I don&#8217;t have to worry about that so much. The white lie? I try as much as possible to avoid it as I don&#8217;t think it does the person much good to sugar coat reality or blow sunshine when there really is none. &#8220;Well that&#8217;s morbid&#8230;&#8221;, yes, yes it is&#8211;but how is someone to deal with hard realities in their life if they are never thrust into the face of them and forced to fix them? And here&#8217;s where I &#8216;catch&#8217; myself&#8230;because while my white lying might be at a minimum&#8230;I am totally an enabler.

I am totally guilty of enabling situations&#8230;not by the use of white lies but in other ways. And I think it demonstrates my lack of trust in God in some ways. And that&#8217;s a hard realization for me to come to. I&#8217;ve always said and believed that I had total trust in God and his provision; but I think quite frankly, perhaps I trust him in most cases with my life and some cases with the lives of those I love. Terrible huh? Truly. And it&#8217;s not that I think I have a better solution&#8230;but its very difficult for me to reconcile the reality of various situations and the faith involved in getting them fixed without physical intervention on my part&#8230;see [previous blog][1].

Secrets and dark thoughts&#8230;well I don&#8217;t have many secrets persay, I&#8217;m pretty open if asked a question. Problems usually arise when the person asking the question doesn&#8217;t want to hear my answer but asks anyway. That&#8217;s bitten me in the @$$ before. I&#8217;ve lost friends because they asked me a question they weren&#8217;t ready to hear the answer to and they held it against me.

Dark thoughts&#8230;sure I have plenty and I regularly have to assess why I am having them and how to dispel them. Everyone is different. Sometimes I have to go outside in the sun and breathe the fresh air, sometimes I have to take a nap, sometimes I have to put on a comedy, sometimes I have to read or occupy my hands to have something else to focus on. And I do pray, but I realize prayer while it may assist in releasing the thought&#8230;until I deal with the root of that thought, I will always be plagued with the dark and twisted aspects of my mind.

And I don&#8217;t mean to end this blog with the twisted morbid mind of Mel. Many who know me wouldn&#8217;t exactly characterize me as a twisted, morbid unhappy person. Quite the contrary, I am a fairly stable optimistic person. And that&#8217;s precisely why you need to know all this. Because even stable, well-adjusted, optimistic people battle inner demons at times. That&#8217;s what allows us to be who we are. Only when dealt with are those demons or dark thoughts released and the cloud lifts.

And they don&#8217;t all have to be dealt with in a crowd. God knows your heart, deal with it on that level. If you need the advice or prayer of someone else then seek it out. Remove the cobwebs, light the dark corners of your mind&#8230;

Don&#8217;t do it out of &#8216;fear&#8217; of the potential ominous projector screen that will flash to everyone at the end of your life. Do it because it will make you a stronger individual, do it because, over and over you will give it to God thinking you&#8217;ve rid yourself of it and it&#8217;ll come back, each time the root becoming more and more clear. Do it because one day you&#8217;ll give it to God, you&#8217;ll let it go and this time, it won&#8217;t come back.

 [1]: http://www.melaniemelendrez.com/2012/06/15/my-definition-of-good-must-be-broken/ "My definition of good must be broken…"
