---
title: 'A voice: repost from Sep 20, 2008'
author: Mel
type: post
date: 2011-08-18T08:24:03+00:00
url: /2011/08/18/a-voice-repost-from-sep-20-2008/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"7eddb45a9d";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";s:17:"wordbook_new_post";s:1:"1";}'
categories:
  - Poetry

---
Current mood: sad<article>A voice</p> 

Give me a voice
  
Expressions of silent sadness
  
Over and over
  
And over again
  
Heartwrenching
  
Beliefwrenching
  
Asking why, asking how…
  
Only watching, unable to move
  
Powerless
  
Silent sadness
  
My voice lost

For a friend: Dec. 31, 1985-Sept. 18, 2008</article> 

<p style="text-align: left;">
  <p>
    &nbsp;
  </p>
