---
title: when life follows you
author: Mel
type: post
date: 2017-11-24T16:38:06+00:00
url: /2017/11/24/when-life-follows-you/
categories:
  - Life or something like it

---
Who the hell voluntarily goes out at night looking for alligators? This girl. Well, this girl along with 15 other students and full disclosure they were Caimans &#8211; so smaller, but still.

![](http://www.melaniemelendrez.com/wp-content/uploads/2017/11/Picture1.png)

I was 21, exploring Ecuador and our guides took us out in boats at night to find Caimans. <!--more-->

![](http://www.melaniemelendrez.com/wp-content/uploads/2017/11/123_509839835159_927_n.jpg)

The draw wasn&#8217;t the caimans for me, it was the darkness. Let me qualify that so I don&#8217;t sound morbid, it was the light emitted from the stars and the quiet of the river. It was chilly, we were in Ecuador, in the northern part of the Amazon basin and it wasn&#8217;t insanely humid at that time of night. There were only 3 of us in our boat so we could spread out and lay across the benches. Odd that such a night would be what I constantly go back to in my mind, the slight breeze was thick with the smell of what I can only describe as a mix of fresh cut grass and warm guava fruit. Our boat was not as motivated to track down caimans&#8230;we just drifted, our guide lazily scanning the water&#8217;s edge for eyes and snouts. After awhile he just turned off his light and it was dark and silent. We floated into a part of the river that widened, not quite to the size of a lake, but a substantial expanse of slow moving water, and there it was, out of the cover of jungle, the milky way in all it&#8217;s brilliance. I&#8217;d seen stars before but not from such a secluded site devoid of all light. We drifted in a circle away from the other boats, the voices of our friends still attempting to find caimans getting smaller and smaller. It was the first time in my life peace actually permeated my entire being. One of my friends leaned over and asked me what I was thinking. &#8220;Nothing&#8221;, I replied, a bit surprised, because it was true.

I&#8217;d get to see the Milky Way again&#8230;5 years later on a sampling trip in Yellowstone National Park near Octopus spring. A bit colder, but no less spectacular.

* * *

Seventeen years under the fluorescent lights of my office I am writing a grant. Such is the life of a scientist whether you work for the government, as I did for several years, or in academia &#8211; much of your life constitutes grant writing. The solitude here is different, not unwelcome, just different. My routine is set, my goals pretty straight forward and I get a ping notice from LinkedIn &#8211; it&#8217;s him, the individual that made me a statistic that allows me to relate to every other woman who told her story via [#metoo][1]. Seriously? Did not blocking you on myspace, hotmail, school email, facebook and gmail not resonate with you?! Perhaps he thought enough time had passed? &#8220;Enough time&#8221; will never pass. I ignore the message and block the profile. Did it transport me to a swirling vortex of emotional flashbacks? No, the kid was an asshole that took advantage of a naive college student and didn&#8217;t take no for an answer.  Over the years he tried to reach out, I never responded and just kept blocking. Crazy no matter what turns your life takes and how far away you can get from something&#8230;how in this day and age it can pop up and follow you, nearly two decades later. I shake my mind free and turn back to the comfort afforded by the fluorescent lights, back to my grant writing.

* * *

Now I find my mind transporting to graduate school in Montana, we had been dating a couple months and I asked, my now husband, &#8220;Hey, you want to trek with my friends across the Crazy mountains over 4th of July weekend?&#8221; He said, absolutely and off we went. We came up over a pass and camped near the top, just on the other side. The clouds were rolling over the pass, up into our campsite then down into the valley below. It was cold but I didn&#8217;t want to put on my coat, it was refreshing, camping on a mountain looking down at the valley in the cold. Clouds feel like smooth beads brushing against your face when they pass by.

In that moment I found the same solitude and peace I&#8217;d felt floating on that river in Ecuador. At any given momen![](http://www.melaniemelendrez.com/wp-content/uploads/2017/11/1005927_10100162754361189_124660799_n.jpg)t I have 100&#8217;s of things coursing through my mind; a quiet mind is not something I am good at achieving. But that day, on that mountain I thought of little else except what clouds &#8216;feel&#8217; like and that I was going to marry that man taking pictures of the valley below.

&nbsp;

&nbsp;

* * *

While some moments in my life have not been awesome, the majority have been amazing. Moments where I can tangibly feel &#8216;existence&#8217; permeating my body, when my mind finally shuts down, takes a break and allows me to breathe.

It doesn&#8217;t bother me when life&#8217;s experiences good or bad follow me. Reminds me of my resolve, my weaknesses and my strengths. I am thankful for this life, the terrible and the beautiful.

Just a bit of reflection on  this Thanksgiving season.

 [1]: http://twitter.com/hashtag/metoo
