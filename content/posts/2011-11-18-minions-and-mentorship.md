---
title: Minions and Mentorship…
author: Mel
type: post
date: 2011-11-18T02:17:27+00:00
url: /2011/11/18/minions-and-mentorship/
categories:
  - Devotion
tags:
  - God
  - inspiration
  - leadership
  - marriage
  - mentorship
  - passion

---
Devotional Blog:

Topic: &#8220;Mentor Me&#8221;, 11/16/2011, Proverbs 9:9-12

I don&#8217;t know how many times since starting my postdoc that I&#8217;ve desired minions. Cohorts in my pursuit of viral evolution and ecology. Kindred spirits clad in lab coats and laced with the smell of phenol chloroform. Ok&#8230;really I just need people to help me with lab work that I like doing but I have less and less time to do as analysis and writing alone consumes me sometimes. But in return I&#8217;d like to mentor.

I mentored a very motivated undergraduate student while in grad school and she turned out quite apt and successful so I figure I did something right&#8230;and I tried at all costs to minimize her interactions with my boss who also happened to be her academic mentor. He had the uncanny ability to make many women (including myself at times) who worked in his lab want to staple things to his head and leave in a hail of frustrated cuss words and gunfire.

I mentioned in my last post that many topics in this book seem to be on repeat and I realize I posted a blog earlier on &#8216;leadership&#8217; but I think this is different&#8230;the idea of leadership and mentorship. Ideally they should go hand in hand and I aspire to that but many times they don&#8217;t. There are many leaders that are terrible mentors and mentors that if you put them in charge of something wouldn&#8217;t know left from right practically speaking&#8211;rather they are gurus of &#8216;sense&#8217;. They are often the ones that you want to quote a lot because they inspire you, even if practically speaking they may not get a whole lot done.

Leaders you follow, mentors you quote. And if you have someone that is both, then  you get the great leaders of our time. But they all had to start somewhere.<!--more-->

I&#8217;ve heard about many times of mentors or &#8216;life coaches&#8217; as I&#8217;ve heard them called. People you pay to motivate you and help you satisfy goals you&#8217;ve set for your appearance, career, life etc. Me, best I can do is in my field. I love to teach despite being jaded quite a bit by the &#8216;academic machine&#8217; and &#8216;politics&#8217; that I&#8217;ve seen at work in it. And I feel in my graduate work I had clear divisions between my mentors and the leader.

A good mentor in science?

  1. Knowledgeable and when he/she doesn&#8217;t know admits it and goes and figures it out.
  2. Inspiring and encouraging: some of us take &#8216;longer to develop&#8217; than others that doesn&#8217;t make us any less passionate about our field. The more you beat someone down the less likely in most cases they will be to succeed.
  3. Organized.
  4. Dependable: when they set a meeting they come and aren&#8217;t late and if they are have the courtesy of letting someone know.
  5. Is human. I think too often we put mentors and leaders on a pedestal forgetting they are human and when we treat them as such they begin to lose their own grip of their humanity, their fallibility&#8211;creating the &#8216;don&#8217;t challenge me&#8217; you&#8217;re just a child a peasant in this field and I am the all knowing, been in this field however long guru.
  6. Does not mock and uses genuine communication: There&#8217;s a difference between genuine honesty and a mocking patronizing honesty.
  7. Gives a shit. The careers of their students should be just as important as their own.
  8. Takes risks, admits when they are wrong and doesn&#8217;t dwell on it but rather works to fix it. Likewise when their students are not &#8216;lighting up&#8217; the research, they don&#8217;t dwell on their mistakes, they correct them and move on.

Quite the list I suppose and I think I have some of these qualities but I know they need to be developed more before I should be blessed with minions that I get to mess up in my own special way. Its an amusing picture to have in my head. When I was little, the word minions always conjured up thoughts of evil geniuses with their cohort of little people laughing evil-ly with them and assisting their &#8216;mad&#8217; genius in his experiments usually to the detriment of the world. And here I am saying I want minions. But I solemnly swear to use my powers and minions only for the betterment of humanity&#8211;only for good rather than evil.  I don&#8217;t particularly figure I have much to worry about. Although, I have a friend that would say I&#8217;m evil just by my nature as a woman and well as much as I adore them, [they can suck it and read my blog on what I think about that][1]&#8230; And I&#8217;m a far cry from a genius. So Evil Genius I don&#8217;t see going on my resume any time soon.

Ok&#8230;back to the topic at hand&#8230;and the devotional book. The author talks about a &#8216;womanly mentor&#8217; and what she should be like and when you look for a mentor what should you look for. She takes her criteria from Titus 2:3-5.

Reverent: holy, sacred character that is fitting to a person consecrated to God

  * Doh! Ok I&#8217;m working on this, I really am. I&#8217;m doing devotionals from a book that sometimes makes me puke rainbow butterflies. I&#8217;m reading my Bible more. I am endeavouring to pray more. I am trying to remember my faith in all things and learn more about my beliefs, faith and God. So this is &#8216;IP&#8217;&#8211;in progress.

Not a slanderer: not defaming or maligning

  * I&#8217;ve had enough mean things said about me in my life that I don&#8217;t feel the need to add to the cesspool of human verbal excrement. I did go through a gossip phase but got that swiftly kicked out of me.

Not addicted: not under bondage or controlled by anything

  * I might have mentioned in a previous blog that at sometimes during the pressure of getting my degree it took a bottle&#8230;or two of wine JUST to calm me enough to sleep and I built up such a tolerance that I was only buzzed by the end of two bottles, not even drunk. When I realized this I cut wine out cold turkey until I had myself under control and haven&#8217;t had a problem with it since. But I do find myself on occasion desiring its drowsing affects&#8230;it became a security blanket for me. I see no problem with drinking and enjoying a glass of wine or other &#8216;tasty&#8217; beverage, but when it becomes a crutch or escape rather than occasional enjoyment&#8230;its an addiction.

Loves husband and children: sacrificing for the good of loved ones.

  * Not married yet nor do I have kids but I can say I love my fiance with every fiber within me and would do absolutely anything for him. How do I know? Because I would give up my career to be with him and for anyone that knows me that is something I would&#8217;ve never done. But given the awesome man he is, he&#8217;d never ask me to do so. But I would, I would in a heartbeat&#8230;God maybe be the center and soul in my life, but Tyghe is my heartbeat.

Self-controlled: self-disciplined

  * I was discouraged and minorly &#8216;banned&#8217; if you will from teaching kids at my church here because I live with my fiance. Even though we&#8217;ve never had sex and don&#8217;t plan to til we are married, they apparently couldn&#8217;t &#8216;figure&#8217; that a man and woman could live together without that. Well it&#8217;s called SELF CONTROL and respect for what each other wants out of our relationship and sex is not the basis of our relationship. I won&#8217;t get into how Tyghe and I came to this decision, that&#8217;s for another blog, but fact is we did come to it. But I am not allowed in ministry til we are married. Unfortunately a double standard exists where he can be involved but I cannot&#8230;but not all churches treat men and women equally&#8230;again for another blog.

Pure: discreet and chaste

  * I am by no means pure, discreet or chaste. In fact I laughed out loud when I came to this one. Rather I&#8217;m quite blunt although I do try to consider others feelings in what I say I&#8217;m more of a &#8216;louder&#8217; personality sometimes. And chaste? Well I have plenty in my past that would drop kick me out of this category.

Busy at : working to keep your home a priority in your heart

  * I&#8217;ll never been Donna Reed or Mrs. Cleaver&#8230;I just never will be. But my family is my priority in everything. I don&#8217;t think of this in the traditional sense of playing the &#8216;good housewife&#8217;. When I think housewife my stomach turns and I have visions of terrible reality TV&#8230;the &#8216;real&#8217; housewives (induce vomiting now). But my family will always be my priority and my life, I will always support them in anyway I can and be there for them as much as I can.

Kind: Sympathetic and helpful

  * DOH! Can I take back the &#8216;suck it&#8217; remark I made earlier to my &#8216;friend&#8217; that thinks women are evil? I consider myself fairly sympathetic although I have been thought of as a callous bitch before, but I can do very little about the opinions of others and hopefully more people think of me as more sympathetic than callous. And I try to be as helpful as I can, but I have my limits and strengths. I know myself and I won&#8217;t volunteer if I think I&#8217;ll be impeding something more than helping something.

Subject to husband: choosing to rank yourself under your leadership.

  * Now this is interesting. I think of some past relationships where I know I would&#8217;ve considered marrying the guy. Could I have done this? Ya perhaps but I would&#8217;ve been one majorly frustrated and pissed off wife a lot and I don&#8217;t think that&#8217;s what&#8217;s supposed to be. Now I&#8217;m not married yet so I haven&#8217;t entered that &#8216;phase&#8217; of being &#8216;subject&#8217; to my husband persay. But I see this differently as well. I have no problem being &#8216;subject&#8217; to my husband and making him a priority because he should make me a priority. If we are thinking of each other more than ourselves then our decisions or his decisions will be for &#8216;us&#8217; not him. In the movie, my big fat greek wedding, the mother says&#8230;&#8217;the man may be the head of the house, but the woman is the neck and she can turn the head anyway she pleases&#8217;. And really I don&#8217;t agree with this. There should be no &#8216;force&#8217; in a marriage, its a partnership. Women should not be treated as frail creatures incapable of strong thoughts and desires and ambition&#8211;that should be patronized and &#8216;taken care of&#8217;. Nor should men be treated as ogres of power out to dominion-ize their women that undermine and ignore their intelligence. Again this is for a different blog, but it makes good fodder for conversation amongst those who believe in marriage and man/woman roles and those that do not believe in a more traditional sense of marriage.

Wow&#8230;so I look at these traits for being a &#8216;Godly&#8217; mentor and man&#8230;I kind of suck at this point in my life. But its valuable to evaluate my life according to what the Bible says it should be and see where I may fall short and why.

One of the things I do have is quite a bit of &#8216;specialized&#8217; life experience which I know and hope one day may help other women. And I&#8217;m sure God will hone me into something he can use and actually I think he still uses me, in my imperfect state probably without me realizing it. He probably uses my cock ups as much as my successes&#8230;and I hope its all for good in the end.

Do I want to be a leader or a mentor? I&#8217;d love to be both but if I can&#8217;t be both I think I&#8217;d want to be a mentor. I want to inspire people&#8230;even if its from the sidelines.

 [1]: http://www.melaniemelendrez.com/2011/11/07/women-are/ "Women are…"
