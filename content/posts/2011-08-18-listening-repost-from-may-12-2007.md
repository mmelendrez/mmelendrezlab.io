---
title: 'Listening: repost from May 12, 2007'
author: Mel
type: post
date: 2011-08-18T08:57:05+00:00
url: /2011/08/18/listening-repost-from-may-12-2007/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"7eddb45a9d";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";s:17:"wordbook_new_post";s:1:"1";}'
categories:
  - Poetry

---
#### <span style="text-decoration: underline;">Listening&#8230;</span>

<h4 align="center">
  Listen to me
</h4>

<h4 align="center">
  I&#8217;m falling
</h4>

<h4 align="center">
  Away from the voice inside
</h4>

<h4 align="center">
  Wishing to abandon my ridiculous nature,
</h4>

<h4 align="center">
  Wishing to go after something
</h4>

<h4 align="center">
  I can actually have
</h4>

#### Angry at my own nature

#### Frustrated since the fault is mine

#### Busy with the life that&#8217;s around me

#### Forgetting at times what&#8217;s important to find

<h4 align="center">
  Listen to me
</h4>

<h4 align="center">
  I&#8217;m smiling
</h4>

<h4 align="center">
  Amused by things I cannot control
</h4>

<h4 align="center">
  When all you can do is laugh
</h4>

<h4 align="center">
  Mixing sadness, hurt, and relief in your soul
</h4>

#### Focused on the tunnel in front of me

#### Desperately trying to stay on the path

#### Believing my head

#### Doubting my heart

#### Saying nothing, seeing the end

<h4 align="center">
  Listen to me
</h4>

<h4 align="center">
  I&#8217;m quiet
</h4>

<h4 align="center">
  My mind rebelling inside
</h4>

<h4 align="center">
  Not wishing to upset the balance
</h4>

<h4 align="center">
  Not wishing anymore to fight
</h4>

#### Accepting what I cannot have.
