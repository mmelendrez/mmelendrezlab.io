---
title: The sky is crying
author: Mel
type: post
date: 2011-08-18T09:09:37+00:00
url: /2011/08/18/the-sky-is-crying/
wordbooker_options:
  - 'a:11:{s:18:"wordbook_noncename";s:10:"7eddb45a9d";s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";s:17:"wordbook_new_post";s:1:"1";}'
categories:
  - Poetry

---
<h4 align="center">
  <em><strong>The Sky is Crying</strong></em>
</h4>

<h4 align="center">
  <em>The sky is crying</em>
</h4>

<h4 align="center">
  <em>And I dont know why.</em>
</h4>

<h4 align="center">
  <em>Washing the heat,</em>
</h4>

<h4 align="center">
  <em>Washing the dust</em>
</h4>

<h4 align="center">
  <em>From a predetermined life.</em>
</h4>

<h4 align="center">
  <em> </em>
</h4>

<h4 align="center">
  <em>Rivulets at my ankles,</em>
</h4>

<h4 align="center">
  <em>Wind in my face,</em>
</h4>

<h4 align="center">
  <em>Uncertainty before me,</em>
</h4>

<h4 align="center">
  <em>Uncertainty within me</em>
</h4>

<h4 align="center">
  <em>Just trying to keep pace.</em>
</h4>

<h4 align="center">
  <em> </em>
</h4>

<h4 align="center">
  <em>I feel the lightening</em>
</h4>

<h4 align="center">
  <em>I feel the thunder</em>
</h4>

<h4 align="center">
  <em>I feel the frustration</em>
</h4>

<h4 align="center">
  <em>I feel nothing.</em>
</h4>

<h4 align="center">
  <em> </em>
</h4>

<h4 align="center">
  <em>The sky is crying</em>
</h4>

<h4 align="center">
  <em>And I do know why</em>
</h4>

<h4 align="center">
  <em>Washing the heat</em>
</h4>

<h4 align="center">
  <em>Washing the dust</em>
</h4>

<h4 style="text-align: center;">
  <em>From a predetermined life.</em>
</h4>
