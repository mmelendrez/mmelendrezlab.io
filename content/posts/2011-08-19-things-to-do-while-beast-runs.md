---
title: Things to do while BEAST runs
author: Mel
type: post
date: 2011-08-19T03:18:30+00:00
url: /2011/08/19/things-to-do-while-beast-runs/
wordbooker_options:
  - 'a:9:{s:18:"wordbook_noncename";s:10:"08439e6799";s:18:"wordbook_page_post";s:4:"-100";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_default_author";s:1:"1";s:23:"wordbook_extract_length";s:3:"256";s:19:"wordbook_actionlink";s:3:"300";s:26:"wordbooker_publish_default";s:2:"on";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";}'
categories:
  - I will do Science to it!
tags:
  - BEAST
  - GenBank
  - phylogenetics
  - software

---
So in the world of molecular evolution one of the cu-de-gras-de-analysis programs would have to be BEAST. A power-packed bayesian analysis software that makes phylogenetic trees, calculates the time to the most recent common ancestor (tMRCA) and substitution rates, geographic partitioning, can handle copious amounts of data and pretty much squeezes blood from a turnip&#8230;walks on water&#8230;heals your mother, in short, it&#8217;s cool.

Sound awesome? It is. For a more technical in depth discussion and introduction to BEAST software I suggest reading the [Wiki][1] and attacking the tutorials with full force as well as reading some awesome books on phylogenetic inference such as [The Phylogenetic Handbook][2]. If you are super impatient and channeling your inner terrible twos about phylogenetic analysis then read [Phylogenetic Trees Made Easy][3]. It has a nice introduction and literal button for button how tos on different software packages including Bayesian ones. When you&#8217;ve finished your tantrum, enter the adult world and read [Felsenstein][4] or the phylogenetic handbook mentioned above. Now that you&#8217;ve been introduced to phylogenetic inference and genetic analysis with forays into evolution over time&#8230;jump into BEAST. Although the BEAST wiki and [manual][5] are still navigate-able without that background but you&#8217;ll be scratching your head a bit and heading to google for answers.

<!--more-->Now the caveats to BEAST are that if you have a lot of sequence data and a cheap low running computer&#8230;perhaps you&#8217;ll get output from a run of 100 million generations/iterations in oh say&#8230;when Christ comes again. The default setting in BEAST is 10 million generations which on my lovely military issued computer from the dark ages takes a good 2-4 hours to run depending on the dataset&#8230;2 hours for 50 sequences of length 1701 nucleotides with a strict clock and little variation. Unfortunately the ESS values were such an angry red, it was as if they were cursing me from the bayesian matrix beyond. Ok&#8230;up to 20 million iterations&#8230;10 hrs later still many angry red ESS values and 1 or two mellow yellow warnings&#8230;

I checked parameters with two different wonderful individuals with BEAST experience, parameters were tweaked, prior constraints applied or were fine: further suggestions? Run longer&#8230;ok! 40 million iterations, 30 hrs later&#8211;still correlated values more yellow ESS values and a couple red. I gave up for the time being and attended a conference, where it was suggested that 100 million should be the standard for a BEAST run&#8230;I noted it and was ready to rock 100 million iterations.

So like any good scientist I crave data to make my analysis run better and more robustly&#8230;enter [GenBank Influenza Resource Database][6]! Yes, currently I am working on flu. With some choosing, hemming and hawing, screening and random picking over the course of a year and such I was able to boost my sequences to an informative 472 representing isolates from the globe covering just about every month of the 2009 influenza pandemic; which will put my sequences in better perspective. Now traditionally, I can make a tree of up to 500 sequences with not too much problem using Maximum likelihood approaches/software, so I figured BEAST would be fine. What I didn&#8217;t bet on was my slow ass PC computer.

So I figured I&#8217;d be smart about this and run the analysis on my Linux box which may have the latest Ubuntu on it (11.04) the computer itself is from B.C., I could&#8217;ve sworn the [Babylonian map of the world][7] was etched into it&#8230;

Estimated time to completion? 4.63 hours/million states x 100 million states = 463 hours = 19 days!!! DOH! Computing power FAIL!!!

Old hardware aside, BEAST analysis takes a long time to run, and until I obtain favor from the grant gods these computers are what I have to work with until I find a free cluster to run the analysis on&#8230;see #22 below.

So, lets run down a list of how to keep oneself occupied when not working while BEAST runs:

  1. Blog about BEAST.
  2. Maximize your terminal screen and watch BEAST run&#8230;iteration 4238000&#8230;.iteration 4239000, 4240000, 4241000&#8230;it&#8217;s like a matrix lullaby lulling you to sleep&#8230;but not really.
  3. Go blind watching BEAST run
  4. Go get coffee&#8230;at 7:30a, 9am, 9:30am, 10:30 am, 11am
  5. Go get lunch&#8230;at 11am
  6. Actually work&#8230;
  7. Go get more coffee
  8. Hunt the [BEAST user group][8] for why BEAST is so damn slow on your computer
  9. Realize its not BEAST its your damn computer
 10. Tap your IV of coffee, make sure it&#8217;s still flowing.
 11. Pull ones hair out
 12. Then put it back together
 13. Finish work, go 
 14. Go for a run
 15. Remember you live in hot humid Thailand&#8230;don&#8217;t go for a run
 16. Do pushups everytime your BEAST logs to screen, something anything&#8230;
 17. See how many pushups you can do between logs to screen
 18. Drink a glass of wine&#8230;watch BEAST run
 19. Switch to vodka&#8230;watch BEAST run
 20. Switch to whisky and take a shot everytime BEAST logs to screen
 21. Pass out with status lines running through your head
 22. Get to work decide to hunt down free clusters to run BEAST on remotely&#8230;

I&#8217;m currently on #22

Awesome software&#8230;but you need the power to run it&#8230;currently searching for &#8216;more power&#8217;! And yes, for those of you who&#8217;ve ever watched [Home Improvement][9] as kids that was a [Tim the tool man Taylor][10] reference.

And for those of you gravely concerned I have no life and never work given the above list I assure you this blog is tongue in cheek. I have an amazing life quite productive in lab work, manuscript writing, data analysis, Asia exploration, wine drinking, amusing my fiance with my antics&#8230;and BEAST analysis.

Cheers.

 [1]: http://beast.bio.ed.ac.uk/Main_Page
 [2]: http://www.amazon.com/Phylogenetic-Handbook-Practical-Approach-Phylogeny/dp/052180390X
 [3]: http://www.amazon.com/Phylogenetic-Trees-Made-Easy-How/dp/0878933123
 [4]: http://www.google.com/products/catalog?q=Inferring+phylogenies&oe=utf-8&rls=org.mozilla:en-US:official&client=firefox-a&um=1&ie=UTF-8&tbm=shop&cid=10167509640074530723&sa=X&ei=-tJNTo3tFIbsrQe5k8yrAw&ved=0CCMQ8wIwAg
 [5]: http://beast.bio.ed.ac.uk/Main_Page#A_Rough_Guide_to_BEAST_1.4
 [6]: http://www.ncbi.nlm.nih.gov/genomes/FLU/
 [7]: http://en.wikipedia.org/wiki/Babylonian_Map_of_the_World
 [8]: http://groups.google.com/group/beast-users
 [9]: http://en.wikipedia.org/wiki/Home_Improvement_%28TV_series%29
 [10]: http://en.wikipedia.org/wiki/Tim_Taylor_%28character%29
