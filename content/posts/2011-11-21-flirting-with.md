---
title: flirting with…
author: Mel
type: post
date: 2011-11-21T02:28:58+00:00
url: /2011/11/21/flirting-with/
categories:
  - Devotion
tags:
  - God
  - sin

---
Devotional Blog:

Topic: &#8220;Not a Hint&#8221;, 11/19/2001, Ephesians 5: 3-7

The author has the uncanny ability to piss me off in some of these entries. Perhaps one page or less isn&#8217;t enough space for her to fully explain what she means by what she says. Or perhaps she intends to bother her readers and sound a bit high and mighty.  I&#8217;m not saying her choice of verses and topics aren&#8217;t good ones&#8230;though not all of them I can relate with, hence do not write about. I suppose I wish she was a little more encompasing in her topics. No I don&#8217;t want her to sugar coat &#8216;sin&#8217; as she defines it but I&#8217;d like it if she didn&#8217;t freak out &#8216;new&#8217; Christians who might pick up her book and think&#8211;holy &#8216;$%!@&#8217; and question Christianitys sincerity. I&#8217;ll explain further.<!--more-->

The verse is Ephesians 5: 3-7 and she highlights verse 3:

> But among you there must not be even a hint of sexual immorality, or of any kind of impurity, or of greed, because these are improper for God&#8217;s holy people&#8230; (NIV)

She goes on to say that God asks that we live in such a way that there is not even a hint of sin and lists what she considers &#8216;hints&#8217;.

  * Maybe you aren&#8217;t having sex, but do you spend the night alone at someone&#8217;s house when you are not married?
  * Maybe you don&#8217;t drink but do you hang out at parties where they serve alcohol and maybe drugs?
  * Maybe you don&#8217;t cheat, but do you allow someone to cheat off your paper?
  * Maybe you don&#8217;t steal but do you look the other way when others at the company do?
  * Maybe you obey your parents, but do you cover for your siblings who are rebelling?
  * Maybe you don&#8217;t beat your children but do you ignore a situation where another child might be abused?
  * Maybe you aren&#8217;t a prostitute but do you dress like one?
  * Maybe you don&#8217;t buy porn but do you read explicit romance literature or watch movies with nudity?

She finishes the entry with &#8220;Look at the hints you are leaving in life. Do they point to the Savior or sin?&#8221;

This is the part where I am supposed to fall down on my knees crying and repenting about the hints of sin in my life totally &#8216;convicted&#8217; right? Now I understand this verse and many others&#8230;more than you may know.  I&#8217;ve had them thrown at me throughout my life by various people, more recently by my church here which went to great lengths to split Tyghe and I up because they said we shouldn&#8217;t no live together prior to marriage even though we were not sleeping together.

Now I completely understand the premise of, as a Christian, not being a stumbling block to others&#8211;absolutely, so I have no problem not being involved in my church until Tyghe and I are married, that&#8217;s fine. And if someone were to ask me about whether to live together before marriage I&#8217;d probably say no, I do not see it as necessary to live with someone prior to marriage&#8230;in order to justify marrying them. Either you love them and you are going to get married or not, I don&#8217;t believe living together should be the hinge that decides whether you marry someone, although for some people I know it is. Us living together was not and is not a &#8216;test run&#8217; on marriage with an opt out option. We live together because, aside from the fact we are a couple, I moved around the world and he came with me and I simply refused to live alone. If that makes me a sinner, so be it, not exactly a surprise&#8211;we are all sinners&#8230;you all higher and mightier than me can pray about that. As far as I can find in the Bible it discusses living together is not desirable because it&#8217;ll &#8216;lead&#8217; to things (enter ominous music) and apparently young people have no self-control. Actually according to the Bible, by some interpretations, none of us should leave our parents houses til we get married (Genesis 2:24-25, Matthew 19:5&#8230;and most other verses like in Matthew re-quote what is said in Genesis), &#8211;DOH! I think half of civilization is screwing that one up then.

Moving down the list&#8230;not drinking but attending a party where there is drinking. Some of my friends, very devout Christians I&#8217;ve seen in the mix of a party without drinking alcohol. Do I immediately think sinner or hypocrite? No. Nor do I think most people would think that. If anything I think it opens up conversation because most people become curious as to why you are not drinking and it opens a door to talk about your faith and why.

She, the author, gives a very &#8216;guilt by association&#8217; outlook on this. We live in a world of sin, we are surrounded by people who do horrible things everyday. Some of which we don&#8217;t &#8216;point&#8217; out and cry &#8216;foul&#8217; for safety reasons or simply because perhaps out intervention would harm more than help. A child being abused? That&#8217;s everyone&#8217;s business. If you have indications a child is being abused for the love of &#8216;insert who you believe in&#8217;, tell someone! A couple living together or a Christian friend attending a party where there&#8217;s alcohol even though they are not drinking&#8230;unless they bring it up to you and ask your advice/counsel, no not your business. If you are curious as to why they are doing something, ask, don&#8217;t condemn.

Living lives that are around &#8216;contentious&#8217; or &#8216;sensitive&#8217; areas or topics doesn&#8217;t mean we are &#8216;flirting with all kinds of lurid sin&#8217;. It means we choose not to live in a the limited holier-than-thou bubble where the non-Christian is unreachable. I had a friend that would routinely tell people they were going to hell when they cussed or discussed situations that the Bible says to stay out of. Needless to say she didn&#8217;t &#8216;reach&#8217; many people because they were too busy hating her and ostracizing her. You can live an upright life and be a good example without making it habit to damn people to hell. I&#8217;m sure they&#8217;d appreciate it.

I myself am jaded by my fellow Christians. I don&#8217;t like talking to them and many times I am very cautious meeting other Christians, because I automatically assume they are full of &#8216;false-concern&#8217;, judgement and &#8216;wow I am just so better than you attitude&#8217;. I find myself always questioning their sincerity&#8230;terrible isn&#8217;t it? I&#8217;ll open up to a non-believer SO much faster than I will to a  Christian. Not because I think I&#8217;ll get approval from a non-believer, I don&#8217;t open up with the intention of getting &#8216;approval&#8217; for my life, but non-believers are so much less judgemental! When one opens up about their life and yes their sins or what may appear to be &#8216;flirting with&#8230;&#8217; sin&#8211;one doesn&#8217;t want to be confronted with judgement and condemnation; especially from someone they barely know and that barely knows them. I&#8217;m not saying sins shouldn&#8217;t be dealt with but the first time someone opens up their heart and life to you is NOT it or rather shouldn&#8217;t be it.

Are there &#8216;hint&#8217;s of sin&#8217; in my life? Am I flirting with &#8216;fill in the blank&#8217;? The way I see my life and what&#8217;s in my heart&#8230;No. I know exactly where I stand on those issues. As it outwardly appears to others&#8230;then yes, perhaps there are hints or &#8216;flirts&#8217; in my life that may point to &#8216;sin&#8217; when others look at my life. So then comes the retort that I am a stumbling block for others because I am not pious enough. Fair enough, but I am not involved in ministry and I suppose I give more people credit for their views and beliefs. Somehow if someone seeks out my life and finds these things they don&#8217;t understand, I really don&#8217;t feel its going to toss them into the downward spiral of backsliding and subsequent hell. I don&#8217;t seek approval for it, for my life. If my choices illicit curiosity then people should ask.

Besides I challenge every pious person to really examine there life&#8230;everyone has &#8216;flirted with&#8230;&#8217; aspects of sin or been implicated in situations that could &#8216;point to sin&#8217;, ie. a party with alcohol, watching your siblings disobey, noticing others steal or cheat, or watching a movie where someone frolicks nude. And if you haven&#8217;t at any time in your life, wow, kudos to you I am truly impressed&#8230;now if you are also a sincere person then by all means pray for me. I apparently could use all I can get.
