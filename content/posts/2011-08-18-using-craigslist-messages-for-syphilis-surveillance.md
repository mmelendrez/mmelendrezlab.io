---
title: Using craigslist messages for syphilis surveillance…
author: Mel
type: post
date: 2011-08-18T09:27:16+00:00
url: /2011/08/18/using-craigslist-messages-for-syphilis-surveillance/
wordbooker_options:
  - 'a:9:{s:23:"wordbook_default_author";s:1:"0";s:29:"wordbook_republish_time_frame";s:2:"10";s:18:"wordbook_attribute";s:31:"Posted a new post on their blog";s:29:"wordbooker_status_update_text";s:35:": New blog post :  %title% - %link%";s:19:"wordbook_actionlink";s:3:"300";s:18:"wordbook_orandpage";s:1:"2";s:23:"wordbook_extract_length";s:3:"256";s:18:"wordbook_page_post";s:4:"-100";s:32:"wordbook_description_meta_length";s:3:"350";}'
categories:
  - I will do Science to it!
tags:
  - craigslist
  - HIV
  - meeting
  - syphilis

---
FIRST and foremost it was anonymous info gathering, no way to link anyone to anything here&#8211;anon posts were used. AND I have the authors permission and enthusiastic support in fact to share this information from his paper/poster.

The Authors: **JA Fries (Computer Science Graduate Student)**, TY Ho (Comp Sci graduate student), PM Polgreen (his boss and assistant prof at Univ of Iowa), AM Serge.

#### <!--more-->Notes: This a referenced abstract/paper/poster but I didn&#8217;t put them in, ask me if you want them. Also, I&#8217;m shortening a lot to get the main points out.

**Backgroud:**

#### This paper describes a novel method of conducting large scale syphilis surveillance by examining anonymous Craigslist posts by men who have sex with men (MSM). A 20th Century by the CDC showed a 39% increase in syphilis cases from 2006-2009. Majority of the increase is among MSM community. The anonymity and relative ease of finding partners on the internet has facilitated a culture of casual sexual encounters encompassing a variety of unsafe sexual practices e.g. anonymous partners, illegal drug use, unprotected sex, group sex etc&#8230; These anonymous sexual encounters make it more difficult for public health officials to notify exposed partners. Craigslist is a website specializing in online classifieds and contains a large community of casual sex participants. Our hypothesis is that a community&#8217;s rate of risky behaviour requests can be correlated to syphilis rates.

#### 

#### **Methods:**

#### Daily craigslist RSS feeds were collected for 416 sites around the US (54,450,547 indiv posts as of 9/1/10). 2,377,449 were identified as MSM specific and coded to counties in CA. Messages were searched for key words to identify explicit requests for unprotected sex.

#### Alot of math, computer coding and statistics ensued **(he didn&#8217;t actually say this he put the math/stats methods, let me know if you want to know)**

#### 

#### **Results:**

#### Messages requesting unprotected sex were positively correlated with syphilis rates significant at p < 0.05.

#### 

#### **Conclusion:**

#### Craigslist messages contain useful information for disease surveillance. Keyword based message classification techniques show that the rate of unprotected sex requests in MSM specific messages can function as a proxy for syphilis rates in CA at the county level.

#### 

#### The Research Groups website [click here.][1] 

#### This specific project link: [click here][2]

#### **(his current poster on syphilis that he presented should be up and he uses similar ideas/methods dealing with HIV/AIDS)**

 [1]: http://compepi.cs.uiowa.edu/
 [2]: http://compepi.cs.uiowa.edu/index.php/Publications/ISDS10a
