---
title: Assessing the pathogenic potential of people
author: Mel
type: post
date: 2011-12-09T04:24:45+00:00
url: /2011/12/09/pathogenic-potential/
categories:
  - I will do Science to it!
  - Life or something like it
tags:
  - cloning
  - cure
  - dengue
  - family
  - friends
  - pathogenic
  - people
  - Philadelphia
  - population
  - quasispecies
  - sequencing

---
So it&#8217;s been a good while since I have posted anything as I&#8217;ve been attending a conference in Philadelphia, PA put on by the American Society of Tropical Medicine and Hygiene. Following the denouement of the conference I found myself at [El Vez restaurant][1], awesome restaurant by the way, just down from my hotel sipping on a very strong (apparently) pomegranate margarita and going over my notes whilst eating lunch. And for those of you following my blog and know I&#8217;m fasting&#8211;before you cry foul, this week I&#8217;m off it due to a number of reasons but will promptly re-initiate when I return to Bangkok. When one is fasting for a year&#8230;actually turns out to be a little longer, I have to allow myself a certain modicum of sanity. Or rather preserve what I have. In anycase, back to matter at hand&#8230;

So I&#8217;ve been at this conference for the past week and it&#8217;s proven very informative though I feel like a small genetic fish swimming in a sea of immunology and epidemiology which is a bit disconcerting, especially since I come from a completely environmental background with minimal medical/clinical knowledge.

Now I am a genetic data cruncher who enjoys population level analysis with some mathematical modelling thrown in for good measure&#8230;

I know right? The personal ad practically writes itself.<!--more-->

When I described my work this weekend, I realized it basically boiled down to assessing the pathogenic potential of viral populations of dengue virus. For those unfamiliar with terms, &#8220;Pathogenic/pathogen&#8221; = **infectious agent** &#8211; in colloquial terms, a **germ** — is a microbe or microorganism such as a virus, bacterium, prion, or fungus that causes disease in its animal or plant host ([wikipedia][2], [dictionary][3]). In terms of my research, I want to search out what is the potential for a virus to cause disease given it is actually not &#8216;one&#8217; virus but rather a population of viruses very similar (all dengue) but with distinct genetic backgrounds that could conceivably make them a problem for disease and vaccine development. Essentially think of it as a small cloud of viruses, all dengue, that infect you&#8211;all only slightly different, some can infect you some cannot. Usually there is a dominant virus in the mix that causes the disease&#8211;it wins out against the cloud of other possibilities for various reasons I will not get into right at this moment. Now I use a series of techniques to accomplish my research:

  1. I obtain samples from people and mosquitoes affected by dengue
  2. I can then do one or both of the following: cloning the viruses, sequencing them followed by analysis or no cloning and direct sequencing using next generation sequencing tools.

After obtaining the data I then apply a number of logic based or mathematically based models or analyses to dissect the very core and explain why this virus or viral population behaves the way it does. All in an effort to do an in depth analysis of what makes these viruses tick and figure out ways to combat and eliminate the virus altogether.

It is highly unlikely this virus will kill you provided you can get medical support if your symptoms get bad&#8230;or you develop a more virulent form of the disease. BUT it is bothersome, annoying, can spread rather quickly and can get serious enough that we really must do something.

The resemblance is &#8216;uncanny&#8217; when it comes to people we meet/know in our lives. And before you blame my second margarita on this analogy hear me out&#8230;

Few of us realize the pathogenic potential of people in our lives, especially when we first meet them. When I suspect someone in my life is particularly toxic&#8230;I use a series of techniques to assess why this is.

  1. I collect observational data&#8230;unfortunately many times this requires me to subject myself to mutual interaction. Though if I don&#8217;t realize this person is as toxic as they are, mutual interaction would be fine, despite the slow poisoning I am receiving from said person.
  2. Then I can do one or both of the following: or rather&#8230;perhaps only one of the following as if it is established that said person is pathogenic, I probably don&#8217;t want to clone them&#8230;my current approach fails here&#8211;damn. The other option&#8230;deep analysis of their genetic character and attempt to look for a remedy as there is currently no vaccine or therapy that cures the pathogenic person.

Unfortunately, logic and mathematically based reasoning/modelling fails here as the sheer absurdity and unpredictability of their behavior turns out to be too great a confounding factor. Often as is the case with dengue fever, we fail to see their immediate danger at first. I start coughing or &#8216;achooing&#8217;, perhaps I feel tired and instead of thinking I have a disease, I think I need more vitamins or more rest. I meet someone that comes off very trustworthy or awesome at first, soon I am noticing grating aspects of their personality, little white lies that escalate, manipulation of my other friends or family&#8211;perhaps I pass it off as my own insecurity or nitpickiness.  Soon I have a full blown disease with the potential for hemorrhaging&#8230;Soon I have a whole host of friends and family who don&#8217;t want to be with me because of said person. Does the viral storm pass? With rest and symptom treating, yes it does and your body emerges stronger in the face of that infection in the future. With a person, hopefully it is the same, you emerge stronger and less prone to duping by pathogenic people&#8230;assuming you figure it out and remedy it to begin with.

Now I&#8217;m missing a whole nother layer of dengue pathogenesis&#8230;the secondary infection with a different form (serotype) of the disease; which can cause more severe disease manifestation than the first depending on your immunity etc. This also applies to pathogenic people. Once we&#8217;ve realized and &#8216;cured&#8217; ourself of the pathogenic person, that doesn&#8217;t mean we&#8217;ll forever be able to avoid a re-infection. The &#8216;disease&#8217; rather real or figuratively has its &#8216;hooks&#8217; in you, your body now knows what it is&#8230;but that&#8217;s precisely the problem. You, your body knows what it is, the disease, the person&#8211;you&#8217;ve already experienced the worst this disease and/or person has to offer. A second &#8216;attack&#8217; can therefore be more detrimental. Perhaps you have this false hope that previous experience with said pathogenic person makes you impermeable to their bullsh!t. Should they dupe you again&#8230;the fallout could cause a lot of emotional hemorrhaging. Emotional hemorrhaging&#8230;doesn&#8217;t sound like a spa treatment.

What are our options? Quarantine? Unfortunately it is as difficult discerning pathogenic people from normal people as it is determining if an infection is dengue or the common cold or some other rather benign infection/virus. It would be advantageous for someone to develop a human &#8216;BS factor radar gun&#8217; that could evaluate and grade the pathogenic potential a person has before we let them enter out lives. Preferably without the [&#8216;cancer scare&#8217;][4] associated with putting it in our laps or near our eyes/head.

And with that I will return to my pomegranate margarita, chips and salsa and continue to ponder.

Ponder the analysis of the pathogenic potential of dengue viral quasispecies populations where cures are actively in development&#8230;

and bemoan the existence and pathogenic potential of people in our lives of which there is currently no cure.

 [1]: http://www.elvezrestaurant.com/
 [2]: http://en.wikipedia.org/wiki/Pathogen
 [3]: http://dictionary.reference.com/browse/pathogenic
 [4]: http://www.osha.gov/SLTC/radiofrequencyradiation/fnradpub.html
