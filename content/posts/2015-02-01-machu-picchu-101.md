---
title: Machu Picchu 101 – for those that have limited time and aren’t using a company
author: Mel
type: post
date: 2015-02-01T18:04:31+00:00
url: /2015/02/01/machu-picchu-101/
categories:
  - Life or something like it
  - Not Asia
tags:
  - Machu Picchu
  - Peru

---
> <p style="text-align: center;">
>   “Twenty years from now you will be more disappointed by the things you didn’t do than by the ones you did do. So throw off the bowlines, sail away from the safe harbor. Catch the trade winds in your sails. Explore. Dream. Discover.” &#8211; Mark Twain
> </p><figure id="attachment_945" style="width: 616px" class="wp-caption aligncenter">

<img class="wp-image-945 size-full" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150118_063255.jpg" alt="IMG_20150118_063255" width="616" height="822" /><figcaption class="wp-caption-text">A hassle that&#8217;s totally worth it.</figcaption></figure> 

&nbsp;

So I figured it might be helpful to post my adventure getting up to Machu Picchu in January 2015 because it&#8217;s not as straightforward as one would desire for such a trip. I had limited time and didn&#8217;t want to go through an expensive travel company that would book me at a budget hostel or expensive hotel that I wouldn&#8217;t get to pick and give me an itinerary and guide I didn&#8217;t want. I like to explore things on my own so being on a regimented tour is not my idea of fun.

The below is for a 4 day trip Friday to Monday. But you can do this in 3 days too&#8230;see below.

_**Checklist (from Lima) &#8211; I&#8217;ll elaborate below:**_
  
1. Get your tickets to Machu Picchu
  
2. Get your train (or train+bus in rainy season) tickets
  
3. Get to Cusco
  
4. Either go directly to Aguas Calientes (now called Machu Picchu Pueblo) immediately after arriving in Cusco or stay a couple days &#8211; if altitude sickness bites you like it bit us, staying in Cusco is going to blow royally and you&#8217;ll need time to acclimate. Given my work schedule we had to fly to Cusco at night so we stayed one night. But you can get early early flights to Cusco that&#8217;ll get you to train transfer in time.
  
5. Get your bus tickets to Machu Picchu
  
6. Enjoy.

Lastly&#8230;cheaper alternatives and links at the bottom&#8230;

_**Elaboration:**_

**1. Get your tickets to Machu Picchu:**
  
So the first thing I did while in Lima was research how to get tickets into the site. They only allow 2000 visitors into Machu Picchu per day and 200 into Huayapicchu which is an epic hike up a mountain for a different vantage point of the lost city. I started off by looking at http://www.seat61.com/Peru.htm because if you are looking to travel via train &#8211; he&#8217;s the best. His train time tables off a bit though. But we&#8217;ll get to that. Tickets first! Seat 61 (and the internet in general) say buy your tickets from http://www.machupicchu.gob.pe/ . Now this site does not function fantastically on the English version &#8211; I couldn&#8217;t get past selecting my dates to actually seeing confirmation of my reservation. So I switched to the Spanish version which I recommend if you know Spanish. I was able to get through the whole reservation process this way and get my confirmation number of my reservation. Now here&#8217;s the kicker &#8211; _**your reservation is only good for 6 hours!**_ You have 6 hours to pay and MC/VISA didn&#8217;t seem to work for me no matter what I tried on the website in English or Spanish. I assumed my reservation would be good and I could pay onsite when I got there so I left it at that. Yes, you can pay onsite &#8211; no they don&#8217;t hold your reservation beyond 6 hrs. SO, if you want to guarantee your reservation especially if you&#8217;ve reserved going up Huayapicchu (which fills up even in the rainy/low season) then go through paypal on the website instead. This option seems to work well and you can get your official tickets this way. Make sure to print them out and bring them with you. OH &#8211; and the government site didn&#8217;t work for me on Firefox &#8211; I had to use Chrome.

Cost of Tickets to Machu Picchu Only: ~128 soles/person (~42 USD)

Cost of Tickets if you go to Machu Picchu and Huayapicchu: ~152 soles/person (~50 USD)

<img class="  wp-image-942 alignright" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150117_122239.jpg" alt="IMG_20150117_122239" width="232" height="310" />

Because I failed on the whole only having 6 hrs to pay &#8211; we ended up having to pay onsite for tickets when we arrived at Machu Picchu Pueblo. When you get off the train &#8211; head through the market and downhill essentially til you hit the main plaza &#8211; the tourist office will be off one of the corners of the Plaza (or just ask anyone and they&#8217;ll point you the right direction. &#8220;donde esta la plaza?&#8221; or &#8220;boleta machu picchu?&#8221;).

Only Machu Picchu tickets were left, Huayapicchu was booked &#8211; it ended up being ok because apparently it&#8217;s a death hike that&#8217;s super cool at the top, but still a pretty ridiculous hike and because we were still acclimating to altitude we ultimately were pretty thankful we didn&#8217;t go.

<span style="color: #ff0000;">Tickets to Machu Picchu &#8211; CHECK</span>

**2 and 3. Train/Bus and Air tickets:**
  
So this will be fun as there are only morning trains from Cusco to Machu Picchu Pueblo and only late afternoon trains from Machu Picchu Pueblo back to Cusco so you&#8217;ll have to arrange your flights accordingly. We flew LAN Airlines (which was comfortable and they give you a snack). Yes snacks matter when you&#8217;re from the US and you pay for absa-fing-everything on any flight. We close to fly into Cusco Friday night then catch the early train Saturday morning&#8230;I didn&#8217;t want to stress about delayed flights. Know that in the rainy season electric and thunderstorms are a daily occurrence in Cusco &#8211; so your flight could be delayed or cancelled. Alternatively, you can travel on the first LAN flight of the day then take a taxi straight to the train/bus depot which assuming no delays you&#8217;ll be tired, but it&#8217;ll work out. And bad weather typically arrives in the afternoon/evenings in Cusco so your flight should be pretty solid on timing.

Our flight was ~320 USD/person (LAN Airlines: http://www.lan.com/en\_us/sitio\_personas/index.html) and you have to book it on the English version of the site (or via Orbitz, Hipmunk&#8230;). It won&#8217;t let you pay with a US credit card if you book your flight in Lima from the Spanish version of the site. When I did the conversion though, if I had been able to book it in country on the Spanish site the price diff wasn&#8217;t that big &#8211; about ~20-40 USD. At this point if you are going to Machu Picchu you should be resigned to spend money &#8211;  especially if on a tight schedule like we were, so this didn&#8217;t matter that much.

Seat 61 (http://www.seat61.com/Peru.htm) has a great rundown of the PeruRail (http://www.perurail.com/) train options with pictures. We took Vistadome as that was the one that provided the easiest schedule for us. Remember &#8211; <span style="color: #0000ff;">TO</span> Machu Picchu Pueblo, morning schedules only. <span style="color: #0000ff;">FROM</span> Machu Picchu back to Cusco afternoon/evening schedules only. So if you want to get back earlier than the offered train schedule you&#8217;ll need to go to **Ollantaytambo** which runs just about every hour during the day (timetable: http://www.perurail.com/destination-machu-picchu). Then you&#8217;ll need to pay a private taxi to take you back to Cusco ~20-25 USD and could be the &#8216;ride of your life&#8217; &#8211; but it&#8217;ll work. We picked Vistadome (The Backpacker is slightly cheaper on PeruRail but left earlier and well&#8230;we&#8217;re lazy and like to sleep) &#8211; cost RT 175 USD/person.

  * Double check train and flight schedules so you don&#8217;t miss your connections or try to arrive in Cusco a day early and spend the night, catching the train in the morning.
  * We rode PeruRail but you can check out Inca Rail too (http://incarail.com/) perhaps their scheduling might be more convenient.
  * The latest Vistadome (or any train) that leaves is at 7:50 am in the rainy season (Jan/Feb) when they do bus+train mix and you&#8217;ll arrive in Machu Picchu Pueblo just after noon &#8211; so plan your flight accordingly if you are going to come in day of your train ride.
  * The last full trip all the way back to Cusco leaves at 5:25 pm but the last LAN flight in the evening is 7:10 pm &#8211; so even if you take the first schedule back at 3:20 pm from Machu Picchu Pueblo to Cusco you won&#8217;t arrive til 7:30 pm so you&#8217;ll still miss that last flight on LAN. Alternatively, as mentioned above you can get back in time if you go to **Ollantaytambo** and pay a private taxi to get you back in time.
  * Also remember, rainy season = afternoon/evening lightening/thunderstorms so you could get stuck. We came back on Sunday night and flew Monday afternoon 4:30 pm flight &#8211; and ours was the last flight out &#8211; all the rest were cancelled because of epic storms coming through and our flight was also delayed because of an electric storm.
  * We stayed the night in Cusco at the El Balcon Hostel (http://www.balconcusco.com/en/cusco-hostel-el-balcon, http://www.tripadvisor.com/Hotel\_Review-g294314-d318940-Reviews-El\_Balcon\_Cusco\_Hostel-Cusco\_Cusco\_Region.html) It was adorable, quaint, clean but a bit of a hike up a hill to get to which isn&#8217;t a problem if you take a taxi. But we just went down to the plaza (3 blocks away) to get dinner and nearly died getting back &#8211; we also got altitude sickness which doesn&#8217;t help.  They offer Coca tea upon arrival which is awesome and tasty but unfortunately didn&#8217;t help me overcome being super ill that night. We were on the second floor and from the balcony could see the city &#8211; it was awesome. They offer complimentary taxi to the hostel when you come in, but you&#8217;ll have to pay when you leave. Cost ~50 USD/night (my review on TripAdvisor: http://www.tripadvisor.com/ShowUserReviews-g294314-d318940-r251143026-El\_Balcon\_Cusco\_Hostel-Cusco\_Cusco\_Region.html#CHECK\_RATES_CONT)

<span style="color: #ff0000;">Tickets to Cusco &#8211;  CHECK</span>

<span style="color: #ff0000;">Tickets on Vistadome RT to Machu Picchu &#8211; CHECK</span>

**4. All I want to say here is what I said above: Either go directly to Machu Picchu Pueblo immediately after arriving in Cusco or stay a couple days** &#8211; if altitude sickness bites you like it bit us, staying in Cusco is going to blow royally and you&#8217;ll need time to acclimate. If you go directly to Machu Picchu Pueblo the effects of altitude won&#8217;t be as onerous and might be worth the earlier schedule &#8211; you&#8217;ll be tired but altitude sickness I think is worse.

**5. Machu Picchu Pueblo and Buying your Bus Ticket EARLY!:** <figure id="attachment_944" style="width: 222px" class="wp-caption alignright">

<img class="  wp-image-944" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150118_054106.jpg" alt="IMG_20150118_054106" width="222" height="296" /><figcaption class="wp-caption-text">The line for tickets is on the left of the picture, the line to get on the bus is on the right side of the picture. You have to have a ticket when in line for the bus &#8211; they check while you are in line.</figcaption></figure> 

<img class="  wp-image-943 alignright" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150118_053538.jpg" alt="IMG_20150118_053538" width="225" height="300" />

So in addition to all the tickets above you&#8217;ll also need to buy a specific ticket to actually take you from Machu Picchu Pueblo up to Machu Picchu itself. The ride is about 30-40 min. Cost 19 USD/person. What we should&#8217;ve done since we were in the town a day early was get the bus tickets for the following morning right away &#8211; but we waited til about 5:00 am the following morning and the line was out to kingdom come. Now this ultimately didn&#8217;t matter too much cause even though it seemed like  it was sunrise and getting super light out, sunrise actually up at Machu Picchu doesn&#8217;t get up over the Andes into the Incan city until between 6:15-6:30 (rainy season). So initially we were super bummed because we thought we&#8217;d missed the sunrise but after buying tickets then waiting in an even longer line to get on the bus &#8211; we did end up getting there just as the sun peaked over the Andes =  awesome. But know that the later you go, the more people will end up in your pictures &#8211; so if you truly want a &#8216;solitary&#8217; experience &#8211; buy your bus tickets the day before and be at the bus depot at 4:30-4:45 am to get in line for a bus; that&#8217;ll ensure you are literally on one of the first buses up. The site doesn&#8217;t open til 6 am though and the first bus will not leave til around 5:30 am but the line for the buses will be insanely long so I still suggest an early arrival if you are a die hard about getting on that 5:30 am bus to be apart of the first group into the site.

<span style="color: #ff0000;">Bus Tickets to Machu Picchu Site &#8211; CHECK</span>

  * In Machu Picchu Pueblo we stayed at the Panorama B&B (http://www.mapipanorama.com/en/, my review: http://www.tripadvisor.com/ShowUserReviews-g304036-d7159063-r251145329-Panorama\_B\_B-Aguas\_Calientes\_Sacred\_Valley\_Cusco\_Region.html#CHECK\_RATES_CONT). Biggest pluses: Christophe will store your packs while you explore Machu Picchu, breakfast served at 4:30 am, free bottle of wine (or beer), AMAZING view from balcony or river, quiet area of town yet close to everything. Cost ~$60/night double.<figure id="attachment_946" style="width: 309px" class="wp-caption aligncenter">

<img class=" wp-image-946" src="http://www.melaniemelendrez.com/wp-content/uploads/IMG_20150118_112840.jpg" alt="View from the front of Panorama B&B. We were on the 4th floor. Amazing view of river - very serene." width="309" height="413" /><figcaption class="wp-caption-text">View from the front of Panorama B&B. We were on the 4th floor. Amazing view of river &#8211; very serene.</figcaption></figure> 

6. Enjoy. It was a hassle to plan but totally worth it.

**COST SUMMARY:**

  * LAN Air tickets RT Lima > Cusco > Lima: $320/person
  * Cusco accomodation &#8211; El Balcon: $50/night
  * Machu Picchu ONLY site tickets: $42/person
  * PeruRail Vistadome RT Cusco > Machu Picchu Pueblo > Cusco: $175/person
  * Machu Picchu Pueblo accomodation &#8211; Panorama B&B: $60/night
  * Another night in Cusco before flying out &#8211; El Balcon: $50/night

**Total: $697/person**

_**Cheaper Alternatives:**_

  * Stay in cheaper accomodation than we did. So our accommodation was between 150-245 soles/night which is actually pretty high but we&#8217;ve done the &#8216;gap-year-backpacker-don&#8217;t-care-where-i-sleep-hope-bugs-don&#8217;t-crawl-on-me&#8217; traveling thing. We&#8217;re older, have good jobs and quite frankly like to sleep a little more comfortably. We didn&#8217;t need five star &#8211; but we didn&#8217;t want the backpacker hostel experience either (been there done that, was great when I was 23). You can get accommodation for a lot cheaper at backpacking hostels that still offer warm showers and private rooms (25-50 soles/night) &#8211; so do a little research and look at the links below on blog posts of people that did this cheaper than us. 🙂
  * Take more time &#8211; do your research and find a cost effective tour doing the Incan Trail. Here&#8217;s a blog post to get you started &#8211; your biggest concern is going to be finding a reputable group that will not rip your off or leave you stranded (it happens). http://www.budgettravel.com/feature/trek-the-inca-trail-to-machu-picchu,3268/.
  * Advice for traveling on the cheap via TripAdvisor: http://www.tripadvisor.com/ShowTopic-g294318-i3352-k5199780-Cheapest\_way\_to\_do\_Machu\_Picchu-Machu\_Picchu\_Sacred\_Valley\_Cusco\_Region.html
  * Trekking &#8211; permits sell out quick, book in advance with a good company. Take a look at the reviews for these companies &#8211; many offer the Incan Trek as well: http://www.tripadvisor.com/Attractions-g294314-Activities-Cusco\_Cusco\_Region.html. Alternatively, check out the reviews of the Incan trail and the high reviews &#8211; see if they mention what company they went with: http://www.tripadvisor.com/Attraction\_Review-g294314-d311715-Reviews-Inca\_Trail-Cusco\_Cusco\_Region.html
  * _Machu Picchu on the Cheap Blog Post 1:_ First Day was travel from Cusco to Machu Picchu Pueblo, Second day was Machu Picchu, Third day heading back to Cusco. Prices are a little different as her travel was in 2013. This does not include the cost of getting nor staying in Cusco prior to actually doing the trip. But it is pretty darn cheap how she did it &#8211; $115 total. She has a lot of good tips as well.  http://undertheyewtree.com/cheap-way-to-do-machu-picchu/
  * _Machu Picchu on the Cheap Blog Post 2:_ Not as informational and doesn&#8217;t list her final costs but still a good read and the more tips the merrier: http://fullofwanderlust.com/machu-picchu-on-a-budget/.
  *  _Machu Picchu on the Cheap Blog Post 3_: A GREAT blog post &#8211; again doesn&#8217;t include cost of getting to Cusco or staying in Cusco prior to your adventure  &#8211; but breaks it down lovely and end cost/person awas ~$107 USD. Bear in mind this was posted in 2009 but still super informational with pictures and advice. (http://www.chanatrek.com/the-cheapest-way-to-get-to-machu-picchu-machu-pichu-peru/)
  * _Machu Picchu on the Cheap Blog Posts 4 and 5:
  
_ Another great breakdown: http://www.drivenachodrive.com/how-to-visit-machu-picchu-on-the-cheap/ and experience: http://www.drivenachodrive.com/2013/01/death-road-to-machu-picchu/ by Drive Nacho.

Best of Luck and Happy Travels!
